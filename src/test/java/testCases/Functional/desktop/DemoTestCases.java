package testCases.Functional.desktop;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import actions.piechart.CaptureScreenShot;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import reusableModules.desktop.Desk_HomePage_Reusable;
import reusableModules.desktop.ProductionReusable;
import setup.MultipleBrowser;
import setup.WinAuth;

@Test
public class DemoTestCases extends MultipleBrowser {

 GenericMethods genericMethods;
 Desk_BNY_RegistrationPageObject Registtration_PageObjects;
 Desk_HomePage_Reusable homePage_Reusable;
 ProductionReusable loginRegisterReusable;
 ProductionReusable myBagCheckout_Reusable;
 ProductionReusable myAccountLanding_Reusable;
 ProductionReusable checkoutshipping_Reusable;
 ProductionReusable categoryProductClick_Reusable;
 ProductionReusable pdp_Reusable;
 ProductionReusable globalNav_Reusable;
 ProductionReusable checkoutBilling_Reuable;
 ProductionReusable orderReview_Reuable;
 String login, bnyURL, whsURL;
 String currentPageTitle;
 CaptureScreenShot capturescreenshot;



 @BeforeMethod(groups = "Before_Method")
 @Parameters({
  "DEVICE",
  "BROWSER",
  "BNYURL",
  "WHSURL"
 })
 public void Start(@Optional String Device, String Browser, String bny_URL, String whs_URL) {
  super.setuptest(Device, Browser);
  driver.manage().window().maximize();
  driver.manage().deleteAllCookies();
  //driver.get(URL);
  bnyURL = bny_URL;
  whsURL = whs_URL;
  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 }

 
 @Test(groups = "Demo", dataProvider = "ShippingFormData", priority = 1, testName = "PassInStockProductCheck", enabled = true)
 public void PassInStockProductCheck(String email, String oldPassword, String country, String FName, String LName, String ShippingAddress1, String ShippingAddress2, String ShippingCity, String ShippingState, String ShippingZip, String ShippingPhone, String CardType, String CardNo, String CardCVV, String BillingAddress1, String BillingCity, String BillingState, String BillingZip, String Promocode, String BarneysUrl1, String WareHouseUrl2) {
	ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
	ATUReports.setTestCaseReqCoverage("Test case to verify only instock product and add them to cart page.");
		 
		 
	  loginRegisterReusable = new ProductionReusable();
	  globalNav_Reusable = new ProductionReusable();
	  categoryProductClick_Reusable = new ProductionReusable();
	  pdp_Reusable = new ProductionReusable();
	  myBagCheckout_Reusable = new ProductionReusable();
	  checkoutshipping_Reusable = new ProductionReusable();
	  myAccountLanding_Reusable = new ProductionReusable();
	  checkoutBilling_Reuable = new ProductionReusable();
	  orderReview_Reuable = new ProductionReusable();
	  genericMethods = new GenericMethods();
	  capturescreenshot=new CaptureScreenShot();
  
	  driver.get(bnyURL);
	  GenericMethods.Waitformilliseconds(2000);
	  WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
  //----------------------Category Browse--------------------------
 
	  globalNav_Reusable.clickSubCategoryName(driver, "Women", "Clothing");
	  ATUReports.add("Check Expanded Navigation", "Global nav should be expanded", "Its Expanded", true, LogAs.PASSED);
	  categoryProductClick_Reusable.browseProductandClickonBNY(driver, "500");
	  GenericMethods.waitForPageLoaded(driver);

  //----------------------------PDP---------------------------------			
	  pdp_Reusable.selectAvailableColorOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.selectAvailableSizeOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.AddtoCartButtononPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopuponPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
	  GenericMethods.waitForPageLoaded(driver);

  //------------------My Bag(Logged In User)---------------------------------
	  myBagCheckout_Reusable.selectCountry(driver, country);
	  GenericMethods.waitForPageLoaded(driver);
	  myBagCheckout_Reusable.clickCheckoutButton(driver);
	  GenericMethods.waitForPageLoaded(driver);

 }

 @Test(groups = "Demo", dataProvider = "ShippingFormData", priority = 2, testName = "FailInStockProductCheck", enabled = true)
 public void FailInStockProductCheck(String email, String oldPassword, String country, String FName, String LName, String ShippingAddress1, String ShippingAddress2, String ShippingCity, String ShippingState, String ShippingZip, String ShippingPhone, String CardType, String CardNo, String CardCVV, String BillingAddress1, String BillingCity, String BillingState, String BillingZip, String Promocode, String BarneysUrl1, String WareHouseUrl2) {
	ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
	ATUReports.setTestCaseReqCoverage("This test case will not check for available size instead will click on any visible size and will fail.");
		 
		 
	  loginRegisterReusable = new ProductionReusable();
	  globalNav_Reusable = new ProductionReusable();
	  categoryProductClick_Reusable = new ProductionReusable();
	  pdp_Reusable = new ProductionReusable();
	  myBagCheckout_Reusable = new ProductionReusable();
	  checkoutshipping_Reusable = new ProductionReusable();
	  myAccountLanding_Reusable = new ProductionReusable();
	  checkoutBilling_Reuable = new ProductionReusable();
	  orderReview_Reuable = new ProductionReusable();
	  genericMethods = new GenericMethods();
	  capturescreenshot=new CaptureScreenShot();
  
	  driver.get(bnyURL);
	  GenericMethods.Waitformilliseconds(5000);
		try {
			Runtime.getRuntime().exec("src/test/java/lib/Authentication1.exe");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  //----------------------Category Browse--------------------------
 
	  globalNav_Reusable.clickSubCategoryName(driver, "Women", "Clothing");
	  ATUReports.add("Check Expanded Navigation", "Global nav should be expanded", "Its Expanded", true, LogAs.PASSED);
	  categoryProductClick_Reusable.browseProductandClickonBNY(driver, "500");
	  GenericMethods.waitForPageLoaded(driver);

  //----------------------------PDP---------------------------------			
	  pdp_Reusable.selectAvailableColorOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.SimpleselectAvailableSizeOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.AddtoCartButtononPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopuponPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
	  GenericMethods.waitForPageLoaded(driver);

  //------------------My Bag(Logged In User)---------------------------------
	  myBagCheckout_Reusable.selectCountry(driver, country);
	  GenericMethods.waitForPageLoaded(driver);
	  myBagCheckout_Reusable.clickCheckoutButton(driver);
	  GenericMethods.waitForPageLoaded(driver);

 }

 
 @AfterMethod(groups = "After_Method")
	 public void Stop() {
	  driver.quit();
	  GenericMethods.Waitformilliseconds(2000);
	 }

 @DataProvider(name = "ShippingFormData")
	 public static Object[][] ProvideDataToAboveMeth() throws IOException {
	  ReturnDataObject returnData1 = new ReturnDataObject();
	  return returnData1.read("src/test/java/testDataFiles/desktop/Shipping_billingPage.xls", "shippinig");
	 }


}