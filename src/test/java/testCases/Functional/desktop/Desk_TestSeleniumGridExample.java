package testCases.Functional.desktop;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import PageObjects.desktop.Desk_BNY_PDPPageObjects;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;


public class Desk_TestSeleniumGridExample {
	WebDriver driver;
  	GenericMethods genericMethods;
	//String baseURL="https://development-www-barneys.demandware.net/s/BNY/homeshow";
	WebDriverWait xplicitWait ;
	Desk_BNY_PDPPageObjects PDP_Objects;
	Boolean isElementVisible=true;
	String isElementPresent1;
	String baseURL,nodeURL;

	@BeforeMethod
	public void StartSampleTest() throws MalformedURLException{
		baseURL="http://www.barneys.com";
		nodeURL="http://192.168.0.138:5566/wd/hub";
		ATUReports.indexPageDescription = "Barneys Shipping Page";
DesiredCapabilities capability=DesiredCapabilities.firefox();
capability.setBrowserName("firefox");
capability.setPlatform(Platform.WINDOWS);
driver=new RemoteWebDriver(new URL(nodeURL), capability);	
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		}
	
 @Test(groups = "BNY_Sniff")
	public void PDPSectionSelectColourandSize() throws NullPointerException,InterruptedException{
		
	 driver.get(baseURL);
	 System.out.println(driver.getTitle());
 }
	
	@AfterMethod(groups = "BNY_Sniff")
	public void Stop(){
		GenericMethods.Waitformilliseconds(2000);
		driver.quit();
	}
	
		
}
