package testCases.Functional.desktop;
/*
 * 
CheckShip_TC_01_addinShippingAndVerifyAddressInMyAccount:Verify Address Added during checkout is visible on MY Account Address Book Section
CheckShip_TC_02_checkDefauldAddressInShippingDropdown : Verify Default address is selected on Checkout Shipping 'Shipping Address' Dropdown
CheckShip_TC_03_updateAddress: Update Address on Shipping and Verify on My Account Landing Page
CheckShip_TC_04_checkShippingmethods: Verify Shipping Methods available and their Price are reflected under pricing Section
CheckShip_TC_05_verificationOfGiftWrapsection: Verify Functionality of GiftWrap and GiftMessage Section
 * 
 */

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import reusableModules.desktop.Desk_CategoryProductClick_Reusable;
import reusableModules.desktop.Desk_CheckoutShipping_Reusable;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_HomePage_Reusable;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_MyAccountAddress_Reusable;
import reusableModules.desktop.Desk_MyAccountLanding_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import setup.MultipleBrowser;
import PageObjects.desktop.Desk_BNY_BillingPageObject;
import PageObjects.desktop.Desk_BNY_LoginRegistrationPageObject;
import PageObjects.desktop.Desk_BNY_MyAccountPageObject;
import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import atu.testng.reports.ATUReports;

public class Desk_ShippingTestCases extends MultipleBrowser{
	GenericMethods genericMethods;
	Desk_HomePage_Reusable homePage_Reusable;
	Desk_LoginRegister_Reusable loginRegister_Reusable;		
	Desk_MYBagCheckout_Reusable myBagCheckout_Reusable;
	Desk_MyAccountLanding_Reusable myAccountLanding_Reusable;
	Desk_CheckoutShipping_Reusable checkoutshipping_Reusable;
	Desk_CategoryProductClick_Reusable categoryProductClick_Reusable;
	Desk_PDP_Reusable pdp_Reusable;
	Desk_MyAccountAddress_Reusable myAccountAddress_Reusable;
	Desk_GlobalNav_Reusable globalNav_Reusable;
	Desk_BNY_LoginRegistrationPageObject loginRegister_PageObjects;
	Desk_BNY_RegistrationPageObject Registtration_PageObjects;
	Desk_BNY_HomePageObject homePage_PageObjects;
	Desk_BNY_ShippingPageObject shipping_PageObjects;
	Desk_BNY_MyAccountPageObject myAccount_PageObjects;
	Desk_BNY_BillingPageObject billing_PageObjects;
	String urlFromXML;
	String currentPageTitle;
	String timeStamp;
	String registeredUserName;
	String Password="12345678";
	
  
	
	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String BNYURL,String WHSURL){
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		driver.get(BNYURL);	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
		}
	
//CheckShip_TC_01_addinShippingAndVerifyAddressInMyAccount:Verify Address Added during checkout is visible on MY Account Address Book Section 
@Test(groups={"CheckoutShipping","Regression","BlackBox"},dataProvider="ShippingFormData" , priority=1,testName="CheckShip_TC_01_addinShippingAndVerifyAddressInMyAccount",enabled=true)	 
	 public void CheckShip_TC_01_addinShippingAndVerifyAddressInMyAccount(String email , String oldPassword ,String country ,String FName,String LName ,String	ShippingAddress1 ,String ShippingAddress2 ,String ShippingCity ,String ShippingState,String	ShippingZip	,String ShippingPhone ,String	CardType ,String CardNo	,String CardCVV ,String	BillingAddress1 ,String	BillingCity	,String BillingState ,String BillingZip	,String Promocode ,String BarneysUrl1, String WareHouseUrl2){
		 	ATUReports.setAuthorInfo("Adapty QA", "25th Aug 2016","1.0"); 		 	 
	 		ATUReports.setTestCaseReqCoverage("Verify Address Added during checkout is visible on MY Account Address Book Section");
	 		Registtration_PageObjects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);
			homePage_PageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			loginRegister_PageObjects = PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
	 	    billing_PageObjects=PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			loginRegister_Reusable=new Desk_LoginRegister_Reusable(); 
	 	    myBagCheckout_Reusable = new Desk_MYBagCheckout_Reusable();
	 	    checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
	 	    categoryProductClick_Reusable=new Desk_CategoryProductClick_Reusable();
	 	    myAccountLanding_Reusable=new Desk_MyAccountLanding_Reusable();
	 	    pdp_Reusable=new Desk_PDP_Reusable();
	 	    homePage_Reusable = new Desk_HomePage_Reusable();
	 	    globalNav_Reusable =new Desk_GlobalNav_Reusable();
			genericMethods=new GenericMethods();
			
			timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
						
			//-------------Registration Module--------------------------			
			loginRegister_Reusable.registerFromHeader(driver);
				
			//-------------------Empty My Bag module ---------------		
			currentPageTitle=genericMethods.getTitle(driver);				
			if (currentPageTitle.equalsIgnoreCase("Barneys Warehouse")){	
				myBagCheckout_Reusable.emptyMyBag(driver, "WHS");
			}else{
				myBagCheckout_Reusable.emptyMyBag(driver, "BNY");
			}
			//-------------------Add Product to Bag ,Add Shipping and verify My account---------------
			//----------------------Category Browse--------------------------
	      	
			globalNav_Reusable.clickSubCategoryName(driver,"Women", "Clothing");				
			categoryProductClick_Reusable.browseProductandClickonBNY(driver,"1000");
			
			//----------------------------PDP---------------------------------			
			pdp_Reusable.selectAvailableColorOnPDP(driver);
			GenericMethods.waitForPageLoaded(driver);
			pdp_Reusable.selectAvailableSizeOnPDP(driver);
			GenericMethods.waitForPageLoaded(driver);
			pdp_Reusable.AddtoCartButtononPDP(driver);
			GenericMethods.waitForPageLoaded(driver);
			pdp_Reusable.miniBagPopuponPDP(driver);
			GenericMethods.waitForPageLoaded(driver);
			pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
			GenericMethods.waitForPageLoaded(driver);
	      
			//------------------My Bag(Logged In User)---------------------------------
			myBagCheckout_Reusable.selectCountry(driver, country);
			GenericMethods.waitForPageLoaded(driver);
			myBagCheckout_Reusable.clickCheckoutButton(driver);
	  	    
	 		//****************Start Filling Shipping Address on Checkout Shipping**************
	 	    checkoutshipping_Reusable.startFillingShippingForm(driver, FName, LName, ShippingAddress1, ShippingAddress2, ShippingCity, ShippingState,ShippingZip, ShippingPhone);
	 	   GenericMethods.waitForPageLoaded(driver);
	 	    checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
	 	   GenericMethods.waitForPageLoaded(driver);
	 	    genericMethods.getTextAndCompare(driver, billing_PageObjects.billing_PageTitle_TV, "Billing");
	 	    homePage_Reusable.goToHomePageUsingLogo(driver, "BNY");//Go to Home Page
	 	    homePage_Reusable.navigateToMyAccount(driver);	//Go to My account
	 	   GenericMethods.waitForPageLoaded(driver);
	 	    myAccountLanding_Reusable.checkLandingPageAddressMyaccount(driver, ShippingAddress1+" "+ShippingAddress2);//Verify Address on My Account Landing Page 
		}	
	 
//CheckShip_TC_02_checkDefauldAddressInShippingDropdown : Verify Default address is selected on Checkout Shipping 'Shipping Address' Dropdown
@Test(groups={"CheckoutShipping","Regression","BlackBox"}, priority=2,testName="CheckShip_TC_02_checkDefauldAddressInShippingDropdown",enabled=true)
	 public void CheckShip_TC_02_checkDefauldAddressInShippingDropdown() {                       
	ATUReports.setAuthorInfo("Adapty QA", "25th Aug 2016","1.0");   
		ATUReports.setTestCaseReqCoverage("Verify Default address is selected on Checkout Shipping 'Shipping Address' Dropdown");
		homePage_PageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		
		loginRegister_Reusable=new Desk_LoginRegister_Reusable(); 
	    myBagCheckout_Reusable = new Desk_MYBagCheckout_Reusable();
		myAccountAddress_Reusable = new Desk_MyAccountAddress_Reusable();
		checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
		globalNav_Reusable=new Desk_GlobalNav_Reusable();
		categoryProductClick_Reusable=new Desk_CategoryProductClick_Reusable();
		
		
		pdp_Reusable=new Desk_PDP_Reusable();
		genericMethods=new GenericMethods();
		//--------------------------Registration Module-------------------
		loginRegister_Reusable.loginFromHeader(driver,"bisnathdubey.adapty@gmail.com", "123456789");
		//----------------------Navigate to Cart--------------------------
		genericMethods.Click(driver, homePage_PageObjects.BNY_Banner_MyBag_LK, "My Bag Link");
 		myBagCheckout_Reusable.selectCountry(driver, "United States");//Select United States as Shipping Country
		myBagCheckout_Reusable.clickCheckoutButton(driver);
		
		checkoutshipping_Reusable.CheckDefaultAddress(driver);//Verify Default Address is selected	    
}

//CheckShip_TC_03_updateAddress: Update Address on Shipping and Verify on My Account Landing Page
@Test(groups={"CheckoutShipping","Regression","BlackBox"}, priority=3,testName="CheckShip_TC_03_updateAddress",enabled=true)	 
	 public void CheckShip_TC_03_updateAddress(){                       
		ATUReports.setAuthorInfo("Adapty Solution", "25th Aug 2016","1.0");		 	    
 		ATUReports.setTestCaseReqCoverage("Update Address on Shipping and Verify on My Account Landing Page");
 		myAccount_PageObjects=PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
 		shipping_PageObjects=PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
 		homePage_PageObjects=PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
 	    loginRegister_Reusable=new Desk_LoginRegister_Reusable(); 
 	    myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
		checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
		homePage_Reusable=new Desk_HomePage_Reusable();
		genericMethods=new GenericMethods();		
		String addressBeforeUpdate;
		String addressAfterUpdate;
		String updateToOriginalAddress;
 		
		loginRegister_Reusable.loginFromHeader(driver, "bisnathdubey.adapty@gmail.com", "123456789");
		homePage_Reusable.navigateToMyAccount(driver);
		addressBeforeUpdate=genericMethods.getText(driver, myAccount_PageObjects.MyAccLanding_AddressSection_DefaultAddress_TV);
		//----------------------Navigate to Cart--------------------------
		genericMethods.Click(driver, homePage_PageObjects.BNY_Banner_MyBag_LK, "My Bag Link");
      
		//------------------My Bag(Logged In User)---------------------------------
		myBagCheckout_Reusable.selectCountry(driver, "United States");
		myBagCheckout_Reusable.clickCheckoutButton(driver);
		
		//********************************Update Address**************************************************
		genericMethods.Click(driver, shipping_PageObjects.shipping_EditAddress_LK, "Edit Address");
		checkoutshipping_Reusable.startFillingShippingForm(driver, "Govind1", "Varma1", "600 Massachusetts Avenue", "apt 3100", "Boston", "Massachusetts", "02118", "4444444444");
 	    checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
		
 	    homePage_Reusable.goToHomePageUsingLogo(driver, "BNY");
		homePage_Reusable.navigateToMyAccount(driver);
		addressAfterUpdate=genericMethods.getText(driver, myAccount_PageObjects.MyAccLanding_AddressSection_DefaultAddress_TV);
	
		///*********************************Update Address back to Original********************************	
		genericMethods.Click(driver, homePage_PageObjects.BNY_Banner_MyBag_LK, "My Bag Link");
		myBagCheckout_Reusable.selectCountry(driver, "United States");
		GenericMethods.waitForPageLoaded(driver);
		myBagCheckout_Reusable.clickCheckoutButton(driver);
		GenericMethods.waitForPageLoaded(driver);
		
		
		//********************************Shipping page*****************************
		genericMethods.Click(driver, shipping_PageObjects.shipping_EditAddress_LK, "Edit Address");
		checkoutshipping_Reusable.startFillingShippingForm(driver, "Govind", "Varma", "3100 Fulton Street", "apt 1100", "Brooklyn", "New York", "10021", "3333333333");
 	    checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
 	    GenericMethods.waitForPageLoaded(driver);
 	    
 	    homePage_Reusable.goToHomePageUsingLogo(driver, "BNY");
 	    homePage_Reusable.navigateToMyAccount(driver);
 	    updateToOriginalAddress=genericMethods.getText(driver, myAccount_PageObjects.MyAccLanding_AddressSection_DefaultAddress_TV);
	
 	    //***********************Verification Of Address*****************************************************
 	    System.out.println("*****Address before Update******:\n"+addressBeforeUpdate+"\n\n*****Address After Update:*****\n"+addressAfterUpdate+"\n\n*****Address Chnaged Back*****:\n"+updateToOriginalAddress);
 	    Assert.assertEquals(addressBeforeUpdate, updateToOriginalAddress);
 	    Assert.assertNotEquals(addressBeforeUpdate, addressAfterUpdate);
 	    
	}

//CheckShip_TC_04_checkShippingmethods: Verify Shipping Methods available and their Price are reflected under pricing Section 
@Test(groups={"CheckoutShipping","Regression","BlackBox"}, priority=4,testName="CheckShip_TC_04_checkShippingmethods",enabled=true)	 
	 public void CheckShip_TC_04_checkShippingmethods(){
		ATUReports.setAuthorInfo("Adapty QA", "25th Aug 2016","1.0");
 		ATUReports.setTestCaseReqCoverage("Verify Shipping Methods available and their Price are reflected under pricing Section");
 		loginRegister_Reusable=new Desk_LoginRegister_Reusable();
 		homePage_PageObjects=PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
 		genericMethods=new GenericMethods();
 		myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
 		checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
 			
 		loginRegister_Reusable.loginFromHeader(driver, "bisnathdubey.adapty@gmail.com" , "123456789");
 		
 		//********************************Cart Page******************************
 		genericMethods.Click(driver, homePage_PageObjects.BNY_Banner_MyBag_LK, "My Bag Link");
 		myBagCheckout_Reusable.selectCountry(driver, "United States");//Select United States as Shipping Country
		myBagCheckout_Reusable.clickCheckoutButton(driver);
		
		//******************************Shipping methods******************************		
		//checkoutshipping_Reusable.SelectShippingmethod(driver, "United States", "sameDay"); 
		checkoutshipping_Reusable.SelectShippingmethod(driver, "United States", "nextDay");
		GenericMethods.waitForPageLoaded(driver);
		checkoutshipping_Reusable.SelectShippingmethod(driver, "United States", "twoDay"); 
		GenericMethods.waitForPageLoaded(driver);
		checkoutshipping_Reusable.SelectShippingmethod(driver, "United States", "ground"); 
		GenericMethods.waitForPageLoaded(driver);
 	}

//CheckShip_TC_05_verificationOfGiftWrapsection: Verify Functionality of GiftWrap and GiftMessage Section
@Test(groups={"CheckoutShipping","Regression","BlackBox"}, priority=5,testName="CheckShip_TC_05_verificationOfGiftWrapsection",enabled=true) 
	 public void CheckShip_TC_05_verificationOfGiftWrapsection(){                       
		ATUReports.setAuthorInfo("Adapty QA", "25th Aug 2016","1.0"); 			 	    
 		ATUReports.setTestCaseReqCoverage("Verify Functionality of GiftWrap and GiftMessage Section");
 		homePage_PageObjects=PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
 	    loginRegister_Reusable=new Desk_LoginRegister_Reusable(); 
 	    genericMethods=new GenericMethods();
		checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
		myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
		
		loginRegister_Reusable.loginFromHeader(driver, "bisnathdubey.adapty@gmail.com" , "123456789");
 		
 		//********************************Cart Page******************************
 		genericMethods.Click(driver, homePage_PageObjects.BNY_Banner_MyBag_LK, "My Bag Link");
 		myBagCheckout_Reusable.selectCountry(driver, "United States");//Select United States as Shipping Country
		myBagCheckout_Reusable.clickCheckoutButton(driver);
		
 		checkoutshipping_Reusable.GiftWrap(driver);			 	    
	 }
	 	 
	 @DataProvider(name = "Loginform")
	 public static Object[][] ProvideDataToAboveMeth1() throws IOException {    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","Sheet1");  
	  }
	 
	 @DataProvider(name = "ShippingFormData")
	 public static Object[][] ProvideDataToAboveMeth() throws IOException{	   
	 	  ReturnDataObject returnData1=new ReturnDataObject();
	 	  return returnData1.read("src/test/java/testDataFiles/desktop/Shipping_billingPage.xls","shippinig");		
	 }
	 
	 @DataProvider(name = "MyAccountFormData")
	 public static Object[][] ProvideDataToAboveMeth2() throws IOException {	   
	 	  ReturnDataObject returnData1=new ReturnDataObject();
	 	  return returnData1.read("src/test/java/testDataFiles/desktop/MyAccountTestCase1.xls","Sheet1");		
	 }

	 @DataProvider(name = "url")
	 public static Object[][] ProvideDataToAboveMeth3() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/url.xls","Sheet1");  
	  }
	 
	 @AfterMethod(groups="After_Method")
	 public void Stop(){
		  driver.close();
		  driver.quit();
	}
	 
	 
}