package testCases.Functional.desktop;

/*
 MyAcc_TC_01_verifyAddAddressInMyAccountAddressBook: This test case verifies if new user is able to add the new address.
 MyAcc_TC_02_verifyEditAddressInMyAccountAddressBook : This test case verifies if user is able to add and edit the address in address book.
 MyAcc_TC_03_verifyDeleteAddressInMyAccountAddressBook : This test case verifies if user is able to delete the address from address book.
 MyAcc_TC_04_MakeDefaultAddressInMyAccountAddressBook : This test case is for making the address as default in address book.
 MyAcc_TC_05_MyAccountAddressBookValidation : This test case is to validate the address field while keeping the address field blank in address book.
 MyAcc_TC_06_BNYMyAccountPaymentAdd : This test case verify if user is able to  add the new payment option.
 MyAcc_TC_07_BNYMyAccountPaymentDelete : This test case verify if user is able to delete the  added payment from payment option page.
 MyAcc_TC_08_BNYMyAccountPaymentEdit : This test is to verify if user is able to edit the saved payment option.
 MyAcc_TC_09_BNYMyAccountPaymentMakeDefault : This test is to verify if user is able to make the selected payment option as a default payment in payment option page.
 MyAcc_TC_10_MyAccountPaymentValidation : This test is to verify if all the mandatory fields are filled while adding payment.
 MyAcc_TC_11_MyDesignerSearchSelectAndClearAll : This test case is to verify if the First Designer is in the List, clickalbe and checked. 2) And also to verify if the User is able to clear all selected designer's from My designer page.
 MyAcc_TC_12_SearchAndAdddesigner : This test verify if user is able to search the desinger and able to  add that designer to my designer.
 MyAcc_TC_13_UpadtePersonlInfo: Updating User profile on MY ACCOUNT Personal info Page
 MyAcc_TC_14_ChangeEmailid : changing and verify Email id from My Account Personal Info section
 MyAcc_TC_15_ChangePassword:Verify Change password functionality by Changing Account Password from MY ACCOUNT>>PERSONAL INFO
 MyAcc_TC_16_CheckEditProfilevalidation: Verify validation messages on FirstName & LastName fields validation on PERSONAL INFO page
 MyAcc_TC_17_ChangeEmailValidation: Verify Email Address , Confirm Email Address , Password fields validation messages on CHANGE EMAIL ADDRESS Page
 MyAcc_TC_18_ChangeEmailValidation: Verify Old Password, New Password, Confirm Password field validation and Messages on SET NEW PASSWORD  Page
 MyAcc_TC_19_verifyBNYemailpreferences : Verify if user unsubscribe the all email preferences and subscribe for BNY email
 MyAcc_TC_20_verifyWHSemailpreferences : Verify if user unsubscribe the all email preferences and subscribe for WHS email
 MyAcc_TC_21_verifyBNYWHSemailpreferences : Verify if user unsubscribe the all email preferences and subscribe for BNY & WHS email list
 MyAcc_TC_01_AddNewAddress_Edit_Delete: Adding New Address, Edit & Delete.
 MyAcc_TC_02_PaymentAdd_Edit_Delete : Adding New payment, Edit & Delete.
 MyAcc_TC_03_MyDesignerSearchSelectAndClearAll : Search,Add Designer and Clear all designer.
 */


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import atu.testng.reports.ATUReports;
import reusableModules.desktop.Desk_CheckoutShipping_Reusable;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_GetOrderNumber_Reusable;
import reusableModules.desktop.Desk_Logout_Reusable;
import reusableModules.desktop.Desk_LookBook_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_MyAccountAddress_Reusable;
import reusableModules.desktop.Desk_MyAccountEmailPreferences_Reusable;
import reusableModules.desktop.Desk_MyAccountPayment_Reusable;
import reusableModules.desktop.Desk_MyAccountPersonalInfo_Reusable;
import reusableModules.desktop.Desk_MyDesigner_Reusable;
import setup.MultipleBrowser;

public class Desk_MyAccountTestCase extends MultipleBrowser{
	Desk_BNY_ShippingPageObject ShippingPageObjects;		
	Desk_BNY_NavigationPageObject globalNavPageObjects;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	Desk_Logout_Reusable logoutReusable;
	Desk_MyAccountPersonalInfo_Reusable personalInfoReusable;
	Desk_MyAccountEmailPreferences_Reusable myAccEmailPreferencesReusable;
	Desk_LookBook_Reusable lookbookReusable;
	Desk_MYBagCheckout_Reusable mybagCheckoutReusable;
	Desk_CheckoutShipping_Reusable checkoutShippingReusable;
	Desk_MYBagCheckout_Reusable myBagCheckoutReusable;
	Desk_GetOrderNumber_Reusable getOrderNumberReusable;
	Desk_MyAccountAddress_Reusable myaccAddressReusable;
	Desk_MyAccountPayment_Reusable myaccountPaymentReusable;
	Desk_MyDesigner_Reusable myDesignerReusable;
	GenericMethods genericMethods;
  	WebDriverWait xplicitWait ;
  	String Emailid="a1@yopmail.com";
	String Password="12345678";
	String CurrentPageTitle;
	Desk_LoginRegister_Reusable bnyLoginforShipping;
	String bnyURL;
	String whsURL;

	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(String Device, String Browser,String bny_URL,String whs_URL){
		//super.Browser=System.getProperty("accountBrowser");
		//System.out.println(System.getProperty("accountBrowser"));
		//super.Device=Device;
		super.setuptest(Device,Browser);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		//driver.get(URL);
		bnyURL= bny_URL;
	//	whsURL= whs_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}

//MyAcc_TC_01_verifyAddAddressInMyAccountAddressBook: This test case verifies if new user is able to add the new address.
	@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=1,testName="MyAcc_TC_01_verifyAddAddressInMyAccountAddressBook",enabled=true)
	public void MyAcc_TC_01_verifyAddAddressInMyAccountAddressBook(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - This test case verifies if new user is able to add the new address.");		
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		logoutReusable =new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
	//------------------------Registration  Module--------------------------						
		logoutReusable.logOutifLoggedIn(driver);
		loginRegisterReusable.registerFromHeader(driver);

	//---------------------My Account--------------------------		
			myaccAddressReusable.myAccountAddress(driver, firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName);
}			

//MyAcc_TC_02_verifyEditAddressInMyAccountAddressBook : This test case verifies if user is able to add and edit the address in address book.
	@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=2,testName="MyAcc_TC_02_verifyEditAddressInMyAccountAddressBook",enabled=true)
	public void MyAcc_TC_02_verifyEditAddressInMyAccountAddressBook(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount -This test case verifies if user is able to add and edit the address in address book.");		
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
	
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Registration Module--------------------------				
		loginRegisterReusable.registerFromHeader(driver);

		//---------------------My Account--------------------------	
		myaccAddressReusable.myAccountAddress(driver,firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName);
		myaccAddressReusable.MyAccountAddressEdit(driver,AddressName,firstName,lastName,address1+"1",address2,city,State,zipCode,shippingPhone);
		System.out.println(myaccAddressReusable);
}

// MyAcc_TC_03_verifyDeleteAddressInMyAccountAddressBook : This test case verifies if user is able to delete the address from address book.
	@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=3,testName="MyAcc_TC_03_verifyDeleteAddressInMyAccountAddressBook",enabled=true)
	public void MyAcc_TC_03_verifyDeleteAddressInMyAccountAddressBook(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount -This test case verifies if user is able to delete the address from address book.");		
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		logoutReusable=new Desk_Logout_Reusable();

		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Registration Module--------------------------					
		loginRegisterReusable.registerFromHeader(driver);
				
	//---------------------My Account--------------------------		
		myaccAddressReusable.myAccountAddress(driver,firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName);
		myaccAddressReusable.MyAccountAddressDelete(driver);	
}
	
//MyAcc_TC_04_MakeDefaultAddressInMyAccountAddressBook : This test case is for making the address as default in address book.
	@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=4,testName="MyAcc_TC_04_MakeDefaultAddressInMyAccountAddressBook",enabled=true)
	public void MyAcc_TC_04_MakeDefaultAddressInMyAccountAddressBook(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {		
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - This test case is for making the address as default in address book.");		
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Registration Module--------------------------			
		
		loginRegisterReusable.registerFromHeader(driver);
		
	
	//---------------------My Account--------------------------	
		myaccAddressReusable.myAccountAddress(driver, firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName);
		myaccAddressReusable.MyAccountAddressManualAdd(driver);
		myaccAddressReusable.MyAccountAddressMakeDefault(driver);
}
	
//MyAcc_TC_05_MyAccountAddressBookValidation : This test case is to validate the address field while keeping the address field blank in address book.
	@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=5,testName="MyAcc_TC_05_MyAccountAddressBookValidation",enabled=true)
	public void MyAcc_TC_05_MyAccountAddressBookValidation(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("AQ Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount -This test case is to validate the address field while keeping the address field blank in address book.");		
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		bnyLoginforShipping = new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
	
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Register Module--------------------------			
		
		bnyLoginforShipping.registerFromHeader(driver);
	
	//---------------------My Account--------------------------		
		myaccAddressReusable.myAccountAddressValidation(driver);
}
		
//MyAcc_TC_06_BNYMyAccountPaymentAdd : This test case verify if user is able to  add the new payment option.
	@Test(groups={"MyAccount","MyAccountPayment","Regression","BlackBox"},dataProvider="MyAccountData",priority=6,testName="MyAcc_TC_06_BNYMyAccountPaymentAdd",enabled=true)
	public void MyAcc_TC_06_BNYMyAccountPaymentAdd(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName,String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - This test case verify if user is able to  add the new payment option.");		
		myaccountPaymentReusable=new Desk_MyAccountPayment_Reusable();
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		logoutReusable=new Desk_Logout_Reusable();

		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
		//------------------------Registration Module--------------------------	
		//loginRegisterReusable.loginFromHeader(driver, "bisnathdubey@gmail.com", "123456789");
		loginRegisterReusable.registerFromHeader(driver);
		//---------------------My Account--------------------------			
		myaccountPaymentReusable.MyAccountPaymentMethod(driver,firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName, CardNo, cvv, Email,CardHolderName,Month,Year);
				
}

//MyAcc_TC_07_BNYMyAccountPaymentDelete : This test case verify if user is able to delete the  added payment from payment option page.
	@Test(groups={"MyAccount","MyAccountPayment","Regression","BlackBox"},dataProvider="MyAccountData",priority=7,testName="MyAcc_TC_07_BNYMyAccountPaymentDelete",enabled=true)
	public void MyAcc_TC_07_BNYMyAccountPaymentDelete(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - This test case verify if user is able to delete the  added payment from payment option page.");		
		myaccountPaymentReusable=new Desk_MyAccountPayment_Reusable();
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
	
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Registration Module--------------------------		
		
		loginRegisterReusable.registerFromHeader(driver);
		
/*		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
*/		
	//---------------------My Account--------------------------		
		myaccountPaymentReusable.MyAccountPaymentMethod(driver, firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName, CardNo, cvv, Email,CardHolderName,Month,Year);
		myaccountPaymentReusable.MyAccountPaymentManual(driver);
		myaccountPaymentReusable.MyAccountPaymentDelete(driver);		
	
	}

//MyAcc_TC_08_BNYMyAccountPaymentEdit : This test is to verify if user is able to edit the saved payment option.
	@Test(groups={"MyAccount","MyAccountPayment","Regression","BlackBox"},dataProvider="MyAccountData",priority=8,testName="MyAcc_TC_08_BNYMyAccountPaymentEdit",enabled=true)
	public void MyAcc_TC_08_BNYMyAccountPaymentEdit(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - This test is to verify if user is able to edit the saved payment option.");		
		myaccountPaymentReusable=new Desk_MyAccountPayment_Reusable();
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
//------------------------Registration Module--------------------------				
		loginRegisterReusable.registerFromHeader(driver); 

/*		
		//-------------------Empty My Bag module --------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
*/		
//---------------------My Account--------------------------	
		myaccountPaymentReusable.MyAccountPaymentMethod(driver, firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName, CardNo, cvv, Email,CardHolderName,Month,Year);
		myaccountPaymentReusable.MyAccountAddressEdit(driver);		
	}
		
//MyAcc_TC_09_BNYMyAccountPaymentMakeDefault : This test is to verify if user is able to make the selected payment option as a default payment in payment option page.
	@Test(groups={"MyAccount","MyAccountPayment","Regression","BlackBox"},dataProvider="MyAccountData",priority=9,testName="MyAcc_TC_09_BNYMyAccountPaymentMakeDefault",enabled=true)
	public void MyAcc_TC_09_BNYMyAccountPaymentMakeDefault(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - This test is to verify if user is able to make the selected payment option as a default payment in payment option page.");		
		myaccountPaymentReusable=new Desk_MyAccountPayment_Reusable();
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Registration Module--------------------------		
		//loginRegisterReusable.loginFromHeader(driver, "bisnathdubey@gmail.com", "123456789");
		loginRegisterReusable.registerFromHeader(driver);
/*		
		//-------------------Empty My Bag module ---------------			
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
*/		
	//---------------------My Account--------------------------			
		myaccountPaymentReusable.MyAccountPaymentMethod(driver, firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName, CardNo, cvv, Email,CardHolderName,Month,Year);
		myaccountPaymentReusable.MyAccountPaymentManual(driver);
		myaccountPaymentReusable.MyAccountPaymentDefault(driver);
		
	}

//MyAcc_TC_10_MyAccountPaymentValidation : This test is to verify if all the mandatory fields are filled while adding payment.
	@Test(groups={"MyAccount","MyAccountPayment","Regression","BlackBox"},dataProvider="MyAccountData",priority=10,testName="MyAcc_TC_10_MyAccountPaymentValidation",enabled=true)
	public void MyAcc_TC_10_MyAccountPaymentValidation(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - This test is to verify if all the mandatory fields are filled while adding payment.");		
		myaccountPaymentReusable=new Desk_MyAccountPayment_Reusable();
		bnyLoginforShipping = new Desk_LoginRegister_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
			
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Login Module--------------------------				
		bnyLoginforShipping.loginFromHeader(driver,Emailid,Password);
/*		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
*/		
	//---------------------My Account--------------------------			
		myaccountPaymentReusable.MyAccountPaymentValidation(driver);	
	}


//MyAcc_TC_11_MyDesignerSearchSelectAndClearAll : This test case is to verify if the First Designer is in the List and it is clickalbe and checked. 2) And also to verify if the User is able to clear all selected designer's from My designer page.
	@Test(groups={"MyAccount","MyDesigners","Regression"},priority=11,testName="MyAcc_TC_11_MyDesignerSearchSelectAndClearAll",enabled=true) 
	public void MyAcc_TC_11_MyDesignerSearchSelectAndClearAll ()  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount -This test case is to verify if the First Designer is in the Listand it is clickalbe and checked. 2) And also to verify if the User is able to clear all selected designer's from My designer page.");	
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		myDesignerReusable=new Desk_MyDesigner_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		String DesignerName="3LAB";
	
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
	//------------------------Login Module--------------------------				
		loginRegisterReusable.registerFromHeader(driver);
	
/*		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}	
*/		
	//---------------------My Account--------------------------		
			myDesignerReusable.addAndRemoveDesignerFromMyDesigners(driver,DesignerName);
			//System.out.println(DesignerName);
}

//MyAcc_TC_12_SearchAndAdddesigner : This test verify if user is able to search the desinger and able to  add that designer to my designer.
	@Test(groups={"MyAccount","MyDesigners","Regression"},priority=12,testName="MyAcc_TC_12_SearchAndAdddesigner",enabled=true)
	public void MyAcc_TC_12_SearchAndAdddesigner()  {
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - Adding a Designer in an account");		
		myDesignerReusable=new Desk_MyDesigner_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		String DesignerName="3LAB";

		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
//------------------------Registration Module--------------------------				
		loginRegisterReusable.registerFromHeader(driver);
	/*	//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
	*/	
	//---------------------My Account--------------------------						
			myDesignerReusable.searchAndAddDesignerFromMyDesigners(driver,DesignerName);						
}
	
//MyAcc_TC_13_ProfileUpdate: Updating User profile on MY ACCOUNT Personal info Page.
 @Test(groups={"MyAccount","MAPersonalInfo1","Regression","My Account","BlackBox"},dataProvider="PersonalInfo" , priority=13,testName="MyAcc_TC_13_UpadtePersonlInfo",enabled=true)
	public void MyAcc_TC_13_ProfileUpdate(String Email, String oldPassword,  String firstname , String Lastname , String Month, String day ,String Year, String Gender){
		ATUReports.setAuthorInfo("Adapty QA","5th May 2016","1.0"); 
	    ATUReports.setTestCaseReqCoverage("MyAccount - Updating User profile on MY ACCOUNT Personal info Page.");	  			
	    loginRegisterReusable=new Desk_LoginRegister_Reusable(); 
		personalInfoReusable= new Desk_MyAccountPersonalInfo_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
		loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.loginFromHeader(driver, Email, oldPassword);	  
		personalInfoReusable.updatePersonalInfo(driver,firstname ,Lastname, Month, day , Year , Gender );			  
 }
 
//MyAcc_TC_14_ChangeEmailid : changing and verify Email id from My Account Personal Info section
@Test(groups={"MyAccount","MAPersonalInfo","Regression","My Account","BlackBox"},dataProvider="ChangeEmail" , priority=14,testName="MyAcc_TC_14_ChangeEmailid",enabled=true)
	public void MyAcc_TC_14_ChangeEmailid(String Email, String oldPassword , String ChangeEmail) {
			ATUReports.setAuthorInfo("Adapty QA","5th May 2016","1.0"); 
		    ATUReports.setTestCaseReqCoverage("changing and verify Email id from My Account Personal Info section");		    			   
		    loginRegisterReusable=new Desk_LoginRegister_Reusable();
			personalInfoReusable= new Desk_MyAccountPersonalInfo_Reusable();
			logoutReusable=new Desk_Logout_Reusable();
			
			driver.get(bnyURL);
			logoutReusable.logOutifLoggedIn(driver);
			loginRegisterReusable.verifyHomePage(driver);
			loginRegisterReusable.loginFromHeader(driver, Email , oldPassword);
			personalInfoReusable.ChangeEmailID(driver,Email, oldPassword, ChangeEmail);
			personalInfoReusable.UpdatePreviousEmail(driver, Email, oldPassword);			  
	 }

// MyAcc_TC_15_ChangePassword :Verify Change password functionality by Changing Account Password from MY ACCOUNT>>PERSONAL INFO 
@Test(groups={"MyAccount","MAPersonalInfo","Regression","My Account","BlackBox1"},dataProvider="ChangePWD" , priority=15,testName="MyAcc_TC_15_ChangePassword",enabled=true)
	public void  MyAcc_TC_15_ChangePassword(String Email, String oldPassword , String newPassword) {
		ATUReports.setAuthorInfo("Adapty QA","5th May 2016","1.0"); 
		ATUReports.setTestCaseReqCoverage("Verify Change password functionality by Changing Account Password from MY ACCOUNT>>PERSONAL INFO");		
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		personalInfoReusable = new Desk_MyAccountPersonalInfo_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
		loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.loginFromHeader(driver, Email , oldPassword);
		personalInfoReusable.ChangeMyPassword(driver, Email, oldPassword, newPassword);
		personalInfoReusable.updateexitingpassword(driver, newPassword, oldPassword);	 		  
	} 
	 	

//MyAcc_TC_16_CheckEditProfilevalidation: Verify validation messages on FirstName & LastName fields validation on PERSONAL INFO page
@Test(groups={"MyAccount","MAPersonalInfo","Regression","My Account","BlackBox"},dataProvider="PersonalInfo" , priority=16,testName="MyAcc_TC_16_CheckEditProfilevalidation",enabled=true)
	public void  MyAcc_TC_16_CheckEditProfilevalidation(String Email, String oldPassword , String firstname , String Lastname , String Month ,String day ,String Year,String Gender) {


	ATUReports.setAuthorInfo("Adapty QA","5th May 2016","1.0"); 
		ATUReports.setTestCaseReqCoverage("Verify validation messages on FirstName & LastName fields validation on PERSONAL INFO page");			
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		personalInfoReusable = new Desk_MyAccountPersonalInfo_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
		loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.loginFromHeader(driver, Email , oldPassword);
		personalInfoReusable.EditProfileValidation(driver);	 	 
	}

//MyAcc_TC_17_ChangeEmailValidation: Verify Email Address , Confirm Email Address , Password fields validation messages on CHANGE EMAIL ADDRESS Page
@Test(groups={"MyAccount","MAPersonalInfo","Regression","My Account","BlackBox2"},dataProvider="PersonalInfo" , priority=17,testName="MyAcc_TC_17_ChangeEmailValidation",enabled=true)
	public void  MyAcc_TC_17_ChangeEmailValidation(String Email, String oldPassword , String firstname , String Lastname , String Month ,String day ,String Year,String Gender) {
		ATUReports.setAuthorInfo("Adapty QA","5th May 2016","1.0"); 
		ATUReports.setTestCaseReqCoverage("Verify Email Address , Confirm Email Address , Password fields validation messages on CHANGE EMAIL ADDRESS Page");		
		personalInfoReusable = new Desk_MyAccountPersonalInfo_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
	  
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
		loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.loginFromHeader(driver, Email , oldPassword);
		personalInfoReusable.ChangeEmailAddressValidation(driver);	 	 
	}			


//MyAcc_TC_18_ChangePasswordValidation: Verify Old Password, New Password, Confirm Password field validation and Messages on SET NEW PASSWORD  Page
@Test(groups={"MyAccount","MAPersonalInfo","Regression","My Account","BlackBox"},dataProvider="PersonalInfo" , priority=18,testName="MyAcc_TC_18_ChangePasswordValidation",enabled=true )
	public void  MyAcc_TC_18_ChangePasswordValidation (String Email, String oldPassword , String firstname , String Lastname , String Month ,String day ,String Year,String Gender) {
		ATUReports.setAuthorInfo("Adapty QA","5th May 2016","1.0"); 
		ATUReports.setTestCaseReqCoverage("Verify Old Password, New Password, Confirm Password field validation and Messages on SET NEW PASSWORD  Page");		
		loginRegisterReusable=new Desk_LoginRegister_Reusable(); 
		personalInfoReusable = new Desk_MyAccountPersonalInfo_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		logoutReusable.logOutifLoggedIn(driver);
		loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.loginFromHeader(driver, Email , oldPassword);
		personalInfoReusable.ChangePasswordPageValidation(driver);		 
	}


//MyAcc_TC_19_verifyBNYemailpreferences : Verify if user unsubscribe the all email preferences and subscribe for BNY email
@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=19,testName="MyAcc_TC_19_verifyBNYemailpreferences",enabled=true)
	public void MyAcc_TC_119_verifyBNYemailpreferences(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
	 ATUReports.setAuthorInfo("Adapty QA","25th August 2016","1.0"); 
	 ATUReports.setTestCaseReqCoverage("Verify if user unsubscribe the all email preferences and subscribe for BNY email");
	 myAccEmailPreferencesReusable=new Desk_MyAccountEmailPreferences_Reusable();
	   loginRegisterReusable=new Desk_LoginRegister_Reusable(); 
	   myAccEmailPreferencesReusable = new Desk_MyAccountEmailPreferences_Reusable();
	   loginRegisterReusable = new Desk_LoginRegister_Reusable();
	   driver.get(bnyURL);
	   
	   
	   loginRegisterReusable.loginFromHeader(driver, Emailid, Password);
	   myAccEmailPreferencesReusable.unSubScribeFromAllEmailList(driver);
	   myAccEmailPreferencesReusable.subScribetoBNYEmailList(driver);
	   

}	
//MyAcc_TC_20_verifyWHSemailpreferences : Verify if user unsubscribe the all email preferences and subscribe for WHS email
@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=20,testName="MyAcc_TC_20_verifyWHSemailpreferences",enabled=true)
	public void MyAcc_TC_20_verifyWHSemailpreferences(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
	 ATUReports.setAuthorInfo("Adapty QA","25th August 2016","1.0"); 
	 ATUReports.setTestCaseReqCoverage("Verify if user unsubscribe the all email preferences and subscribe for WHS email");
	 myAccEmailPreferencesReusable=new Desk_MyAccountEmailPreferences_Reusable();
	   loginRegisterReusable=new Desk_LoginRegister_Reusable(); 
	   myAccEmailPreferencesReusable = new Desk_MyAccountEmailPreferences_Reusable();
	   loginRegisterReusable = new Desk_LoginRegister_Reusable();
	   
	   driver.get(bnyURL);
	   
	 //------------------------Registration Module--------------------------				
	   loginRegisterReusable.registerFromHeader(driver);
	   myAccEmailPreferencesReusable.unSubScribeFromAllEmailList(driver);
	   myAccEmailPreferencesReusable.subScribetoWHSEmailList(driver);
	   

}
//MyAcc_TC_21_verifyBNYWHSemailpreferences : Verify if user unsubscribe the all email preferences and subscribe for BNY & WHS email list
@Test(groups={"MyAccount","MyAccountAddress","Regression"},dataProvider="MyAccountData",priority=21,testName="MyAcc_TC_21_verifyBNYWHSemailpreferences",enabled=true)
	public void MyAcc_TC_21_verifyBNYWHSemailpreferences(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
	 ATUReports.setAuthorInfo("Adapty QA","25th August 2016","1.0"); 
	 ATUReports.setTestCaseReqCoverage("Verify if user unsubscribe the all email preferences and subscribe for BNY & WHS email list");
	 myAccEmailPreferencesReusable=new Desk_MyAccountEmailPreferences_Reusable();
	   loginRegisterReusable=new Desk_LoginRegister_Reusable(); 
	   myAccEmailPreferencesReusable = new Desk_MyAccountEmailPreferences_Reusable();
	   loginRegisterReusable = new Desk_LoginRegister_Reusable();
	   driver.get(bnyURL);
	   
	   
	 //------------------------Registration Module--------------------------				
	   loginRegisterReusable.registerFromHeader(driver);
	   myAccEmailPreferencesReusable.unSubScribeFromAllEmailList(driver);
	   myAccEmailPreferencesReusable.subScribetoBNYWHSEmailList(driver);
	   
}

//MyAcc_TC_01_AddNewAddress_Edit_Delete: Adding New Address, Edit & Delete.
	@Test(groups={"BlackBox"},dataProvider="MyAccountData",priority=1,testName="MyAcc_TC_01_AddNewAddress_Edit_Delete",enabled=true)
	public void MyAcc_TC_01_AddNewAddress_Edit_Delete(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
		ATUReports.setTestCaseReqCoverage("MyAccount - Adding New Address, Edit & Delete.");		
		myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		logoutReusable =new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
	//------------------------Registration  Module--------------------------						
		logoutReusable.logOutifLoggedIn(driver);
		loginRegisterReusable.registerFromHeader(driver);

	//---------------------My Account--------------------------		
			myaccAddressReusable.myAccountAddress(driver, firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName);
	//--------------------Edit--------------------------------		
		myaccAddressReusable.MyAccountAddressEdit(driver,AddressName,firstName,lastName,address1+"1",address2,city,State,zipCode,shippingPhone);	
	//--------------------Delete------------------------------
		myaccAddressReusable.MyAccountAddressDelete(driver);	
}

	//MyAcc_TC_02_PaymentAdd_Edit_Delete : Adding New payment, Edit & Delete.
		@Test(groups={"BlackBox"},dataProvider="MyAccountData",priority=6,testName="MyAcc_TC_02_PaymentAdd_Edit_Delete",enabled=true)
		public void MyAcc_TC_02_PaymentAdd_Edit_Delete(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName,String CardType, String CardNo, String cvv, String Email,String Emailid, String Password,String CardHolderName, String Month,String Year)  {
			
			ATUReports.setTestCaseReqCoverage("MyAccount - Adding New payment, Edit & Delete.");		
			myaccountPaymentReusable=new Desk_MyAccountPayment_Reusable();
			myaccAddressReusable=new Desk_MyAccountAddress_Reusable();
			loginRegisterReusable = new Desk_LoginRegister_Reusable();
			logoutReusable=new Desk_Logout_Reusable();

			driver.get(bnyURL);
			logoutReusable.logOutifLoggedIn(driver);
			//------------------------Registration Module--------------------------	
			loginRegisterReusable.loginFromHeader(driver, "kaling46@yopmail.com", "12345678");
			
			//---------------------My Account Add Payment--------------------------	
			myaccountPaymentReusable.MyAccountPaymentMethod(driver,firstName,lastName,address1,address2,city,zipCode,shippingPhone,State,AddressName, CardNo, cvv, Email,CardHolderName,Month,Year);
			
			//-------------------Edit----------------------------------------------
			myaccountPaymentReusable.MyAccountAddressEdit(driver);
			
			//-----------------------Delete------------------------------------------
			myaccountPaymentReusable.MyAccountPaymentManual(driver);
			myaccountPaymentReusable.MyAccountPaymentDelete(driver);
					
	}
		
		
	
		//MyAcc_TC_03_MyDesignerSearchSelectAndClearAll : Search,Add Designer and Clear all designer. 
		@Test(groups={"BlackBox"},priority=11,testName="MyAcc_TC_03_MyDesignerSearchSelectAndClearAll",enabled=true) 
		public void MyAcc_TC_03_MyDesignerSearchSelectAndClearAll ()  {
			
			ATUReports.setTestCaseReqCoverage("MyAccount - Search,Add Designer and Clear all designer. ");	
			loginRegisterReusable=new Desk_LoginRegister_Reusable();
			myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
			myDesignerReusable=new Desk_MyDesigner_Reusable();
			logoutReusable=new Desk_Logout_Reusable();
			String DesignerName="3LAB";
		
			driver.get(bnyURL);
			logoutReusable.logOutifLoggedIn(driver);
		//------------------------Login Module--------------------------				
			loginRegisterReusable.registerFromHeader(driver);
			
		//---------------------My Account--------------------------		
				myDesignerReusable.addAndRemoveDesignerFromMyDesigners(driver,DesignerName);
			
	}



/*	LookBook test case 
	@Test(groups="Lookbook")
	public void TestLookBook(){
		 ATUReports.setTestCaseReqCoverage("Testing Lookbook");
		 lookbookReusable= new LookBook();
		 lookbookReusable.mylook(driver, genericMethods); 
	}
	
	@Test(groups="EmailPreferences1")
		public void subscribetoBNYEmailList(){
			myAccEmailPreferencesReusable=new MyAccountEmailPreferences();
			loginRegisterReusable=new BNYLoginReusable(); 
			loginRegisterReusable.Login(driver, "gvarma0904@yopmail.com" , "12345678");
			driver.get(bnyURL);	
			ATUReports.setTestCaseReqCoverage("Test My Account Email Preferences");
			myAccEmailPreferencesReusable.unSubScribeFromAllEmailList(driver);
			myAccEmailPreferencesReusable.subScribetoBNYEmailList(driver);		
		}
			
	@Test(groups="EmailPreferences2")
		public void subscribetoWHSEmailList(){
			myAccEmailPreferencesReusable=new MyAccountEmailPreferences();
			loginRegisterReusable=new BNYLoginReusable(); 
			loginRegisterReusable.Login(driver, "gvarma0904@yopmail.com" , "12345678");
			driver.get(bnyURL);
			ATUReports.setTestCaseReqCoverage("Test My Account Email Preferences");
			myAccEmailPreferencesReusable.unSubScribeFromAllEmailList(driver);
			myAccEmailPreferencesReusable.subScribetoWHSEmailList(driver);		
		}
			
	@Test(groups="EmailPreferences3")
		public void subscribetoBNYWHSEmailList(){
			myAccEmailPreferencesReusable=new MyAccountEmailPreferences();
			loginRegisterReusable=new BNYLoginReusable(); 
			loginRegisterReusable.Login(driver, "gvarma0904@yopmail.com" , "12345678");
			driver.get(bnyURL);	
			ATUReports.setTestCaseReqCoverage("Test My Account Email Preferences");
			myAccEmailPreferencesReusable.unSubScribeFromAllEmailList(driver);
			myAccEmailPreferencesReusable.subScribetoBNYWHSEmailList(driver);		
		}

/* ======================*********Test Case 12 starts*********===============================		
@Test(groups={"MyAccount3","Regression"},dataProvider="MyAccountData",priority=12,testName="MyAcc_TC_12_MyDesignerClearAll",enabled=true)
	public void MyAcc_TC_12_MyDesignerClearAll(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email,String Emailid, String Password)  {
		
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount - Adding a Designer in an account");
		genericMethods =  new GenericMethods();
		myDesignerReusable=new Desk_MyDesigner_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		String DesignerName="3LAB";
		
		//------------------------Registration Module--------------------------				
		loginRegisterReusable.registerFromHeader(driver);
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//---------------------My Account--------------------------						
		myDesignerReusable.addAndRemoveDesignerFromMyDesigners(driver,DesignerName);					
		myDesignerReusable.DesignerDelete(driver);
	}
				
//========================*********Test Case 13 starts*********===============================
	@Test(groups="MyAccount",priority=14)
	public void DesignerSelectCheckBoxFromMyAccountMyDesigner()  {
		
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount -Adding a Designer in an account using Checkbox ");
		genericMethods =  new GenericMethods();
		
		//------------------------Registration Module--------------------------
		loginRegisterReusable=new Desk_LoginRegister_Reusable();	
		myDesignerReusable=new Desk_MyDesigner_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();		
		loginRegisterReusable.registerFromHeader(driver);
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		myDesignerReusable.DesignerSelectCheckBox(driver, genericMethods);
	}

//========================*********Test Case 14 starts*********===============================
@Test(groups="MyAccount",priority=15)
	public void DesignerUnSelectCheckBoxFromMyAccountMyDesigner()  {
		
		ATUReports.setAuthorInfo("QA Team", "9th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("MyAccount -Adding and Deleting a Designer in an account using Checkbox ");
		genericMethods =  new GenericMethods();
		myDesignerReusable=new Desk_MyDesigner_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		
		//------------------------Registration Module--------------------------				
		loginRegisterReusable.registerFromHeader(driver);
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		myDesignerReusable.DesignerSelectCheckBox(driver, genericMethods);
		myDesignerReusable.DesignerUnselectCheckBox(driver, genericMethods);
	}
*/	
		
			
	@AfterMethod(groups = "After_Method")
	public void stop(){
		GenericMethods.Waitformilliseconds(2000);		
		driver.quit();
	}
	
	@DataProvider(name = "MyAccountData")
		public static Object[][] provideDataToAboveMeth() throws IOException{	   
		   ReturnDataObject returnData1=new ReturnDataObject();
		   return returnData1.read("src/test/java/testDataFiles/desktop/MyAccountTestCase.xls","Sheet1");		
	}
	
	 @DataProvider(name = "ProfileValidation")
	 public static Object[][] ProvideDataToAboveMeth1() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","ProfileValidation");  
	  }//PersonalInfo
	 
	 @DataProvider(name = "PersonalInfo")
	 public static Object[][] provideDataToPersonalInfoTC() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","PersonalInfo");  
	  }
	

	 @DataProvider(name = "Sheet2")
	 public static Object[][] provideDataToSheet2() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","Sheet2");  
	 }
	 
	 @DataProvider(name = "ChangeEmail")
	 public static Object[][] provideDataToChnageEmailTC() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","ChangeEmail");  
	  }
	 
	 @DataProvider(name = "ChangePWD")
	 public static Object[][] provideDataToChnagePasswordTC() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","ChangePWD");  
	  }
	 
	 @DataProvider(name = "Configuration")
	 public static Object[][] provideDataToBeforeMethod() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Configuration.xls","BeforeMethod");  
	  }
	
}
