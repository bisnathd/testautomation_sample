package testCases.Functional.desktop;

/*
BP_TC_01_BNYFilterBySize : Apply filter by  size on browse page.
BP_TC_02_BNYFilterByColor : Apply filter by Color on browse page.
BP_TC_03_BrowseDesignerFilter : This test case verifies browse page after applying Designer filter ON BNY.
BP_TC_04_LogInMyDesignerFilter : This test case verifies if system display login page for guest user when click on my designer link from left nav.
BP_TC_05_BrowsePagination :This test case verifies specific subsubcategory browse page after applying particular pagination number on BNY.
BP_TC_06_FiltersSortViewsBNY_96 : This test case verifies specific browse page with 96 view on BNY.
BP_TC_07_SortViewsWHS48_96_4x_6x : This test case verifies specific Subsubcategory browse page after applying Pricewise sorting(LOW TO HIGH) for 6x products on WHS.
BP_TC_08_EditMyDesignerFilter : This test case verifies if use is able to edit designer from browse page.
BP_TC_09_SortByPriceLowtoHigh96ViewsBNY: This test case verify the sort by Price Low to High  with 96 view.
BP_TC_10_SortByPriceHighToLow96ViewsBNY: This test case verify the sort by Price High To Low with 96 view. 
*/

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import reusableModules.desktop.Desk_CheckoutShipping_Reusable;
import reusableModules.desktop.Desk_GetOrderNumber_Reusable;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_Logout_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_MyAccountAddress_Reusable;
import reusableModules.desktop.Desk_MyAccountPayment_Reusable;
import reusableModules.desktop.Desk_MyDesigner_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import reusableModules.desktop.Desk_SearchResultsAndFilters_Reusable;
import reusableModules.desktop.Desk_SortBy_Reusable;
import setup.MultipleBrowser;

public class Desk_BrowseTestCases extends MultipleBrowser{
	
	GenericMethods genericMethods;
	Desk_BNY_ShippingPageObject ShippingPage_Objects;
	WebDriverWait xplicitWait ;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	Desk_CheckoutShipping_Reusable checkoutShippingReusable;
	Desk_BNY_NavigationPageObject globalNav_Objects;
	Desk_PDP_Reusable pdpReusable;
	Desk_MYBagCheckout_Reusable CheckOutMyBag;
	Desk_GetOrderNumber_Reusable getOrderNumberReusable;
	Desk_MyAccountAddress_Reusable myaccAddressReusable;
	Desk_MyAccountPayment_Reusable myaccountPayment;
	Desk_Logout_Reusable logoutReusable;
	Desk_SearchResultsAndFilters_Reusable searchResultsAndFiltersReusable;
	Desk_MyDesigner_Reusable bnyDesigner;
	Desk_SortBy_Reusable sortbyReusable;
	Desk_GlobalNav_Reusable globalNavReusable;
	String bnyURL;
	String login;
	String whsURL;
	String categoryName = "Women";
	String subCategoryName = "Clothing";
	String correctSearchTerm = "jeans";
	String wrongSearchTerm = "hcl";


	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		//driver.get(URL);
		bnyURL= bny_URL;
		whsURL= whs_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}
 

//BP_TC_01_BNYFilterBySize : Apply filter by  size on browse page.
	@Test(groups={"Browsepage","Regression"},priority=1,testName="BP_TC_01_BNYFilterBySize",enabled=true)
	public void BP_TC_01_BNYFilterBySize()  {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Browse page - Apply filter by  size on browse page.");
		globalNavReusable = new Desk_GlobalNav_Reusable();	
		genericMethods =  new GenericMethods();
		sortbyReusable=new Desk_SortBy_Reusable();
		
		driver.get(bnyURL);
		//----------------------Category Browse--------------------------
		globalNavReusable.CategoryMouseHover(driver, categoryName);
		globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
		ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
		sortbyReusable.selectFilters(driver, "size");
	}

//BP_TC_02_BNYFilterByColor : Apply filter by Color on browse page.
	@Test(groups={"Browsepage","Regression"},priority=2,testName="BP_TC_02_BNYFilterByColor",enabled=true)
	public void BP_TC_02_BNYFilterByColor()  {
		globalNavReusable = new Desk_GlobalNav_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Browse page - Apply filter by Color on browse page.");
		genericMethods =  new GenericMethods();
		sortbyReusable=new Desk_SortBy_Reusable();
		
		driver.get(bnyURL);
		//----------------------Category Browse--------------------------
		globalNavReusable.CategoryMouseHover(driver, categoryName);
		globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);	
		ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);			
		sortbyReusable.selectFilters(driver,"color");	
	}

//BP_TC_03_BrowseDesignerFilter : This test case verifies browse page after applying Designer filter ON BNY.
	 @Test(groups={"Browsepage","Regression"},priority=3,testName="BP_TC_03_BrowseDesignerFilter",enabled=true)
	 public void BP_TC_03_BrowseDesignerFilter(){
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
	     ATUReports.setTestCaseReqCoverage("Browse - This test case verifies browse page after applying Designer filter ON BNY.");
	     sortbyReusable=new Desk_SortBy_Reusable();
	     
	 	driver.get(bnyURL);
	   //----------------------Category Browse--------------------------
		 globalNavReusable.CategoryMouseHover(driver, categoryName);
		 globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);	
		 sortbyReusable.selectFilters(driver, "designer");	
	  }
/*	 
 * 					####################3THIS FUNCTIONALITY HAS BEEN REMOVED###################
 
//BP_TC_04_LogInMyDesignerFilter : This test case verifies if system display login page for guest user when click on my designer link from left nav.
	 @Test(groups={"Browsepage","Regression"},priority=4,testName="BP_TC_04_LogInMyDesignerFilter",enabled=true)
		public void BP_TC_04_LogInMyDesignerFilter()  {
		 	ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Browse - This test case verifies if system display login page for guest user when click on my designer link from left nav.");
			sortbyReusable=new Desk_SortBy_Reusable();	
			globalNavReusable = new Desk_GlobalNav_Reusable();
			loginRegisterReusable=new Desk_LoginRegister_Reusable();				
			bnyDesigner=new Desk_MyDesigner_Reusable();
			logoutReusable=new Desk_Logout_Reusable();
			
			driver.get(bnyURL);
			loginRegisterReusable.registerFromHeader(driver);
			login= loginRegisterReusable.sTime;
			System.out.println("Hi"+login);
			
			//----------------------Category Browse--------------------------
			 globalNavReusable.CategoryMouseHover(driver, categoryName); 
			 globalNavReusable.clickSubSubCategoryName(driver, categoryName, "All Designers (A-Z)");	
			 ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED); 
			 sortbyReusable.AddDesignersfromAllDesignersDLP(driver);	
			 GenericMethods.Waitformilliseconds(4000);
			 //-----------------LogOut if login-------------------------------
			 logoutReusable.logOutifLoggedIn(driver);
			 globalNavReusable.CategoryMouseHover(driver, categoryName);
			 globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
			 //-------lick on designer from left nav-------------------------------
			 sortbyReusable.FilterByMyDesigners(driver);
			 //--------Log in---------------------------------------------------
			 loginRegisterReusable.generalLogin1(driver,login);
			 ATUReports.add("Check if newly added designer is displayed.", "Newly added designers product is displayed on browse page", "product with newly added designer is displayed", true,LogAs.PASSED);
		}

*/	 
//BP_TC_05_BrowsePagination :This test case verifies specific subsubcategory browse page after applying particular pagination number on BNY.
	 @Test(groups={"Browsepage","Regression"},priority=5,testName="BP_TC_05_BrowsePagination",enabled=true)
	 public void BP_TC_05_BrowsePagination(){
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 sortbyReusable=new Desk_SortBy_Reusable();
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
	     ATUReports.setTestCaseReqCoverage("Browser -This test case verifies specific subsubcategory browse page after applying particular pagination number on BNY.");
		 
	     	driver.get(bnyURL);
	     	//----------------------Category Browse--------------------------
			//globalNavReusable.CategoryMouseHover(driver, categoryName);
			globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
			//------------------Pagination-----------------------------------
			sortbyReusable.selectPageination(driver,2 );
	  }
	 
//BP_TC_06_FiltersSortViewsBNY_96 : This test case verifies specific browse page with 96 view on BNY.
	 @Test(groups={"Browsepage","Regression"},priority=6,testName="BP_TC_06_FiltersSortViewsBNY_96",enabled=true)
	 public void BP_TC_06_FiltersSortViewsBNY_96(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
	     ATUReports.setTestCaseReqCoverage("Browse -This test case verifies specific browse page with 96 view on BNY.");
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 sortbyReusable=new Desk_SortBy_Reusable();	

	  	 driver.get(bnyURL); 	
	  	//----------------------Category Browse--------------------------
		 globalNavReusable.CategoryMouseHover(driver, categoryName);
		 globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
		//----------------------Click on 96 button on browse-----------------------
		 sortbyReusable.selectView(driver, "96");

	 }
	 
//BP_TC_07_SortViewsWHS48_96_4x_6x : This test case verifies specific Subsubcategory browse page after applying pricewise sorting(LOW TO HIGH) for 6x products on WHS.
	 @Test(groups={"Browsepage","Regression"},priority=14,testName="BP_TC_07_SortViewsWHS48_96_4x_6x",enabled=true)
	 public void BP_TC_07_SortViewsWHS48_96_4x_6x(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
	     ATUReports.setTestCaseReqCoverage("Browse -This test case verifies specific Subsubcategory browse page after applying pricewise sorting(LOW TO HIGH) for 6x products on WHS.");
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 sortbyReusable=new Desk_SortBy_Reusable();	
		 
	     driver.get(whsURL);
	   //----------------------Category Browse--------------------------
	     globalNavReusable.CategoryMouseHover(driver, categoryName);
		 globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
		//----------------------sort by price low to high--------------------------
		 sortbyReusable.applySort(driver,"Price Low to High");
		//----------------------Click on 96 button on browse-----------------------
		 sortbyReusable.selectView4x6x(driver, "6x");
	  }
	 


//BP_TC_08_EditMyDesignerFilter : This test case verifies if use is able to edit designer from browse page.
 @Test(groups={"Browsepage","Regression"},priority=8,testName="BP_TC_08_EditMyDesignerFilter",enabled=true) 
 public void BP_TC_08_EditMyDesignerFilter()  {
	 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
	 ATUReports.setTestCaseReqCoverage("Browse - This test case verifies if use is able to edit designer from browse page.");
	 globalNavReusable = new Desk_GlobalNav_Reusable();
	 sortbyReusable=new Desk_SortBy_Reusable();	
	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	bnyDesigner=new Desk_MyDesigner_Reusable();
	logoutReusable=new Desk_Logout_Reusable();
    String designerName="A.L.C.";
	
	 driver.get(bnyURL);
	//----------------------Registerting new user--------------------------
	 loginRegisterReusable.registerFromHeader(driver);	
	//----------------------Category Browse--------------------------
     globalNavReusable.CategoryMouseHover(driver, categoryName);
	 globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
	 ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED); 
	//----------------------Edit link on designer--------------------------
	sortbyReusable.EditMyDesignersFilter(driver);
	//----------adding designer from my designer under my account.----------
	bnyDesigner.searchAndAddDesignerFromMyDesigners(driver, designerName);
	//----------------------Category Browse--------------------------
    globalNavReusable.CategoryMouseHover(driver, categoryName);
	globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
	//-------------------Clicking on my Desinger link on left Nav------------------
	sortbyReusable.FilterByMyDesigners(driver);
}
		 
//BP_TC_09_SortByPriceLowtoHigh96ViewsBNY: This test case verify the sort by Price Low to High  with 96 view.
	 @Test(groups={"Browsepage","Regression"},priority=13,testName="BP_TC_09_SortByPriceLowtoHigh96ViewsBNY",enabled=true)
	 public void BP_TC_09_SortByPriceLowtoHigh96ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Browse - This test case verify the sort by Price Low to High  with 96 view.");
		 sortbyReusable=new Desk_SortBy_Reusable();	
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 	 
		 driver.get(bnyURL);
		//----------------------Category Browse--------------------------
		globalNavReusable.CategoryMouseHover(driver, categoryName);
		globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
		 //------------Sort by price low to high------------------------
		 sortbyReusable.applySort(driver,"Price Low to High");
		//----------------------Click on 96 button on browse-----------------------
		 sortbyReusable.selectView(driver, "96");
	  }
		 
//BP_TC_10_SortByPriceHighToLow96ViewsBNY: This test case verify the sort by Price High To Low with 96 view. 
	 @Test(groups={"Browsepage","Regression"},priority=14,testName="BP_TC_10_SortByPriceHighToLow96ViewsBNY",enabled=true)
	 public void BP_TC_10_SortByPriceHighToLow96ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Browse - This test case verify the sort by Price High To Low  with 96 view.");
		 sortbyReusable=new Desk_SortBy_Reusable();	
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 
		 driver.get(bnyURL);
		//----------------------Category Browse--------------------------
		 globalNavReusable.CategoryMouseHover(driver, categoryName);
		 globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
		//------------Sort by price low to high------------------------
		 sortbyReusable.applySort(driver,"Price High to Low");
		//----------------------Click on 96 button on browse-----------------------
		 sortbyReusable.selectView(driver, "96");		 
	 }
 
/*	 
	@Test(groups="CategoryFunc",priority=5)
	public void BNYFilter()  {
		globalNavReusable = new Desk_GlobalNav_Reusable();
		ATUReports.setTestCaseReqCoverage("CategoryBrowse - Filter By Size");
		genericMethods =  new GenericMethods();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		sortbyReusable=new Desk_SortBy_Reusable();		
		bnyDesigner=new Desk_MyDesigner_Reusable();
		
		loginRegisterReusable.registerFromHeader(driver);
		 globalNavReusable.CategoryMouseHover(driver, categoryName);
		 globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);	
		globalNavReusable.CategoryMouseHover(driver, "womens");
		globalNavReusable.clickSubCategoryName(driver, "womens", "All Designers (A-Z)");
		ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
		sortbyReusable.AddDesignersfromAllDesignerDLP(driver);
		globalNavReusable.CategoryMouseHover(driver, "womens");
		globalNavReusable.clickSubCategoryName(driver,"womens", "Dresses");
		sortbyReusable.FilterByMyDesigner(driver);
		sortbyReusable.VerifyMyDesignerFilter(driver);			
	}*/
/*
	@Test(groups="CategoryFunc",priority=6)
	public void NoDesignersInMyDesigners()  {
		globalNavReusable = new Desk_GlobalNav_Reusable();
		ATUReports.setTestCaseReqCoverage("CategoryBrowse - Filter By Size");
		genericMethods =  new GenericMethods();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		bnyDesigner=new Desk_MyDesigner_Reusable();
		sortbyReusable=new Desk_SortBy_Reusable();
		
		loginRegisterReusable.registerFromHeader(driver);
		globalNavReusable.CategoryMouseHover(driver, "womens");
		globalNavReusable.clickSubCategoryName(driver, "womens", "Dresses");
		sortbyReusable.FilterByMyDesignerNoDesignerSaved(driver);	
	}
*/
	
	
	
	 


 

	
	 //========================*********Test Case 1 starts*********===============================
	 /*
	 @Test(groups={"CategoryFunc1","Regression"},priority=1,testName="SR_TC_07_SortByZtoA48ViewsBNY",enabled=true)		
	 	public void BNYSortBy()  {
	 		globalNavReusable = new Desk_GlobalNav_Reusable();
	 		//ATUReports.setTestCaseReqCoverage("CategoryBrowse -Sorting By Price");
	 		sortbyReusable=new Desk_SortBy_Reusable();
	 		
	 		driver.get(bnyURL);
	 		//----------------------Category Browse--------------------------
	 		globalNavReusable.CategoryMouseHover(driver, categoryName);
	 		globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
	 		ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);	
	 		sortbyReusable.SortByHighToLow(driver,"Price Low to High");
	 		sortbyReusable.PriceHighToLow(driver,"Price Low To High");
	 		//sortbyReusable.SortByLowToHigh(Driver);
	 		//sortbyReusable.PriceLowToHigh(Driver);	
	 	}
	 */
	 //========================*********Test Case 1 starts*********===============================
	 	/*@Test(groups="CategoryFunc1",priority=2)
	 	public void BNYFilterByDesigner()  {
	 		globalNavReusable = new Desk_GlobalNav_Reusable();
	 		ATUReports.setTestCaseReqCoverage("CategoryBrowse -Filter By Designer");
	 		genericMethods =  new GenericMethods();
	 		sortbyReusable=new Desk_SortBy_Reusable();
	 		
	 		//----------------------Category Browse--------------------------
	 		globalNavReusable.CategoryMouseHover(driver, categoryName);
	 		globalNavReusable.clickSubCategoryName(driver, categoryName, subCategoryName);
	 		ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	 		sortbyReusable.FilterByDesigner(driver);
	 		sortbyReusable.DesignerFilter(driver);	
	 	}*/
	 
	@AfterMethod(groups = "After_Method")
	public void Stop(){		
		driver.quit();
		GenericMethods.Waitformilliseconds(2000);
	}
}
