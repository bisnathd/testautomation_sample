package testCases.Functional.desktop;
/*
 SR_TC_01_VerificationCategoryLandingPage:This test case verifies specific category landing page on BNY.
 SR_TC_02_VerificationDLP :This test case verifies specific Designer landing page(DLP) on BNY.
 SR_TC_03_VerificationSearchText: This test case verifies specific Category Browse page on BNY.
 SR_TC_04_VerificationZEROResults :This test case verifies(search)Zero search result page for random words on BNY.
 SE_TC_05_SearchFromZeroResultpage : This test verify if user is able to search from zero search result pa
 ge.
 SR_TC_06_SortByAtoZ48ViewsBNY: This test case verify the sort by "Name (A-Z)" with 48 view.
SR_TC_07_SortByPriceHighToLow48ViewsBNY: This test case verify the sort by Price High To Low with 48 view. 
SR_TC_08_SortByNewArrivals48ViewsBNY: This test case verify the sort by New Arrivals with 48 view. 
SR_TC_09_SortByAtoZ96ViewsBNY: This test case verify the sort by "Name (A-Z)" with 96 view. 
SR_TC_10_SortByPriceLowtoHigh96ViewsBNY: This test case verify the sort by Price Low to High  with 96 view.
SR_TC_11_SortByNewArrivals96ViewsBNY: This test case verify the sort by New Arrivals with 96 view. 
SR_TC_12_SortByAtoZALLViewsBNY: This test case verify the sort by "Name (A-Z)" with ALL view.
SR_TC_13_SearchResultApplyDesignerFilter: This test case verifies Subcategory products after applying DESIGNERWISE filter from left nav on BNY..
SR_TC_14_SearchResultForPagination: This test case search specific Subcategory products after selecting particular pagination number on BNY.
SE_TC_15_VerifySpecificTermSearchResult : This test case verify if the search results page is displayed for specific term search. 
SR_TC_16_FiltersSortViewsWHS48_96_4x_6x :This test case filter and sort the Subsubcategory products pricewise(Low to High)in 96 view for 6x products on WHS.
  */

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_SearchResultsAndFilters_Reusable;
import setup.MultipleBrowser;
import actions.GenericMethods;
import actions.ReturnDataObject;
import atu.testng.reports.ATUReports;

public class Desk_SearchTestCases extends MultipleBrowser{

	GenericMethods genericMethods;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	Desk_SearchResultsAndFilters_Reusable searchResultsAndFiltersReusable;
	String bnyURL;
	String whsURL;
	String categoryName = "Women";
	String subCategoryName = "Platforms";
	String correctSearchTerm = "jeans";
	String wrongSearchTerm = "hcl";
	
	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		//driver.get(URL);
		bnyURL= bny_URL;
		whsURL= whs_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}
	 
	
	 //----------------------Search page test case -------------------
	 
	//SR_TC_01_VerificationCategoryLandingPage :This test case verifies specific category landing page on BNY.
	@Test(groups={"search","Regression"}, priority=1,testName="SR_TC_01_VerificationCategoryLandingPage",enabled=true)
	 public void SR_TC_01_VerificationCategoryLandingPage(){	
		 String SearchText ="women";
		 ATUReports.setAuthorInfo("Adapty QA Team", "20th Aug 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search -This test case verifies specific category Landing page on BNY.");
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 
		 driver.get(bnyURL);		 
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver,"CATEGORY",SearchText);		 
	  }
	 
	//SR_TC_02_VerificationDLP:This test case verifies specific Designer landing page(DLP) on BNY.
	 @Test(groups={"search","Regression"}, priority=2,testName="SR_TC_02_VerificationDLP",enabled=true)
	 public void SR_TC_02_VerificationDLP(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verifies specific Designer landing page(DLP) on BNY.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "DLP","VALENTINO");		 
	  }
	 
	 //SR_TC_03_VerificationSearchText: This test case verifies specific Category Browse page on BNY.
	 @Test(groups={"search","Regression"},priority=3,testName="SR_TC_03_VerificationSearchText",enabled=true)
	 public void SR_TC_03_VerificationSearchText(){
		 ATUReports.setAuthorInfo("Adapty QA  eam", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search -This test case verifies specific Category Browse page on BNY. ");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","DRESSES");		 
	  }
	 

	 //SR_TC_04_VerificationZEROResults :This test case verifies(search)Zero search result page for random words on BNY.
	 @Test(groups={"search","Regression"},priority=4,testName="SR_TC_04_VerificationZEROResults",enabled=true)
	 public void SR_TC_04_VerificationZEROResults(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search -This test case verifies(search)Zero search result page for random words on BNY.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.zeroSearchResultPage(driver,"fjdjfjkdjfkdjfkdj");		 
	  }
	 
	
	//SE_TC_05_SearchFromZeroResultpage : This test verify if user is able to search from zero search result page.
	@Test(groups={"search","Regression"},priority=5,testName="SE_TC_05_SearchFromZeroResultpage",enabled=true) 
	public void SE_TC_05_SearchFromZeroResultpage() {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Search - This test verify if user is able to search from zero search result page.");
		driver.get(bnyURL);
		searchResultsAndFiltersReusable = new Desk_SearchResultsAndFilters_Reusable();		
		searchResultsAndFiltersReusable.ZeroSearchResultpage(driver,"fjdjfjkdjfkdjfkdj");
		searchResultsAndFiltersReusable.searchFromZeroSearchResultpage(driver, "Dresses");
	}
	/* 
	 //SR_TC_06_SortByAtoZ48ViewsBNY: This test case verify the sort by "Name (A-Z)" with 48 view. 
	 @Test(groups={"search","Regression"},priority=6,testName="SR_TC_06_SortByAtoZ48ViewsBNY",enabled=true)
	 public void SR_TC_06_SortByAtoZ48ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Name (A-Z) with 48 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"ALPHABATICALLY (A-Z)");
		 searchResultsAndFiltersReusable.selectView(driver, "48");
	  }

	 //SR_TC_07_SortByZtoA48ViewsBNY: This test case verify the sort by "Name (Z-A)" with 48 view. 
	 @Test(groups={"search","Regression"},priority=7,testName="SR_TC_07_SortByZtoA48ViewsBNY",enabled=true)
	 public void SR_TC_07_SortByZtoA48ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Name (Z-A) with 48 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Alphabetically (Z-A)");
		 searchResultsAndFiltersReusable.selectView(driver, "48");
	  }
	*//* 
	 //SR_TC_08_SortByPriceLowtoHigh48ViewsBNY: This test case verify the sort by Price Low to High  with 48 view.
	 @Test(groups={"search","Regression"},priority=8,testName="SR_TC_08_SortByPriceLowtoHigh48ViewsBNY",enabled=true)
	 public void SR_TC_08_SortByPriceLowtoHigh48ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Price Low to High  with 48 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Price Low to High");
		 searchResultsAndFiltersReusable.selectView(driver, "48");
	  }

	 //SR_TC_07_SortByPriceHighToLow48ViewsBNY: This test case verify the sort by Price High To Low with 48 view. 
	 @Test(groups={"search","Regression"},priority=7,testName="SR_TC_07_SortByPriceHighToLow48ViewsBNY",enabled=true)
	 public void SR_TC_07_SortByPriceHighToLow48ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Price High To Low  with 48 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Price High to Low");
		 searchResultsAndFiltersReusable.selectView(driver, "48");
		 
	  }	*/ 
	 
	 //SR_TC_06_SortByNewArrivals48ViewsBNY: This test case verify the sort by New Arrivals with 48 view. 
	 @Test(groups={"search","Regression"},priority=6,testName="SR_TC_06_SortByNewArrivals48ViewsBNY",enabled=true)
	 public void SR_TC_06_SortByNewArrivals48ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by New Arrivals  with 48 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"New Arrivals");
		 searchResultsAndFiltersReusable.selectView(driver, "48");
	  }
		 
	 //SR_TC_07_SortByAtoZ96ViewsBNY: This test case verify the sort by "Name (A-Z)" with 96 view. 
	 @Test(groups={"search","Regression","BlackBox"},priority=7,testName="SR_TC_07_SortByAtoZ96ViewsBNY",enabled=true)
	 public void SR_TC_07_SortByAtoZ96ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Name (A-Z) with 96 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Alphabetically (A-Z)");
		 searchResultsAndFiltersReusable.selectView(driver, "96");
	  }
/*	 
	 //SR_TC_12_SortByZtoA96ViewsBNY: This test case verify the sort by "Name (Z-A)" with 96 view. 
	 @Test(groups={"search","Regression"},priority=12,testName="SR_TC_12_SortByZtoA96ViewsBNY",enabled=true)
	 public void SR_TC_12_SortByZtoA96ViewsBNY(){ 
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Name (Z-A) with 96 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Alphabetically (Z-A)");
		 searchResultsAndFiltersReusable.selectView(driver, "96");
	  }
*/	 
	 //SR_TC_08_SortByPriceLowtoHigh96ViewsBNY: This test case verify the sort by Price Low to High  with 96 view.
	 @Test(groups={"search","Regression"},priority=8,testName="SR_TC_08_SortByPriceLowtoHigh96ViewsBNY",enabled=true)
	 public void SR_TC_08_SortByPriceLowtoHigh96ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Price Low to High  with 96 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Price Low to High");
		 searchResultsAndFiltersReusable.selectView(driver, "96");
	  }
/*	 
	 //SR_TC_14_SortByPriceHighToLow96ViewsBNY: This test case verify the sort by Price High To Low with 96 view. 
	 @Test(groups={"search","Regression"},priority=14,testName="SR_TC_14_SortByPriceHighToLow96ViewsBNY",enabled=true)
	 public void SR_TC_14_SortByPriceHighToLow96ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Price High To Low  with 96 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Price High to Low");
		 searchResultsAndFiltersReusable.selectView(driver, "96");
	  }
	 
*/	 //SR_TC_09_SortByNewArrivals96ViewsBNY: This test case verify the sort by New Arrivals with 96 view. 
	 @Test(groups={"search","Regression"},priority=9,testName="SR_TC_09_SortByNewArrivals96ViewsBNY",enabled=true)
	 public void SR_TC_09_SortByNewArrivals96ViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by New Arrivals  with 96 view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"New Arrivals");
		 searchResultsAndFiltersReusable.selectView(driver, "96");
	  }
	 	 
	 //SR_TC_10_SortByAtoZALLViewsBNY: This test case verify the sort by "Name (A-Z)" with ALL view. 
	 @Test(groups={"search1","Regression"},priority=10,testName="SR_TC_10_SortByAtoZALLViewsBNY",enabled=true)
	 public void SR_TC_10_SortByAtoZALLViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Name (A-Z) with ALL view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Alphabetically (A-Z)");
		 searchResultsAndFiltersReusable.selectView(driver, "ALL");
	  }
	 
/*	 //SR_TC_17_SortByZtoAALLViewsBNY: This test case verify the sort by "Name (Z-A)" with ALL view. 
	 @Test(groups={"search","Regression"},priority=17,testName="SR_TC_17_SortByZtoAALLViewsBNY",enabled=true)
	 public void SR_TC_17_SortByZtoAALLViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Name (Z-A) with ALL view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Alphabetically (Z-A)");
		 searchResultsAndFiltersReusable.selectView(driver, "ALL");
	  }
*//*	 
	 //SR_TC_18_SortByPriceLowtoHighALLViewsBNY: This test case verify the sort by Price Low to High  with ALL view.
	 @Test(groups={"search","Regression"},priority=18,testName="SR_TC_18_SortByPriceLowtoHighALLViewsBNY",enabled=true)
	 public void SR_TC_18_SortByPriceLowtoHighALLViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Price Low to High  with ALL view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Price Low to High");
		 searchResultsAndFiltersReusable.selectView(driver, "ALL");
	  }
	 
	 //SR_TC_19_SortByPriceHighToLowALLViewsBNY: This test case verify the sort by Price High To Low with ALL view. 
	 @Test(groups={"search","Regression"},priority=19,testName="SR_TC_19_SortByPriceHighToLowALLViewsBNY",enabled=true)
	 public void SR_TC_19_SortByPriceHighToLowALLViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by Price High To Low  with ALL view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Price High to Low");
		 searchResultsAndFiltersReusable.selectView(driver, "ALL");
	  }

	 
	 //SR_TC_20_SortByNewArrivalsALLViewsBNY: This test case verify the sort by New Arrivals with ALL view. 
	 @Test(groups={"search","Regression"},priority=20,testName="SR_TC_20_SortByNewArrivalsALLViewsBNY",enabled=true)
	 public void SR_TC_20_SortByNewArrivalsALLViewsBNY(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verify the sort by New Arrivals  with ALL view.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"New Arrivals");
		 searchResultsAndFiltersReusable.selectView(driver, "ALL");
	  }
*/
	 
	 //SR_TC_11_SearchResultApplyDesignerFilter: This test case verifies Subcategory products after applying DESIGNERWISE filter from left nav on BNY..
	 @Test(groups={"search","Regression","BlackBox"},priority=11,testName="SR_TC_11_SearchResultApplyDesignerFilter",enabled=true)
	 public void SR_TC_11_SearchResultApplyDesignerFilter(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search -This test case verifies Subcategory products after applying DESIGNERWISE filter from left nav on BNY.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","clothing");
		 searchResultsAndFiltersReusable.selectFilters(driver, "designer");		 
	  }
	 
/*	 //SR_TC_22_SearchResultApplyColorFilter: This test case verifies Subcategory products after applying color filter from left nav on BNY.
	 @Test(groups={"search","Regression"},priority=22,testName="SR_TC_22_SearchResultApplyColorFilter",enabled=true)
	 public void SR_TC_22_SearchResultApplyColorFilter(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verifies Subcategory products after applying color filter from left nav on BNY.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.selectFilters(driver, "color");	
	 }
	 */
/*	 
 * This test case has been removed, becuase left nav filter is configured throug expirence manager.****
 * //SR_TC_23_SearchResultApplySizeFilter: This test case verifies Subcategory products after applying size fifilter from left nav on BNY.
	 @Test(groups={"search","Regression"},priority=23,testName="SR_TC_23_SearchResultApplySizeFilter",enabled=true)
	 public void SR_TC_23_SearchResultApplySizeFilter(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verifies Subcategory products after applying size filter from left nav on BNY.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.selectFilters(driver, "size");	
	 }
	 
	 //SR_TC_24_SearchResultApplyMaterialFilter: This test case verifies Subcategory products after applying material filter from left nav on BNY.
	 @Test(groups={"search","Regression"},priority=24,testName="SR_TC_24_SearchResultApplyMaterialFilter",enabled=true)
	 public void SR_TC_24_SearchResultApplyMaterialFilter(){ 
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verifies Subcategory products after applying material filter from left nav on BNY.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.selectFilters(driver,"material");	
	 }
	 
*/
	 
/*	 //SR_TC_14_SearchResultApplyNewArrivalFilter: This test case verifies Subcategory products after applying New Arrival filter from left nav on BNY.
	 @Test(groups={"search","Regression"},priority=14,testName="SR_TC_14_SearchResultApplyNewArrivalFilter",enabled=true)
	 public void SR_TC_14_SearchResultApplyNewArrivalFilter(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search - This test case verifies Subcategory products after applying New Arrival filter from left nav on BNY.");
		 driver.get(bnyURL);
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.selectFilters(driver,"NewArrival");	
	 }*/
	 
	 //SR_TC_12_SearchResultForPagination: This test case search specific Subcategory products after selecting particular pagination number on BNY.
	 @Test(groups={"search","Regression","BlackBox"},priority=12,testName="SR_TC_12_SearchResultForPagination",enabled=true)

	 public void SR_TC_12_SearchResultForPagination(){
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
	     ATUReports.setTestCaseReqCoverage("Search -This test case search specific Subcategory products after selecting particular pagination number on BNY");
	     driver.get(bnyURL);
	     searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable();
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchBNY(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.selectPageination(driver, 2);	
	 }
	
	//SE_TC_13_VerifySpecificTermSearchResult : This test case verify if the search results page is displayed for specific term search. 
	@Test(groups={"search","Regression"},priority=13,testName="SE_TC_13_VerifySpecificTermSearchResult",enabled=true) 
	public void SE_TC_13_VerifySpecificTermSearchResult() {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Search- This test verify if the search results page is displayed for specific term search.");
		driver.get(bnyURL);	
		searchResultsAndFiltersReusable = new Desk_SearchResultsAndFilters_Reusable();
		searchResultsAndFiltersReusable.specificTerm(driver);
	
	}
	
	
	 //SR_TC_14_FiltersSortViewsWHS48_96_4x_6x :This test case filter and sort the Subsubcategory products pricewise(Low to High)in 96 view for 6x products on WHS.
	 @Test(groups={"search","Regression"},priority=14,testName="SR_TC_14_FiltersSortViewsWHS48_96_4x_6x",enabled=true)
	public void SR_TC_14_FiltersSortViewsWHS48_96_4x_6x(){
		 driver.get(whsURL);
		 ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		 ATUReports.setTestCaseReqCoverage("Search -This test case filter and sort the Subsubcategory products pricewise(Low to High)in 96 view for 6x products on WHS ");
		 searchResultsAndFiltersReusable=new Desk_SearchResultsAndFilters_Reusable(
				 );
		 searchResultsAndFiltersReusable.enterSearchTextandStartSearchWHS(driver, "TEXT","dresses");
		 searchResultsAndFiltersReusable.applySort(driver,"Price Low to High");
		 //searchResultsAndFiltersReusable.selectView(driver, "96");
		 searchResultsAndFiltersReusable.selectView4x6x(driver, "6x");
	  }
	 
		
	
		 
 
	 
	 @AfterMethod(groups = "After_Method")
	 public void Stop(){  
		 GenericMethods.Waitformilliseconds(2000);
		 driver.quit();
	  
	 }
}
