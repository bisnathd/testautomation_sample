package testCases.Functional.desktop;

/*
 
 GN_TC_01_verifyCategoryClickTC : This test case verifies if GlobalNav is present and if yes then specific landing page is displayed
 GN_TC_02_verifySubCategoryClickTC : This test case verifies after clicking on SubCategory specific browse page is displayed
 */

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import actions.GenericMethods;
import atu.testng.reports.ATUReports;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_HomePage_Reusable;
import setup.MultipleBrowser;
import reusableModules.desktop.Desk_Logout_Reusable;

public class Desk_GlobalNavTestCase extends MultipleBrowser {

	GenericMethods genericMethods;
	WebDriverWait xplicitWait;
	Desk_HomePage_Reusable homePage;
	Desk_Logout_Reusable LogOut;
	Desk_GlobalNav_Reusable globalNav;
	String categoryName = "Women";
	String subCategoryName = "Shoes";
	String bnyURL;
	String whsURL;
	
	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
		super.setuptest(Device,Browser);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		//driver.get(URL);
		bnyURL= bny_URL;
		whsURL= whs_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
		}

	
	//GN_TC_01_verifyCategoryClickTC : This test case verifies if GlobalNav is present and if yes then specific landing page is displayed
	@Test(groups={"GlobalNav","Regression"},priority=1,testName="GN_TC_01_verifyCategoryClickTC",enabled=true)
	public void GN_TC_01_verifyCategoryClickTC(){
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Global Nav - This test case verifies if GlobalNav is present and if yes then specific landing page is displayed");
		driver.get(bnyURL);
		globalNav = new Desk_GlobalNav_Reusable();
		LogOut= new Desk_Logout_Reusable();
		LogOut.logOutifLoggedIn(driver);
		globalNav.categoryClick(driver, categoryName);
	}
	
	//GN_TC_02_verifySubCategoryClickTC : This test case verifies after clicking on SubCategory specific browse page is displayed
	@Test(groups={"GlobalNav","Regression"},priority=2,testName="GN_TC_02_verifySubCategoryClickTC",enabled=true) 
	public void GN_TC_02_verifySubCategoryClickTC() {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Global Nav - This test case verifies after clicking on SubCategory specific browse page is displayed");
		driver.get(bnyURL);
		globalNav = new Desk_GlobalNav_Reusable();
		LogOut= new Desk_Logout_Reusable();
		LogOut.logOutifLoggedIn(driver);
		globalNav.clickSubCategoryName(driver, categoryName, subCategoryName);
	}
	
	@AfterMethod(groups = "After_Method")
	public void Stop(){		
		driver.quit();
		GenericMethods.Waitformilliseconds(2000);
	}
}
