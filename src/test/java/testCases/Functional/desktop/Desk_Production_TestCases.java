package testCases.Functional.desktop;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import actions.piechart.CaptureScreenShot;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import reusableModules.desktop.Desk_HomePage_Reusable;
import reusableModules.desktop.ProductionReusable;
import setup.MultipleBrowser;

@Test
public class Desk_Production_TestCases extends MultipleBrowser {

 GenericMethods genericMethods;
 Desk_BNY_RegistrationPageObject Registtration_PageObjects;
 Desk_HomePage_Reusable homePage_Reusable;
 ProductionReusable loginRegisterReusable;
 ProductionReusable myBagCheckout_Reusable;
 ProductionReusable myAccountLanding_Reusable;
 ProductionReusable checkoutshipping_Reusable;
 ProductionReusable categoryProductClick_Reusable;
 ProductionReusable pdp_Reusable;
 ProductionReusable globalNav_Reusable;
 ProductionReusable checkoutBilling_Reuable;
 ProductionReusable orderReview_Reuable;
 String login, bnyURL, whsURL;
 String currentPageTitle;
 CaptureScreenShot capturescreenshot;



 @BeforeMethod(groups = "Before_Method")
 @Parameters({
  "DEVICE",
  "BROWSER",
  "BNYURL",
  "WHSURL"
 })
 public void Start(@Optional String Device, String Browser, String bny_URL, String whs_URL) {
  super.setuptest(Device, Browser);
  driver.manage().window().maximize();
  driver.manage().deleteAllCookies();
  //driver.get(URL);
  bnyURL = bny_URL;
  whsURL = whs_URL;
  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
 }

 //ProductionReusable BNY testing.
 @Test(groups = "Production", dataProvider = "ShippingFormData", priority = 1, testName = "CheckShip_TC_01_addinShippingAndVerifyAddressInMyAccount", enabled = true)
 public void LoggedInUser_ProductionBNY(String email, String oldPassword, String country, String FName, String LName, String ShippingAddress1, String ShippingAddress2, String ShippingCity, String ShippingState, String ShippingZip, String ShippingPhone, String CardType, String CardNo, String CardCVV, String BillingAddress1, String BillingCity, String BillingState, String BillingZip, String Promocode, String BarneysUrl1, String WareHouseUrl2) {
  
  loginRegisterReusable = new ProductionReusable();
  globalNav_Reusable = new ProductionReusable();
  categoryProductClick_Reusable = new ProductionReusable();
  pdp_Reusable = new ProductionReusable();
  myBagCheckout_Reusable = new ProductionReusable();
  checkoutshipping_Reusable = new ProductionReusable();
  myAccountLanding_Reusable = new ProductionReusable();
  checkoutBilling_Reuable = new ProductionReusable();
  orderReview_Reuable = new ProductionReusable();
  genericMethods = new GenericMethods();
  capturescreenshot=new CaptureScreenShot();
  
  driver.get(bnyURL);
  //------------------------Login-----------------------------------
  
   loginRegisterReusable.loginFromHeader(driver, "bisnathdubey@gmail.com", "123456789");
 //capturescreenshot.captureScreenshot(driver);
  GenericMethods.waitForPageLoaded(driver);

  //-------------------Empty My Bag module ---------------		
  currentPageTitle = genericMethods.getTitle(driver);
  if (currentPageTitle.equalsIgnoreCase("Barneys Warehouse")) {
   myBagCheckout_Reusable.emptyMyBag(driver, "WHS");
  } else {
   myBagCheckout_Reusable.emptyMyBag(driver, "BNY");
  }
  GenericMethods.Waitformilliseconds(5000);
  //----------------------Category Browse--------------------------
  //globalNavReusable.CategoryMouseHover(driver, "Women");
  globalNav_Reusable.clickSubCategoryName(driver, "Women", "Clothing");

  ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true, LogAs.PASSED);
  categoryProductClick_Reusable.browseProductandClickonBNY(driver, "500");
  GenericMethods.waitForPageLoaded(driver);

  //----------------------------PDP---------------------------------			
  pdp_Reusable.selectAvailableColorOnPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.selectAvailableSizeOnPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.AddtoCartButtononPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.miniBagPopuponPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
  GenericMethods.waitForPageLoaded(driver);

  //------------------My Bag(Logged In User)---------------------------------
  myBagCheckout_Reusable.selectCountry(driver, country);
  GenericMethods.waitForPageLoaded(driver);
  myBagCheckout_Reusable.clickCheckoutButton(driver);
  GenericMethods.waitForPageLoaded(driver);

  //------------------Logged In User CLick on Continue button on Shipping----
  checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
  GenericMethods.waitForPageLoaded(driver);
  //------------Logged in User enters CVV # on billing page-------------------

  checkoutBilling_Reuable.enterCVVForLoggedInUser(driver, CardCVV);
  checkoutBilling_Reuable.clickbillingPage_ContinueButton(driver);
  GenericMethods.waitForPageLoaded(driver);
  

  //-----------Order Review page-------------------------------------
  ATUReports.add("Verify the Order Review page", "Order review page is displayed", "Order Review page", true, LogAs.PASSED);
  orderReview_Reuable.VerifyBillingPage(driver);

 }

 //ProductionReusable WHS testing.
 @Test(groups = "Production", dataProvider = "ShippingFormData", priority = 2, testName = "CheckShip_TC_01_addinShippingAndVerifyAddressInMyAccount", enabled = true)
 public void LoggedInUser_ProductionWHS(String email, String oldPassword, String country, String FName, String LName, String ShippingAddress1, String ShippingAddress2, String ShippingCity, String ShippingState, String ShippingZip, String ShippingPhone, String CardType, String CardNo, String CardCVV, String BillingAddress1, String BillingCity, String BillingState, String BillingZip, String Promocode, String BarneysUrl1, String WareHouseUrl2) {

  loginRegisterReusable = new ProductionReusable();
  globalNav_Reusable = new ProductionReusable();
  categoryProductClick_Reusable = new ProductionReusable();
  pdp_Reusable = new ProductionReusable();
  myBagCheckout_Reusable = new ProductionReusable();
  checkoutshipping_Reusable = new ProductionReusable();
  myAccountLanding_Reusable = new ProductionReusable();
  checkoutBilling_Reuable = new ProductionReusable();
  orderReview_Reuable = new ProductionReusable();
  genericMethods = new GenericMethods();

  driver.get(whsURL);
  //------------------------Login-----------------------------------
  loginRegisterReusable.loginFromHeader(driver, "bisnathdubey@gmail.com", "123456789");
  GenericMethods.waitForPageLoaded(driver);

  //-------------------Empty My Bag module ---------------		
  currentPageTitle = genericMethods.getTitle(driver);
  if (currentPageTitle.equalsIgnoreCase("Barneys Warehouse")) {
   myBagCheckout_Reusable.emptyMyBag(driver, "WHS");
  } else {
   myBagCheckout_Reusable.emptyMyBag(driver, "BNY");
  }
  GenericMethods.Waitformilliseconds(5000);
  //----------------------Category Browse--------------------------
  //globalNavReusable.CategoryMouseHover(driver, "Women");
  globalNav_Reusable.clickSubCategoryName(driver, "Women", "Clothing");
  
  ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true, LogAs.PASSED);
  categoryProductClick_Reusable.browseProductandClickonWHS(driver, "600");
  GenericMethods.waitForPageLoaded(driver);

  //----------------------------PDP---------------------------------			
  pdp_Reusable.selectAvailableColorOnPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.selectAvailableSizeOnPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.AddtoCartButtononPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.miniBagPopuponPDP(driver);
  GenericMethods.waitForPageLoaded(driver);
  pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
  GenericMethods.waitForPageLoaded(driver);

  //------------------My Bag(Logged In User)---------------------------------
  myBagCheckout_Reusable.selectCountry(driver, country);
  GenericMethods.waitForPageLoaded(driver);
  myBagCheckout_Reusable.clickCheckoutButton(driver);
  GenericMethods.waitForPageLoaded(driver);

  //------------------Logged In User CLick on Continue button on Shipping----
  checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
  GenericMethods.waitForPageLoaded(driver);


  //------------Logged in User enters CVV # on billing page-------------------

  checkoutBilling_Reuable.enterCVVForLoggedInUser(driver, CardCVV);
  checkoutBilling_Reuable.clickbillingPage_ContinueButton(driver);
  GenericMethods.waitForPageLoaded(driver);

  //-----------Order Review page-------------------------------------
  ATUReports.add("Verify the Order Review page", "Order review page is displayed", "Order Review page", true, LogAs.PASSED);
  orderReview_Reuable.VerifyBillingPage(driver);

 }



 @AfterMethod(groups = "After_Method")
 public void Stop() {
  driver.quit();
  GenericMethods.Waitformilliseconds(2000);
 }

 @DataProvider(name = "ShippingFormData")
 public static Object[][] ProvideDataToAboveMeth() throws IOException {
  ReturnDataObject returnData1 = new ReturnDataObject();
  return returnData1.read("src/test/java/testDataFiles/desktop/Shipping_billingPage.xls", "shippinig");
 }


}