
package testCases.Functional.desktop;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import reusableModules.desktop.Desk_CheckoutShipping_Reusable;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_GetOrderNumber_Reusable;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_Logout_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_MyAccountAddress_Reusable;
import reusableModules.desktop.Desk_MyAccountPayment_Reusable;
import reusableModules.desktop.Desk_MyDesigner_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import reusableModules.desktop.Desk_SearchResultsAndFilters_Reusable;
import setup.MultipleBrowser;

public class Desk_DesignerTestCase extends MultipleBrowser{
  	GenericMethods genericMethods;
	Desk_BNY_ShippingPageObject ShippingPage_Objects;
	WebDriverWait xplicitWait ;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	Desk_CheckoutShipping_Reusable checkoutShippingReusable;
	Desk_BNY_NavigationPageObject globalNav_Objects;
	Desk_PDP_Reusable pdpReusable;
	Desk_MYBagCheckout_Reusable CheckOutMyBag;
	Desk_SearchResultsAndFilters_Reusable searchResultsAndFilters;
	Desk_GetOrderNumber_Reusable getOrderNumberReusable;
	Desk_MyAccountAddress_Reusable myAccountReusable;
	Desk_Logout_Reusable logoutReusable;
	Desk_GlobalNav_Reusable globalNavReusable;
	Desk_MyAccountPayment_Reusable myaccountPayment;
	Desk_MyDesigner_Reusable bnyDesigner;
	String Emailid="a1@yopmail.com";
	String Password="12345678";
	String DesignerName;
	String login;
	String bnyURL;
	String whsURL;
	

@BeforeMethod(groups = "Before_Method")
@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
	super.Browser=Browser;
	super.Device=Device;
	bnyURL=bny_URL;
	whsURL=whs_URL;
	super.setuptest(Device,Browser);
	driver.manage().window().maximize();
	driver.get(bny_URL);
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
}

//MyDes_TC_01_myDesignerSearchSelect : This test case verifies Adding a Designer in an account
@Test(groups={"MyAccount","Regression"},dataProvider="MyAccountData",priority=1,testName="MyDes_TC_01_myDesignerSearchSelect",enabled=true)
public void MyDes_TC_01_myDesignerSearchSelect(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email, String Emailid, String Password, String CardHolderName, String Month, String Year)  {
		ATUReports.setTestCaseReqCoverage("MyAccount - This test case verifies Adding a Designer in an account");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		genericMethods = new GenericMethods();
		bnyDesigner=new Desk_MyDesigner_Reusable();
		//------------------------Login Module--------------------------	
		loginRegisterReusable=new Desk_LoginRegister_Reusable();	
		loginRegisterReusable.registerFromHeader(driver);
		//---------------------My Account--------------------------
		DesignerName="Saint Laurent";
		bnyDesigner.DesignerSelect(driver,DesignerName);					
		}

//MyDes_TC_02_myDesignerClearAll : This test case verifies Adding a Designer in an account and then deleting the designer by clicking on 'Clear All Designers' link
@Test(groups={"MyAccount","Regression"},dataProvider="MyAccountData",priority=2,testName="MyDes_TC_02_myDesignerClearAll",enabled=true)
public void MyDes_TC_02_myDesignerClearAll(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email, String EmailID, String Password, String CardHolderName, String Month, String Year){
		ATUReports.setTestCaseReqCoverage("MyAccount -Adding a Designer in an account and then deleting the designer by clicking on 'Clear All Designers' link");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		genericMethods =  new GenericMethods();
		bnyDesigner=new Desk_MyDesigner_Reusable();	
		loginRegisterReusable=new Desk_LoginRegister_Reusable();		
		//------------------------Login Module--------------------------	
		loginRegisterReusable.registerFromHeader(driver);
		//---------------------My Account--------------------------
		DesignerName="Saint Laurent";
		bnyDesigner.DesignerSelect(driver,DesignerName);
		bnyDesigner.DesignerDelete(driver);
	}

//MyDes_TC_03_designerSelectCheckBoxFromMyAccountMyDesigner : This test case verifies Adding a Designer in an account using Checkbox		
@Test(groups={"MyAccount","Regression"},priority=3,testName="MyDes_TC_03_designerSelectCheckBoxFromMyAccountMyDesigner",enabled=true)
	public void MyDes_TC_03_designerSelectCheckBoxFromMyAccountMyDesigner()  {
		ATUReports.setTestCaseReqCoverage("MyAccount -Adding a Designer in an account using Checkbox ");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		genericMethods =  new GenericMethods();
		bnyDesigner=new Desk_MyDesigner_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();		
		//---------------------My Account--------------------------
		loginRegisterReusable.registerFromHeader(driver);
		bnyDesigner.DesignerSelectCheckBox(driver);
	}

//MyDes_TC_04_addDeleteDesignerusingCheckboxFromMyAccountMyDesigner : This test case verifies adding and deleting the designer in my account using Checkbox
@Test(groups={"MyAccount","Regression"},priority=4,testName="MyDes_TC_04_addDeleteDesignerusingCheckbox",enabled=true)
	public void MyDes_TC_04_addDeleteDesignerusingCheckboxFromMyAccountMyDesigner()  {
		ATUReports.setTestCaseReqCoverage("This test case verifies adding and deleting the designer in my account using Checkbox");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		genericMethods =  new GenericMethods();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();	
		bnyDesigner=new Desk_MyDesigner_Reusable();		
		//---------------------My Account--------------------------
		loginRegisterReusable.registerFromHeader(driver);
		bnyDesigner.DesignerSelectCheckBox(driver);
		bnyDesigner.DesignerUnselectCheckBox(driver);
	}
		
//MyDes_TC_05_designerAddUsingDLPTopNav : This test case verifies adding a Designer from DLP page using top navigation.
@Test(groups={"MyAccount","Regression"},priority=5,testName="MyDes_TC_05_designerAddUsingDLPTopNav",enabled=true)
	public void MyDes_TC_05_designerAddUsingDLPTopNav()  {
		ATUReports.setTestCaseReqCoverage("This test case verifies adding a Designer from DLP page using top navigation.");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		genericMethods =  new GenericMethods();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		bnyDesigner=new Desk_MyDesigner_Reusable();		
		//---------------------My Account--------------------------			
		String Category="Women";
		String SubCategory="Saint Laurent";
		loginRegisterReusable.registerFromHeader(driver);
		bnyDesigner.DesignerAddUsingDLPTopNav(driver, Category,SubCategory);
	}				
				
				
//MyDes_TC_06_designerRemoveUsingDLPTopNav : This test case verifies adding and deleting a Designer from DLP page using top navigation
@Test(groups={"MyAccount","Regression"},priority=6,testName="MyDes_TC_06_designerRemoveUsingDLPTopNav",enabled=true)
    public void MyDes_TC_06_designerRemoveUsingDLPTopNav()  {
		ATUReports.setTestCaseReqCoverage("This test case verifies adding and deleting a Designer from DLP page using top navigation");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		genericMethods =  new GenericMethods();
		bnyDesigner=new Desk_MyDesigner_Reusable();
		//---------------------My Account--------------------------
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		
		String Category="Women";
		String SubCategory="Saint Laurent";
		loginRegisterReusable.registerFromHeader(driver);
		bnyDesigner.DesignerAddUsingDLPTopNav(driver,Category,SubCategory);
		bnyDesigner.DesignerRemoveUsingDLPTopNav(driver,Category,SubCategory);
	}
				

//MyDes_TC_07_designerAddfromDLPSearch : This test case verifies adding a Designer from DLP page using search
@Test(groups={"MyAccount","Regression"}, priority=7,testName="MyDes_TC_07_designerAddfromDLPSearch",enabled=true)
	public void MyDes_TC_07_designerAddfromDLPSearch() {
		ATUReports.setTestCaseReqCoverage("This test case verifies adding a Designer from DLP page using search");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		loginRegisterReusable=new Desk_LoginRegister_Reusable();	
		bnyDesigner=new Desk_MyDesigner_Reusable();
		logoutReusable=new Desk_Logout_Reusable();
		searchResultsAndFilters=new Desk_SearchResultsAndFilters_Reusable();
		
		loginRegisterReusable.registerFromHeader(driver);
		String login= loginRegisterReusable.sTime;
		logoutReusable.generallogout(driver);
		searchResultsAndFilters.enterSearchTextandStartSearchBNY(driver, "DLP","Alexander Wang");
		bnyDesigner.GuestDesignerAddUsingDLPSearch(driver, login,"12345678");

	}
				 

//MyDes_TC_08_designerRemovefromDLPSearch : This test case verifies deleting a Designer from DLP page using search.
@Test(groups={"MyAccount","Regression"}, priority=8,testName="MyDes_TC_08_designerRemovefromDLPSearch",enabled=true)
	public void MyDes_TC_08_designerRemovefromDLPSearch() {
	 	ATUReports.setTestCaseReqCoverage("This test case verifies deleting a Designer from DLP page using search");
	 	ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
	 	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	 	bnyDesigner=new Desk_MyDesigner_Reusable();
	 	logoutReusable=new Desk_Logout_Reusable();
	 	searchResultsAndFilters=new Desk_SearchResultsAndFilters_Reusable();
		loginRegisterReusable.registerFromHeader(driver);
		
		login= loginRegisterReusable.sTime;
		logoutReusable.generallogout(driver);
		
		searchResultsAndFilters.enterSearchTextandStartSearchBNY(driver, "DLP","Alexander Wang");
		bnyDesigner.GuestDesignerAddUsingDLPSearch(driver, login,"12345678");
		bnyDesigner.DesignerRemoveUsingDLP(driver);
	}

//MyDes_TC_09_guestDesignerAddfromDLPTopNavAllDesigners : This test case verifies adding a Designer as guest from DLP page using All Designers from TopNavigation
@Test(groups={"MyAccount","Regression"},priority=9,testName="MyDes_TC_09_guestDesignerAddfromDLPTopNavAllDesigners",enabled=true)				
    public void MyDes_TC_09_guestDesignerAddfromDLPTopNavAllDesigners() {
	 	ATUReports.setTestCaseReqCoverage("This test case verifies adding a Designer as guest from DLP page using All Designers from TopNavigation");
	 	ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
	 	loginRegisterReusable=new Desk_LoginRegister_Reusable();	
	 	logoutReusable=new Desk_Logout_Reusable();
		bnyDesigner=new Desk_MyDesigner_Reusable();
		globalNavReusable = new Desk_GlobalNav_Reusable();
		
		loginRegisterReusable.registerFromHeader(driver);
		login= loginRegisterReusable.sTime;		
		logoutReusable.generallogout(driver);

		GenericMethods.Waitformilliseconds(2000);
		globalNavReusable.CategoryMouseHover(driver, "women");
		ATUReports.add("Hover over top navigation category ", "Sub Categories should be displayed on top navigation  ", "Sub Categories is displayed on top navigation", true,LogAs.INFO);
		globalNavReusable.clickSubCategoryName(driver, "Women","All Designers (A-Z)");
		ATUReports.add("Click on 'All Designer' from top navigation ", "'All Designer' should be clicked from top navigation ", "'All Designer' is clicked from top navigation and DLP page is displayed", true,LogAs.INFO);
		ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
		bnyDesigner.GuestAddDesignersfromAllDesignerDLP(driver, login, "12345678");
		
	}
				 
//MyDes_TC_10_guestDesignerRemovefromDLPTopNavAllDesigners : This test case verifies deleting a Designer as guest from DLP page using All Designers from TopNavigation
 @Test(groups={"MyAccount","Regression"}, priority=10,testName="MyDes_TC_10_guestDesignerRemovefromDLPTopNavAllDesigners",enabled=true)
	public void MyDes_TC_10_guestDesignerRemovefromDLPTopNavAllDesigners() {
	 	ATUReports.setTestCaseReqCoverage("This test case verifies deleting a Designer as guest from DLP page using All Designers from TopNavigation");
	 	ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
	 	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	 	bnyDesigner=new Desk_MyDesigner_Reusable();
	 	logoutReusable=new Desk_Logout_Reusable();
	 	globalNavReusable = new Desk_GlobalNav_Reusable();
	 	genericMethods=new GenericMethods();
	 	
		loginRegisterReusable.registerFromHeader(driver);
	 	login= loginRegisterReusable.sTime;
		logoutReusable.generallogout(driver);
		GenericMethods.Waitformilliseconds(2000);	
		globalNavReusable.CategoryMouseHover(driver, "Women");		
		ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
		globalNavReusable.clickSubCategoryName(driver, "Women","All Designers (A-Z)");
		ATUReports.add("Click on 'All Designer' from top navigation ", "'All Designer' should be clicked from top navigation ", "'All Designer' is clicked from top navigation and DLP page is displayed", true,LogAs.INFO);
		ATUReports.add("Hover over top navigation category ", "Sub Categories should be displayed on top navigation  ", "Sub Categories is displayed on top navigation", true,LogAs.INFO);
		bnyDesigner.GuestAddDesignersfromAllDesignerDLP(driver, login, "12345678");
		bnyDesigner.GuestDesignerRemoveUsingDLPAllDesigners(driver);
	}
				 
//MyDes_TC_11_myDesignerRemoveDLP : This test case verifies removing a Designer from MyAccount using DLP
@Test(groups={"MyAccount","Regression"},dataProvider="MyAccountData",priority=11,testName="MyDes_TC_11_myDesignerRemoveDLP",enabled=true)
	public void MyDes_TC_11_myDesignerRemoveDLP(String firstName, String lastName,String address1,String address2,String city,String zipCode,String shippingPhone,String State, String AddressName, String CardType, String CardNo, String cvv, String Email, String EmailID, String Password, String CardHolderName, String Month, String Year)  {
		ATUReports.setTestCaseReqCoverage("This test case verifies removing a Designer from MyAccount using DLP");
		ATUReports.setAuthorInfo("Adapty QA Team", "23rd Sep 2016","1.0");
		genericMethods =  new GenericMethods();
		bnyDesigner=new Desk_MyDesigner_Reusable();
		globalNavReusable = new Desk_GlobalNav_Reusable();
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		
		String Category="Women";
		String SubCategory="Saint Laurent";
		//------------------------Login Module--------------------------				
		loginRegisterReusable.registerFromHeader(driver);
		//driver.navigate().to("https://development-www-barneys.demandware.net/s/BNY/cart");		
		globalNavReusable.CategoryMouseHover(driver, Category);
		globalNavReusable.clickDesignerName(driver, Category, SubCategory);		
		//---------------------My Account--------------------------		
		bnyDesigner.DesignersAddUsingDLPTopNav(driver,SubCategory);
		bnyDesigner.DesignerClickfromMyDesigners(driver);
		bnyDesigner.DesignerRemoveUsingDLP(driver);			

	}														
				 
	@AfterMethod(groups = "After_Method")
	public void Stop(){		
		driver.quit();
		GenericMethods.Waitformilliseconds(2000);
	}
	
	@DataProvider(name = "MyAccountData")
		public static Object[][] ProvideDataToAboveMeth() throws IOException{	   
		   ReturnDataObject returnData1=new ReturnDataObject();
		   return returnData1.read("src/test/java/testDataFiles/desktop/MyAccountTestCase.xls","Sheet1");		
		    }
	
}
