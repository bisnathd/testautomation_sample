package testCases.Functional.desktop;
/*
 1. Cart_TC_01_addProdfromPDPandVerifycartpage : 1)This test case verifies if user is able to add product from PDP. 2)Click on edit link. 3) Click on view product link from pop up.
 2. Cart_TC_02_myBagRemoveprod : This test case verifies if user is able to delete the product from cart.
 3. Cart_TC_03_emptyMyBagverification : This test case verifies empty cart page.
 4. Cart_TC_04_ToupdateQtyonMyBag:This test case verifies if user is able to update quantity of product on cart page 
 */


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import setup.MultipleBrowser;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import actions.GenericMethods;
import actions.ReturnDataObject;
import reusableModules.desktop.Desk_CategoryProductClick_Reusable;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import reusableModules.desktop.Desk_LoginRegister_Reusable;

public class Desk_CartPageTestCase extends MultipleBrowser{
	GenericMethods genericMethods;
	Desk_MYBagCheckout_Reusable myBagCheckout_Reusable;
	Desk_LoginRegister_Reusable login;
	Desk_CategoryProductClick_Reusable categoryProductClick_Reusable;
	Desk_GlobalNav_Reusable globalNav_Reusable;
    Desk_PDP_Reusable pdp_Reusable;
	String urlFromXML;
	String bnyURL;
	String whsURL;
	Desk_PDP_Reusable pdpReusable;
	String categoryName = "Women";
	String subCategoryName = "Clothing";
	
	
	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(String Device, String Browser,String bny_URL, String whs_URL){
		super.Browser=Browser;
		super.Device=Device;
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		bnyURL= bny_URL;
		whsURL=whs_URL;
		urlFromXML=bny_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}
	 
	 //----------------------MyCart page test case -------------------
	

// 1. Add product from PDP and verify the Cart page.
//Cart_TC_01_addProdfromPDPandVerifycartpage : 1)This test case verifies if user is able to add product from PDP. 2)Click on edit link. 3) Click on view product link from pop up.
@Test(groups={"My_Bag1","Regression","BlackBox"},dataProvider="url", priority = 1,testName="Cart_TC_01_addProdfromPDPandVerifycartpage",enabled=true)
 public void Cart_TC_01_addProdfromPDPandVerifycartpage (String BarneysUrl1,String WareHouseUrl2){  
	 ATUReports.setAuthorInfo("Adapty QA Team", "31st May 2016","1.0");
	 ATUReports.setTestCaseReqCoverage(" Add product from PDP and verify the Cart page."); 
	 myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
	 pdpReusable = new Desk_PDP_Reusable();
	 globalNav_Reusable = new Desk_GlobalNav_Reusable();
	 categoryProductClick_Reusable = new Desk_CategoryProductClick_Reusable();

	 driver.get(bnyURL);  // need to changes according to global nav.
	
	 globalNav_Reusable.clickSubCategoryName(driver, categoryName, subCategoryName);		
	 categoryProductClick_Reusable.browseProductandClickonBNY(driver,"500");
	 //myBagCheckout_Reusable.GotoPDP(driver, urlFromXML,BarneysUrl1 ,  WareHouseUrl2);
//Click on add to cart button on PDP.
		pdpReusable.AddtoCartButtononPDP(driver);
		GenericMethods.waitForPageLoaded(driver);
	//Mini Bag POP UP.
		pdpReusable.miniBagPopuponPDP(driver);
		GenericMethods.waitForPageLoaded(driver);
//Mini Bag POP UP click on Checkout button.
		pdpReusable.miniBagPopupclickOnCheckout_BT(driver);	
		GenericMethods.waitForPageLoaded(driver);
		myBagCheckout_Reusable.addProductFromPDPandVerifyCartPage(driver); 
  }
	

// 2. Cart page Test case to remove the product from cart.
//Cart_TC_02_myBagRemoveprod : This test case verifies if user is able to delete the product from cart.
@Test(groups={"My_Bag","Regression","BlackBox"} , dataProvider="url" , priority = 2,testName="Cart_TC_02_myBagRemoveprod",enabled=true)
	 public void Cart_TC_02_myBagRemoveprod(String BarneysUrl1 , String WareHouseUrl2){
		ATUReports.setAuthorInfo("Adapty QA Team", "31st May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Cart page Test case to remove the product from cart.");
		myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
		pdpReusable = new Desk_PDP_Reusable();
		
		myBagCheckout_Reusable.GotoPDP(driver, urlFromXML,BarneysUrl1 ,  WareHouseUrl2);
//Click on add to cart button on PDP.
		pdpReusable.AddtoCartButtononPDP(driver);
		GenericMethods.waitForPageLoaded(driver);
	
//MiniBag POP UP.
		pdpReusable.miniBagPopuponPDP(driver);
		GenericMethods.waitForPageLoaded(driver);
//MiniBag POP UP click on Checkout button.
		pdpReusable.miniBagPopupclickOnCheckout_BT(driver);	
		GenericMethods.waitForPageLoaded(driver);
//Remove product from cart page.
		myBagCheckout_Reusable.removeProductonCartPage(driver);	  
		GenericMethods.waitForPageLoaded(driver);
	 }


//3. Cart page Test case to verifies the element's on empty cart with no products.
//Cart_TC_03_emptyMyBagverification : This test case verifies empty cart page.
@Test(groups={"My_Bag","Regression","BlackBox"} , dataProvider="url" , priority = 3,testName="Cart_TC_03_emptyMyBagverification",enabled=true)
	public void Cart_TC_03_emptyMyBagverification(String BarneysUrl1 , String WareHouseUrl2){
		ATUReports.setAuthorInfo("Adapty QA Team", "31st May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Cart page Test case to verifies the element's on empty cart with no products.");
		myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
		
		
		myBagCheckout_Reusable.GotoPDP(driver, urlFromXML,BarneysUrl1 ,  WareHouseUrl2);
//Click on add to cart button on PDP.
		pdpReusable.AddtoCartButtononPDP(driver);
	
//Mini Bag POP UP.
		pdpReusable.miniBagPopuponPDP(driver);
//Mini Bag POP UP click on Checkout button.
		pdpReusable.miniBagPopupclickOnCheckout_BT(driver);	
//remove the product from cart page.
		myBagCheckout_Reusable.emptyMyBag(driver, "BNY");
 	}


// 4. Cart page test case to update quantity of product on cart page
//Cart_TC_04_ToupdateQtyonMyBag :This test case verifies if user is able to update quantity of product on cart page 
@Test(groups={"My_Bag","Regression","BlackBox"},dataProvider="url",priority = 4,testName="Cart_TC_04_ToupdateQtyonMyBag",enabled=true)
public void Cart_TC_04_ToupdateQtyonMyBag(String BarneysUrl1,String WareHouseUrl2)throws Exception{  
	 ATUReports.setAuthorInfo("Adapty QA Team", "31st May 2016","1.0");
	 ATUReports.setTestCaseReqCoverage(" Add product from PDP and verify the Cart page."); 
	 myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
	 pdpReusable = new Desk_PDP_Reusable();
	 globalNav_Reusable = new Desk_GlobalNav_Reusable();
	 categoryProductClick_Reusable = new Desk_CategoryProductClick_Reusable();
	 genericMethods= new GenericMethods();
	 
	 
	 myBagCheckout_Reusable.GotoPDP(driver, urlFromXML,BarneysUrl1 ,  WareHouseUrl2);
//Click on add to cart button on PDP.
	pdpReusable.AddtoCartButtononPDP(driver);
	
//Mini Bag POP UP.	
	pdpReusable.miniBagPopuponPDP(driver);
//Click on checkout button.
	pdpReusable.miniBagPopupclickOnCheckout_BT(driver);
//----------Update button on cart page.--------------------------
	myBagCheckout_Reusable.UpateQtyOnCartPage(driver, "2");
	 
	}

	 public static Object[][] ProvideDataToAboveMeth1() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","Sheet1");  
	  }
	 
	 @DataProvider(name = "url")
	 public static Object[][] ProvideDataToAboveMeth() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/url.xls","Sheet2");  
	  }
	 
	@AfterMethod(groups = "After_Method")
	 public void Stop(){
	  driver.close();
	  driver.quit();
	 }
	
}