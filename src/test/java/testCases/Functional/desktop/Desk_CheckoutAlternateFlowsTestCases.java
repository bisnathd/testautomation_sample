package testCases.Functional.desktop;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import atu.testng.reports.ATUReports;
import reusableModules.desktop.Desk_CheckoutBilling_Reusable;
//import reusableModules.GlobalNavActions;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_CheckoutAlternateFlows_Reusable;
import reusableModules.desktop.Desk_CheckoutShipping_Reusable;
import reusableModules.desktop.Desk_GetOrderNumber_Reusable;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import setup.MultipleBrowser;

public class Desk_CheckoutAlternateFlowsTestCases extends MultipleBrowser{
  	GenericMethods genericMethods;
	Desk_BNY_ShippingPageObject ShippingPage_Objects;
	WebDriverWait xplicitWait ;
	Desk_LoginRegister_Reusable loginResgisterReusable;
	Desk_CheckoutShipping_Reusable checkoutShippingReusable;
	Desk_BNY_NavigationPageObject globalNav_Objects;
	Desk_GlobalNav_Reusable globalNavActions;
	Desk_PDP_Reusable pdpReusable;
	Desk_MYBagCheckout_Reusable CheckOutMyBag;
	Desk_CheckoutBilling_Reusable checkoutBillingReusable;
	Desk_GetOrderNumber_Reusable getOrderNumber;
	Desk_CheckoutAlternateFlows_Reusable alternateCheckout;
	String URL;

	@BeforeMethod(groups = "Before_Method")
	@Parameters({"Device","BROWSER","URL"})
	public void Start(@Optional String Device, String Browser,String URL){
		super.Browser=Browser;
		super.Device=Device;
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		driver.get(URL);	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
		}
	
	//====================*********Test Case 1 starts(Gift With Purchase)*********======================
	@Test(groups="CheckoutAlternateFlows1",dataProvider="AlternateCheckout_GWP")
	 public void GWPpurchaseProductOnBNYandWHSDevelopment(String userType,String SKU_URL,String country,String firstName, String lastName,String shipAddress1,String shipAddress2,String shipCity,String shipZipCode,String shippingPhone,String cardType, String cardNo, String cardCVV,String billingAddress1,String billingCity,String billingState,String billingZipcode) throws NullPointerException, InterruptedException  {
		driver.get(URL+SKU_URL);
		genericMethods=new GenericMethods();
		genericMethods.isAlertPresent(driver);
		alternateCheckout=new Desk_CheckoutAlternateFlows_Reusable() ;		
		alternateCheckout.giftWithPurchaseCheckoutonBNY(driver, userType ,country,firstName, lastName,shipAddress1,shipAddress2,shipCity,shipZipCode,shippingPhone,cardType,cardNo,cardCVV,billingAddress1,billingCity,billingState,billingZipcode);
  }
	
	//====================*********Test Case 2 starts(Pre Order)*********===============================
	@Test(groups="CheckoutAlternateFlows",dataProvider="AlternateCheckout_PreOrder")
	 public void purchasePreOrderProductOnBNYandWHSDevelopment(String userType,String SKU_URL,String country,String firstName, String lastName,String shipAddress1,String shipAddress2,String shipCity,String shipZipCode,String shippingPhone,String cardType, String cardNo, String cardCVV,String billingAddress1,String billingCity,String billingState,String billingZipcode) throws NullPointerException, InterruptedException  {
		driver.get(URL);
		alternateCheckout=new Desk_CheckoutAlternateFlows_Reusable() ;		
		alternateCheckout.preOrderProductCheckout(driver, userType ,country,firstName, lastName,shipAddress1,shipAddress2,shipCity,shipZipCode,shippingPhone,cardType,cardNo,cardCVV,billingAddress1,billingCity,billingState,billingZipcode);
  }
	
	
	//====================*********Test Case 3 starts(Gift With Purchase)::Production*********======================
		@Test(groups="CheckoutAlternateFlowsPROD",dataProvider="AlternateCheckout_GWP")
		 public void GWPpurchaseProductOnBNYandWHSProduction(String userType,String SKU_URL,String country,String firstName, String lastName,String shipAddress1,String shipAddress2,String shipCity,String shipZipCode,String shippingPhone,String cardType, String cardNo, String cardCVV,String billingAddress1,String billingCity,String billingState,String billingZipcode) throws NullPointerException, InterruptedException  {
			driver.get(URL+SKU_URL);			
			alternateCheckout=new Desk_CheckoutAlternateFlows_Reusable() ;			
			alternateCheckout.giftWithPurchaseCheckoutonBNYPROD(driver, userType ,country,firstName, lastName,shipAddress1,shipAddress2,shipCity,shipZipCode,shippingPhone,cardType,cardNo,cardCVV,billingAddress1,billingCity,billingState,billingZipcode);
	  }
		
		//====================*********Test Case 4 starts(Pre Order):: Production*********===============================
		@Test(groups="CheckoutAlternateFlowsPROD",dataProvider="AlternateCheckout_PreOrder")
		 public void purchasePreOrderProductOnBNYandWHSProduction(String userType,String SKU_URL,String country,String firstName, String lastName,String shipAddress1,String shipAddress2,String shipCity,String shipZipCode,String shippingPhone,String cardType, String cardNo, String cardCVV,String billingAddress1,String billingCity,String billingState,String billingZipcode) throws NullPointerException, InterruptedException  {
			driver.get(URL);			
			alternateCheckout=new Desk_CheckoutAlternateFlows_Reusable() ;
			alternateCheckout.preOrderProductCheckoutPROD(driver, userType ,country,firstName, lastName,shipAddress1,shipAddress2,shipCity,shipZipCode,shippingPhone,cardType,cardNo,cardCVV,billingAddress1,billingCity,billingState,billingZipcode);
	  }
	//===================*********Test Case 5 starts(Restricted Products)*********=======================
	@Test(groups="CheckoutAlternateFlowsDevProd1",dataProvider="AlternateCheckout_Restricted")
	 public void RestrictedProductOnBNYandWHS(String userType,String category,String subCategory,String priceLimit, String country) throws NullPointerException, InterruptedException  {
		driver.get(URL);
		alternateCheckout=new Desk_CheckoutAlternateFlows_Reusable() ;		
		alternateCheckout.restrictedProductCheckout(driver, userType, category,subCategory, priceLimit, country);
 }

	@AfterMethod(groups = "After_Method")
	public void Stop(){
		driver.close();
		driver.quit();
	}
	
	@DataProvider(name = "AlternateCheckout_PreOrder")
		public static Object[][] checkoutDataForPreOrder() throws IOException{	   
		   ReturnDataObject returnData1=new ReturnDataObject();
		   return returnData1.read("src/test/java/testDataFiles/desktop/BNY_WHS_Sniff.xls","PreOrder");		
		    }
	
	@DataProvider(name = "AlternateCheckout_GWP")
	public static Object[][] checkoutDataForGWP() throws IOException{	   
	   ReturnDataObject returnData1=new ReturnDataObject();
	   return returnData1.read("src/test/java/testDataFiles/desktop/BNY_WHS_Sniff.xls","GWP");		
	    }
	
	@DataProvider(name = "AlternateCheckout_Restricted")
	public static Object[][] checkoutDataForRestricted() throws IOException{	   
	   ReturnDataObject returnData1=new ReturnDataObject();
	   return returnData1.read("src/test/java/testDataFiles/desktop/BNY_WHS_Sniff.xls","RESTRICTED");		
	    }
	
}
