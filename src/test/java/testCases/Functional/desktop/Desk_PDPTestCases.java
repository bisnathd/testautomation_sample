package testCases.Functional.desktop;

//PDP_TC_01_verifyPDPSize : This test case verifies that Size of the product is selected
//PDP_TC_02_verifyCheckDefaultAccordianStatus : This test case verifies default accordian status on PDP
//PDP_TC_03_verifyPDPColor : PDP_TC_03_verifyPDPColor : This test case verifies that Color of the product is selected
//PDP_TC_04_verifySocialIconLinks : This test case verifies the Social Icon links on PDP
//PDP_TC_05_seeAllandCOllapseAll : This test case verifies see all collapse all link on PDP
//PDP_TC_06_verifyFeaturingAndRecommended : This test case verifies featuring and recommended section PDP
//PDP_TC_07_AddToFavoritesGuestUser : This test case verifies guest user can add to favorites
//PDP_TC_08_verifyQuantityAddedtoCart : This test case verifies quantities of product added to the cartpackage testCases.Functional.desktop;
//PDP_TC_03_verifyBreadcrumbLinks: This test case verifies the Breadcrumb links on PDP

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import actions.GenericMethods;
import actions.ReturnDataObject;
import atu.testng.reports.ATUReports;
import reusableModules.desktop.Desk_Logout_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import reusableModules.desktop.Desk_CheckLinks_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import setup.MultipleBrowser;

public class Desk_PDPTestCases extends MultipleBrowser {
Desk_PDP_Reusable bnyPDPreusable;
String bnyURL;
String whsURL;
Desk_MYBagCheckout_Reusable mybagCheckout;
Desk_Logout_Reusable logoutReusable;
String productURL;

@BeforeMethod(groups = "Before_Method")
@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL","PRODUCTURL"})
public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL,String product_URL){
	super.Browser=Browser;
	super.Device=Device;
	bnyURL=bny_URL;
	whsURL=whs_URL;
	super.setuptest(Device,Browser);
	driver.manage().window().maximize();
	driver.get(bny_URL);
	productURL=product_URL;
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
}

//========================*********Test Case 1 starts*********===============================


//PDP_TC_01_verifyPDPSize : This test case verifies that Size of the product is selected
@Test(groups={"PDP","Regression","BlackBox"},priority=1,testName="PDP_TC_01_verifyPDPSize",enabled=true)
	public void PDP_TC_01_verifyPDPSize() {	
	    bnyPDPreusable=new Desk_PDP_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies that Size of the product is selected");
		driver.get(bnyURL+productURL);
		bnyPDPreusable.selectAvailableSizeOnPDP(driver);
	}

//PDP_TC_02_verifyCheckDefaultAccordianStatus : This test case verifies default accordian status on PDP
@Test(groups={"PDP","Regression","BlackBox"},priority=2,testName="PDP_TC_02_verifyCheckDefaultAccordianStatus",enabled=true)
	public void PDP_TC_02_verifyCheckDefaultAccordianStatus() {	
	    bnyPDPreusable=new Desk_PDP_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies default accordian status on PDP");
		driver.get(bnyURL+productURL);
        bnyPDPreusable.verifyPDPAccordianStatus(driver);
	}

//PDP_TC_03_verifyPDPColor : This test case verifies if user is able to select the color of the product on PDP.
@Test(groups={"PDP","Regression","BlackBox"},priority=9,testName="PDP_TC_03_verifyPDPColor",enabled=true) 
	public void PDP_TC_03_verifyPDPColor() {
		bnyPDPreusable=new Desk_PDP_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
	    ATUReports.setTestCaseReqCoverage("This test case verifies if user is able to select the color of the product on PDP");
	    bnyPDPreusable=new Desk_PDP_Reusable();	    
		driver.get(bnyURL+"product/frame-le-skinny-de-jeanne-jeans-502944005.html");
		GenericMethods.waitForPageLoaded(driver);
		bnyPDPreusable.changeSelectedColour(driver);  			
	}

//PDP_TC_04_verifySocialIconLinks : This test case verifies the Social Icon links on PDP
@Test(groups={"PDP","Regression","BlackBox"},priority=4,testName="PDP_TC_04_verifySocialIconLinks",enabled=true)
	public void PDP_TC_04_verifySocialIconLinks() {
	    bnyPDPreusable=new Desk_PDP_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies the Social Icon links on PDP");
		driver.get(bnyURL+productURL);
		Desk_CheckLinks_Reusable.checkAllPageLinks(driver, bnyURL+productURL, "//nav[@class='social']/a");
	}

//PDP_TC_05_seeAllandCOllapseAll : This test case verifies see all collapse all link on PDP
@Test(groups={"PDP","Regression","BlackBox"},priority=5,testName="PDP_TC_05_seeAllandCOllapseAll",enabled=true)
	public void PDP_TC_05_seeAllandCOllapseAll(){	
	    bnyPDPreusable=new Desk_PDP_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies see all collapse all link on PDP");		
		driver.get(bnyURL+productURL);		
		bnyPDPreusable.verifyImagesAfterSeeAll(driver);
	}

//PDP_TC_06_verifyFeaturingAndRecommended : This test case verifies featuring and recommended section PDP
@Test(groups={"PDP","Regression","BlackBox"},priority=6,testName="PDP_TC_06_verifyFeaturingAndRecommended",enabled=true)
	public void PDP_TC_06_verifyFeaturingAndRecommended(){	
	    bnyPDPreusable=new Desk_PDP_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies featuring and recommended section PDP");		
		driver.get(bnyURL+productURL);
        bnyPDPreusable.verifyNumberOfProductsUnderRecommendedandFeaturing(driver);

	}

//PDP_TC_07_AddToFavoritesGuestUser : This test case verifies guest user can add to favorites
@Test(groups={"PDP","Regression","BlackBox"},priority=7,testName="PDP_TC_07_AddToFavoritesGuestUser",enabled=true)
	public void PDP_TC_07_AddToFavoritesGuestUser(){	
	    bnyPDPreusable=new Desk_PDP_Reusable();
		logoutReusable= new Desk_Logout_Reusable();	
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies guest user can add to favorites");
	    logoutReusable.logOutifLoggedIn(driver);
		driver.get(bnyURL+productURL);		
		bnyPDPreusable.AddToFavorites(driver);
	}

//PDP_TC_08_verifyQuantityAddedtoCart : This test case verifies quantities of product added to the cart
@Test(groups={"PDP","Regression","BlackBox"},priority=8,testName="PDP_TC_08_verifyQuantityAddedtoCart",enabled=true)
	public void PDP_TC_08_verifyQuantityAddedtoCart(){
	    bnyPDPreusable=new Desk_PDP_Reusable();
	    mybagCheckout =new Desk_MYBagCheckout_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", "29th August 2016","1.0");
		ATUReports.setTestCaseReqCoverage("PDP -This test case verifies quantities of product added to the cart");
	    driver.get(bnyURL+productURL);
	    //mybagCheckout.emptyMyBag(driver, "BNY");			
		bnyPDPreusable.verifyQuantityadded(driver,3);
	}

/*//PDP_TC_03_verifyBreadcrumbLinks : This test case verifies the Breadcrumb links on PDP
@Test(groups={"PDP","Regression"},dataProvider="pdpURL",priority=3,testName="PDP_TC_03_verifyBreadcrumbLinks",enabled=true)
	public void PDP_TC_03_verifyBreadcrumbLinks(String productURL) {	
	    bnyPDPreusable=new Desk_PDP_Reusable();
		ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies the Breadcrumb links on PDP");
		driver.get(bnyURL+productURL);
		Desk_CheckLinks_Reusable.checkAllPageLinks(driver, bnyURL+productURL, "//ol[@class='breadcrumb']");
	}
*/



@AfterMethod(groups = "After_Method")
		public void Stop(){
			GenericMethods.Waitformilliseconds(2000);
			driver.close();
			driver.quit();
		}

@DataProvider(name = "CheckoutData")
	public static Object[][] ProvideDataToAboveMeth() throws IOException{	   
	   ReturnDataObject returnData1=new ReturnDataObject();
	   return returnData1.read("src/test/java/testDataFiles/desktop/BNY_WHS_Sniff.xls","Checkout");		
    }

@DataProvider(name = "pdpURL")
public static Object[][] ProvideDataToAboveMeth1() throws IOException{	   
   ReturnDataObject returnData1=new ReturnDataObject();
   return returnData1.read("src/test/java/testDataFiles/desktop/PDP.xls","PDPURL");		
	}
	
}
