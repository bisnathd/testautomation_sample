package testCases.Functional.desktop;
/*
 TC_01_VerifyEditorialLandingPage : This test case verify if the lookbook landing page is displayed.
 TC_02_VerifyWomensLookBookBrowsePage : This test case verify the womens lookbook browse page.
 */

import java.util.concurrent.TimeUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import reusableModules.desktop.Desk_LookBook_Reusable;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import setup.MultipleBrowser;

public class Desk_EditorialTestCase extends MultipleBrowser{
	
	GenericMethods genericMethods;
	Desk_LookBook_Reusable Lookbook;
	String bnyurl;
	String whsurl;
	
	@BeforeMethod(groups ="Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void start(String Device, String Browser,String Bny_URL,String Whs_URL){
	super.Browser=Browser;
	super.Device=Device;
	super.setuptest(Device, Browser);
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	bnyurl=Bny_URL;
	whsurl=Whs_URL;
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	//TC_01_VerifyEditorialLandingPage : This test case verify if the lookbook landing page is displayed.
	@Test(groups={"Editorial1","Regression","BlackBox"},priority=1,testName="TC_01_VerifyEditorialLandingPage",enabled=true)
	public void TC_01_VerifyEditorialLandingPage() {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Editorial - This test case verify if the lookbook landing page is displayed.");
		driver.get(bnyurl);
		Lookbook= new Desk_LookBook_Reusable();
		Lookbook.LookBook(driver);
	}
	
	//TC_02_VerifyWomensLookBookBrowsePage : This test case verify the womens lookbook browse page.
	@Test(groups={"Editorial","Regression","BlackBox"},priority=2,testName="TC_02_VerifyWomensLookBookBrowsePage",enabled=true)
	public void TC_02_VerifyWomensLookBookBrowsePage() {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("Editorial - This test case verify the womens lookbook browse page.");
		driver.get(bnyurl);
		Lookbook= new Desk_LookBook_Reusable();
		Lookbook.lookbookBrowerPage(driver,"Women's Lookbooks");
	}
	
	
	
	@AfterMethod(groups = "After_Method")
	public void Stop(){		
		driver.quit();
		GenericMethods.Waitformilliseconds(2000);
	}
	

}
