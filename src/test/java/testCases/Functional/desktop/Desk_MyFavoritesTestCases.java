package testCases.Functional.desktop;

/*
MyFav_TC_01_LoginUserAddFavoritePDP : This test case verifies that Login user adds product to favorites from PDP
MyFav_TC_02_LoginUserAddFavoriteCatBrowse : This test case verifies that Login user adds product to favorites from Category Browse Page
MyFav_TC_03_LoggedInUserAddFavMyBag : This test case verifies that Login user adds product to favorites from My Bag Page
MyFav_TC_04_GuestUserAddFavoritePDP : This test case verifies that Guest user adds product to favorites from PDP Page
MyFav_TC_05_GuestUserAddFavoriteMyBag : This test case verifies that Guest user adds product to favorites from My Bag Page
MyFav_TC_06_GuestUserAddFavoriteCatBrowse : This test case verifies that Guest user adds product to favorites from Category Browse Page
MyFav_TC_07_LoginUserRemoveFavoritePDP : This test case verifies that Login user adds product to favorites from PDP and then remove it.
MyFav_TC_08_LoginUserRemoveFavoriteMyBag : This test case verifies that Login user adds product to favorites from MyBag Page and then remove it.
MyFav_TC_09_LoginUserRemoveFavoriteCatBrowse : This test case verifies that Login user adds product to favorites from Category Browse Page and then remove it.
MyFav_TC_10_AddListToMyFavFromPDP : This test case verifies that user can add list to My Favorites from PDP
MyFav_TC_11_AddListToMyFavFromMyAccout : This test case verifies to add list to My Favorites from My Account
*/

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_Logout_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import reusableModules.desktop.Desk_SearchResultsAndFilters_Reusable;
import setup.MultipleBrowser;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_PDPPageObjects;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_MyFavoritesPageObjects;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import reusableModules.desktop.Desk_MyAccMyFavorites_Resuable;

public class Desk_MyFavoritesTestCases extends MultipleBrowser {

	GenericMethods genericMethods;
	WebDriverWait xplicitWait ;
	Desk_LoginRegister_Reusable bnyLogin;
	Desk_BNY_NavigationPageObject globalNav_Objects;
	Desk_PDP_Reusable buyProduct;
	Desk_MYBagCheckout_Reusable CheckOutMyBag;
	Desk_SearchResultsAndFilters_Reusable searchResultsAndFilters;
	Desk_Logout_Reusable logout;
	Desk_GlobalNav_Reusable globalNavModule;
	Desk_LoginRegister_Reusable bnyRegistration;
	Desk_BNY_PDPPageObjects PDPObjects;
	Desk_BNY_HomePageObject homePageObj;
	Desk_MyAccMyFavorites_Resuable FavModule;
	String Category="Women";
	String SubCategory="Clothing";
	String Emailid="bisnathdubey@mail.com";
	String Password="123456789";
	static	String login, product_heartIcon_clicked, product_added,PDPPage;
	String ListName, ListDescription;
	Desk_BNY_MyFavoritesPageObjects FavObjects;
	String bnyURL;
	String whsURL;
	
	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
		super.Browser=Browser;
		super.Device=Device;
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		driver.get(bny_URL);
		bnyURL= bny_URL;
		whsURL= whs_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}
	
//MyFav_TC_01_LoginUserAddFavoritePDP : This test case verifies that Login user adds product to favorites from PDP	
	@Test(groups={"Favorite","Regression","BlackBox"},priority=1,testName="MyFav_TC_01_LoginUserAddFavoritePDP",enabled=true)
		public void MyFav_TC_01_LoginUserAddFavoritePDP() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Logged in User - Add a product to Favorites from PDP page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			
			bnyRegistration.registerFromHeader(driver);
			//bnyRegistration.loginFromHeader(Driver, Emailid , Password);
			
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);
			FavModule.ClickHeartIconPDP(driver);
			FavModule.VerifyProductAddedToFavorites(driver);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page", "Product is added to favorites page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page","Product is not added to favorites page", true,LogAs.FAILED);
				Assert.assertEquals(product_added,product_heartIcon_clicked);				
			}
		}
/*	 
 * ################## This functionality has been removed #################
 * 
//MyFav_TC_02_LoginUserAddFavoriteCatBrowse : This test case verifies that Login user adds product to favorites from Category Browse Page
	@Test(groups={"Favorite","Regression"},priority=2,testName="MyFav_TC_02_LoginUserAddFavoriteCatBrowse",enabled=true)
		public void MyFav_TC_02_LoginUserAddFavoriteCatBrowse()	{
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Logged in User - Add a product to Favorites from Category Browse page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			
			//bnyRegistration.registerFromHeader(driver);
			bnyRegistration.loginFromHeader(driver, Emailid , Password);
			
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver,Category, SubCategory);
			FavModule.ClickHeartIconCatBrowse(driver);
			FavModule.VerifyProductAddedToFavorites(driver);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page", "Product is added to favorites page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page","Product is not added to favorites page", true,LogAs.FAILED);
				Assert.assertEquals(product_heartIcon_clicked, product_added);
			}
		}

*/	
//MyFav_TC_02_LoggedInUserAddFavMyBag : This test case verifies that Login user adds product to favorites from My Bag Page
	@Test(groups={"Favorite","Regression","BlackBox"},priority=2,testName="MyFav_TC_02_LoggedInUserAddFavMyBag",enabled=true)
		public void MyFav_TC_02LoggedInUserAddFavMyBag(){
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Logged in User - Add a product to Favorites from My Bag page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			
			//bnyRegistration.registerFromHeader(driver);
			bnyRegistration.loginFromHeader(driver, Emailid , Password);
			
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);			
			FavModule.ClickHeartIconMyBag(driver,"login");
			FavModule.VerifyProductAddedToFavorites(driver);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page", "Product is added to favorites page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page","Product is not added to favorites page", true,LogAs.FAILED);
				Assert.assertEquals(product_heartIcon_clicked, product_added);
			}		
		}
	
//MyFav_TC_03_GuestUserAddFavoritePDP : This test case verifies that Guest user adds product to favorites from PDP Page
	@Test(groups={"Favorite","Regression","BlackBox"},priority=3,testName="MyFav_TC_03_GuestUserAddFavoritePDP",enabled=true)
		public void MyFav_TC_03_GuestUserAddFavoritePDP() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Guest User - Add a product to Favorites from PDP page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			bnyRegistration=new Desk_LoginRegister_Reusable();
			bnyLogin=new Desk_LoginRegister_Reusable();
			PDPObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
			//------------------------------------------Register a user and logout -----------------------------------
			bnyRegistration.registerFromHeader(driver);
			login= bnyRegistration.sTime;
			System.out.println("Hi"+login);
			logout=new Desk_Logout_Reusable();
			logout.logOutifLoggedIn(driver);
			//-------------------------------------------Go to PDP page and add a product to favorites-----------------
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);
			FavModule.ClickHeartIconPDP(driver);
			//------------------------------------------Login----------------------------------------------------------
			bnyLogin.generalLogin1(driver,login);
			String Product=genericMethods.getText(driver, PDPObjects.PDP_ProductName_LB);
			product_heartIcon_clicked= FavModule.ProductName;
			if (Product.equalsIgnoreCase(product_heartIcon_clicked)){
				ATUReports.add("Verify user is redicted to PDP page ",  "User should be redirected to PDP page", "User is redirected to PDP page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify user is redicted to PDP page ",  "User should be redirected to PDP page", "User is not redirected to PDP page", true,LogAs.PASSED);
				Assert.assertEquals(Product, product_heartIcon_clicked);
			}
			FavModule.VerifyProductAddedToFavorites(driver);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page", "Product is added to favorites page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page","Product is not added to favorites page", true,LogAs.FAILED);
				Assert.assertEquals(product_added,product_heartIcon_clicked);				
			}
		}

	
//MyFav_TC_04_GuestUserAddFavoriteMyBag : This test case verifies that Guest user adds product to favorites from My Bag Page
	@Test(groups={"Favorite","Regression","BlackBox"},priority=4,testName="MyFav_TC_04_GuestUserAddFavoriteMyBag",enabled=true)
		public void MyFav_TC_04_GuestUserAddFavoriteMyBag() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Guest User - Add a product to Favorites from My Bag page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			bnyRegistration=new Desk_LoginRegister_Reusable();
			bnyLogin=new Desk_LoginRegister_Reusable();
			FavObjects = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
			//------------------------------------------Register a user and logout -----------------------------------
			bnyRegistration.registerFromHeader(driver);
			login= bnyRegistration.sTime;
			System.out.println("Hi"+login);
			logout=new Desk_Logout_Reusable();
			logout.logOutifLoggedIn(driver);
			//-------------------------------------------Go to My Bag page and add a product to favorites-----------------
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);
			FavModule.ClickHeartIconMyBag(driver,"guest");
			//------------------------------------------Login----------------------------------------------------------
			bnyLogin.generalLogin1(driver,login);
			//bnyRegistration.loginFromHeader(Driver, Emailid , Password);
			String Product=genericMethods.getText(driver, FavObjects.MyBag_Fav_ProductName);
			product_heartIcon_clicked= FavModule.ProductName;
			if (Product.equalsIgnoreCase(product_heartIcon_clicked)){
				ATUReports.add("Verify user is redicted to My Bag page ",  "User should be redirected to My Bag page", "User is redirected to My Bag page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify user is redicted to My Bag page ",  "User should be redirected to My Bag page", "User is not redirected to My Bag page", true,LogAs.PASSED);
				Assert.assertEquals(Product, product_heartIcon_clicked);
			}
			FavModule.VerifyProductAddedToFavorites(driver);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page", "Product is added to favorites page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page","Product is not added to favorites page", true,LogAs.FAILED);
				Assert.assertEquals(product_added,product_heartIcon_clicked);				
			}
		}
		
//MyFav_TC_05_GuestUserAddFavoriteCatBrowse : This test case verifies that Guest user adds product to favorites from Category Browse Page
	@Test(groups={"Favorite","Regression","BlackBox"},priority=5,testName="MyFav_TC_05_GuestUserAddFavoriteCatBrowse",enabled=true)
		public void MyFav_TC_05_GuestUserAddFavoriteCatBrowse() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Guest User - Add a product to Favorites from Category Browse page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			bnyRegistration=new Desk_LoginRegister_Reusable();
			bnyLogin=new Desk_LoginRegister_Reusable();
			FavObjects = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
			//------------------------------------------Register a user and logout -----------------------------------
			bnyRegistration.registerFromHeader(driver);
			login= bnyRegistration.sTime;
			System.out.println("Hi"+login);
			logout=new Desk_Logout_Reusable();
			logout.logOutifLoggedIn(driver);
			//-------------------------------------------Go to My Bag page and add a product to favorites-----------------
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver,Category, SubCategory);
			FavModule.GuestUserClickHeartIconCatBrowse(driver);
			//------------------------------------------Login----------------------------------------------------------
			bnyLogin.generalLogin1(driver,login);
			//bnyRegistration.loginFromHeader(Driver, Emailid , Password);
			String Product=genericMethods.getText(driver, FavObjects.CatBrowse_FirstImage_ProductName_LK);
			product_heartIcon_clicked= FavModule.ProductName;
			if (Product.equalsIgnoreCase(product_heartIcon_clicked)){
				ATUReports.add("Verify user is redicted to Category Browse page ",  "User should be redirected to Category Browse page", "User is redirected to Category Browse page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify user is redicted to Category Browse page ",  "User should be redirected to Category Browse page", "User is not redirected to Category Browse page", true,LogAs.PASSED);
				Assert.assertEquals(Product, product_heartIcon_clicked);
			}
			FavModule.VerifyProductAddedToFavorites(driver);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page", "Product is added to favorites page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to favorites page ",  "Product should be added to favorites page","Product is not added to favorites page", true,LogAs.FAILED);
				Assert.assertEquals(product_added,product_heartIcon_clicked);				
			}
		}
		
//MyFav_TC_06_LoginUserRemoveFavoritePDP : This test case verifies that Login user adds product to favorites from PDP and then remove it.
	@Test(groups={"Favorite","Regression","BlackBox"},priority=6,testName="MyFav_TC_06_LoginUserRemoveFavoritePDP",enabled=true)
		public void MyFav_TC_06_LoginUserRemoveFavoritePDP() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Add and then remove a Favorites product from PDP page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			bnyRegistration.registerFromHeader(driver);
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);
			FavModule.ClickHeartIconPDP(driver);
			GenericMethods.Waitformilliseconds(2000);
			FavModule.UnclickHeartIconFromPDP(driver);
			FavModule.VerifyProductNotAddedToFavorites(driver);			
		}
		
//MyFav_TC_07_LoginUserRemoveFavoriteMyBag : This test case verifies that Login user adds product to favorites from MyBag Page and then remove it.
	@Test(groups={"Favorite","Regression","BlackBox"},priority=7,testName="MyFav_TC_07_LoginUserRemoveFavoriteMyBag",enabled=true)
		public void MyFav_TC_07_LoginUserRemoveFavoriteMyBag() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Add and then remove a Favorites product  from My Bag page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			bnyRegistration.registerFromHeader(driver);
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);
			FavModule.ClickHeartIconMyBag(driver,"Login");
			FavModule.UnclickHeartIconFromMyBag(driver);
			FavModule.VerifyProductNotAddedToFavorites(driver);			
		}
		
//MyFav_TC_08_LoginUserRemoveFavoriteCatBrowse : This test case verifies that Login user adds product to favorites from Category Browse Page and then remove it.
	@Test(groups={"Favorite","Regression","BlackBox"},priority=8,testName="MyFav_TC_08_LoginUserRemoveFavoriteCatBrowse",enabled=true)
		public void MyFav_TC_08_LoginUserRemoveFavoriteCatBrowse() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Add and then remove a Favorites product  from Category Browse page");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			bnyRegistration.registerFromHeader(driver);
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);
			FavModule.ClickHeartIconCatBrowse(driver);
			FavModule.UnclickHeartIconFromCatBrowse(driver);
			FavModule.VerifyProductNotAddedToFavorites(driver);			
		}
	
//MyFav_TC_09_AddListToMyFavFromPDP : This test case verifies that user can add list to My Favorites from PDP
	@Test(groups={"Favorite","Regression","BlackBox"},priority=9,testName="MyFav_TC_09_AddListToMyFavFromPDP",enabled=true)
		public void MyFav_TC_09_AddListToMyFavFromPDP() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Add List to My Favorites from PDP");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			FavObjects = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
			PDPObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
			bnyRegistration.registerFromHeader(driver);
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver, Category, SubCategory);
			FavModule.AddingFavoriteListFromPDP(driver);
			ListName="List1";
			ListDescription="List1 Description";
			FavModule.MyFavCreateNewList(driver, ListName,ListDescription);
			
			PDPPage=genericMethods.getText(driver, PDPObjects.PDP_ProductName_LB);
			if (PDPPage.equalsIgnoreCase(FavModule.ProductName)){
				ATUReports.add("Verify user is redicted to PDP page", "User should be redirected to PDP page", "User is redirected to PDP page", true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify user is redicted to PDP page", "User should be redirected to PDP page", "User is not redirected to PDP page", true, LogAs.PASSED);
				Assert.assertEquals(PDPPage, FavModule.ProductName);
			}
			FavModule.VerifyProductAddedInNewList(driver,ListName);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to the list ",  "Product should be added to the list", "Product is added to the list", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to the list ",  "Product should be added to the list", "Product is not added to the list", true,LogAs.FAILED);
				Assert.assertEquals(product_added,product_heartIcon_clicked);				
			}
		}
	
//MyFav_TC_10_AddListToMyFavFromMyAccout : This test case verifies to add list to My Favorites from My Account
	@Test(groups={"Favorite","Regression","BlackBox"},priority=10,testName="MyFav_TC_10_AddListToMyFavFromMyAccout",enabled=true)
		public void MyFav_TC_10_AddListToMyFavFromMyAccout() {
			ATUReports.setAuthorInfo("Adapty QA Team", "30th May 2016","1.0");
			ATUReports.setTestCaseReqCoverage("Add list to My Favorites from My Account");
			genericMethods=new GenericMethods();
			bnyRegistration= new Desk_LoginRegister_Reusable();
			globalNavModule= new Desk_GlobalNav_Reusable();
			FavModule= new Desk_MyAccMyFavorites_Resuable();
			FavObjects = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
			PDPObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
			bnyRegistration.registerFromHeader(driver);
			globalNavModule.CategoryMouseHover(driver, Category );
			globalNavModule.clickSubCategoryName(driver,Category, SubCategory);
			FavModule.AddingFavoriteListFromPDP(driver);
			ListName="List1";
			ListDescription="List1 Description";
			FavModule.MyFavCreateNewList(driver, ListName,ListDescription);
			
			PDPPage=genericMethods.getText(driver, PDPObjects.PDP_ProductName_LB);
			if (PDPPage.equalsIgnoreCase(FavModule.ProductName)){
				ATUReports.add("Verify user is redicted to PDP page", "User should be redirected to PDP page", "User is redirected to PDP page", true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify user is redicted to PDP page", "User should be redirected to PDP page", "User is not redirected to PDP page", true, LogAs.PASSED);
				Assert.assertEquals(PDPPage, FavModule.ProductName);
			}
			FavModule.VerifyProductAddedInNewList(driver, ListName);
			product_heartIcon_clicked= FavModule.ProductName;
			product_added=FavModule.FavProductName;
			if (product_heartIcon_clicked.equalsIgnoreCase(product_added)){
				ATUReports.add("Verify Product is added to the list ",  "Product should be added to the list", "Product is added to the list", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Product is added to the list ",  "Product should be added to the list", "Product is not added to the list", true,LogAs.FAILED);
				Assert.assertEquals(product_added,product_heartIcon_clicked);				
			}
		}
		
		@AfterMethod(groups="After_Method")
		public void Stop(){
			driver.close();
			GenericMethods.Waitformilliseconds(2000);
		}		
			
	}