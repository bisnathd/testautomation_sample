package testCases.Functional.desktop;
/*
CheckBill_TC_01_guestUserAddingBillingAddress : This test case verify  if guest user is able to add billing address and payment details.
CheckBill_TC_02_registereduserAddsBillingWithNoPaymentSaved - This test case verify if newly registered user is able to add new payment details on billing page,with no payment saved in my account
*/
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.util.HSSFColor.TURQUOISE;
import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



//import com.gargoylesoftware.htmlunit.Page;
import setup.MultipleBrowser;
import reusableModules.desktop.Desk_CategoryProductClick_Reusable;
import reusableModules.desktop.Desk_CheckoutBilling_Reusable;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_CheckoutShipping_Reusable;
import reusableModules.desktop.Desk_GetOrderNumber_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import reusableModules.desktop.Desk_MyAccountAddress_Reusable;
import reusableModules.desktop.Desk_MyAccountPayment_Reusable;
import reusableModules.desktop.Desk_PDP_Reusable;
import PageObjects.desktop.Desk_BNY_BillingPageObject;
import PageObjects.desktop.Desk_BNY_MYBagPageObjects;
import PageObjects.desktop.Desk_BNY_MyAccountPaymentPaegObjects;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_OrderReviewPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_CheckoutBillingTestCases extends MultipleBrowser {

	Desk_BNY_HomePageObject home_PageObjects;
	Desk_BNY_HomePageObject loginRegister_PageObjects;
	Desk_BNY_NavigationPageObject globalNav_PageObjects;
	Desk_BNY_MYBagPageObjects myBag_PageObjects;
	Desk_BNY_ShippingPageObject shipping_PageObjects;
	Desk_BNY_BillingPageObject billing_PageObjects;
	Desk_BNY_OrderReviewPageObject orderReview_PageObjects;
	Desk_BNY_MyAccountPaymentPaegObjects myaccountPayment_PageObjects;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	Desk_GlobalNav_Reusable globalNavReusable;
	Desk_CategoryProductClick_Reusable categoryProductClickReusable;
	Desk_PDP_Reusable pdpReusable;
	Desk_MYBagCheckout_Reusable myBagCheckout_Reusable;
	Desk_CheckoutShipping_Reusable checkoutshipping_Reusable;
	Desk_CheckoutShipping_Reusable checkoutShippingReusable;
	Desk_CheckoutBilling_Reusable checkoutBillingReusable;		
	Desk_GetOrderNumber_Reusable getOrderNumber;	
	Desk_MyAccountAddress_Reusable myaccAddress;
	Desk_MyAccountPayment_Reusable myaccountPayment;	
	GenericMethods genericMethods;
	String CurrentPageTitle;
	String urlFromXML;
	WebDriverWait xplicitWait;
	String cardHolderName="bisnath Visa" , CardMonth="April", year="2020",bnyURL,whsURL;
	String	PaymentDropdow;
	
	
	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		//driver.get(URL);
		bnyURL= bny_URL;
		whsURL= whs_URL;
		urlFromXML=bny_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}


//CheckBill_TC_01_guestUserAddingBillingAddress : This test case verify  if guest user is able to add billing address and payment details.
@Test(groups="checkoutBilling",dataProvider="BillingFormData" , priority=1,testName="CheckBill_TC_01_guestUserAddingBillingAddress",enabled=true)	
public void CheckBill_TC_01_guestUserAddingBillingAddress(String SubCategory, String Category, String SfirstName, String lastName,
		 String address1, String address2, String city, String zipCode,
		 String phone, String billingState, String addressName, String cardType,
		 String cardNo, String cardCVV, String billingEmail, String promo, String GC, String BarneysUrl1, String WareHouseUrl2) {
		 ATUReports.setAuthorInfo("Adapty QA", "25th Aug 2016", "1.0");
		 ATUReports.setTestCaseReqCoverage("CheckoutBilling : This test case verify  if guest user is able to add billing address and payment details.");
		 loginRegister_PageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		 myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		 billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		 orderReview_PageObjects = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		 genericMethods = new GenericMethods();
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 pdpReusable = new Desk_PDP_Reusable();
		 myBagCheckout_Reusable = new Desk_MYBagCheckout_Reusable();
		 checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
		 checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		 categoryProductClickReusable = new Desk_CategoryProductClick_Reusable();

		 driver.get(bnyURL);

		 //----------------------Category Browse--------------------------
		 globalNavReusable.CategoryMouseHover(driver, "Women");
		 globalNavReusable.clickSubCategoryName(driver, "Women", "Dresses");
		 categoryProductClickReusable.browseProductandClickonBNY(driver, "1000");
		 //----------------------------PDP---------------------------------
		 pdpReusable.selectAvailableColorOnPDP(driver);
		 pdpReusable.selectAvailableSizeOnPDP(driver);
		 pdpReusable.AddtoCartButtononPDP(driver);
		 pdpReusable.miniBagPopuponPDP(driver);
		 pdpReusable.miniBagPopupclickOnCheckout_BT(driver);
		 //------------------My Bag(Logged In User)---------------------------------
		 myBagCheckout_Reusable.selectCountry(driver, "United States"); //Select United States as Shipping Country
		 myBagCheckout_Reusable.clickCheckoutButton(driver);
		 //------------Login as a guest OR logged in user on checkout pop up----------------
		 myBagCheckout_Reusable.CheckoutLogInPopupOncart(driver, "guest", "barneys1@yopmail.com", "123456789");
		 //------------------Start Filling Shipping Address on Checkout Shipping-------------
		 checkoutshipping_Reusable.startFillingShippingForm(driver, SfirstName, lastName, address1, address2, city, billingState, zipCode, phone);
		 checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
		 //--------------------Start billing payment on Checkout Shipping------------
		 checkoutBillingReusable.GuestaddPaymentDetails(driver, cardHolderName, cardNo, CardMonth, year, cardCVV);
		 ATUReports.add("Verify if the payment details is added", "Payment details should be added", "Guest has entered payment detail successfully", true, LogAs.INFO);
		 //--------------------Start billing address on Checkout Shipping------------
		 checkoutBillingReusable.addBillingAddressDetails(driver, SfirstName, lastName, address1, address2, city, billingState, zipCode, billingEmail, phone);
		 ATUReports.add("Verify if the Billing details is added", "Billing details should be added", "Guest has entered billing detail successfully", true, LogAs.INFO);
		 genericMethods.Click(driver, billing_PageObjects.billing_GuestContinue_BTN, "Billing Continue Button");
		 System.out.println(genericMethods.getText(driver, orderReview_PageObjects.orderReview_shippingAddres_TV));
		 System.out.println(genericMethods.getText(driver, orderReview_PageObjects.orderReview_billingAddress_TV));
		}

//CheckBill_TC_02_registereduserAddsBillingWithNoPaymentSaved - This test case verify if newly registered user is able to add new payment details on billing page,with no payment saved in my account
@Test(groups="checkoutBilling1",dataProvider="BillingFormData" , priority=2,testName="CheckBill_TC_02_registereduserAddsBillingWithNoPaymentSaved",enabled=true)
public void CheckBill_TC_02_registereduserAddsBillingWithNoPaymentSaved(String category,
		 String SubCategory, String firstName, String lastName,
		 String address1, String address2, String city, String zipCode,
		 String Phone, String State, String AddressName, String cardType,
		 String cardNo, String cardCVV, String Email, String promo, String GC, String BarneysUrl1, String WareHouseUrl2) {

		 ATUReports.setAuthorInfo("Adapty QA", "25th Aug 2016", "1.0");
		 ATUReports.setTestCaseReqCoverage("CheckoutBilling : This test case verify if newly registered user is able to add new payment details on billing page,with no payment saved in my account");
		 genericMethods = new GenericMethods();
		 loginRegisterReusable = new Desk_LoginRegister_Reusable();
		 myBagCheckout_Reusable = new Desk_MYBagCheckout_Reusable();
		 globalNavReusable = new Desk_GlobalNav_Reusable();
		 checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		 categoryProductClickReusable = new Desk_CategoryProductClick_Reusable();
		 checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
		 pdpReusable = new Desk_PDP_Reusable();
		 driver.get(bnyURL);

		 loginRegisterReusable.registerFromHeader(driver);
		 //-------------------Empty My Bag module ---------------				
		 CurrentPageTitle = genericMethods.getTitle(driver);
		 if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")) {
		  myBagCheckout_Reusable.emptyMyBag(driver, "WHS");
		 } else {
		  myBagCheckout_Reusable.emptyMyBag(driver, "BNY");

		  myBagCheckout_Reusable.GotoPDP(driver, urlFromXML, BarneysUrl1, WareHouseUrl2);  
		 }
		 //----------------------Category Browse--------------------------
		 globalNavReusable.CategoryMouseHover(driver, "Women");
			globalNavReusable.clickSubCategoryName(driver, "Women", "Clothing");
		 categoryProductClickReusable.browseProductandClickonBNY(driver,"1000");
		 //----------------------------PDP---------------------------------
		 pdpReusable.selectAvailableColorOnPDP(driver);
		 pdpReusable.selectAvailableSizeOnPDP(driver);
		 pdpReusable.AddtoCartButtononPDP(driver);
		 pdpReusable.miniBagPopuponPDP(driver);
		 pdpReusable.miniBagPopupclickOnCheckout_BT(driver);
		 //------------------My Bag(Logged In User)---------------------------------
		 myBagCheckout_Reusable.selectCountry(driver, "United States"); //Select United States as Shipping Country
		 myBagCheckout_Reusable.clickCheckoutButton(driver);
		 //------------------Start Filling Shipping Address on Checkout Shipping-------------
		 checkoutshipping_Reusable.startFillingShippingForm(driver, firstName, lastName, address1, address2, city, State, zipCode, Phone);
		 checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
		 //--------------------To check if payment dropdown is displayed OR not.------------
		 if (checkoutBillingReusable.verifyPresenceOfPaymentDropdown(driver)) {
		  ATUReports.add("Verify if payment dropdown is displayed", "Payment dropdown should not be displayed", "Payment dropdown is displayed", "Payment dropdown is still displayed", true, LogAs.FAILED);
		 } else {
		  ATUReports.add("Verify if payment dropdown is displayed", "Payment dropdown should not be displayed", "Payment dropdown is not displayed", "Registered user does not have payment saved,so payment dropdown is not displayed", true, LogAs.PASSED);
		  //--------------------Start filling billing payment on billing page------------
		  checkoutBillingReusable.AddPaymentDetails(driver, cardHolderName, cardNo, CardMonth, year, cardCVV);
		  //--------------------Start billing address on on billing page------------
		  checkoutBillingReusable.addBillingAddressDetails(driver, firstName, lastName, address1, address2, city, State, zipCode, Email, Phone);
		  ATUReports.add("Verify if the Billing details is added", "Billing details should be added", "Registered user has entered billing detail successfully", true, LogAs.PASSED);
		 }

		}


	@Test(groups = "checkoutBilling12", dataProvider = "BillingFormData", priority = 3)
	public void registerUserUsesBillingWithPaymentSaved(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC,String BarneysUrl1, String WareHouseUrl2){
		
		genericMethods = new GenericMethods();
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		myaccountPayment = new Desk_MyAccountPayment_Reusable();
		myaccAddress=new Desk_MyAccountAddress_Reusable();
		globalNavReusable = new Desk_GlobalNav_Reusable();
		checkoutshipping_Reusable = new Desk_CheckoutShipping_Reusable();
		categoryProductClickReusable = new Desk_CategoryProductClick_Reusable();
		pdpReusable = new Desk_PDP_Reusable();
		driver.get(bnyURL);

		loginRegisterReusable.registerFromHeader(driver);
		//loginRegisterReusable.loginFromHeader(driver, "bisnathdubey@gmail.com", "123456789");
		//---------------------My Account--------------------------		
		myaccountPayment.MyAccountPaymentMethod(driver,firstName,lastName,address1,address2,city,zipCode,Phone,State,AddressName, cardNo, cardCVV, Email,cardHolderName,CardMonth,year);
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckout_Reusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckout_Reusable.emptyMyBag(driver, "BNY");
		}
		//----------------------Category Browse--------------------------
		 globalNavReusable.CategoryMouseHover(driver, "Women");
		 globalNavReusable.clickSubCategoryName(driver,"Women", "Clothing");	
		 categoryProductClickReusable.browseProductandClickonBNY(driver,"1000");
		 //----------------------------PDP---------------------------------
		 pdpReusable.selectAvailableColorOnPDP(driver);
		 pdpReusable.selectAvailableSizeOnPDP(driver);
		 pdpReusable.AddtoCartButtononPDP(driver);
		 pdpReusable.miniBagPopuponPDP(driver);
		 pdpReusable.miniBagPopupclickOnCheckout_BT(driver);
		 //------------------My Bag(Logged In User)---------------------------------
		 myBagCheckout_Reusable.selectCountry(driver, "United States"); //Select United States as Shipping Country
		 myBagCheckout_Reusable.clickCheckoutButton(driver);
		 //------------------Start Filling Shipping Address on Checkout Shipping-------------
		 checkoutshipping_Reusable.startFillingShippingForm(driver, firstName, lastName, address1, address2, city, State, zipCode, Phone);
		 checkoutshipping_Reusable.clickShippingPage_ContinueButton(driver);
		 
		 if (checkoutBillingReusable.verifyPresenceOfPaymentDropdown(driver)) {
			  ATUReports.add("Verify if payment dropdown is displayed", "Payment dropdown should not be displayed", "Payment dropdown is displayed", "Payment dropdown is displayed", true, LogAs.PASSED);
			 } else {
				 ATUReports.add("Verify if payment dropdown is displayed", "Payment dropdown should be displayed", "Payment dropdown should be displayed", "Payment dropdown is not displayed", true, LogAs.FAILED);
			 }
		 //checkoutBillingReusable.VerifyCardType(driver);
		checkoutBillingReusable.BillingPaymentSaved(driver,category, SubCategory, firstName, lastName, address1,address2, city, zipCode, Phone, State, AddressName,cardType, cardNo, cardCVV, Email, promo, GC,cardHolderName);
	}

/*
	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 4)
	public void registeruserUses2ndBillingWithTwoPaymentSaved(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC){
		
		genericMethods = new GenericMethods();
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		myaccountPayment = new Desk_MyAccountPayment_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		
		loginRegisterReusable.registerFromHeader(driver);	
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------		
		myaccountPayment.MyAccountPaymentMethod(driver, firstName, lastName, address1, address2, city, zipCode, Phone,State, AddressName, cardType, cardNo, cardCVV, Email);
		myaccountPayment.MyAccountPaymentManual(driver);
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.LoggedInUS(driver, category,SubCategory, firstName, lastName, address1, address2, city,zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.BillingTwoPaymentSaved(driver, category, SubCategory, firstName, lastName, address1,address2, city, zipCode, Phone, State, AddressName,cardType, cardNo, cardCVV, Email, promo, GC);

	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 5)
	public void ToCheckAddNewPaymentLastOption(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC)
			 {
		
		genericMethods = new GenericMethods();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegisterReusable.registerFromHeader(driver);
		
		//-------------------Empty My Bag module ---------------				
		CurrentPageTitle=genericMethods.getTitle(driver);
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		myaccountPayment = new Desk_MyAccountPayment_Reusable();
		myaccountPayment.MyAccountPaymentMethod(driver, 
				firstName, lastName, address1, address2, city, zipCode, Phone,
				State, AddressName, cardType, cardNo, cardCVV, Email);
		myaccountPayment.MyAccountPaymentManual(driver);
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.LoggedInUS(driver, category,
				SubCategory, firstName, lastName, address1, address2, city,
				zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV,
				Email, promo, GC);
		checkoutBillingReusable.AddNewPaymentLastOption(driver,
				category, SubCategory, firstName, lastName, address1,
				address2, city, zipCode, Phone, State, AddressName,
				cardType, cardNo, cardCVV, Email, promo, GC);
	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 6)
	public void TocomparePriceCartandBillingPage(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC)
			 {
		genericMethods = new GenericMethods();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		
		//-------------------Empty My Bag module ---------------
			CurrentPageTitle=genericMethods.getTitle(driver);
			if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
				myBagCheckoutReusable.emptyMyBag(driver, "WHS");
			}else{			
				myBagCheckoutReusable.emptyMyBag(driver, "BNY");
			}
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,
				SubCategory);
		checkoutBillingReusable.GuestPriceCheck(driver,  category,
				SubCategory, firstName, lastName, address1, address2, city,
				zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV,
				Email, promo, GC);
	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 7)
	public void BillingMyBagEditLink(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC)
			 {
		
		genericMethods = new GenericMethods();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.BillingMyBagLinkCheck(driver, category,
				SubCategory, firstName, lastName, address1, address2, city,
				zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV,
				Email, promo, GC);
	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 8)
	public void BillingCountryCheck(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC)
			 {
		genericMethods = new GenericMethods();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		
	//-------------------Empty My Bag module ---------------
		CurrentPageTitle=genericMethods.getTitle(driver);
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
	//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,"Scarves");
		checkoutBillingReusable.BillingCountryCheck(driver, category,
				SubCategory, firstName, lastName, address1, address2, city,
				zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV,
				Email, promo, GC);

	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 9)
	public void BillingSameAsShippingCheck(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC)
			 {
		
		genericMethods = new GenericMethods();		
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		
		//-------------------Empty My Bag module ---------------				
		CurrentPageTitle=genericMethods.getTitle(driver);				
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.BillingSameAsShipping(driver, category, SubCategory, firstName, lastName, address1,address2, city, zipCode, Phone, State, AddressName,cardType, cardNo, cardCVV, Email, promo, GC);

	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 10)
	public void BillingPromoAddCheck(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC)
			 {
		genericMethods = new GenericMethods();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();		
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,
						firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.BillingPromoAdd(driver, category,SubCategory, firstName, lastName, address1, address2, city,zipCode, Phone, State, AddressName, cardType, cardNo,cardCVV, Email, promo, GC);
	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 11)
	public void BillingPromoDeleteCheck(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC)
			 {
		genericMethods = new GenericMethods();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();		
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		
		//-------------------Empty My Bag module ---------------
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.BillingPromoDelete(driver, category, SubCategory, firstName, lastName, address1,address2, city, zipCode, Phone, State, AddressName,cardType, cardNo, cardCVV, Email, promo, GC);
	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 12)
	public void BillingInternationalPromoCheck(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC)
			 {
		genericMethods = new GenericMethods();
		SubCategory = "Scarves";
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		//-------------------Empty My Bag module ---------------
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
	//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.SelectInternational(driver, category,SubCategory, firstName, lastName, address1, address2, city,zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.InternationalPromo(driver, category, SubCategory, firstName, lastName, address1,address2, city, zipCode, Phone, State, AddressName,cardType, cardNo, cardCVV, Email, promo, GC);
	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 13)
	public void BNcardFieldsDisableCheck(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC){
		
			genericMethods = new GenericMethods();			
			checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
			myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
			SubCategory = "Scarves";
			loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
			
			//-------------------Empty My Bag module ---------------			
			CurrentPageTitle=genericMethods.getTitle(driver);		
			if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
				myBagCheckoutReusable.emptyMyBag(driver, "WHS");
			}else{			
				myBagCheckoutReusable.emptyMyBag(driver, "BNY");
			}
			
			//-------------------Empty My Bag module ends---------------
			checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
			checkoutBillingReusable.GuestUS(driver, category, SubCategory,firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
			checkoutBillingReusable.BNcardDisableFields(driver, category, SubCategory, firstName, lastName, address1,address2, city, zipCode, Phone, State, AddressName,cardType, cardNo, cardCVV, Email, promo, GC);
	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 14)
	public void InvalidCVVcheck(String category, String SubCategory,String firstName, String lastName, String address1,String address2, String city, String zipCode, String Phone,String State, String AddressName, String cardType, String cardNo,String cardCVV, String Email, String promo, String GC) {
		genericMethods = new GenericMethods();
		SubCategory = "Scarves";
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		//-------------------Empty My Bag module ---------------
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
	//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.CVVValidation(driver, category,SubCategory, firstName, lastName, address1, address2, city,zipCode, Phone, State, AddressName, cardType, cardNo,cardCVV, Email, promo, GC);


	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 15)
	public void CardImagesAndCVVTooltipIconVerification(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC)
			 {
		
		genericMethods = new GenericMethods();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		SubCategory = "Scarves";
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver,category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,
						firstName, lastName, address1, address2, city, zipCode,
						Phone, State, AddressName, cardType, cardNo, cardCVV,
						Email, promo, GC);
			checkoutBillingReusable.CardImagesAndCVVTooltipIconVerification(driver,
					 category, SubCategory, firstName, lastName,
					address1, address2, city, zipCode, Phone, State,
					AddressName, cardType, cardNo, cardCVV, Email, promo, GC);


	}

	@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 16)
	public void GuestUserEditingBillingAddress(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC)
			 {
		genericMethods = new GenericMethods();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		SubCategory = "Scarves";
		//-------------------Empty My Bag module ---------------
		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,
						firstName, lastName, address1, address2, city, zipCode,
						Phone, State, AddressName, cardType, cardNo, cardCVV,
						Email, promo, GC);

			checkoutBillingReusable.EditBillingAddress(driver,
					category, SubCategory, firstName, lastName, address1,
					address2, city, zipCode, Phone, State, AddressName,
					cardType, cardNo, cardCVV, Email, promo, GC);

	}

	@Test(groups = "CheckoutBillingDev", dataProvider = "BillingFormData", priority = 17)
	public void RegisteredUserEditingPaymentAndBillingAddress(String category,
			String SubCategory, String firstName, String lastName,
			String address1, String address2, String city, String zipCode,
			String Phone, String State, String AddressName, String cardType,
			String cardNo, String cardCVV, String Email, String promo, String GC)
			 {		

		genericMethods = new GenericMethods();	
		loginRegisterReusable = new Desk_LoginRegister_Reusable();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		myaccountPayment = new Desk_MyAccountPayment_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		address1 = "575 fifth avenue";
		address2 = "12th floor";
		city = "New York";
		zipCode = "10017";
		
		//-------------------Empty My Bag module ---------------
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		loginRegisterReusable.registerFromHeader(driver);		
		myaccountPayment.MyAccountPaymentMethod(driver,firstName, lastName, address1, address2, city, zipCode, Phone,State, AddressName, cardType, cardNo, cardCVV, Email);
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.LoggedInUS(driver, category,SubCategory, firstName, lastName, address1, address2, city,zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.BillingPaymentSavedEdit(driver,category, SubCategory, firstName, lastName, address1,address2, city, zipCode, Phone, State, AddressName,cardType, cardNo, cardCVV, Email, promo, GC);

	}

	
@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 18)
	public void ApplyGiftCard(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC)
			 {
	
		genericMethods = new GenericMethods();		
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		SubCategory = "Scarves";
		
		//-------------------Empty My Bag module ---------------		
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver,category,SubCategory);
		checkoutBillingReusable.GuestUS(driver,category, SubCategory,firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.BillingGCAdd(driver, category,SubCategory, firstName, lastName, address1, address2, city,zipCode, Phone, State, AddressName, cardType, cardNo,cardCVV, Email, promo, GC);
	}

@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 19)
	public void RemoveGiftCard(String category, String SubCategory,
			String firstName, String lastName, String address1,
			String address2, String city, String zipCode, String Phone,
			String State, String AddressName, String cardType, String cardNo,
			String cardCVV, String Email, String promo, String GC)
			 {
		
		genericMethods = new GenericMethods();
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		SubCategory = "Scarves";
		
		//-------------------Empty My Bag module ---------------	
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.BillingGCDelete(driver, category,SubCategory, firstName, lastName, address1, address2, city,zipCode, Phone, State, AddressName, cardType, cardNo,cardCVV, Email, promo, GC);
	}

@Test(groups = "checkoutBillingReusable", dataProvider = "BillingFormData", priority = 20)
	public void EditBillingAndCheckPromoGC(String category, String SubCategory,String firstName, String lastName, String address1,String address2, String city, String zipCode, String Phone,String State, String AddressName, String cardType, String cardNo,String cardCVV, String Email, String promo, String GC) {
		genericMethods = new GenericMethods();
		SubCategory = "Scarves";
		loginRegister_PageObjects = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		checkoutBillingReusable = new Desk_CheckoutBilling_Reusable();
		
		//-------------------Empty My Bag module ---------------
		myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckoutReusable.emptyMyBag(driver, "WHS");
		}else{			
			myBagCheckoutReusable.emptyMyBag(driver, "BNY");
		}
		
	//-------------------Empty My Bag module ends---------------
		checkoutBillingReusable.AddToCartProcess(driver, category,SubCategory);
		checkoutBillingReusable.GuestUS(driver, category, SubCategory,firstName, lastName, address1, address2, city, zipCode,Phone, State, AddressName, cardType, cardNo, cardCVV,Email, promo, GC);
		checkoutBillingReusable.EditBillingAndCheckPromoGC(driver, category, SubCategory, firstName, lastName, address1, address2, city, zipCode, Phone, State, AddressName, cardType, cardNo, cardCVV, Email, promo, GC);

	}
*/

	@AfterMethod(groups = "After_Method")
	public void Stop() {
		driver.quit();
		GenericMethods.Waitformilliseconds(2000);
	}

	@DataProvider(name = "BillingFormData")
	public static Object[][] ProvideDataToAboveMeth() throws IOException {
		ReturnDataObject returnData1 = new ReturnDataObject();
		return returnData1.read("src/test/java/testDataFiles/desktop/CheckoutBillingUS.xls","Sheet1");
	}

	@DataProvider(name = "MyAccountFormData")
	public static Object[][] ProvideDataToAboveMeth1() throws IOException {
		ReturnDataObject returnData1 = new ReturnDataObject();
		return returnData1.read("src/test/java/testDataFiles/desktop/MyAccountTestCase.xls","Sheet1");
	}

}
