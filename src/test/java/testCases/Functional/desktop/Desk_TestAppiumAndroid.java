package testCases.Functional.desktop;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import net.sf.cglib.core.MethodWrapper.MethodWrapperKey;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;




import reusableModules.iPhone.Desk_MobileBNYLogin_Reusable;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import io.appium.java_client.android.AndroidDriver;

public class Desk_TestAppiumAndroid {
	{
		System.setProperty("atu.reporter.config", "atu.properties");
		
	}
	public WebDriver  driver;
	GenericMethods genericMethods;
	Desk_BNY_HomePageObject LoginToBNY_Objects;
	Desk_MobileBNYLogin_Reusable login;
	
	@BeforeMethod(groups = "Before_Method" )
	@Parameters({"BROWSER","URL"})
	public void setUp(@Optional String Browser,String URL)
	{
		DesiredCapabilities capabilities= new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.BROWSER_NAME,"Chrome");
		capabilities.setCapability("platform","Android");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "xiaomi-mi_3w-b178afb");
		capabilities.setCapability("version","4.4.4");
		try
		{
			driver=new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities) ;
			//driver= new AppiumDrive(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			
		} 
		catch(Exception e) 
		{
			//TODO Auto-generated catch block
			e.printStackTrace();
		}
		//driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
	}
	
	@Test(groups = "Appium" )
	public void testWebBrowser() throws InterruptedException
	{genericMethods = new GenericMethods();
		LoginToBNY_Objects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		driver.get("https://www.barneys.com");
		ATUReports.add("check if Screen Shot is taken on Mobile", "","",true,LogAs.INFO);
		//driver.findElement(By.xpath("//li/ul[2]/li/a[contains(text(),'Log In')]")).click();
		login=new Desk_MobileBNYLogin_Reusable();
		try {
			login.Login(driver, "gvarma0904@yopmail.com", "12345678");;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@AfterMethod
	public void tearDown() 
		{
			driver.close();
			driver.quit();
		}
	
	/*public void takeMobileScreenShot(WebDriver driver)
	{
		String path;
		path = "C:\\MobileScreens\\" + file.getName();
		driver = new Augmenter().augment(driver);
		File file  = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(file, new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	public void takeMobileScreenShot(WebDriver driver,String className,String methodName)
	{
		//String scrFolder1 = "c:/Result/"+ new SimpleDateFormat("yyyy_MM_dd_HHmmss").format(Calendar.getInstance().getTime()).toString();
		String scrFolder="C:\\MobileScreens\\" + className+"\\"+methodName;
		new File(scrFolder).mkdir();
	    
	  //get the screenshot folder location from enviroment variable set in beginning of test
		driver = new Augmenter().augment(driver);
	    File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	    //copy screenshot to screenshot folder
	    try {
			FileUtils.copyFile(scrFile,new File(scrFolder+ "/screenshot" + new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime()).toString()+ ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

