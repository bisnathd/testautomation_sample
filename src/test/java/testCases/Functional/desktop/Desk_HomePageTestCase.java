package testCases.Functional.desktop;

/*  
 HP_TC_01_verifyHomePageTickerFunctionality: This test case verifies ticker arrow click functionality on BNY
 HP_TC_02_verifyHomePageSiteHeaderElements: This test case verifies SiteHeader displays Warehouse Link,The Window link,The LogIn link,The RegisterLink,The HeartIcon.
 HP_TC_03_verifyHomePageGlobalNavElements : This test case verify if the Top category is hovered
 HP_TC_04_verifyHomePageHeroBanner : This test case verify the arrow on Hero image is clickable.
 HP_TC_05_verifyHomePagePersonalizedSpot : This test case verify the Personalized Spot, Secondary Content Spot and We Adore sectionTest Case for Homepage Personalized Spot
 HP_TC_06_verifyHomePageSocialIntegration : This test case verify the Social Integration icon is clicked on homepage. 
*/

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import atu.testng.reports.utils.Utils;
import reusableModules.desktop.Desk_GlobalNav_Reusable;
import reusableModules.desktop.Desk_HomePage_Reusable;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import setup.MultipleBrowser;
import reusableModules.desktop.Desk_Logout_Reusable;

public class Desk_HomePageTestCase extends MultipleBrowser {

	GenericMethods genericMethods;
	WebDriverWait xplicitWait;
	Desk_HomePage_Reusable homePage_Reusable;
	Desk_LoginRegister_Reusable loginRegister_Reusable;
	Desk_GlobalNav_Reusable globalNav;
	String categoryName = "Women";
	String subCategoryName = "Platforms";
	Desk_Logout_Reusable logoutReusable;
	String bnyURL;
	String whsURL;

	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
		super.Browser=Browser;
		super.Device=Device;
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		//driver.get(URL);
		bnyURL= bny_URL;
		whsURL= whs_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}
	
	
//HP_TC_01_verifyHomePageTickerFunctionality: This test case verifies ticker arrow click functionality on BNY
	@Test(groups={"HomePage","Regression"},priority=2,testName="HP_TC_01_verifyHomePageTickerFunctionality",enabled=true) 
	public void HP_TC_01_verifyHomePageTickerFunctionality() {
		ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
		ATUReports.setTestCaseReqCoverage("This test case verifies ticker arrow click functionality on BNY");	
		homePage_Reusable = new Desk_HomePage_Reusable();		 
		logoutReusable= new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		homePage_Reusable.leftTickerArrowClick(driver);
		homePage_Reusable.rightTickerArrowClick(driver);
	} 
	

	//HP_TC_02_verifyHomePageSiteHeaderElements: This test case verifies SiteHeader displays Warehouse Link,The Window link,The LogIn link,The RegisterLink.	
	@Test(groups={"HomePage","Regression"},priority=1,testName="HP_TC_02_verifyHomePageSiteHeaderElements",enabled=true) 
	public void HP_TC_02_verifyHomePageSiteHeaderElements() {
		ATUReports.setAuthorInfo("Adapty QA Team", "5th May 2016","1.0");
		ATUReports.setTestCaseReqCoverage("HomePage - Site Header Elements");
		homePage_Reusable = new Desk_HomePage_Reusable();
		loginRegister_Reusable=new Desk_LoginRegister_Reusable();
		logoutReusable= new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		homePage_Reusable.verifyWarehouseLinkOnUtilityNav(driver);
		homePage_Reusable.verifyTheWindowLinkOnUtilityNav(driver);
		homePage_Reusable.verifyTopLoginLinkClickable(driver);
		homePage_Reusable.verifyTopRegisterLinkClickable(driver);
		
	}
	
	/* HP_TC_03_verifyHomePageGlobalNavElements : This test case verify if the Top category is hovered */
	@Test(groups={"HomePage","Regression","BlackBox"},priority=3,testName="HP_TC_03_verifyHomePageGlobalNavElements",enabled=true) 
	public void HP_TC_03_verifyHomePageGlobalNavElements() {
		ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
		ATUReports.setTestCaseReqCoverage("HomePage - This test case verify if the Top category is hovered");		
		homePage_Reusable = new Desk_HomePage_Reusable();
		logoutReusable= new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		homePage_Reusable.globalNavElements(driver);
	}
	
	/* HP_TC_04_verifyHomePageHeroBanner : This test case verify the arrow on Hero image is clickable */
	 @Test(groups={"HomePage","Regression"},priority=4,testName="HP_TC_04_verifyHomePageHeroBanner",enabled=true) 
	 public void HP_TC_04_verifyHomePageHeroBanner() {
		ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
		ATUReports.setTestCaseReqCoverage("HomePage - This test case verify the arrow on Hero image is clickable");		
		homePage_Reusable = new Desk_HomePage_Reusable();
		logoutReusable= new Desk_Logout_Reusable();
		driver.get(bnyURL);
		homePage_Reusable.heroBannerRightArrowClick(driver);   // write a code to ignore if arrow is  not present.
		homePage_Reusable.heroBannerLeftArrowClick(driver);
	} 
	 
	 /* HP_TC_05_verifyHomePagePersonalizedSpot : This test case verify the Personalized Spot, Secondary Content Spot and We Adore sectionTest Case for Homepage Personalized Spot */
	 @Test(groups={"HomePage","Regression"},priority=5,testName="HP_TC_05_verifyHomePagePersonalizedSpot",enabled=true)
	 public void HP_TC_05_verifyHomePagePersonalizedSpot() {
		ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
		ATUReports.setTestCaseReqCoverage("HomePage - This test case verify the Personalized Spot, Secondary Content Spot and We Adore sectionTest Case for Homepage Personalized Spot");		
		homePage_Reusable = new Desk_HomePage_Reusable(); 
		logoutReusable= new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		homePage_Reusable.verifyPresenceOfSecondryCOntentSpot(driver);
		homePage_Reusable.verifyPresenceOfWeAdoreContentSpot(driver);
		homePage_Reusable.verifyPresenceOfPersonalizedCOntentSpots(driver);
	}
	
	 /* HP_TC_06_verifyHomePageSocialIntegration : This test case verify the Social Integration icon is clicked on homepage.*/
	 @Test(groups={"HomePages","Regression","BlackBox"},priority=6,testName="HP_TC_06_verifyHomePageSocialIntegration",enabled=true) 
	 public void HP_TC_06_verifyHomePageSocialIntegration() {
		ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
		ATUReports.setTestCaseReqCoverage("HomePage - This test case verify the Social Integration icon is clicked on homepage.");		
		homePage_Reusable = new Desk_HomePage_Reusable();
		logoutReusable= new Desk_Logout_Reusable();
		
		driver.get(bnyURL);
		homePage_Reusable.clickOnSocialIcons(driver,"facebook");
		homePage_Reusable.clickOnSocialIcons(driver,"twitter");
		homePage_Reusable.clickOnSocialIcons(driver,"pintrest");
		homePage_Reusable.clickOnSocialIcons(driver,"instagram");
		homePage_Reusable.clickOnSocialIcons(driver,"youtube");
		homePage_Reusable.clickOnSocialIcons(driver,"google");
		homePage_Reusable.clickOnSocialIcons(driver,"snapchat");
		homePage_Reusable.clickOnSocialIcons(driver,"tumblr");
	}
	 
	 


	
	@AfterMethod(groups = "After_Method")
	public void Stop(){
		GenericMethods.Waitformilliseconds(2000);		
		driver.quit();
	}
}
