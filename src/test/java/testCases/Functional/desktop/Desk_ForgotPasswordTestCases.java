package testCases.Functional.desktop;
/*
 fgtPwd_TC_01_inValidEmailCheck : This test case will check for forgot password when Invalid Email Is passed in Email Field.
 fgtPwd_TC_02_validEmailCheck : This test case will check for forgot Password when Valid Email Is passed in Email Field.
 fgtPwd_TC_03_blankEmailCheck : This test case will check for forgot password when NO Email Is passed in Email Field.

 */

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import reusableModules.desktop.Desk_LoginRegister_Reusable;
import setup.MultipleBrowser;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.utils.Utils;
import reusableModules.desktop.Desk_Logout_Reusable;


public class Desk_ForgotPasswordTestCases extends MultipleBrowser{

	GenericMethods genericMethods;
	Desk_LoginRegister_Reusable forgotPasswordReusable;
	Desk_Logout_Reusable logoutReusable;
	String bnyURL;
	String whsURL;

	@BeforeMethod(groups = "Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(@Optional String Device, String Browser,String bny_URL,String whs_URL){
		super.Browser=Browser;
		super.Device=Device;
		super.setuptest(Device,Browser);
		driver.manage().window().maximize();
		//driver.get(URL);
		bnyURL= bny_URL;
		whsURL= whs_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);		
	}
	 
	 //----------------------MyCart page test case -------------------	
//	fgtPwd_TC_01_inValidEmailCheck : This test case will check for forgot password when Invalid Email Is passed in Email Field.
@Test(groups={"forgotPassword","Regression"}, priority=1,testName="fgtPwd_TC_01_inValidEmailCheck",enabled=true)
	 public void fgtPwd_TC_01_inValidEmailCheck(){
		ATUReports.setAuthorInfo("Adapty QA", "26th May 2016","1.0");
	    ATUReports.setTestCaseReqCoverage("ForgotPassword - This test case will check for forgot password when Invalid Email Is passed in Email Field");	  
	    forgotPasswordReusable = new Desk_LoginRegister_Reusable();
	    logoutReusable= new Desk_Logout_Reusable();	 
	    driver.get(bnyURL);
	    logoutReusable.logOutifLoggedIn(driver);
	    forgotPasswordReusable.forGotPassword("invalid", driver, "alliswellyopmail.com");   	  
	  }

//fgtPwd_TC_02_validEmailCheck : This test case will check for forgot Password when Valid Email Is passed in Email Field.
	 @Test(groups={"forgotPassword","Regression"}, priority=2,testName="fgtPwd_TC_02_validEmailCheck",enabled=true)
	 public void fgtPwd_TC_02_validEmailCheck(){
		ATUReports.setAuthorInfo("Adapty QA", "26th May 2016","1.0");
	    ATUReports.setTestCaseReqCoverage("ForgotPassword - This test case will check for forgot Password when Valid Email Is passed in Email Field.");	  
	    forgotPasswordReusable = new Desk_LoginRegister_Reusable();
	    logoutReusable= new Desk_Logout_Reusable();	
	    driver.get(bnyURL);
	    logoutReusable.logOutifLoggedIn(driver);	    
	    forgotPasswordReusable.forGotPassword("valid", driver, "hal@yopmail.com");   	  
	  }

	 
//fgtPwd_TC_03_blankEmailCheck : This test case will check for forgot password when NO Email Is passed in Email Field.
@Test(groups={"forgotPassword","Regression"}, priority=3,testName="fgtPwd_TC_03_blankEmailCheck",enabled=true)
	 public void fgtPwd_TC_03_blankEmailCheck(){
		ATUReports.setAuthorInfo("Adapty QA", "26th May 2016","1.0");
	    ATUReports.setTestCaseReqCoverage("ForgotPassword - This test case will check for forgot password when NO Email Is passed in Email Field.");	  
	    forgotPasswordReusable = new Desk_LoginRegister_Reusable();	    
	    logoutReusable= new Desk_Logout_Reusable();	 
	    driver.get(bnyURL);
	    logoutReusable.logOutifLoggedIn(driver);
	    forgotPasswordReusable.forGotPassword("empty", driver, "");   	  
	  }
	 
@AfterMethod(groups = "After_Method")
	public void Stop(){	
		GenericMethods.Waitformilliseconds(2000);
		driver.quit();		
	}
}
