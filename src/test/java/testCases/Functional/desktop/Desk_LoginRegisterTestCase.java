package testCases.Functional.desktop;
/*
 LR_TC_01_loginToBNYtopLoginLK : This test case verify if the migrated user is  able to Login from homepage using top Login Link.
 LR_TC_02_registerToBNYtopRegisterLK : This test case verify the user is  able to register from homepage using top Register link.
 LR_TC_03_ExistingUserRegisterTC : This test case check if existing user is able to register again.
 
 */
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import reusableModules.desktop.Desk_Logout_Reusable;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_LoginRegistrationPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import actions.ReturnDataObject;
import actions.Tools;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import reusableModules.desktop.Desk_LoginRegister_Reusable;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
//import reusableModules.desktop.Desk_PDPBuyProduct_Reusable;
import setup.MultipleBrowser;
import setup.WinAuth;


public class Desk_LoginRegisterTestCase extends MultipleBrowser {
	
	GenericMethods genericMethods;	
	WebDriverWait xplicitWait ;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	//Desk_PDPBuyProduct_Reusable buyProduct;
	Desk_Logout_Reusable logoutReusable;
	Desk_BNY_DesignerPageObject bnyDesigner;
	Desk_BNY_LoginRegistrationPageObject LoginRegister;
	Desk_MYBagCheckout_Reusable CheckOutMyBag;
	Desk_BNY_HomePageObject LoginToBNY_Objects;
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	String Email="kaling46@yopmail.com";
	String Password="12345678";
	String ConfirmPassword="12345678";
	Desk_BNY_DesignerPageObject Designer_Objects;
	Desk_Logout_Reusable Logout;
	String bnyURL;
	String whsURL;
	Tools tools;
	String login;

	@BeforeMethod(groups ="Before_Method")
	@Parameters({"DEVICE","BROWSER","BNYURL","WHSURL"})
	public void Start(String Device, String Browser,String Bny_URL,String Whs_URL){
	super.Browser=Browser;
	super.Device=Device;
	super.setuptest(Device, Browser);
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	bnyURL=Bny_URL;
	whsURL=Whs_URL;
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
// LR_TC_01_loginToBNYtopLoginLK : This test case verify if the migrated user is  able to Login from homepage using top Login Link.
	@Test(groups={"LoginRegister1","Regression"},dataProvider="LoginRegister",priority=1,testName="LR_TC_01_loginToBNYtopLoginLK",enabled=true)
	public void LR_TC_01_loginToBNYtopLoginLK(String username,String password)
  {
		ATUReports.setTestCaseReqCoverage("MigratedUserLogin : This test case verify if the migrated user is  able to Login from homepage using top Login Link.");
		
		driver.get(bnyURL);
		WinAuth.aitoIT_AuthHandling(driver, "barneys", "style");
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.loginFromHeader(driver, username, password);
		
	}

// LR_TC_02_registerToBNYtopRegisterLK : This test case verify the user is  able to register from homepage using top Register link.
	 @Test(groups={"LoginRegister","Regression"},priority=2,dataProvider="LoginRegister",testName="LR_TC_02_registerToBNYtopRegisterLK",enabled=true)
	 public void LR_TC_02_registerToBNYtopRegisterLK(String username,String password)  {
		ATUReports.setTestCaseReqCoverage("RegisterFromHomePage : This test case verify the user is  able to register from homepage using top Register link");
		driver.get(bnyURL);
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		//loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.registerFromHeader(driver);
		//GenericMethods.Waitformilliseconds(5000);
		//loginRegisterReusable.VerifyPage(driver, genericMethods, "Home Page");
	}
	 	 
// LR_TC_03_ExistingUserRegisterTC : This test case check if existing user is able to register again.
	@Test(groups={"LoginRegister","Regression"},priority=3,dataProvider="LoginRegister",testName="LR_TC_03_ExistingUserRegisterTC",enabled=true)
	public void LR_TC_03_ExistingUserRegisterTC(String Username,String Password)  {
		ATUReports.setTestCaseReqCoverage("ExistingUserRegisterFromHomePage : This test case check if existing user is able to register again");
		loginRegisterReusable=new Desk_LoginRegister_Reusable();
		logoutReusable= new Desk_Logout_Reusable();		
		driver.get(bnyURL);
		loginRegisterReusable.verifyHomePage(driver);
		loginRegisterReusable.registerFromHeader(driver);
		logoutReusable.logOutifLoggedIn(driver);
		login= loginRegisterReusable.sTime;
		System.out.println(login);
		loginRegisterReusable.registerWithExistingMailID(driver, login+"@barneys.com", "12345678", "12345678");
	}
	
/*
//========================*********Test Case 1 starts*********===============================
 
@Test(groups="LoginRegister",priority=1)
public void BNYCatBrowseLogin()  {

	ATUReports.setTestCaseReqCoverage("Login -Login on Clicking heart icon from category browse page");
	genericMethods =  new GenericMethods();
	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	loginRegisterReusable=new BNYLoginRegister();	
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	loginRegisterReusable.CatBrowseLogin(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}else{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");	
	}
	Login=new BNYLogin();	
	Login.GeneralLogin(driver,  "gvarma0904@yopmail.com","12345678");
	GenericMethods.Waitformilliseconds(5000);
	 loginRegisterReusable.VerifyPage(driver, genericMethods, "Category Browse");
}

@Test(groups="LoginRegister1",priority=2)
public void BNYCatBrowseRegister()  {
	ATUReports.setTestCaseReqCoverage("Register -Register on Clicking heart icon from category browse page");
	genericMethods =  new GenericMethods();		
	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	
	
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);				
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	loginRegisterReusable.CatBrowseLogin(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}else{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");	
	}
		
	genericMethods.Click(driver, LoginRegister.Register_Here_LK);
	ATUReports.add("User clicks on Register here link", true,LogAs.INFO);
	String RegisterPopup;
	RegisterPopup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (RegisterPopup=="notpresent"){
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register page is displayed", true,LogAs.PASSED);
	}else{
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	}	

	Register.GeneralRegister(driver,timeStamp);
	GenericMethods.Waitformilliseconds(5000);	
	 loginRegisterReusable.VerifyPage(driver, genericMethods, "Category Browse");

}

@Test(groups="LoginRegister",priority=3)
public void BNYPDPLogin()  {
	ATUReports.setTestCaseReqCoverage("Login -Login on Clicking heart icon from PDP page");
	genericMethods =  new GenericMethods();
	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Dresses");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	CategoryProductClick.browseProductandClickonBNY(driver,"2000");
	ATUReports.add("User is redirected to PDP  page", true,LogAs.INFO);
	
		
	loginRegisterReusable.PDPLogin(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent")
	{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else
	{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	
	}
	Login=new BNYLogin();	
	Login.GeneralLogin(driver,  "gvarma0904@yopmail.com","12345678");
	GenericMethods.Waitformilliseconds(5000);
	 loginRegisterReusable.VerifyPage(driver, genericMethods, "PDP");

}

@Test(groups="LoginRegister",priority=4)
public void BNYPDPRegister()  {
	
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	ATUReports.setTestCaseReqCoverage("Register -Register on Clicking heart icon from PDP page");
	genericMethods =  new GenericMethods();
	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Dresses");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	CategoryProductClick.browseProductandClickonBNY(driver,"2000");
	ATUReports.add("User is redirected to PDP  page", true,LogAs.INFO);
	System.out.println(timeStamp );
	loginRegisterReusable=new BNYLoginRegister();	
	loginRegisterReusable.PDPLogin(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	
	}
	Register=new BNYRegistration();	
	genericMethods.Click(driver, LoginRegister.Register_Here_LK);
	ATUReports.add("User clicks on Register here link", true,LogAs.INFO);
	String RegisterPopup;
	RegisterPopup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (RegisterPopup=="notpresent"){
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");

	}	
	Register.GeneralRegister(driver,timeStamp);
	GenericMethods.Waitformilliseconds(5000);
	loginRegisterReusable.VerifyPage(driver, genericMethods, "PDP");	
}

@Test(groups="LoginRegister",priority=5)
public void BNYCheckoutLogin()  {	
	ATUReports.setTestCaseReqCoverage("Login -Login from Checkout popup");
	genericMethods =  new GenericMethods();
	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	CategoryProductClick.browseProductandClickonBNY(driver,"1000");
	ATUReports.add("User is redirected to PDP  page", true,LogAs.INFO);
	buyProduct=new PDPBuyProduct();
	buyProduct.PDPSectionSelectColourandSize(driver);	
	CheckOutMyBag=new MYBagCheckout();
	CheckOutMyBag.SelectUnitedStatesLogIn(driver);
	ATUReports.add("Guest user clicks Checkout button on My Bag", true,LogAs.INFO);
	loginRegisterReusable=new BNYLoginRegister();
	loginRegisterReusable.MYBAGCheckoutLogin(driver, genericMethods, Email, Password);
	GenericMethods.Waitformilliseconds(5000);
}

@Test(groups="LoginRegister1",priority=6)
public void BNYCheckoutRegister()  {
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	ATUReports.setTestCaseReqCoverage("Register from Checkout popup");
	genericMethods =  new GenericMethods();
	loginRegisterReusable=new Desk_LoginRegister_Reusable();
	
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GenericMethods.Waitformilliseconds(1000);
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	CategoryProductClick.browseProductandClickonBNY(driver,"1000");
	buyProduct=new PDPBuyProduct();
	ATUReports.add("User is redirected to PDP  page", true,LogAs.INFO);
	buyProduct.PDPSectionSelectColourandSize(driver);
	CheckOutMyBag=new MYBagCheckout();
	CheckOutMyBag.SelectUnitedStatesLogIn(driver);
	ATUReports.add("Guest user clicks Checkout button on My Bag", true,LogAs.INFO);
	loginRegisterReusable=new BNYLoginRegister();
	loginRegisterReusable.MYBAGCheckoutRegister(driver, genericMethods);
	Register.CheckoutRegister(driver, genericMethods,timeStamp);
	GenericMethods.Waitformilliseconds(5000);
}

@Test(groups="LoginRegister",priority=7)
public void BNYDesignerDLPLogin()  {
	ATUReports.setTestCaseReqCoverage("Login from DLP");
	genericMethods =  new GenericMethods();
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	Designer_Objects = PageFactory.initElements(driver, BNYDesignerObject.class);
	GlobalNavActions.CategoryMouseHover(driver, "womens");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "All Designers (A-Z)");
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on 'All Designers (A-Z)' from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to All Designers page page", true,LogAs.INFO);
	bnyDesigner=new reusableModules.BNYDesigner();
	loginRegisterReusable=new BNYLoginRegister();
	loginRegisterReusable.DesignerDLPAdd(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent")
	{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else
	{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	
	}
	Login=new BNYLogin();	
	Login.GeneralLogin(driver,  "gvarma0904@yopmail.com","12345678");
	GenericMethods.Waitformilliseconds(5000);
	String DesignerButton,DesignerPage;
	DesignerPage= genericMethods.isElementPresent(driver, "//div[@class='in-store-buttons']/a");
	if (DesignerPage.equalsIgnoreCase("present")) {
		ATUReports.add("Verify Designer page is displayed",  "Designer page should be displayed", "Designer page is displayed", true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer page is displayed",  "Designer page should be displayed", "Designer page is not displayed", true,LogAs.FAILED);
		Assert.assertEquals("Designer Page is displayed" , "Designer page is not displayed");
	}
	DesignerButton=genericMethods.getText(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN);
	
	if (DesignerButton.equalsIgnoreCase("In My Designers")||DesignerButton.equalsIgnoreCase("Remove from My Designers")) {
		ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is added", true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is not added", true,LogAs.FAILED);
		Assert.assertEquals(DesignerButton, "In My Designers");
	}			
}

@Test(groups="LoginRegister",priority=8)
public void BNYDesignerDLPRegister()  {
	ATUReports.setTestCaseReqCoverage("Registering a user from DLP");
	genericMethods =  new GenericMethods();
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	Designer_Objects = PageFactory.initElements(driver, BNYDesignerObject.class);
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	GlobalNavActions.CategoryMouseHover(driver, "womens");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "All Designers (A-Z)");
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on 'All Designers (A-Z)' from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to All Designers page page", true,LogAs.INFO);
	bnyDesigner=new reusableModules.BNYDesigner();
	loginRegisterReusable=new BNYLoginRegister();
	loginRegisterReusable.DesignerDLPAdd(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else
	{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	
	}
	Register=new BNYRegistration();	
	genericMethods.Click(driver, LoginRegister.Register_Here_LK);
	ATUReports.add("User clicks on 'Register Here'link from login page", true,LogAs.INFO);
	String RegisterPopup;
	RegisterPopup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (RegisterPopup=="notpresent"){
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	}		
	Register.GeneralRegister(driver,timeStamp);
	GenericMethods.Waitformilliseconds(5000);
	String DesignerButton;
	String DesignerPage= genericMethods.isElementPresent(driver, "//div[@class='in-store-buttons']/a");
	if (DesignerPage.equalsIgnoreCase("present")) {
		ATUReports.add("Verify Designer page is displayed",  "Designer page should be displayed", "Designer page is displayed", true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer page is displayed",  "Designer page should be displayed", "Designer page is not displayed", true,LogAs.FAILED);
		Assert.assertEquals("Designer Page is displayed" , "Designer page is not displayed");
	}
	DesignerButton=genericMethods.getText(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN);
	if (DesignerButton.equalsIgnoreCase("In My Designers")||DesignerButton.equalsIgnoreCase("Remove from My Designers")) {
		ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is added", true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is not added", true,LogAs.FAILED);
		//Assert.assertEquals(DesignerButton, "In My Designers");
	}
}

@Test(groups="LoginRegister",priority=9)
public void BNYDesignerFilterLogin()  {
	ATUReports.setTestCaseReqCoverage("Login from My Designer filter");
	genericMethods =  new GenericMethods();
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	Designer_Objects = PageFactory.initElements(driver, BNYDesignerObject.class);
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	loginRegisterReusable=new BNYLoginRegister();	
	loginRegisterReusable.MyDesignerFilterLogin(driver, genericMethods);
	ATUReports.add("Guest User clicks on My Designers filter", true,LogAs.INFO);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	
	}
	Login=new BNYLogin();	
	Login.GeneralLogin(driver,  "gvarma0904@yopmail.com","12345678");
	GenericMethods.Waitformilliseconds(5000);
	loginRegisterReusable.VerifyPage(driver, genericMethods, "Category Browse");	
}

@Test(groups="LoginRegister1",priority=10)
public void BNYDesignerFilterRegister()  {
	ATUReports.setTestCaseReqCoverage("Register from My Designer filter");
	genericMethods =  new GenericMethods();
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	Designer_Objects = PageFactory.initElements(driver, BNYDesignerObject.class);
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	genericMethods =  new GenericMethods();
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	loginRegisterReusable=new BNYLoginRegister();	
	loginRegisterReusable.MyDesignerFilterLogin(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	}
	Register=new BNYRegistration();	
	genericMethods.Click(driver, LoginRegister.Register_Here_LK);
	String RegisterPopup;
	RegisterPopup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (RegisterPopup=="notpresent"){
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	}		
	Register.GeneralRegister(driver,timeStamp);
	GenericMethods.Waitformilliseconds(5000);
	loginRegisterReusable.VerifyPage(driver, genericMethods, "Category Browse");
}

@Test(groups="LoginRegister1",priority=11)
public void BNYDesignerEditLogin()  {
	ATUReports.setTestCaseReqCoverage("Login from My Designer 'Edit' edit link on Category browse page");
	genericMethods =  new GenericMethods();
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	Designer_Objects = PageFactory.initElements(driver, BNYDesignerObject.class);
	genericMethods =  new GenericMethods();
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	loginRegisterReusable=new BNYLoginRegister();	
	loginRegisterReusable.MyDesignerEditLogin(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	}
	Login=new BNYLogin();	
	Login.GeneralLogin(driver,  "gvarma0904@yopmail.com","12345678");
	GenericMethods.Waitformilliseconds(5000);
	loginRegisterReusable.VerifyPage(driver, genericMethods, "My Designers");
}

@Test(groups="LoginRegister1",priority=12)
public void BNYDesignerEditRegister()  {
	ATUReports.setTestCaseReqCoverage("Register from My Designer 'Edit' edit link on Category browse page");
	genericMethods =  new GenericMethods();
	LoginRegister = PageFactory.initElements(driver, BNYLoginRegistrationObject.class);
	Designer_Objects = PageFactory.initElements(driver, BNYDesignerObject.class);
	String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	ATUReports.setTestCaseReqCoverage("Login -Login on Clicking heart icon from category browse page");
	genericMethods =  new GenericMethods();
	GlobalNavActions.CategoryMouseHover(driver, "Women");
	GlobalNavActions.clickSubCategoryName(driver, genericMethods, "Hats");	
	ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
	ATUReports.add("User clicks on Category from expanded navigation", true,LogAs.INFO);
	ATUReports.add("User is redirected to category browse page", true,LogAs.INFO);
	loginRegisterReusable=new BNYLoginRegister();	
	loginRegisterReusable.MyDesignerEditLogin(driver, genericMethods);
	String Popup;
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (Popup=="notpresent"){
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
	}
	else{
		ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	}
	Register=new BNYRegistration();	
	genericMethods.Click(driver, LoginRegister.Register_Here_LK);
	String RegisterPopup;
	RegisterPopup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
	if (RegisterPopup=="notpresent"){
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register page is displayed", true,LogAs.PASSED);
	}else{
		ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register popup is  displayed", true,LogAs.FAILED);
		Assert.assertEquals("Page present", "Popup present");
	}		
	Register.GeneralRegister(driver,timeStamp);
	GenericMethods.Waitformilliseconds(5000);
	loginRegisterReusable.VerifyPage(driver, genericMethods, "My Designers");
}

*/

	@DataProvider(name = "LoginRegister")
	 public static Object[][] provideDataToChnagePasswordTC() throws IOException{    
	     ReturnDataObject returnData1=new ReturnDataObject();
	     return returnData1.read("src/test/java/testDataFiles/desktop/Login.xls","LoginRegister");  
	  }
	

@AfterMethod(groups = "After_Method")
	public void Stop(){		
		driver.quit();
		GenericMethods.Waitformilliseconds(2000);	
	}
}
