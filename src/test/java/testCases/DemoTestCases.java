package testCases;


import java.io.IOException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.Desk_BNY_RegistrationPageObject;
import actionsAndSetup.GenericMethods;
import actionsAndSetup.MultipleBrowser;
import actionsAndSetup.WinAuth;
import actionsAndSetup.dataprovider.ReturnDataObject;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import reusable.ProductionReusable;

public class DemoTestCases extends MultipleBrowser {

 GenericMethods genericMethods;
 Desk_BNY_RegistrationPageObject Registtration_PageObjects;
 ProductionReusable loginRegisterReusable;
 ProductionReusable myBagCheckout_Reusable;
 ProductionReusable myAccountLanding_Reusable;
 ProductionReusable checkoutshipping_Reusable;
 ProductionReusable categoryProductClick_Reusable;
 ProductionReusable pdp_Reusable;
 ProductionReusable globalNav_Reusable;
 ProductionReusable checkoutBilling_Reuable;
 ProductionReusable orderReview_Reuable;
 String login, bnyURL, whsURL;
 String currentPageTitle;



 @BeforeMethod(groups = "beforeMethod")
 @Parameters({"BROWSER","BNYURL"})
 public void Start( String Browser, String bny_URL) {
	 super.setuptest(Browser);
	 driver.get(bny_URL);
	 GenericMethods.Waitformilliseconds(2000);
	 WinAuth.aitoIT_AuthHandling();
 }

 
 @Test(groups = "Demo", dataProvider = "ShippingFormData", priority = 1, testName = "PassInStockProductCheck", enabled = true)
 public void PassInStockProductCheck(String email, String oldPassword, String country, String FName, String LName, String ShippingAddress1, String ShippingAddress2, String ShippingCity, String ShippingState, String ShippingZip, String ShippingPhone, String CardType, String CardNo, String CardCVV, String BillingAddress1, String BillingCity, String BillingState, String BillingZip, String Promocode, String BarneysUrl1, String WareHouseUrl2) {
	ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
	ATUReports.setTestCaseReqCoverage("Test case to verify only instock product and add them to cart page.");
		 
		 
	  loginRegisterReusable = new ProductionReusable();
	  globalNav_Reusable = new ProductionReusable();
	  categoryProductClick_Reusable = new ProductionReusable();
	  pdp_Reusable = new ProductionReusable();
	  myBagCheckout_Reusable = new ProductionReusable();
	  checkoutshipping_Reusable = new ProductionReusable();
	  myAccountLanding_Reusable = new ProductionReusable();
	  checkoutBilling_Reuable = new ProductionReusable();
	  orderReview_Reuable = new ProductionReusable();
	  genericMethods = new GenericMethods();
  
	  
  //----------------------Category Browse--------------------------
 
	  globalNav_Reusable.clickSubCategoryName(driver, "Women", "Clothing");
	  ATUReports.add("Check Expanded Navigation", "Global nav should be expanded", "Its Expanded", "", LogAs.PASSED,new  CaptureScreen(ScreenshotOf.BROWSER_PAGE));
	  categoryProductClick_Reusable.browseProductandClickonBNY(driver, "500");
	  GenericMethods.waitForPageLoaded(driver);

  //----------------------------PDP---------------------------------			
	  pdp_Reusable.selectAvailableColorOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.selectAvailableSizeOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.AddtoCartButtononPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopuponPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
	  GenericMethods.waitForPageLoaded(driver);

  //------------------My Bag(Logged In User)---------------------------------
	  myBagCheckout_Reusable.selectCountry(driver, country);
	  GenericMethods.waitForPageLoaded(driver);
	  myBagCheckout_Reusable.clickCheckoutButton(driver);
	  GenericMethods.waitForPageLoaded(driver);

 }

 @Test(groups = "Demo", dataProvider = "ShippingFormData", priority = 2, testName = "FailInStockProductCheck", enabled = true)
 public void FailInStockProductCheck(String email, String oldPassword, String country, String FName, String LName, String ShippingAddress1, String ShippingAddress2, String ShippingCity, String ShippingState, String ShippingZip, String ShippingPhone, String CardType, String CardNo, String CardCVV, String BillingAddress1, String BillingCity, String BillingState, String BillingZip, String Promocode, String BarneysUrl1, String WareHouseUrl2) {
	ATUReports.setAuthorInfo("Adapty QA Team", Utils.getCurrentTime(),"1.0");
	ATUReports.setTestCaseReqCoverage("This test case will not check for available size instead will click on any visible size and will fail.");		 
	  loginRegisterReusable = new ProductionReusable();
	  globalNav_Reusable = new ProductionReusable();
	  categoryProductClick_Reusable = new ProductionReusable();
	  pdp_Reusable = new ProductionReusable();
	  myBagCheckout_Reusable = new ProductionReusable();
	  checkoutshipping_Reusable = new ProductionReusable();
	  myAccountLanding_Reusable = new ProductionReusable();
	  checkoutBilling_Reuable = new ProductionReusable();
	  orderReview_Reuable = new ProductionReusable();
	  genericMethods = new GenericMethods();
  
  //----------------------Category Browse--------------------------
 
	  globalNav_Reusable.clickSubCategoryName(driver, "Women", "Clothing");
	  ATUReports.add("Check Expanded Navigation", "Global nav should be expanded", "Its Expanded", "", LogAs.PASSED,new  CaptureScreen(ScreenshotOf.BROWSER_PAGE));
	  categoryProductClick_Reusable.browseProductandClickonBNY(driver, "500");
	  GenericMethods.waitForPageLoaded(driver);

  //----------------------------PDP---------------------------------			
	  pdp_Reusable.selectAvailableColorOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.SimpleselectAvailableSizeOnPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.AddtoCartButtononPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopuponPDP(driver);
	  GenericMethods.waitForPageLoaded(driver);
	  pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
	  GenericMethods.waitForPageLoaded(driver);

  //------------------My Bag(Logged In User)---------------------------------
	  myBagCheckout_Reusable.selectCountry(driver, country);
	  GenericMethods.waitForPageLoaded(driver);
	  myBagCheckout_Reusable.clickCheckoutButton(driver);
	  GenericMethods.waitForPageLoaded(driver);

 }

 
 @AfterMethod(groups = "afterMethod")
	 public void Stop() {
	  driver.quit();
	  GenericMethods.Waitformilliseconds(2000);
	 }

 @DataProvider(name = "ShippingFormData")
	 public static Object[][] ProvideDataToAboveMeth() throws IOException {
	  ReturnDataObject returnData1 = new ReturnDataObject();
	  return returnData1.read("src/test/java/data/Shipping_billingPage.xls", "shippinig");
	 }


}