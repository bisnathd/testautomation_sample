package testCases;
/*
 LR_TC_01_loginToBNYtopLoginLK : This test case verify if the migrated user is  able to Login from homepage using top Login Link.
 LR_TC_02_registerToBNYtopRegisterLK : This test case verify the user is  able to register from homepage using top Register link.
 LR_TC_03_ExistingUserRegisterTC : This test case check if existing user is able to register again.
 RegisterAndExistingUserLogin : This test case check user can register and also login again back.
 */

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import actionsAndSetup.GenericMethods;
import actionsAndSetup.MultipleBrowser;
import actionsAndSetup.testCaseReflection.TestCaseExcelRead;
import actionsAndSetup.testCaseReflection.ReflectionClass;
import atu.testng.reports.ATUReports;
import reusable.LoginReusable;

public class Desk_LoginRegisterTestCase extends MultipleBrowser{
	
	//WebDriver driver;
	GenericMethods genericMethods;	
	WebDriverWait xplicitWait ;
	LoginReusable loginRegisterReusable;
	/*String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	String Email="kaling46@yopmail.com";
	String Password="12345678";
	String ConfirmPassword="12345678";*/
	String login;

	@BeforeMethod(groups ="beforeMethod")
	@Parameters({"BROWSER","BNYURL"})
	public void Start(String Browser,String Bny_URL){
		super.setuptest(Browser);
		driver.get(Bny_URL);	
	}

	// LR_TC_01_loginToBNYtopLoginLK : This test case verify if the migrated user is  able to Login from homepage using top Login Link.
		@Test(groups={"LoginRegister","Regression","Login1"},priority=1,testName="LR_TC_01_loginToBNYtopLoginLKExcel",enabled=true)
		public void LR_TC_01_loginToBNYtopLoginLKExcel() throws InvocationTargetException{
			
			GenericMethods.Waitformilliseconds(5000);
			try {
				Runtime.getRuntime().exec("D:/Workspace/NewFrameWorkTemplate/src/test/java/lib/Authentication.exe");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ReflectionClass runReusableMethods = new ReflectionClass();
			ATUReports.setAuthorInfo("Govind", "20th July", "1");
			ATUReports.setTestCaseReqCoverage("This is for positive Login and Password Test case");
			
			runReusableMethods.invokeReusableMethods("src\\test\\java\\testCaseSteps\\testcaseSheet.xls","login1");
		}
		

@AfterMethod(groups = "afterMethod")
	public void Stop(){		
		driver.quit();
		GenericMethods.Waitformilliseconds(2000);	
	}
}
