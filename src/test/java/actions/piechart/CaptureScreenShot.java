package actions.piechart;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

import org.openqa.selenium.WebDriver;

public class CaptureScreenShot {

	
	public static void captureScreenshot( WebDriver driver) {
		//take the screenshot of the entire home page and save it to a png file
		Screenshot screenshot = new AShot().shootingStrategy(new ViewportPastingStrategy(1000)).takeScreenshot(driver);
		
		try {
			ImageIO.write(screenshot.getImage(), "PNG", new File("src\\test\\java\\actions\\prodSanity\\Appd.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}
}
