package actions;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
//import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.Reporter;

import setup.MultipleBrowser;

public class Tools {
	
	//private static final Logger log = Logger.getLogger(Tools.class);
	//protected static ReadPropertiesFile properties = new ReadPropertiesFile();

	public static String generateRandomFilename(Throwable arg0) {
		Calendar c = Calendar.getInstance();
		String filename = arg0.getMessage();
		//log.info("screen capture file name " + filename);
		int i = filename.indexOf('\n');
		filename = filename.substring(0, i).replaceAll("\\s", "_")
				.replaceAll(":", "")
				+ ".jpg";
		filename = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH)
				+ "-" + c.get(Calendar.DAY_OF_MONTH) + "-"
				+ c.get(Calendar.HOUR_OF_DAY) + "-" + c.get(Calendar.MINUTE)
				+ "-" + c.get(Calendar.SECOND) + "-" + filename;
		return filename;
	}

	public static String generateRandomNumber() {
		Calendar c = Calendar.getInstance();
		// String filename = arg0.getMessage();
		// generateRandomNumber = "" + c.get(Calendar.YEAR) +
		// "-" + c.get(Calendar.MONTH) +
		// "-" + c.get(Calendar.DAY_OF_MONTH) +
		// "-" + c.get(Calendar.HOUR_OF_DAY) +
		// "-" + c.get(Calendar.MINUTE) +
		// "-" + c.get(Calendar.SECOND) +
		// "-" + filename;
		String str = String.valueOf(c.get(Calendar.YEAR))
				+ String.valueOf(c.get(Calendar.MONTH))
				+ String.valueOf(c.get(Calendar.DAY_OF_MONTH))
				+ String.valueOf(c.get(Calendar.HOUR_OF_DAY))
				+ String.valueOf(c.get(Calendar.MINUTE))
				+ String.valueOf(c.get(Calendar.SECOND));
		//log.info("str" + str);
		return str;
	}

	public static String generateTenRandomNumber() {
		/*
		 * Calendar c = Calendar.getInstance(); String str =
		 * String.valueOf(c.get(Calendar.YEAR)) +
		 * String.valueOf(c.get(Calendar.MONTH)) +
		 * String.valueOf(c.get(Calendar.DAY_OF_MONTH)) +
		 * String.valueOf(c.get(Calendar.HOUR_OF_DAY)) +
		 * String.valueOf(c.get(Calendar.MINUTE)) +
		 * String.valueOf(c.get(Calendar.SECOND)); log.info("str" +
		 * str);
		 */
		Random rand = new Random();
		return String.valueOf(rand.nextInt(Integer.MAX_VALUE));
		// return str ;
	}

	public static String getMessageForImageFile(Throwable arg0) {
		// Calendar c = Calendar.getInstance();
		String filename = arg0.getMessage();
		//log.info("getmsgimage " + filename);
		int filelenght = filename.length();
		if (filelenght > 40) {
			filelenght = 10;
		}

		// int i = filename.indexOf('\n');

		if (filename.indexOf('\n') > 0) {
			// filename = filename.substring(0,
			// filename.indexOf('\n')).replaceAll("\\s", "_").replaceAll(":",
			// "");}
			filename = filename.replaceAll(
					"^[.\\\\/:*?\"<>|,]?[\\\\/:*?\"<>|]*", "");
		}
		filename = filename.substring(0, filelenght);
		return filename;
	}

	// Capture screenshot method
	// public static void xScreenShot(ITestResult tr) throws
	// InterruptedException {
	public static void ScreenShot(Throwable args, ITestResult tr)
			throws InterruptedException {
		try {

			String NewFileNamePath;

			java.awt.Dimension resolution = Toolkit.getDefaultToolkit()
					.getScreenSize();
			Rectangle rectangle = new Rectangle(resolution);

			// Get the dir path
			File directory = new File("\\src\\test\\java\\TestcaseScreenShots\\");
			// log.info(directory.getCanonicalPath());

			// get current date time with Date() to create unique file name
			DateFormat dateFormat = new SimpleDateFormat(
					"MMM_dd_yyyy__hh_mm_ssaa");
			// get current date time with Date()
			Date date = new Date();
			// log.info(dateFormat.format(date));

			// To identify the system
			// InetAddress ownIP = InetAddress.getLocalHost();
			// log.info("IP of my system is := "+ownIP.getHostAddress());

			NewFileNamePath = directory.getCanonicalPath()
					+ "\\Failed\\"
					// + "tr.getName()" + "___" + dateFormat.format(date) + "_"
					// + generateRandomFilename(args) + "___" +
					// dateFormat.format(date) + "_"
					// + tr.getName() + "_" + getMessageForImageFile(args) + "_"
					// + dateFormat.format(date) + "_"
					+ tr.getName() + "_" + "_" + dateFormat.format(date) + "_"
					+ ".png";
			// Print(NewFileNamePath);

			// Capture the screen shot of the area of the screen defined by the
			// rectangle
			Robot robot = new Robot();
			BufferedImage bi = robot.createScreenCapture(new Rectangle(
					rectangle));
			ImageIO.write(bi, "png", new File(NewFileNamePath));

			// NewFileNamePath = "<a href=" + NewFileNamePath + ">ScreenShot" +
			// "</a>";
			// Place the reference in TestNG web report
			Reporter.setCurrentTestResult(tr);
			// Reporter.log(NewFileNamePath);

			Reporter.log("<a href=\"" + NewFileNamePath + "\">ScreenShot</a>");
			Wait(3000);

		} catch (AWTException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void Wait(int MilliSec) throws InterruptedException {
		Thread.sleep(MilliSec);
	}

	public static boolean isTextPresent(WebDriver driver, String text) {
		WebElement tempText = driver.findElement(By.tagName("body"));
		// log.info("temp text " + tempText.getText());
		if (tempText.getText().contains(text)) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean isTextPresentNew(WebDriver driver, String text, By by)
			throws InterruptedException {
		WebElement tempText = driver.findElement(by);

		for (int second = 0; second < 20; second++) {
			try {

				if (tempText.isDisplayed()) {
					break;
				}
			} catch (Exception ignore) {
			}
			Tools.Wait(1000);
		}

		if (tempText.getText().contains(text)) {
			return true;
		} else {
			return false;
		}

	}

	public static void xScreenshot(Throwable args, ITestResult tr,
			WebDriver driver) {

		Calendar calendar = Calendar.getInstance();

		// Get the users home path and append the screen shots folder
		// destination
		String userHome = System.getProperty("user.dir");
		String screenShotsFolder = userHome + "\\Failed\\";

		// The file includes the the test method and the test class
		String testMethodAndTestClass = tr.getMethod().getMethodName() + "("
				+ tr.getTestClass().getName() + ")";

		System.out
				.println(" *** This is where the capture file is created for the Test \n"
						+ testMethodAndTestClass);

		// Create the filename for the screen shots
		String filename = screenShotsFolder + testMethodAndTestClass + "-"
				+ calendar.get(Calendar.YEAR) + "-"
				+ calendar.get(Calendar.MONTH) + "-"
				+ calendar.get(Calendar.DAY_OF_MONTH) + "-"
				+ calendar.get(Calendar.HOUR_OF_DAY) + "-"
				+ calendar.get(Calendar.MINUTE) + "-"
				+ calendar.get(Calendar.SECOND) + "-"
				+ calendar.get(Calendar.MILLISECOND) + ".png";

		// Take the screen shot and then copy the file to the screen shot folder

		// File scrFile =
		// ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		File scrFile = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		// File scrFile = ((Screenshot)driver).getScreenshotAs(file);

		try {
			FileUtils.copyFile(scrFile, new File(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void captureScreen(WebDriver driver) {

		String path;
		try {
			WebDriver augmentedDriver = new Augmenter().augment(driver);
			File source = ((TakesScreenshot) augmentedDriver)
					.getScreenshotAs(OutputType.FILE);
			path = "C:\\Automation\\" + source.getName();
			FileUtils.copyFile(source, new File(path));
		} catch (IOException e) {
			path = "Failed to capture screenshot: " + e.getMessage();
		}
		// return path;

	}

	public static void WebdDriverScreenShot(Throwable args, ITestResult tr,
			WebDriver driver) throws InterruptedException {
		try {

			String NewFileNamePath;

			java.awt.Dimension resolution = Toolkit.getDefaultToolkit()
					.getScreenSize();
			Rectangle rectangle = new Rectangle(resolution);

			// Get the dir path
			File directory = new File(".\\ScreenCapture");
			DateFormat dateFormat = new SimpleDateFormat(
					"MMM_dd_yyyy__hh_mm_ssaa");
			// get current date time with Date()
			Date date = new Date();

			NewFileNamePath = directory.getCanonicalPath()
					+ "\\Failed\\"
					+ tr.getName() + "_" + "_" + dateFormat.format(date) + "_"
					+ ".png";
			// Print(NewFileNamePath);

			// Capture the screen shot of the area of the screen defined by the
			// rectangle
			Robot robot = new Robot();
			BufferedImage bi = robot.createScreenCapture(new Rectangle(
					rectangle));
			ImageIO.write(bi, "png", new File(NewFileNamePath));

			WebDriver wd = new Augmenter().augment(driver);
			File f = ((TakesScreenshot) wd).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(f, new File(NewFileNamePath));
			} catch (IOException e) {
				e.printStackTrace();
			}

			NewFileNamePath = "<a href=" + NewFileNamePath + ">ScreenShot"
					+ "</a>";
			// Place the reference in TestNG web report
			Reporter.log(NewFileNamePath);
			Wait(3000);

		} catch (AWTException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// @Override
	public static void CaptureScreenShotAndPutInResult(ITestResult tr) {
		try {

			String parent = Constants.REPORT_SOURCE_PATH;
			String imgfile = Constants.SCREENCAPTURE_FOLDER_NAME + new SimpleDateFormat("MM-dd-yyyy_HH-ss-SSS").format(new Date().getTime())+ ".gif";

			File scrFile = null;
			//if(tr.getInstance().toString().contains("com.omedix.testscript.admin.")){
			 scrFile = ((TakesScreenshot) MultipleBrowser.Driver).getScreenshotAs(OutputType.FILE);
		
			
			FileUtils.copyFile(scrFile, new File(parent, imgfile));

			System.setProperty("org.uncommons.reportng.escape-output", "false");
		
			Reporter.log("<a href=" + imgfile + ">" + "<img src=" + imgfile + " alt=\"\"" + "height='100' width='100'/> " + "<br />");
			Reporter.setCurrentTestResult(null);
		} catch (Exception a) {
			a.printStackTrace();
		}
	}
	
	public boolean waitForMsg(String msg) {
		return waitForMsg(msg, 10);
	}
	
	public boolean waitForMsg(String msg, int time) {
		/*while(//check msg appear) {
			Tools.wait(1000);	
		}
		*/
		return false;		
	}

	
	// Check wheather element is present or not
	public static boolean  isElementPresent(WebDriver driver,By by)  
	  {  
	                driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);  
	                try  
	                {  
	                     driver.findElement(by);  
	                    return true;  
	                }  
	                catch(Exception e)  
	                {  
	                    return false;  
	                }  
	               finally  
	               {  
	                   driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);  
	               }  
	     }
	
	
	//Wait till page load	
	public static void waitForPageLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}
	
	public static void handleAuthPopup() {
		GenericMethods.SendKeysXTimes(KeyEvent.VK_ESCAPE, 1);
		/*GenericMethods.Waitformilliseconds(2000);
		GenericMethods.SendKeysXTimes(KeyEvent.VK_S, 1);//s
		GenericMethods.SendKeysXTimes(KeyEvent.VK_T, 1);//t
		GenericMethods.SendKeysXTimes(KeyEvent.VK_O, 1);//o
		GenericMethods.SendKeysXTimes(KeyEvent.VK_R, 1);//r
		GenericMethods.SendKeysXTimes(KeyEvent.VK_E, 1);//e
		GenericMethods.SendKeysXTimes(KeyEvent.VK_F, 1);//f
		GenericMethods.SendKeysXTimes(KeyEvent.VK_R, 1);//r
		GenericMethods.SendKeysXTimes(KeyEvent.VK_O, 1);//o
		GenericMethods.SendKeysXTimes(KeyEvent.VK_N, 1);//n
		GenericMethods.SendKeysXTimes(KeyEvent.VK_T, 1);//t
		
		GenericMethods.SendKeysXTimes(KeyEvent.VK_TAB, 1);
		
		GenericMethods.SendKeysXTimes(KeyEvent.VK_B, 1);//b
		GenericMethods.SendKeysXTimes(KeyEvent.VK_A, 1);//a
		GenericMethods.SendKeysXTimes(KeyEvent.VK_R, 1);//r
		GenericMethods.SendKeysXTimes(KeyEvent.VK_N, 1);//n
		GenericMethods.SendKeysXTimes(KeyEvent.VK_E, 1);//e
		GenericMethods.SendKeysXTimes(KeyEvent.VK_Y, 1);//y
		GenericMethods.SendKeysXTimes(KeyEvent.VK_S, 1);//s
		
		GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);*/
		
		Tools.typeString("strorefront");
		GenericMethods.SendKeysXTimes(KeyEvent.VK_TAB, 1);
		Tools.typeString("barneys");
		GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);
	}
	
	public static void typeString(String characters) {
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    StringSelection stringSelection = new StringSelection( characters );
	    clipboard.setContents(stringSelection, stringSelection);

	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	}
	
}