package actions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CaptureScreenShot {

	static Date date;
	static DateFormat _dateFormat = new SimpleDateFormat("ddMMHHmmss");
	
	public static void captureScreenshot( WebDriver driver,String runningMethod) {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		date = new Date();
		String dateTimeNow=_dateFormat.format(date).toString();
		//System.out.println(dateTimeNow);
		//System.out.println(runningMethod);
        try {
			org.apache.commons.io.FileUtils.copyFile(scrFile, new File("src\\test\\java\\TestcaseScreenShots\\"+dateTimeNow+"_"+runningMethod+".jpeg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
