package actions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class WriteRegistrationData {
	//String filepath="src/test/java/testDataFiles/New_user_registration.xls";
	public void writeDataToExcel(String filepath,String sheetName, String data){
		
		try {
			Workbook workbook = Workbook.getWorkbook(new File(filepath));
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			
			WritableWorkbook writableWorkbook=Workbook.createWorkbook(new File(filepath), workbook);
			WritableSheet writableSheet = writableWorkbook.getSheet(sheetName);
		
			int lastRow=writableSheet.getRows();
			Label writableData = new Label(0, lastRow, data);
			Label dateLabel = new Label(1, lastRow, (dateFormat.format(date)));
			writableSheet.addCell(writableData);
			writableSheet.addCell(dateLabel);
			
			writableWorkbook.write();
			writableWorkbook.close();
			

		} catch (IOException | WriteException | BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
	}

}
