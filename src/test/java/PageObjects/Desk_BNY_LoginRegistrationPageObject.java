package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_LoginRegistrationPageObject {

	
	@FindBy(xpath="//div[@id='search-result-items']//a/img")
	public WebElement First_Item_LK;
	
	@FindBy(xpath="//a[@id='user-register'][contains(text(),'Register')]")
	public WebElement MainMenu_TopRegister_LK;
	
	@FindBy(xpath="//div[@class='primary-content']/div[1]/div/div/div/p[1]/a")
	public WebElement Register_Here_LK;		
	
	@FindBy(xpath="//div[@id='search-result-items']//a[@class='user-login favorite-status hidden-xs']")
	public WebElement CategoryBrowse_HeartIcon_LK;
	
	@FindBy(xpath="//div[@class='product-add-to-cart']/form/fieldset/div[2]/div[1]/a[2]")
	public WebElement PDP_HeartIcon_LK;
	
	@FindBy(xpath="//form[@id='dwfrm_login']/fieldset/div[1]/div/input")
	public WebElement MYBAG_CheckoutEmail_TB;
	
	
	@FindBy(xpath="//form[@id='dwfrm_login']/fieldset/div[2]/div/input")
	public WebElement MYBAG_CheckoutPassword_TB;
	
	@FindBy(xpath="//form[@id='dwfrm_login']/fieldset/div[5]/button")
	public WebElement MYBAG_CheckoutLogin_BTN;
	
	@FindBy(xpath="//div[@class='form-row label-inline']/label/a")
	public WebElement MYBAG_CheckoutRegisterHere_LK;
	
	@FindBy(xpath="//div[@class='select-my-designers filterDesigners']/a[2]")
	public WebElement MyDesignerFilterProd_LK;
	
	@FindBy(xpath="//div[@class='select-my-designers filterDesigners']/a[2]")
	public WebElement MyDesignerFilterDev_LK;
	
	@FindBy(xpath="//div[@class='my-designers']/a[2]")
	public WebElement MyDesignerEditProd_LK;
	
	@FindBy(xpath="//div[@class='my-designers']/a[2]")
	public WebElement MyDesignerEditDev_LK;
	
	@FindBy(xpath="//div[@class='search-header']/h1")
	public WebElement CatBrowseTitle_LB;
	
	
	
}
