package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_CategoryLandingPageObject 
{	
	//Error message when fields are blank for shipping page 
	@FindBy(xpath="//a[@class='search']")
	public WebElement MainNavigation_Search_LK;
	
	@FindBy(xpath="//div[@class='whs-search hidden-xs']/a")
	public WebElement MainNavigation_WHS_Search_LK;
	
	@FindBy(id="atg_store_searchInput")
	public WebElement MainNavigation_Search_TB;
	
	@FindBy(xpath="//input[@type='submit']")
	public WebElement MainNavigation_WHS_SearchICON_LK;
	
	@FindBy(xpath="//div/h1[@class='browse-title hidden-xs']")
	public WebElement Search_PageTitle_TV;
	
	@FindBy(xpath="//div[@class='designer-banner']/div/h1/a")
	public WebElement DLP_ImageText_TV;
	
	@FindBy(xpath="//div[@id='ajaxContainer']//h1")
	public WebElement categoryLanding_pageHeading_TV;
	
	@FindBy(xpath="//div[@class='search-header']/h1")
	public WebElement categoryLanding_WHS_pageHeading_TV;
	
	@FindBy(xpath="//div[@class='searchResult-text']/h1")
	public WebElement zeroSearchResults_pageHeading_TV;
	
	@FindBy(id="sortBySelect")
	public WebElement categoryLanding_SortBy_DD;
	
	@FindBy(xpath="//li[contains (text(),48)]")
	public WebElement categoryLanding_TopView48_LK;
	
	@FindBy(xpath="//ul[@id='atg_store_pagination']//li[@class='ninety-Six active']")
	public WebElement categoryLanding_TopView96_LK;
	
	@FindBy(xpath="//ul[@id='atg_store_pagination']//li/a[contains (text(),'All')]")
	public WebElement categoryLanding_TopViewAll_LK;
	
	@FindBy(xpath="//a[contains(text(),'4x')]")
	public WebElement categoryLanding_View4x_LK;

	
	@FindBy(xpath="//a[contains(text(),'6x')]")
	public WebElement categoryLanding_View6x_LK;
	
	@FindBy(xpath="//a[contains(text(),'2')]")
	public WebElement categoryLanding_PageinationPage2_LK;
	
	@FindBy(xpath="//h1")
	public WebElement Page_title_TV;  

	@FindBy(xpath="//ul[@class='searchText search-designer']/li[3]/a")
    public WebElement Category_Designer3_CB;
	
	@FindBy(xpath="//div[@id='accordion']/div[@class='panel panel-default colorFamily']/div/h4/a/span[2]")
	public WebElement Category_ColorFilter_expand_Arrow;
	
	@FindBy(xpath="//div[@id='Color']//li/a")
	public WebElement Category_ColorFilter_colour1_CB;
	
	@FindBy(xpath="//div[@id='Size']//ul/li")
	public WebElement Category_SizeFilter1_CB;
	

	@FindBy(xpath="//div[@id='Material']//ul/li/a")
	public WebElement Category_MaterialFilter1_CB;
	
	@FindBy(xpath="//div[@id='accordion']//h4/a[contains(text(),'New Arrivals')]")
	public WebElement Category_NewArrivalFilter1_LK;
	
	
	@FindBy(xpath="//span[@class='title']/span[@class='userInput']")
	public WebElement SearchSuggestions_Heading_LK;
	
	//Locator for Top Login Link
	@FindBy(xpath="//div[@id='landing-header']/h6")
	public WebElement CategoryLanding_CategoryHeader_TXT;
	
	//Category browse page.
	@FindBy (xpath="//div[@id='atg_store_prodList']/ul")
	public WebElement ProductResultPage_48_96_All_TK;
	
}	
