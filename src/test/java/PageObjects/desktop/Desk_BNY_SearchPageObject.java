package PageObjects.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_SearchPageObject {
	
public WebDriver driver;
	
	//====================================OC Page Object for Global Nav Elements======================================

    @FindBy(className="search")
    public WebElement MainNavigation_Search_LK;
    
    @FindBy(xpath="//a[@class='search']")
	public WebElement BNY_HomePage_SiteHeader_Search;
    
    @FindBy(xpath="//a[@class='search-open']")
    public WebElement WHS_HomePage_SiteHeader_Search;
    
	@FindBy(id="atg_store_searchInput")
	public WebElement HomePage_Search_Box;
	
	@FindBy(id="atg_store_searchInput-sayt")
	public WebElement Search_Suggestion_List;
	
	@FindBy(xpath="//div[@class='textLabel']")
	public WebElement ZeroSearchResultPage_TryANewSearch_LB;

	@FindBy(xpath="//h1[@class='browse-title hidden-xs']")
	public WebElement SearchResultsPage_Title_LB;
	
	@FindBy(id="atg_store_newSearchInput")
	public WebElement ZeroSearchResultPage_TryANewSearch_TB;
	
	@FindBy(id="sortBySelect")
	public WebElement SearchResultsPage_SortBy_DD;
}
