package PageObjects.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_NavigationPageObject {
	
public WebDriver driver;
	
	//Locator for Top Login Link
	@FindBy(xpath="//ul[@class='topnav-level-1']/li/a[contains(text(),'New Arrivals')]")
	public WebElement TopNavigation_NewArrivals_LK;
	
	//div[@id='top-nav']//a[contains(text(),'Women')]
	@FindBy(xpath="//ul[@id='topnav-level-1']/li/a[contains(text(),'Women')]")
	public WebElement TopNavigation_Women_LK;
	
	@FindBy(xpath="//ul[@id='topnav-level-1']/li/a[contains(text(),'Men')]")
	public WebElement TopNavigation_Men_LK;
	
	@FindBy(xpath="//ul[@class='topnav-level-1']/li/a[contains(text(),'Beauty')]")
	public WebElement TopNavigation_Beauty_LK;
	
	@FindBy(xpath="//ul[@class='topnav-level-1']/li/a[contains(text(),'Home')]")
	public WebElement TopNavigation_Home_LK;
	
	@FindBy(xpath="//ul[@class='topnav-level-1']/li/a[contains(text(),'Kids')]")
	public WebElement TopNavigation_Kids_LK;
	
	@FindBy(xpath="//ul[@class='topnav-level-1']/li/a[contains(text(),'Editorial')]")
	public WebElement TopNavigation_Editorial_LK;
	
	@FindBy(xpath="//a[contains(text(),'Search')]")
	public WebElement TopNavigation_Search_LK;
	
	@FindBy(xpath="//a[contains(text(),'Barneys New York')]")
	public WebElement TopNavigation_BNY_LK;
	
	@FindBy(xpath="//a[contains(text(),'Warehouse')]")
	public WebElement TopNavigation_Warehouse_LK;
	
	@FindBy(xpath="//a[contains(text(),'The Window')]")
	public WebElement TopNavigation_TheWindow_LK;
	
	
	@FindBy(xpath="//ul[@class='topnav-level-1']/li[2]/div/div/div[1]/ul/li[2]/ul/li[1]/a")
	public WebElement TopNavigation_WomenAllDesigner_LK;

    
    @FindBy (xpath="//ul[@class='hidden-xs myBag']/li/a[@id='myWishList']")
    public WebElement TopNavigation_HeartIcon_LK;

}
