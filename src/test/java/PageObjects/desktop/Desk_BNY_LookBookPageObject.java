package PageObjects.desktop;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;


public class Desk_BNY_LookBookPageObject
{
	
	@FindBy(xpath="//div[@id='top-nav']/ul/li/a[contains (text(),'Editorial')]")
	public WebElement Editorial_LK;
	
	@FindBy (xpath="//ul[@id='category-level-1']/li/a[contains (text(),'Lookbooks')]")
	public WebElement LeftNav_LookBook_LK;
	
	@FindBy (xpath="//div[@id='ajaxContainer']/div/h1")
	public WebElement LookbookLanding_PG;
	
	@FindAll({@FindBy(xpath = "//div[@id='ajaxContainer']/div/section[1]/article")})
	public List<WebElement> NoOfArticelsforWomen;
	
	@FindAll({@FindBy(xpath = "//div[@id='ajaxContainer']/div/section[2]/article")})
	public List<WebElement> NoOfArticelsforMen;
	
	@FindBy(xpath="//div[@id='ajaxContainer']/div/div[1]/h2/a")
	public WebElement LookbooksWomen_title;
	
	@FindBy(xpath="//div[@id='ajaxContainer']/div/div[2]/h2/a")
	public WebElement Lookbooksmen_title;
	
	@FindBy(xpath="//div[@id='ajaxContainer']/div/div[2]/span")
	public WebElement NoOfLookbooksForMen;
	
	@FindBy(xpath="//div[@id='ajaxContainer']/div/div[1]/span")
	public WebElement NoOfLookbooksForwomen;
	
	@FindBy(xpath="//div[@id='ajaxContainer']/div/section[1]/div/h4/a")
	public WebElement ViewMore_Women_LK;
	
	@FindBy(xpath="//div[@id='ajaxContainer']/div/section[2]/div/h4/a")
	public WebElement ViewMore_Men_LK;
	 
	//Look book for "Women" 
	
	@FindBy(css="div.public WebElement Editorial_LK;primary-logo > a")
	public WebElement Homepagelogo_LK;
	
	@FindBy(xpath="//div[@id='ajaxContainer']/div/div[1]/h2/a[contains(text(),'Women')]")
	public WebElement Women_Lookbooks_LK;

	@FindBy(xpath="//div[@id='ajaxContainer']/div/section/article[1]/a/h4")
	public WebElement Women_Lookbooks_1stArticle;
	
	//Look book for "Men" 
	
	@FindBy(xpath="//*[@id='primary']/section[1]/article[1]/a/img")
	public WebElement ShopIsabel_LK;
	
	@FindBy(xpath="//*[@id='main']/div[2]/div/div/section[1]/div[2]/div[1]/p")
	public WebElement ShopIsabel_gettext;
	                
	
	@FindBy(xpath="//div[@class='atg_store_main']//ul/li/button[contains(text(),'2')]")
	public WebElement pagination2_BTN;
	
	/*@FindBy(xpath="//*[@id='sp15isabel-2']/h4/a[1]")
	public WebElement pagination2_LK;*/

	@FindBy(xpath="//*[@id='sp15isabel-1']/h4/a[2]")
	public WebElement Viewallproduct_LK;
	
	//Look book for "Men" 
	
	@FindBy(xpath="//*[@id='primary']/section[2]/div[1]/h3/a")
	public WebElement Men_Lookbooks_link;
	
	@FindBy(xpath="//*[@id='primary']/section/article[1]/a/img")
	public WebElement RalphLaurenBlack_LK;
	
	@FindBy(xpath="//*[@id='main']/div[2]/div/div/section[1]/div[2]/div[1]/p")
	public WebElement RalphLaurenBlack_gettext;

	                
	@FindBy(xpath="//*[@id='main']/div[2]/div/section/div[1]/ul/li[2]/button")
	public WebElement Menpagination2_LK;

				
	@FindBy(xpath="//*[@id='sp15ralphlauren-2']/h4/a[2]")
	public WebElement MenViewallproduct_LK;
	
	//Social links
	
	@FindBy(xpath="//*[@id='openShare']/nav/ul/li[1]/a")
	public WebElement Facebook_LK;
	
	@FindBy(xpath="//*[@id='openShare']/nav/ul/li[2]/a")
	public WebElement twitter_LK;
	
	@FindBy(xpath="//*[@id='openShare']/nav/ul/li[3]/a")
	public WebElement Pinterest_LK;
 
	@FindBy(xpath="//*[@id='openShare']/nav/ul/li[4]/a")
	public WebElement Fancy_LK;
}
