package PageObjects.desktop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_OrderReviewPageObject {
	
	@FindBy(xpath="//div[@id='atg_store_checkoutProgressContainer']//span[@class='titleCheckoutStage']")//Order Review Page Title
	public WebElement orderReview_PageTitle_TV;
	
	@FindBy(xpath="//div[contains(@class,'atg_store_groupShippingAddress')]//div[contains(@class,'vcard')]")//Order Review Shipping Info
	public WebElement orderReview_shippingAddres_TV;
	
	@FindBy(xpath="//div[contains(@class,'order-billing confirmColumn')]//div[contains(@class,'vcard')]")//Order Review Billing Info
	public WebElement orderReview_billingAddress_TV;
	
	@FindBy(xpath="//div[contains(@class,'atg_store_groupPayment')]//span[@class='atg_store_groupPaymentCardType']")//Order Review payment Info
	public WebElement orderReview_paymentDetails_TV;
	
	@FindBy(id="atg_store_placeMyOrderButton")//Complete Purchase Button
	public WebElement orderReview_CompletePurchase_BTN;
	
	
	
	@FindBy(xpath="//form[@class='submit-order']/fieldset/div[2]/div[contains(text(),'There was an error processing your order. Please verify your shipping, payment and billing information, and try again.')]")
	public WebElement orderReview_CompletePurchase_ErrorMessage_TV;
	
	@FindBy(xpath="//table[@class='order-totals-table']/tbody/tr[1]/td[2]")
	public WebElement orderReview_billingPage_SubTotal_LB;
		
	@FindBy(xpath="//table[@id='cart-table']/tfoot/tr/td[3]/div/a")
	public WebElement orderReview_promoEdit_LK;
	
	@FindBy(xpath="//div[@id='primary']/div[1]/div[3]/a")
	public WebElement orderReview_billing_AddressEdit_LK;	
	
	@FindBy(xpath="//div[@id='primary']/div[1]/div[5]/a")
	public WebElement orderReview_billing_PaymentEdit_LK;
		
	@FindBy(xpath="//div[@id='primary']/div[1]//div[@class='mini-cc-name']")
	public WebElement PaymentCardHolderName_LB;
	
	@FindBy(xpath="//div[@id='primary']/div[1]/div[5]/div/div")
	public WebElement PaymentGiftCard_LB;
		
	@FindBy(xpath="//table[@id='cart-table']/thead/tr[1]/td[2]/a")
	public WebElement OrderReviewEditMyBag_LK;
	
	// Review page link
		@FindBy(xpath="//div[@id='primary']/div[1]/div[@class='row checkout-header']/h2")
		public WebElement ReviewOrderPage_PageTitle_TV;
		
		@FindBy(xpath="//div[@id='primary']/div[1]/div[2]/a")
		public WebElement ReviewOrder_EditShippingAddress_LK;
		
		@FindBy(xpath="//*[@id='primary']/div[1]/h2")
		public WebElement ShippingPage_GetText;
		
		@FindBy(xpath="//*[@id='primary']/div[1]/div[3]/a")
		public WebElement EditBillingreview_LK;
		
		@FindBy(xpath="//*[@id='primary']/div[1]/h2")
		public WebElement BillingPage_GetText;
		
		
		
		// Shipping pages links		
		@FindBy(xpath="//*[@id='edit-shipping-address']")
		public WebElement EditShipping_LK;
		
		@FindBy(id="dwfrm_singleshipping_shippingAddress_addressFields_firstName")
		public WebElement FirstName_TB;
		
		@FindBy(xpath="//*[@id='dwfrm_addForm']/div[2]/button/span")
		public WebElement UseOriganal_BTN;
		
		//Payment info link		
		@FindBy(xpath="//*[@id='primary']/div[1]/div[5]/a")
		public WebElement Edit_Payment_LK;
		
		
		@FindBy(id="dwfrm_billing_paymentMethods_creditCard_owner")
		public WebElement CardholderName_TB;
		
		//My Bag link 
		
		@FindBy(xpath="//*[@id='cart-table']/thead/tr[1]/td[2]/a")
		public WebElement Edit_MyBag_LK;
		                
		@FindBy(xpath="//*[@id='primary']/div[1]/h1")
		public WebElement Edit_MyBag_gettext;
		
		@FindBy(name="dwfrm_cart_shipments_i0_items_i0_quantity")
		public WebElement MyBagQuantity_TB;
		
		@FindBy(name="dwfrm_cart_updateCart")
		public WebElement Update_LK;
		
		@FindBy(xpath="//*[@id='checkout-form']/fieldset/button[2]")
		public WebElement ContinuefromCart_BTN;
		
		// Order summary section
						
		@FindBy(xpath="//table[@id='cart-table']/tbody/tr/td[4]/span[@class='mini-cart-price']")
		public WebElement PricefromCartpage_gettext;
						
		@FindBy(xpath="//*[@id='cart-table']/tbody/tr/td[4]/span[4]")
		public WebElement PricefromCartpagewithcoupon_gettext;
		
		@FindBy(xpath="//*[@id='primary']/div[2]/div[1]/table/tbody/tr[1]/td[2]")
		public WebElement Pricefromsummary_gettext;
		
		@FindBy(xpath="//div[@class='atg_store_orderSummary aside']//li[3]/span[2]")
		public WebElement taxfromSummary_gettext;
		
		@FindBy(xpath="//*[@id='primary']/div[2]/div[1]/table/tbody/tr[4]/td[2]")
		public WebElement Ordertotalfromsummary_gettext;
		
		
		// HAVE QUESTIONS? section 
		
		@FindBy(xpath="//div[@class='summary-page-sidebar col-xs-12  col-md-4 col-lg-3']//h3[contains(text(),'Have Questions?')]")
		public WebElement havequetion_gettext;
		
		
		@FindBy(xpath="//a[contains(text(),'webservices@barneys.com')]")
		public WebElement Email_LK;
		
		@FindBy(xpath="//a[contains(text(),'Shipping & Returns Policy')]")
		public WebElement policy_LK;
		
		// Coupon code section
		

		@FindBy(xpath="//*[@id='cart-table']/tfoot/tr/td[2]/div[1]")
		public WebElement Couponcode_gettext;
		
		@FindBy(xpath="//*[@id='cart-table']/tfoot/tr/td[4]")
		public WebElement CouponcodeApplied_gettext;
		
		@FindBy(xpath="//*[@id='cart-table']/tfoot/tr/td[2]/div[2]/span[2]")
		public WebElement CouponcodeNumber_gettext;
		
		@FindBy(xpath="//*[@id='cart-table']/tfoot/tr/td[3]/div/a")
		public WebElement EditCopon_LK;
		
		@FindBy(xpath="//*[@id='cart-table']/tbody/tr[2]/td[3]/button")
		public WebElement Remove_LK;
	
}
