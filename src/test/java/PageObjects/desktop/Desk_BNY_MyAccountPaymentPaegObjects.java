package PageObjects.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_MyAccountPaymentPaegObjects {

	//=========================OC Page Objects for Payment Options
	
	@FindBy(xpath="//ul[@class='left-nav']//a[contains(text(),'Payment Options')]")
	public WebElement MyAcc_LeftNav_Payment_LK;
	
	@FindBy(xpath="//span[contains(text(),'Add a Payment Option')]")
	public WebElement MyAcc_PaymentOptions_AddPaymentOption_BTN;
	
	@FindBy(id="atg_store_paymentInfoAddNewCardHolder")
	public WebElement AddPayment_CardHolderName_TB;
	
	@FindBy(id="atg_store_paymentInfoAddNewCardCardNumber")
	public WebElement AddPayment_CardNumber_TB;
	
	@FindBy(id="atg_store_paymentInfoAddNewCardMonth")
	public WebElement AddPayment_ExpirationMonth_DD;
	
	@FindBy(id="atg_store_paymentInfoAddNewCardYear")
	public WebElement AddPayment_ExpirationYear_DD;
	
	@FindBy(id="atg_store_paymentInfoAddNewCardSecurityCode")
	public WebElement AddPayment_CVV_TB;
	
	@FindBy(id="atg_store_firstNameInput")
	public WebElement AddPayment_BillingFirstName_TB;
	
	@FindBy(id="atg_store_lastNameInput")
	public WebElement AddPayment_BillingLastName_TB;
	
	@FindBy(id="atg_store_streetAddressInput")
	public WebElement AddPayment_BillingAddress1_TB;
	
	@FindBy(id="atg_store_streetAddressOptionalInput")
	public WebElement AddPayment_BillingAddress2_TB;
	
	@FindBy(id="atg_store_localityInput")
	public WebElement AddPayment_BillingCity_TB;
	
	@FindBy(id="atg_store_stateSelect")
	public WebElement AddPayment_BillingState_DD;
	
	@FindBy(id="atg_store_postalCodeInput")
	public WebElement AddPayment_BillingZipCodeTB;
	
	@FindBy(id="atg_store_emailInput")
	public WebElement AddPayment_BillingEmail_TB;
	
	@FindBy(id="atg_store_telephoneInput")
	public WebElement AddPayment_BillingPhone_TB;
	
	@FindBy(id="atg_store_paymentInfoAddNewCardAndAddressSubmit")
	public WebElement AddPayment_SAVE_BTN;
	
	@FindBy(xpath="//div[@id='atg_store_storedCreditCards']/ul/li[1]/div")
	public WebElement AddPayment_PaymentFirst_Box;
	
	@FindBy(xpath="//div[@id='atg_store_storedCreditCards']/ul/li[2]/div")
	public WebElement AddPayment_PaymentSecond_Box;
	
	@FindBy(xpath="//span[@id='cardRemove']")
	public WebElement AddPayment_Delete_Symbol;
	
	@FindBy(id="addressPopupDeleteYes")
	public WebElement AddPayment_DeleteThisCard_YES_BTN;
	
	@FindBy(xpath="//div[@id='atg_store_storedCreditCards']/ul/li[2]/div//input[@value='Make Default']")
	public WebElement AddPayment_PaymentSecondBox_MakeDefault_LK;
	
	@FindBy(id="atg_store_editPaymentSubmit")
	public WebElement EditPayment_SAVE_BTN;
	
	
	//=============================Old Page Objects====================================
	@FindBy(xpath="//div[@class='secondary-navigation']/ul/li[4]/a")
	public WebElement MyAcc_Payment_LK;
	
	@FindBy(xpath="//*[@class='create-wrapper']/a[1]")
	public WebElement MyAcc_AddPayment_BTN;
	
	@FindBy(xpath="//*[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_owner']")
	public WebElement MyAcc_CardHolderName_TB;
	
	@FindBy(xpath="//*[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']")
	public WebElement MyAcc_CardType_DD;
	
	@FindBy(xpath="//*[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']/option[@value='BN']")
	public WebElement MyAcc_CardTypeBN_DDOption;
	
	@FindBy(xpath="//*[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']/option[@value='Visa']")
	public WebElement MyAcc_CardTypeVisa_DDOption;
	
	@FindBy(xpath="//*[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']/option[@value='Amex']")
	public WebElement MyAcc_CardTypeAmex_DDOption;
	
	@FindBy(xpath="//*[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']/option[@value='MasterCard']")
	public WebElement MyAcc_CardTypeMasterCard_DDOption;
	
	@FindBy(xpath="//*[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']/option[@value='Discover']")
	public WebElement MyAcc_CardTypeDiscover_DDOption;
	
	@FindBy(xpath="//div/input[@id='atg_store_paymentInfoAddNewCardCardNumber']")
	public WebElement MyAcc_CardNumber_TB;
	
	@FindBy(id="dwfrm_paymentinstruments_creditcards_newcreditcard_month")
	public WebElement MyAcc_paymentExpMonth_DD;
	
	@FindBy(id="dwfrm_paymentinstruments_creditcards_newcreditcard_year")
	public WebElement MyAcc_paymentExpYear_DD;
	
	@FindBy(xpath="//div/input[@id='atg_store_paymentInfoAddNewCardSecurityCode']")
	public WebElement MyAcc_paymentCardCardCVV_TB;
	
	@FindBy(xpath="//div/input[@id='atg_store_firstNameInput']")
	public WebElement MyAcc_PaymentFirstName_TB;
	
	@FindBy(xpath="//div/input[@id='atg_store_lastNameInput']")
	public WebElement MyAcc_PaymentLastName_TB;
	
	@FindBy(xpath="//div/input[@id='atg_store_streetAddressInput']")
	public WebElement MyAcc_PaymentAddressOne_TB;
	
	@FindBy(id="dwfrm_profile_address_address2")
	public WebElement MyAcc_PaymentAddressTwo_TB;
	
	@FindBy(xpath="//div/input[@id='atg_store_localityInput']")
	public WebElement MyAcc_PaymentCity_TB;
	
	@FindBy(xpath="//div/select[@id='atg_store_stateSelect']")
	public WebElement MyAcc_PaymentState_DD;
	
	@FindBy(xpath="//select[@id='dwfrm_profile_address_states_state']/option[@value='NY']")
	public WebElement MyAcc_PaymentStateNewYork_DDOption;
	
	@FindBy(xpath="//div/input[@id='atg_store_postalCodeInput']")
	public WebElement MyAcc_PaymentZipCode_TB;
	
	@FindBy(xpath="//div/input[@id='atg_store_telephoneInput']")
	public WebElement MyAcc_PaymentPhone_TB;
	
	@FindBy(xpath="//div/input[@id='atg_store_emailInput']")
	public WebElement MyAcc_PaymentEmail_TB;
	
	@FindBy(xpath="//div/input[@id='atg_store_paymentInfoAddNewCardAndAddressSubmit']")
	public WebElement MyAcc_PayemntSave_BTN;
	
	
	@FindBy(xpath="//div[@id='payment-methods']/section[2]/ul/li[2]/div")
	public WebElement MyAcc_PayemntSecondBox_LB;
	
	@FindBy(xpath="//div[@id='atg_store_storedCreditCards']//li[1]/div[@class='accountadd']/dd")
	public WebElement MyAcc_PayemntFirstBox_LB;
	
	@FindBy(xpath="//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all delete-confirmation ui-draggable']/div[2]/form/div/fieldset/button")
	public WebElement MyAcc_PayemntDeleteYes_BTN;
	
	@FindBy(xpath="//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all delete-confirmation ui-draggable']/div[2]/form/div/fieldset/a")
	public WebElement MyAcc_PayemntDeleteNo_BTN;
	
	
//================This Xpath for Error message on Payment option for field validation.=============================
	
	//BNY Account holder error message.
	@FindBy(xpath="//span[@id='atg_store_paymentInfoAddNewCardHolder-error']/span[@class='displayTextBny']")
	public WebElement MyAcc_ErrorCardHolderName_LB;
	
	//BNY Account card number error message.
	@FindBy(xpath="//span[@id='atg_store_paymentInfoAddNewCardCardNumber-error']/span[@class='displayTextBny']")
	public WebElement MyAcc_ErrorCardNumber_LB;
	
	@FindBy(xpath="//span[@id='atg_store_paymentInfoAddNewCardMonth-error']")
	public WebElement MyAcc_ErrorMonth_LB;
	
	@FindBy(xpath="//span[@id='atg_store_paymentInfoAddNewCardYear-error']")
	public WebElement MyAcc_ErrorYear_LB;
	
	@FindBy(xpath="//span[@id='atg_store_paymentInfoAddNewCardSecurityCode-error']")
	public WebElement MyAcc_ErrorSecurityCode_LB;

	
	@FindBy(xpath="//span[@id='atg_store_firstNameInput-error']")
	public WebElement MyAcc_ErrorFirstName_LB;		

	@FindBy(xpath="//span[@id='atg_store_lastNameInput-error']")
	public WebElement MyAcc_ErrorLastName_LB;		

	@FindBy(xpath="//span[@id='atg_store_streetAddressInput-error']")
	public WebElement MyAcc_ErrorAddressOne_LB;
	
	//BNY City error message.
	@FindBy(xpath="//span[@id='atg_store_localityInput-error']/span[@class='displayTextBny']")
	public WebElement MyAcc_ErrorCity_LB;
	
	@FindBy(xpath="//span[@id='atg_store_stateSelect-error']")
	public WebElement MyAcc_ErrorState_LB;
	
	@FindBy(xpath="//span[@id='atg_store_postalCodeInput-error']")
	public WebElement MyAcc_ErrorZip_LB;
	
	
	@FindBy(xpath="//span[@id='atg_store_emailInput-error']")
	public WebElement MyAcc_ErrorEmail_LB;
	
	//BNY Phone number error message.
	@FindBy(xpath="//span[@id='atg_store_telephoneInput-error']/span[@class='displayTextBny']")
	public WebElement MyAcc_ErrorPhone_LB;
}
