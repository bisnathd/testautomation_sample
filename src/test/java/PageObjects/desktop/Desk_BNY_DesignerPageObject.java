package PageObjects.desktop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_DesignerPageObject {
	
	//=====================OC Designer Page Objects=========================
	
	//The Designers Label boxes
	//xpath=//ul[@id='alpha-legend']//a[contains(text(),'Q')]
	
	//My Designers Link from Left Nav under My Account page
	@FindBy(xpath="//ul[@class='left-nav']//a[contains(text(),'My Designers')]")
	public WebElement MyAcc_LeftNav_MyDesigner_LK;
	
	//Alphabets section under Manage Designers section
	@FindBy(xpath="//ul[@id='alpha-legend']")
	public WebElement MyDesigner_DesignersAlphabets_Labels;
	
	//Designers List under Manage Designers section
	@FindBy(xpath="//ul[@class='edit-designer-list']")
	public WebElement MyDesigner_DesignersList;
	
	//xpath for added designers (in Registration flow) in the My Designer page to verify whether they are added or not
	@FindBy(xpath="//ul[@id='designer-list']/li/ul//a")
	public WebElement MyDesigner_AddedFirstDesigner_Name;
	
	@FindBy(xpath="//ul[@id='designer-list']")
	public WebElement MyDesigner_AddedDesigner_Names_Space;
	
	@FindBy(xpath="//ul[@class='edit-designer-list']//a")
	public WebElement MyDesigner_SearchedDesigner;
	//The First Designer in the List to be clicked/added to My Designers
	//For example //ul[@class='edit-designer-list']/li[4]//a
	/*@FindBy(xpath="//ul[@class='edit-designer-list']/li[1]//ul/li[1]/a")
	public WebElement MyDesigner_FirstDesignerInList_LK;*/
	
	@FindBy(xpath="//ul[@id='designer-list']/li/ul/li/a[1]")
	public WebElement MyDesigner_FirstDesignerInList_LK;
	
	//Clear All Designers link present in My Designer page
	@FindBy(xpath="//span[contains(text(),'Clear All Designers')]")
	public WebElement MyDesigner_ClearAllDesigners_LK;
	
	//Yes Button present on Delete All Designers Pop Up
	@FindBy(id="clearAllDesigners")
	public WebElement MyDesigner_DeleteAllDesignersPopUp_YES_BTN;
	
	//Search field under Manage Designers section
	@FindBy(xpath="//input[@id='livesearch']")
	public WebElement MyDesigner_Search_TB;
	
	@FindBy(xpath="//ul[@class='row']/li/a")
	public WebElement DesignerCheckbox_CB;	
	
	@FindBy(xpath="//ul[@id='designer-list']/li/ul/li/a")
	public WebElement DesignerAdded_LB;
		
	@FindBy(xpath="//div[@id='designerDeletion']//a[@id='clearAllDesigners']")
	public WebElement ClearDesignerYes_BTN;
	
	@FindBy(xpath="//div[@id='designerDeletion']//a[contains(text(),'no')]")
	public WebElement ClearDesignerNo_BTN;
	
	@FindBy(xpath="//ul[@id='designer-list']//a[contains(text(),'Saint Laurent')]")
	public WebElement DesignerSaintLaurent_LK;
	
	@FindBy(xpath="//ul[@id='alpha-legend']/li[21]/a")
	public WebElement DesignerstartS_LK;
		
	@FindBy(xpath="//ul[@class='topnav-level-1']/li[2]/div/div/div[1]/ul/li[2]/a")
	public WebElement WomenAllDesignerTopNav_LK;
	
	@FindBy(xpath="//ul[@class='topnav-level-1']/li[2]")
	public WebElement WomenTopNav_LK;	
	
	@FindBy(xpath="//input[@class='designerLoginRedirect bny-Button-gray-border']")
	public WebElement DLPGuestAddToMyDesigners_BTN;
	
	@FindBy(xpath="//div[@class='add-button']/a")
	public WebElement DLPLoggedinAddToMyDesigners_BTN;
	
	@FindBy(xpath="//div[@class='add-button']/a[contains(@class,'addToMyDesigner')]")
	public WebElement DLPLoggedinRemoveDesigner_BTN;
	
	@FindBy(xpath="//span[@class='designerNamelist ng-binding']")
	public WebElement DesignerName_CB;
	
	@FindBy(xpath="//div[@class='designer-banner']")
	public WebElement Designer_Banner;

	@FindBy (xpath="//ul[@id='designer-list']/li//li")
	public WebElement AddedDesignerlist;

	//============================Old Page Objects============================
	
	/*@FindBy(xpath="//div[@class='secondary-navigation']/ul/li[8]/a")
	public WebElement MyAcc_Designer_LK;
	
	@FindBy(xpath="//section[@class='account-top']/a")
	public WebElement AddDesigner_BTN;
	
	@FindBy(xpath="//ul[@class='edit-designer-list']/li[5]/ul/li[1]/a")
	public WebElement Designer_CB;
	
	@FindBy(xpath="//p[@class='standard-p clear-designers']")
	public WebElement ClearDesigner_LK;	
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']/ul/li/a[contains(text(),'My Designers')]")
	public WebElement MyAcc_Designers_LK;
	
	@FindBy(xpath="//span[@class='mock-checkbox']")//div[@class='designer-scroller']/div/ul/li[1]/ul/li[6]/a
	public WebElement DesignerCheckbox_CB;*/
	

}
