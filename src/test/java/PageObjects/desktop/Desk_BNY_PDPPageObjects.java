package PageObjects.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_PDPPageObjects {
	
public WebDriver driver;	
	
//-----------------*********Locator for Colour section*******-----------------

@FindBy(xpath="//div[@class='atg_store_pickerContainer']//div[contains(@class,'atg_store_colorPicker')]")
public WebElement PDP_ColorPicker_Element;

@FindBy(xpath="//div[@class='atg_store_pickerContainer']//div[@class='atg_store_colorPicker']//a[@class='colorSwatch color-active']")
public WebElement PDP_SelectableColour_LK;

@FindBy(xpath="//a[contains(@class,'colorSwatch color-active')]")
public WebElement PDP_Color_Selected_Element;

//-----------------*********Locator for Size section*******----------------- 
//Locator for Size section
@FindBy(xpath="//div[@class='atg_store_pickerContainer']//div[contains(@class,'atg_store_sizePicker')]")
public WebElement PDP_SizeSection_Element;

/*
@FindBy(xpath="//div[@class='product-variations']/ul[2]/li/div/ul/li[@class='emptyswatch unselectable']")
public WebElement PDP_Unselectable_Size_Element_LK;
*/
@FindBy(xpath="//div[@class='atg_store_pickerContainer']//div[contains(@class,'atg_store_sizePicker')]//span[@class='selector']//a[contains(@class,'atg_store_oneSize sizePicker')]")
public WebElement PDP_Selectable_Size_Element_LK;

@FindBy(xpath="//a[@class='atg_store_oneSize sizePicker  selected']")
public WebElement PDP_Selected_Size_Element_LK;  

@FindBy(xpath="//div[@class='product-variations']/ul[2]/li/div/ul[@class='swatches size']/li[@class='emptyswatch']")
public WebElement PDP_SelectSize_LK;
		
//-----------------------****For PDP Test Cases*******--------------------------------------------
		
		//@FindBy(id="Quantity")
		@FindBy(xpath="//input[@id='qty_input_id']")
		public WebElement QuantityField_TB;
		
		//div[@class='pdp-faves product-tile']
		@FindBy(xpath="//div[@class='favorites-icon']//span[@class='pdp-fav-hearticon']/a")
		public WebElement PDP_AddToFavForLoggedIn_icon_LK;
		
		//div[@class='pdp-faves product-tile']
		@FindBy(xpath="//div[@class='pdp-faves product-tile']/div[@class='favorites-icon']/a")
		public WebElement PDP_AddToFavForGuest_icon_LK;
		
		//div[@class='pdp-faves']/a[2]
		@FindBy(xpath="//div[@class='pdp-faves product-tile']//span[@class='favorites-arrow  hidden-xs hidden-md hidden-sm']")
		public WebElement PDP_AddToFav_iconArrow_LK;
		
		@FindBy(xpath="//div[@class='panel panel-default']//h4/a")
		public WebElement PDP_Deatails_accordian_LK;
		
		@FindBy(xpath="//div[@id='accordion']/div[2]/div/h4/a")
		public WebElement PDP_SizeFit_accordian_LK;
		
		@FindBy(xpath="//div[@id='accordion']/div[3]/div/h4/a")
		public WebElement PDP_Designer_accordian_LK;
		
		@FindBy(xpath="//div[@id='accordion']/div[4]/div/h4/a")
		public WebElement PDP_ShippingAndReturns_accordian_LK;
		
		@FindBy(xpath="//div[@class='atg_store_productImage']//a[contains (text(),'See all Images')]")
		public WebElement PDP_SeeAllImages_BTN;
		
		@FindBy(xpath="//div[@class='atg_store_productImage']//a[contains (text(),'Collapse Images')]")
		public WebElement PDP_CollapseAllImages_BTN;
		
		@FindBy(xpath="//a[@id='SIZE_CHART_WOMEN']")
		public WebElement PDP_SizeChart_Womens_BTN;
		
		@FindBy(xpath="//a[@id='SIZE_CHART_MEN']")
		public WebElement PDP_SizeChart_Men_BTN;
		
		@FindBy(xpath="//a[@id='SIZE_CHART_KIDS']")
		public WebElement PDP_SizeChart_Kids_BTN;
		
		@FindBy(xpath="//div[@class='close-size-chart']/a[contains(text(),'Close')]")
		public WebElement PDP_SizeChart_Close_BTN;
		
		
//-----------------------**********************---------------------------------------------		
		@FindBy(id="atg_behavior_addItemToCart")
		public WebElement PDP_BUY_BTN;
			
		//span[@class='price-sales']
		@FindBy(xpath="//div[@id='atg_store_picker']//div[@class='picker_price_attribute']")
		public WebElement PDP_SalesPrice_TV;
		
		@FindBy(xpath="//span[@class='mini-cart-price']")
		public WebElement PDP_miniCart_SalesPrice_TV;
//--------------------**********My BAG and Mini Cart**********---------------------------------------------		
		
		@FindBy(xpath="//table[@id='cart-table']/tbody/tr/td[@class='item-price']")
		public WebElement MYBAG_Product1_SalesPrice_TV;
		
		@FindBy(xpath="//table[@id='cart-table']/tbody/tr[2]/td[@class='item-price']")
		public WebElement MYBAG_Product2_SalesPrice_TV;
		
		@FindBy(xpath="//a[contains(text(),'Checkout')]")
		public WebElement MiniCart_Checkout_BTN;
		
		@FindBy(xpath="(//a[contains(@href, '/barneys-new-york/women')])[13]")
		public WebElement WomenFromHeader_LK;
		
		@FindBy(xpath="(//a[contains(text(),'Shoes')])[6]")
		public WebElement Shoes_LK;
		
		@FindBy(css="#bc5bwiaaiyjGwaaadkSiUg4pmi > div.product-image.grid_flexH > a.thumb-link > img.gridImg")
		public WebElement Shoesimg_LK;
		
		@FindBy(css="a.swatchanchor > img[alt=\"Prada Suede Wingtip Derbys - Shoes - Barneys.com\"]")
		public WebElement Shoesimgcolor_LK;
	  
		@FindBy(linkText="6.5")
		public WebElement ShoesSize_LK;
		
		@FindBy(linkText="Size and Fit")
		public WebElement ShoesSizeFit_LK;
		
		@FindBy(linkText="Details")
		public WebElement Detail_LK;
		
		@FindBy(linkText="About Prada")
		public WebElement AboutProduct_LK;
		
		@FindBy(linkText="Customer Service / Shipping & Returns")
		public WebElement Customer_LK;
		
		@FindBy(xpath="//a[contains(text(),'See All Images')]")
		public WebElement SeeAllImg_BTN;
		
		@FindBy(linkText="//a[contains(text(),'Collapse Images')]")
		public WebElement CloseImge_BTN;
			
		//@FindBy(id="add-to-cart")
		@FindBy(xpath="//input[@id='atg_behavior_addItemToCart']")
		public WebElement AddToCart_BTN;
		
		@FindBy(css="span.mini-cart-close")
		public WebElement Close_MiniCart_LK;

		
		@FindBy(xpath="//div[@id='productInfoContainer']/h2")
        public WebElement PDP_ProductName_LB;

		//div[@class='mini-cart-pricing']/span[@class='mini-cart-price']
		@FindBy(xpath="//div[@class='mini-cart-pricing']//span")
        public WebElement MiniCart_ProductSalesPrice_TV;
        
//****************** MY Bag ************************************
        
        @FindBy (xpath="//div[@class='product-tile']")
        public WebElement ProductImage_OnMyBag;
}
