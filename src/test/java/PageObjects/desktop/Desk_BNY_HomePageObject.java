package PageObjects.desktop;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_HomePageObject {
	
public WebDriver driver;

    //====================================OC Page Object for Ticker======================================

	@FindBy(xpath="//ul[@id='hpTicker']//button[@class='slick-prev slick-arrow']")
	public WebElement HomePage_Ticker_LeftArrow;
	
	@FindBy(xpath="//ul[@id='hpTicker']/button[@class='slick-next slick-arrow']")
	public WebElement HomePage_Ticker_RightArrow;
	
	@FindBy(xpath="//li[contains(@class,'slick-current slick-active')]")
	public WebElement HomePage_Ticker_ActiveTicker_TV;

	//====================================OC Page Object for Site Header======================================
	
	//@FindBy(class="warehouse-link")
	//@FindBy(className="warehouse-link")
	//a[contains
	@FindBy(xpath="//a[@class='barneyswarehouseLink']") 
	public WebElement HomePage_SiteHeader_WHS_LK;
	
	@FindBy(xpath="//a[@class='thewindowLink']")
	public WebElement HomePage_SiteHeader_THEWINDOW_LK;
	
	@FindBy(xpath="//div[@class='primary-logo']")
	public WebElement HomePage_BNY_SiteHeader_LOGO;
	
	@FindBy(xpath="//div[@class='customer-info floatright']//li[@class='favorites']/a[@id='myWishList']")
	public WebElement HomePage_SiteHeader_HeartIcon;
	
	@FindBy(xpath="//div[@id='top-nav']/ul[@id='topnav-level-1']")
	public WebElement HomePage_SiteHeader_TopNav_Elements;
	
	@FindAll({
		@FindBy(xpath="//div[@id='top-nav']/ul[@id='topnav-level-1']/li")
		})
	public List<WebElement> HomePage_SiteHeader_allTopGlobalNavElements_LK;
	
	@FindBy(xpath="//nav[@class='social clearfix']/ul[@class='list-unstyled list-inline']")
	public WebElement HomePage_SocialIntegration_List;
	
	@FindBy(xpath="//a[@class='search']")
	public WebElement HomePage_SiteHeader_Search;
	
	@FindBy(id="atg_store_searchInput")
	public WebElement HomePage_Search_Box;
	
	@FindBy(id="atg_store_searchInput-sayt")
	public WebElement HomePage_Search_Suggestion_List;
	
	@FindBy(xpath="div[@class='search-suggestion-box']/ul[@id='atg_store_searchInput-sayt']/div[@class='term-col']/li[1]")
	public WebElement HomePage_Search_FirstSuggestion;
	
	//====================================OC Page Object for top login link======================================
	@FindBy(xpath="//a[@title='Log in to your account']")
	public WebElement Home_UtilityNav_TopLogin_LK;
	
	@FindBy(xpath="//a[contains(text(),'Register')]")
	public WebElement Home_UtilityNav_TopRegister_LK;
	
    @FindBy(id="atg_store_registerLoginEmailAddress")
	public WebElement LoginPage_LoginPanel_Email_TB;
	
	@FindBy(id="atg_store_registerLoginPassword")
	public WebElement LoginPage_LoginPanel_Password_TB;

	@FindBy(id="atg_store_loginButton")
	public WebElement LoginPage_LoginPanel_LOGIN_BTN;
		
	@FindBy(xpath="//a[@class='userName']/span/span[@class='atg_store_loggedInUserName']")
	public WebElement Home_UtilityNav_LoggedInUser_LK;
	
	@FindBy(id="atg_store_registerLoginForm")
	public WebElement LoginPage_LoginPanel_FORM;
	
	@FindBy(xpath="//div[@class='home-recommendations clearfix']")
	public WebElement BNYHomePage_CONTENT;
	
	@FindBy(xpath="//div[@class='whs-home-recommendations row container']")
	public WebElement WHSHomePage_CONTENT;
	
	@FindBy(xpath="//span[@class='icon dropdown-toggle hidden-xs dropdown-button']")
	public WebElement Home_TopNav_AccountArrow_LK;
	
	@FindBy(xpath="//div[@class='headersection clearfix']//a[@class='userName']/span")
	public WebElement Home_TopNav_Account_LK;
	
	@FindBy(xpath="//a[contains(text(),'My Account')]")
    public WebElement Home_TopNav_DD_MyAccount_LK;
	
	@FindBy(xpath="//div[@id='atg_store_registerIntro']/h1[contains(text(),'Log in to your account')]")
    public WebElement Home_Login_Header_TX;
	
	@FindBy(xpath="//div[@id='atg_store_registerIntro']/h1[contains(text(),'Create An Account')]")
    public WebElement Home_Register_Header_TX;
	

	
	//==============================================OC Page Object for top LogOut link==========================================
	
	@FindBy(xpath="//a[contains(text(),'Log Out')]")
	public WebElement Home_UtilityNav_TopLogout_LK;
	
	//=============================================OC Page Object for HomePage Hero Banner======================================
	
	@FindBy(xpath="//div[@class='hp-wrapper clearfix']//button[@class='slick-prev slick-arrow']")
    public WebElement HomePage_HeroBanner_LeftArrow;
	
	@FindBy(xpath="//div[@class='hp-wrapper clearfix']//button[@class='slick-next slick-arrow']")
	public WebElement HomePage_HeroBanner_RightArrow;
	
	@FindBy(xpath="//div[@class='hp-wrapper clearfix']//a[contains(text(),'Shop')]")
    public WebElement HomePage_HeroBanner_ShopNow_LK;
	////div[@class='atg_store_main']//div[@class='slick-list draggable']/div/div[contains(@class,'slick-active')]
	
	@FindBy(xpath="//div[@class='atg_store_main']//div[@class='slick-list draggable']/div/div[contains(@class,'slick-active')]")
	public WebElement HomePage_HeroImage_ActiveHero_TV;
	
	//=============================================OC Page Object for BNY Body======================================
	
	@FindBy(css="img.image")
    public WebElement HomePage_SecondarySpot_Image;
	
	@FindBy(id="weAdore")
    public WebElement HomePage_WeAdore_Image;
	
	@FindAll({
		@FindBy(xpath="//div[@id='rr_placement_0']//div[@class='rrRecs']/div")
		})
	public List<WebElement> HomePage_ContentSpots_WeAdore_IMG;
	
	@FindBy(xpath="//div[@id='rr_placement_0']//div[@class='rrRecs']")
	public WebElement HomePage_WeAdore_Section;
	
	@FindBy(id="personalizedRRContent")
    public WebElement HomePage_PersonalizedContentSpot_Image;
	
	//================================OC Forgot Password Page Objects======================================
	
	@FindBy(xpath="//a[contains(text(),'Forgot your password?')]")
	public WebElement LoginPopup_ForgotPassword_LK;
	
	@FindBy(id="atg_store_profilePasswordForgotEmail")
	public WebElement LoginPopup_ForgotPassword_Email_TB;
	
	@FindBy(id="atg_store_profilePasswordForgotSubmit")
	public WebElement LoginPopup_ForgotPassword_Send_BTN;
	
	@FindBy(xpath="//div[@class=' atg_store_generalMessage']/p")
	public WebElement LoginPopup_ForgotPassword_BlankEmail_TV;
	
	@FindBy(xpath="//div[@id='atg_store_formValidationError']/p/span")
	public WebElement LoginPopup_ForgotPassword_InvalidMail_Validation_TV;
	
	@FindBy(xpath="//div[@id='passwordSuccess']//p")
	public WebElement LoginPopup_ForgotPassword_MesgAfterSuccess_TV;
	
	//=================================OC Footer Social Icons=========================
	@FindBy(xpath="//a[@class='facebook icon']")
	public WebElement Footer_Social_Facebook_LK;
	
	@FindBy(xpath="//a[@class='twitter icon']")
	public WebElement Footer_Social_twitter_LK;
	
	@FindBy(xpath="//a[@class='pinterest icon']")
	public WebElement Footer_Social_Pinterest_LK;
	
	@FindBy(xpath="//a[@class='instagram icon']")
	public WebElement Footer_Social_Instagram_LK;
	
	@FindBy(xpath="//a[@class='youtube icon']")
	public WebElement Footer_Social_youtube_LK;
	
	@FindBy(xpath="//a[@class='google icon']")
	public WebElement Footer_Social_Google_LK;
	
	@FindBy(xpath="//a[@class='tumblr icon']")
	public WebElement Footer_Social_tumblr_LK;
	
	@FindBy(xpath="//a[@class='snap-chat']")
	public WebElement Footer_Social_snapchat_LK;
	
	//=============================================old page objects======================================
	@FindBy(xpath="//a[contains(text(),'Log In')]")
	public WebElement WHS_Home_TopLogin_LK;
	
	/*@FindBy(xpath="//a[contains(text(),'Forgot Your Password?')]")
	public WebElement LoginPopup_ForgotPassword_LK;
	
	@FindBy(id="dwfrm_requestpassword_email")
	public WebElement LoginPopup_ForgotPassword_Email_TB;
	
	@FindBy(xpath="//button[contains(text(),'Send')]")
	public WebElement LoginPopup_ForgotPassword_Send_BTN;
	
	@FindBy(xpath="//form[@id='PasswordResetForm']/fieldset/div/span")
	public WebElement LoginPopup_ForgotPassword_Validation_TV; 
	
	@FindBy(xpath="//div[@id='dialog-container']/p[@class='reset-popup']")
	public WebElement LoginPopup_ForgotPassword_InvalidMail_Validation_TV;
	
	@FindBy(xpath="//div[@class='mobile-page']/p[1]")
	public WebElement LoginPopup_ForgotPassword_MesgAfterSuccess_TV;*/
	
	@FindBy(xpath="//a[contains(text(),'Go to the homepage')]")
	public WebElement LoginPopup_ForgotPassword_MesgAfterSuccess_GoToHome_LK;

	@FindBy(xpath="//ul[@class='dropdown-menu']/li[4]/a")	
	public WebElement Home_TopDDLogout_DD;
	
	/*@FindBy(xpath="//ul[@class='dropdown-menu']/li[1]/a")	
	public WebElement Home_TopDDMyAccount_DD;*/
			

	@FindBy(xpath="//div[@class='accountInfo hidden-xs']//a[@title='My Favorites']")	
	public WebElement Home_TopDDMyFavorites_DD;

	@FindBy(xpath="//nav[@class='customer-info']/ul/li[1]/ul/li[2]/a[1]")
	public WebElement BNY_Home_Loggedin_LK;

	@FindBy(xpath="//html/body/div[200]/div[2]/p/a")
	public WebElement BNY_Popup_Register_LK;

	@FindBy(xpath="//a[@class='mini-cart-link']/span")
	public WebElement BNY_Banner_MyBag_LK;
}
