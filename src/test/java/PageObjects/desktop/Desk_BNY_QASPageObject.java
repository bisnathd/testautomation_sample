package PageObjects.desktop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_QASPageObject {
	
	//User Original Button on QAS page
		@FindBy(xpath="//button/span[contains(text(),'Use Original')]")
		public WebElement QAS_UseOriginal_BTN;
		
		//Select First radio button on the QAS page
		@FindBy(xpath="//form[@id='dwfrm_addForm']/fieldset/table/tbody/tr/td[1]/input")
		public WebElement QAS_SelectfirstSuggestion_RB;
						
		//Select First radio button on the QAS page
		@FindBy(xpath="//button/span[contains(text(),'Submit')]")
		public WebElement QAS_Submit_BTN;
}
