package PageObjects.desktop;
/*
*
*******Conventions to identify Type of element(This has to be appended as a Suffix in the web-element name********
*******Element Types******
Textbox:---------------><WebElement>_TB
Link:------------------><WebElement>_LK
Button:----------------><WebElement>_BTN
Drodpdown:-------------><WebElement>_DD
Dropdown Option:-------><WebElement>_DDOption
Radio Button:----------><WebElement>_RB
Text for Verification:-><WebElement>_TV
Checkbox:--------------><WebElement>_CB
Image:-----------------><WebElement>_IMG	
* 
* 
********Convention for Defining Webelement Name:********
Page Name_Module/SectionName_ElementName_Element Type
*
*/
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_ChangePasswordPageObject 
{
//===========================================OC Page Objects============================================
	
	@FindBy(xpath="//a[@class='changePwdlink']")
	public WebElement MyAccount_ChangePassword_LK;
	
	@FindBy(id="atg_store_profilePasswordEditOldPassword")
	public WebElement SetNewPasswordPage_OldPassword_TB;
	
	@FindBy(id="atg_store_profilePasswordEditNewPassword")
	public WebElement SetNewPasswordPage_NewPassword_TB;
  
	@FindBy(id="atg_store_profilePasswordEditRetypePassword")
	public WebElement SetNewPasswordPage_ConfirmPassword_TB;
	
	@FindBy(id="atg_store_profilePasswordEditSubmit")
	public WebElement SetNewPasswordPage_Submit_TB;
	
	@FindBy(xpath="//span[@class='icon dropdown-toggle hidden-xs dropdown-button']")
	public WebElement UtilityNav_TopNavigation_LoggedInUser_DD;
	
	@FindBy(linkText="Log Out")
	public WebElement LogOut_LK;
	
	@FindBy(xpath="//div[@class='atg_store_main atg_store_myAccount col-sm-9 col-lg-10 primary-content desktop30']")
	public WebElement SetNewPasswordPage_FORM;
	
	@FindBy(xpath="//a[contains(text(),'Log In')]")
	public WebElement Home_UtilityNav_TopLogin_LK;
	
//=================================================Old Page Objects======================================	
	//@FindBy(css="b.caret")
	@FindBy(xpath="//a[@class='userName']")
	public WebElement HeaderUserName_LK;
	
	@FindBy(xpath="//*[@id='header']/section[1]/div/nav[3]/ul/li[1]/ul/li[2]/a[2]/b")
	public WebElement HeaderUserName1_LK;
	
	@FindBy(xpath="//*[@id='header']/div[2]/section[2]/div[1]/a")
	public WebElement Homepage_logo;
	
	@FindBy(xpath="//*[@id='header']/section[3]/div/h1/a")
	public WebElement WAREHOUSE_logo_LK;
}
