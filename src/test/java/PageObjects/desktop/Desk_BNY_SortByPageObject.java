package PageObjects.desktop;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_SortByPageObject {

	@FindBy(id="sortBySelect")
	public WebElement BrowsePage_SortBy_DD;
	
	//Category browse page.
	@FindBy (xpath="//div[@id='atg_store_prodList']/ul")
	public WebElement ProductResultPage_48_96_All_TK;
	
	@FindBy(xpath="//li[contains (text(),48)]")
	public WebElement categoryLanding_TopView48_LK;
	
	
	@FindBy(xpath="//ul[@id='atg_store_pagination']/li/a[contains (text(),'96')]")
	public WebElement categoryLanding_TopView96_LK;
	
	
	@FindBy(xpath="//ul[@id='atg_store_pagination']//li/a[contains (text(),'All')]")
	public WebElement categoryLanding_TopViewAll_LK;
	
	
	@FindBy(xpath="//div[@class='jspContainer']/div/li[9]/a")
	public WebElement MyAcc_DesignerFilter_LK;
	
	@FindBy(xpath="//div[@id='secondary']/div[3]/div[5]/div[1]/h4/a")
	public WebElement MyAcc_SizeFilter_LK;
	
	@FindBy(xpath="//div[@id='Size']//ul/li")
	public WebElement Category_SizeFilter1_CB;
	
	@FindBy(xpath="//div[@id='accordion']/div[5]/div[1]/h4/a")
	public WebElement MyAcc_ColorFilter_LK;
	
	
	@FindBy(xpath="//div[@id='accordion']/div[5]/div[2]/div/div/ul/div/div[1]/li[1]/a")
	public WebElement MyAcc_FirstColorFilter_LK;
	
	@FindBy(xpath="//ul[@class='scrollable jspScrollable']/div/div[1]/li[1]/a")
	public WebElement MyAcc_FirstSizeSelect_LK;
	
	
	@FindBy(xpath="//div[@id='Designer']//a[contains(text(),'My Designers')]")
	public WebElement GuestMyDesignersFilter_LK;
	
	@FindBy(xpath="//div[@id='collapsebrand']/div/div[2]/div/a")
	public WebElement LoggedInMyDesignersFilter_LK;
	
	
	@FindBy(xpath="//div[@id='Designer']//a[contains(text(),'My Designers')]")
	public WebElement MyDesignersFilter_LK;
	
	@FindBy(xpath="//div[@id='collapsebrand']/div/div[2]/a[2]")
	public WebElement GuestMyDesignersEdit_LK;
		
	
	@FindBy(xpath="//div[@id='collapsebrand']/div/div[2]/a")
	public WebElement LoggedInMyDesignersEdit_LK;
	
	@FindBy(xpath="//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable']/div[11]/div/button[1]")
	public WebElement NoDesignerSavedAddDesigner_BTN;
	
	
	/*@FindBy(xpath="//section[@class='account-top']/a")
	public WebElement DesignerPageAddDesigner_BTN;*/
	
	@FindBy(xpath="//div[@id='Designer']//ul/li")
    public WebElement Category_Designer3_CB;
	
	
	@FindBy(xpath="//div[@id='Color']//ul/li/a")
	public WebElement Category_ColorFilter_colour1_CB;
	
	@FindBy(xpath="//div[@id='Material']//ul/li/a")
	public WebElement Category_MaterialFilter1_CB;
	

	@FindBy(xpath="//div[@id='accordion']//h4/a[contains(text(),'New Arrivals')]")
	public WebElement Category_NewArrivalFilter1_LK;
	
	@FindBy(xpath="//a[contains(text(),'4x')]")
	public WebElement categoryLanding_View4x_LK;

	
	@FindBy(xpath="//a[contains(text(),'6x')]")
	public WebElement categoryLanding_View6x_LK;
	
	
	@FindBy(xpath="//div[@id='Designer']//a[contains(text(),'Edit')]")
	public WebElement BrowsePage_DesignerEdit_LK;
}
