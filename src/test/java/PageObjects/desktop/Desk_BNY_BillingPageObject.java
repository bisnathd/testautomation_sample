package PageObjects.desktop;
/*
*
Conventions to identify Type of element(This has to be appended as a Suffix in the web-element name*
*Element Types
Textbox:---------------><WebElement>_TB
Link:------------------><WebElement>_LK
Button:----------------><WebElement>_BTN
Drodpdown:-------------><WebElement>_DD
Dropdown Option:-------><WebElement>_DDOption
Radio Button:----------><WebElement>_RB
Text for Verification:-><WebElement>_TV
Checkbox:--------------><WebElement>_CB
Image:-----------------><WebElement>_IMG 
* 
* 
Convention for Defining Webelement Name:
Page Name_Module/SectionName_ElementName_Element Type
*
*/
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_BillingPageObject {
	
		@FindBy(xpath="//div[@id='atg_store_checkoutProgressContainer']/h2/span[contains(text(),'Billing')]")
		public WebElement billing_PageTitle_TV;
		
		//*********************Payment Related Page Objects*********************************
		@FindBy(id="atg_store_savedCreditCard")
		public WebElement billing_payment_DD;
		
		@FindBy(xpath="//select[@id='creditCardList']/option[2]")
		public WebElement billing_SecondOption_DD;
		
		@FindBy(xpath="//select[@id='creditCardList']/option[3]")
		public WebElement billing_ThirdOption_DD;
		
		@FindBy(xpath="//select[@id='creditCardList']/option[value='add-new-creditcard']")
		public WebElement billing_payment_AddNew_DDOption;
						
		@FindBy(id="dwfrm_billing_paymentMethods_creditCard_type")//This is only for Internatinal Orders
		public WebElement billing_paymentCardType_DD;
		
		@FindBy(xpath="//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='Visa']")//for International Checkout Only
		public WebElement billing_paymentCardType_VISA_DDOption;
	
		@FindBy(xpath="//div[@id='ym']/div[3]/div[1]/span")
		public WebElement billing_CVVError_LB;
		
		//div[@id='ym']/div[3]/div[2]/a
		
		@FindBy(xpath="//div[@id='ym']/div[3]/div[2]/a")
		public WebElement billing_CVVToolTip_Icon;
		/*
		 //Values of different Card No:
		VISA: 				Visa
		Barneys Card: 		BN
		American Express: 	Amex
		Master Card: 		MasterCard
		Discover Card: 		Discover
		
	*/	
		
		//******************Payment Page Objects************************
		@FindBy(id="atg_store_paymentInfoAddNewCardHolder")
		public WebElement billing_payment_CardHolderName_TB;
		
	
		@FindBy(id="atg_store_paymentInfoAddNewCardCardNumber")
		public WebElement billing_payment_CardCardNo_TB;
		
		@FindBy(xpath="//div[@class='cardIcons']/div[not(contains(@class,'fade_icon'))]")
		public WebElement billing_ActiveCCImage_IMG;
		
		@FindBy(id="atg_store_paymentInfoAddNewCardMonth")
		public WebElement billing_payment_ExpMonth_DD;
		
		@FindBy(id="atg_store_paymentInfoAddNewCardYear")
		public WebElement billing_payment_ExpYear_DD;
		
		@FindBy(id="atg_store_paymentInfoAddNewCardSecurityCode")
		public WebElement billing_payment_CardCardCVV_TB;
		
		
		//******************Billing Address Page Objects************************
		@FindBy(xpath="//div[@class='same-as-shipping ']//div[@class='bt-checkbox']/a")
		public WebElement billing_Address_SameAsShipping_CB;
		
		@FindBy(id="atg_store_countryNameSelect_display")
		public WebElement billing_Address_ShipCountry_CB;
		
		@FindBy(id="atg_store_firstNameInput")
		public WebElement billing_Address_FName_TB;
		
		@FindBy(id="atg_store_lastNameInput")
		public WebElement billing_Address_LName_TB;
		
		@FindBy(id="atg_store_streetAddressInput")
		public WebElement billing_Address_Address1_TB;
		
		@FindBy(id="atg_store_streetAddressOptionalInput")
		public WebElement billing_Address_Address2_TB;
		
		@FindBy(id="atg_store_localityInput")
		public WebElement billing_Address_City_TB;
		
		@FindBy(id="atg_store_stateSelect")
		public WebElement billing_Address_State_DD;
		
		@FindBy(id="atg_store_postalCodeInput")
		public WebElement billing_Address_zip_TB;
		
		@FindBy(id="atg_store_emailInput")
		public WebElement billing_Address_Email_TB;
		
		@FindBy(id="atg_store_telephoneInput")
		public WebElement billing_Address_phone_TB;
		
		@FindBy(xpath="//input[contains(@id,'atg_store_continueButton')]")
		public WebElement billing_Continue_BTN;
		
		@FindBy(id="atg_store_continueButton_newCard")
		public WebElement billing_GuestContinue_BTN;
		

		@FindBy(id="atg_store_continueButton")
		public WebElement billing_LoggedInContinue_BTN;
		
		

		//********************Billing Promo and Gift Card Section*******************
		@FindBy(xpath="//form[@id='dwfrm_billing']/div[1]/div/h3/a")
		public WebElement billing_PromoSection_LK;
		
		@FindBy(xpath="//*[@id='dwfrm_billing_couponCode']")
		public WebElement billing_Promo_TB;
		
		@FindBy(id="add-coupon")
		public WebElement billing_Promo_BTN;
		
		@FindBy(xpath="//div[@id='redeem-gift-card']/h3/a")
		public WebElement billing_GiftCardSection_LK;
		
		@FindBy(xpath="//input[@id='dwfrm_billing_giftCertCode']")
		public WebElement billing_GiftCard_TB;
		
		@FindBy(id="gc-apply")
		public WebElement billing_GiftCard_BTN;
		
		//*****************************************************************************
		@FindBy(xpath="//div[@class='mini-billing-address order-component-block']/div")
		public WebElement billingAddressAdded_LB;

		@FindBy(xpath="//div[@class='checkout-mini-cart']/div/div[3]/span")
		public WebElement billingItemPrice_LB;
		
		@FindBy(xpath="//div[@class='checkout-order-totals']/table/tbody/tr[1]/td[2]")
		public WebElement billingSubTotalPrice_LB;
		
		@FindBy(xpath="//div[@class='checkout-order-totals']/table/tbody/tr[2]/td[2]")
		public WebElement billingShippingPrice_LB;
		
		@FindBy(xpath="//div[@class='checkout-order-totals']/table/tbody/tr[3]/td[2]")
		public WebElement billingTaxPrice_LB;
		
		@FindBy(xpath="//div[@class='checkout-order-totals']/table/tbody/tr[4]/td[2]")
		public WebElement BillingTotalPrice_LB;
		
		@FindBy(xpath="//div[@id='secondary']/div[1]/div[2]/table/tbody/tr[4]/td[2]")
		public WebElement billing_EstimatedTotal_LB;
		
		@FindBy(xpath="//div[@id='secondary']/div[1]/div[2]/a")
		public WebElement billing_MyBag_LK;
		
		@FindBy(xpath="//form[@id='dwfrm_billing']/fieldset/div[2]/div[2]/div/div	")
		public WebElement billing_Country_LB;
		
		@FindBy(xpath="//form[@id='dwfrm_billing']/fieldset/div[1]/div/a")
		public WebElement billing_SameAsShipping_CB;
		
		@FindBy(xpath="//div[@class='checkout-mini-cart']/div/div[3]/span")
		public WebElement billing_ItemPrice_LB;	
	
		@FindBy(xpath="//header[@id='header']/div[4]")
		public WebElement billingAddressEditNotification_LB;			

		@FindBy(xpath="//div[@id='accordion']/fieldset/div/div/div/div[2]/div[1]/div")
		public WebElement billingGCAppliedMessage_LB;	
		
		@FindBy(xpath="//div[@id='gift-card-area']//a[@class='gc-remove']/span[contains(text(),'Remove')]")
		public WebElement billingGCAppliedRemove_LK;
				
		@FindBy(xpath="//table[@class='order-totals-table']/tbody/tr[2]/td[1]")
		public WebElement billingGCAppliedYourOrder_LB;
		
		
		
}
