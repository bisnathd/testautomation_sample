package PageObjects.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_MyAccountPageObject {
	public WebDriver driver;
	//================================OC Page Objects=================================	
	
	//Left nav Page Objects
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'My Account')]")
	public WebElement MyAccLanding_LeftNav_MyAccount_LK;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'Personal Info')]")
	public WebElement MyAccLanding_LeftNav_PersInfo_LK;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'Address Book')]")
	public WebElement MyAccLanding_LeftNav_AddresssBook_LK;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'Payment Options')]")
	public WebElement MyAccLanding_LeftNav_PaymentOpt_LK;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'My Orders')]")
	public WebElement MyAccLanding_LeftNav_MyOrders_LK;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'Email Preferences')]")
	public WebElement MyAccLanding_LeftNav_EmailPref_LK;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'My Favorites')]")
	public WebElement MyAccLanding_LeftNav_MyFavorites_LK;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//a[contains(text(),'My Designers')]")
	public WebElement MyAccLanding_LeftNav_MyDesigners_LK;
	
	//**********************View Edit Links for All My Account Modules*************************
	@FindBy(xpath="//div[@class='atg_store_myProfileInfo account-option']//section[contains(@class,'overview-personalinfo')]/span[contains(text(),'View/Edit')]")
	public WebElement MyAccLanding_PersInfo_ViewEdit_LK;
	
	@FindBy(xpath="//div[@class='atg_store_myProfileInfo account-option']//section[contains(@class,'overview-addresses')]/span[contains(text(),'View/Edit')]")
	public WebElement MyAccLanding_Address_ViewEdit_LK;
	
	@FindBy(xpath="//div[@class='atg_store_myProfileInfo account-option']//section[contains(@class,'overview-payments')]/span[contains(text(),'View/Edit')]")
	public WebElement MyAccLanding_Payments_ViewEdit_LK;
	
	@FindBy(xpath="//div[@class='atg_store_myProfileInfo account-option']//section[contains(@class,'overview-orders')]/span[contains(text(),'View/Edit')]")
	public WebElement MyAccLanding_Orders_ViewEdit_LK;
	
	@FindBy(xpath="//div[@class='atg_store_myProfileInfo account-option']//section[contains(@class,'overview-emailprefs')]/span[contains(text(),'View/Edit')]")
	public WebElement MyAccLanding_EmailPref_ViewEdit_LK;
	
	@FindBy(xpath="//div[@class='atg_store_myProfileInfo account-option']//section[contains(@class,'overview-favorites')]/span[contains(text(),'View/Edit')]")
	public WebElement MyAccLanding_Favorites_ViewEdit_LK;
	
	@FindBy(xpath="//div[@class='atg_store_myProfileInfo account-option']//section[contains(@class,'overview-designers')]/span[contains(text(),'View/Edit')]")
	public WebElement MyAccLanding_Designers_ViewEdit_LK;
	//****************************************************************************************************
	@FindBy(xpath="//h1[contains(text(),'Welcome')]")
	public WebElement MyAcc_Landing_Welcome_TV;
	
	@FindBy(xpath="//span[contains(text(), 'Add a New Address')]")
	public WebElement MyAcc_AddressBook_AddNewAddr_BTN;
	
	@FindBy(xpath="//input[@id='atg_store_editAddressNickname'][2]")
	public WebElement MyAcc_AddNewAddrPopUp_AddressName_TB;
	
	@FindBy(id="atg_store_firstNameInput")
	public WebElement MyAcc_AddNewAddrPopUp_FirstName_TB;
	
	@FindBy(id="atg_store_lastNameInput")
	public WebElement MyAcc_AddNewAddrPopUp_LastName_TB;
	
	@FindBy(id="atg_store_streetAddressInput")
	public WebElement MyAcc_AddNewAddrPopUp_Address1_TB;
	
	@FindBy(id="atg_store_streetAddressOptionalInput")
	public WebElement MyAcc_AddNewAddrPopUp_Address2_TB;
	
	@FindBy(id="atg_store_localityInput")
	public WebElement MyAcc_AddNewAddrPopUp_City_TB;
	
	@FindBy(id="atg_store_postalCodeInput")
	public WebElement MyAcc_AddNewAddrPopUp_Zip_TB;
	
	@FindBy(id="atg_store_telephoneInput")
	public WebElement MyAcc_AddNewAddrPopUp_Phone_TB;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']/ul/li[3]")
	public WebElement MyAcc_AddressBook_LK;
	
	@FindBy(xpath="//div[@class='account-address row']/ul/li")
	public WebElement MyAcc_AddressBook_PANEL;

	@FindBy(id="atg_store_stateSelect")
	public WebElement MyAcc_AddNewAddrPopUp_State_DD;
	
	@FindBy(xpath="//input[@id='atg_store_editAddressSubmit']")
	public WebElement MyAcc_AddNewAddrPopUp_Apply_BTN;
	
	@FindBy(xpath="//div[@class='account-address row']/ul/li[1]/div[@class='accountadd']")
	public WebElement MyAcc_AddressBook_DefaultBox_LB;
	
	@FindBy(xpath="//div[@class='account-address row']/ul/li[2]/div[@class='accountadd']")
	public WebElement MyAcc_AddressBook_SecondBox_LB;
	
	@FindBy(xpath="//div[@class='popup forgetpwd']/div/a")
	public WebElement MyAcc_DeleteAddressPopUp_Yes_BTN;
	
	@FindBy(xpath="//div[@class='account-address row']/ul/li[1]")
	public WebElement MyAcc_AddressBook_DefaultAddress;
	
	@FindBy(id="atg_store_editAddressSubmit")
	public WebElement MyAcc_EditAddressPopUp_Apply_BTN;
	
	@FindBy(id="atg_store_editAddressname")
	public WebElement MyAcc_EditAddressPopUp_AddressName_TB;
	
	@FindBy(xpath="//div[@id='edit-address']//form[@class='validation']")
	public WebElement MyAcc_EditAddressPopUp_FORM;
	
	@FindBy(id="atg_store_editAddressCancel")
	public WebElement MyAcc_EditAddressPopUp_Cancel_BTN;
	
	@FindBy(id="atg_store_firstNameInput-error")
	public WebElement MyAcc_AddressPopUp_FirstNameError_TV;
	
	@FindBy(id="atg_store_lastNameInput-error")
	public WebElement MyAcc_AddressPopUp_LastNameError_TV;
	
	@FindBy(id="atg_store_streetAddressInput-error")
	public WebElement MyAcc_AddressPopUp_AddressOneError_TV;
	
	@FindBy(id="atg_store_localityInput-error")
	public WebElement MyAcc_AddressPopUp_CityError_TV;
	
	@FindBy(id="atg_store_stateSelect-error")
	public WebElement MyAcc_AddressPopUp_StateError_TV;
	
	@FindBy(id="atg_store_postalCodeInput-error")
	public WebElement MyAcc_AddressPopUp_ZipError_TV;
	
	@FindBy(id="atg_store_telephoneInput-error")
	public WebElement MyAcc_AddressPopUp_PhoneError_TV;
		
	@FindBy(xpath="//div[@class='addr-display']")
	public WebElement MyAccLanding_AddressSection_DefaultAddress_TV;

}
