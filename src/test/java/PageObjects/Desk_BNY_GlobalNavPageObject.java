package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_GlobalNavPageObject {
	
	public WebDriver driver;
	
	//====================================OC Page Object for Global Nav Elements======================================

	@FindBy(xpath="//ul[@class='sub_category topnav-level-2']")
	public WebElement GlobalNav_ParentCategory_LK;
	
	@FindBy(xpath="//ul[@id='topnav-level-1']/li/a[contains(text(),'Women')]")
	public WebElement GlobalNavigation_TopCategory_LK;
	
	@FindBy(xpath="//div[@class='atg_store_refinements']")
	public WebElement TopCategory_In_BreadCrumb_LeftNav_LK;
	
	@FindBy(id="atg_store_refinementAncestorsLastLink")
	public WebElement LastLink_In_BreadCrumb_OnLeftNav;
	
}
