package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;



public class Desk_BNY_MyAccountEmailPreferencesPageObject
{
	//@FindBy(xpath="//ul[@class='menu-utility-user']/li/a[@class='user-account']")
	//public WebElement TopHeader_MyAccount_LK;
	
	@FindBy(xpath="//span[@class='atg_store_welcome hidden-xs']")
	public WebElement TopHeader_MyAccount_LK;
	
	@FindBy(xpath="//section[@class='col-xs-12 col-sm-6 col-md-6 col-lg-4 overview-emailprefs']//div[@class='profile-details marginT32']/p")
	public WebElement MyAccount_Home_EmailPreferences_SubscribedEmailList_TV;
	
	@FindBy(xpath="//section[@class='col-xs-12 col-sm-6 col-md-6 col-lg-4 overview-emailprefs']/a[@class='view-edit full-width-mobile-button']")
	public WebElement MyAccount_Home_EmailPreferences_ViewEdit_BTN;
	
	@FindBy(xpath="//fieldset//input[@name='barneysEmailSignup']")
	public WebElement EmailPreferencesPage_EmailList_BNY_CB;
	
	@FindBy(xpath="//fieldset//input[@name='warehouseEmailSignup']")
	public WebElement EmailPreferencesPage_EmailList_BNYWH_CB;
	
	@FindBy(xpath="//div[@class='col-xs-12 col-sm-4 col-lg-3 email-pref-update-btn']/input[@type='submit']")
	public WebElement EmailPreferencesPage_EmailList_Update_BTN;	
  
}
