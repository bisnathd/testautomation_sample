package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_ShippingPageObject {
	
public WebDriver driver;
	
	//Shipping address dropdowm
	@FindBy(id="dwfrm_singleshipping_addressList")
	public WebElement shipping_ShippingAddress_DD;
	
	//Shipping address dropdowm
	@FindBy(xpath="//select[@id='shippingAddressDropDown']/option[1]")
	public WebElement shipping_ShippingAddress1_TV;
	
	//Option 3 in Shipping address dropdown
	@FindBy(xpath="//select[@id='dwfrm_singleshipping_addressList']/option[3]")
	public WebElement shipping_ShippingAddress_DDOption3;
	
	@FindBy(xpath="//span[@class='atg_store_loggedInUserName']")
	public WebElement HomePageUtilityNav_loggedInUserName_LK;
	
	@FindBy(xpath="//a[@class='editShippingAddress'][@id='edit']")
	public WebElement shipping_EditAddress_LK;
	
	//Start Filling Shipping address form
		//First Name
	
		@FindBy(xpath="//input[contains(@id,'atg_store_firstNameInput')]")
		public WebElement shipping_ShippingAddress_FirstName_TB;
		
		//Last Name
		@FindBy(xpath="//input[contains(@id,'atg_store_lastNameInput')]")
		public WebElement shipping_ShippingAddress_LastName_TB;
		
		//Address 1
		@FindBy(xpath="//input[contains(@id,'atg_store_streetAddressInput')]")
		public WebElement shipping_ShippingAddress_Address1_TB;
		
		//Address 2
		@FindBy(xpath="//input[contains(@id,'atg_store_streetAddressOptionalInput')]")
		public WebElement shipping_ShippingAddress_Address2_TB;
		
		//City
		@FindBy(xpath="//input[contains(@id,'atg_store_localityInput')]")
		public WebElement shipping_ShippingAddress_CITY_TB;
		
		//State dropdowm
		@FindBy(xpath="//select[contains(@id,'atg_store_stateSelect')]")
		public WebElement shipping_ShippingAddress_State_DD;
				
		//Zip Code
		@FindBy(xpath="//input[contains(@id,'atg_store_postalCodeInput')]")
		public WebElement shipping_ShippingAddress_ZIP_TB;
		
		//Shipping phone Number
		@FindBy(xpath="//input[contains(@id,'atg_store_telephoneInput')]")
		public WebElement shipping_ShippingAddress_Phone_TB;
		
		//Update address in address book checkbox
		@FindBy(xpath="//li[@class='addressEditSaveAddressInput']/div/a")
		public WebElement shipping_ShippingAddress_UpdateAddress_CB;
			
		@FindBy(xpath="//input[@value='Continue']")
		public WebElement shipping_CONTINUE_BTN;
		
		
	// More objects for shipping page checkout process		
		@FindBy(name="dwfrm_singleshipping_addressList")
		public WebElement Shipping_Dropdown_GetText;
		
		@FindBy(xpath="//*[@id='wrapper']/div[2]/div/div[1]/a")
		public WebElement HeaderShipping_LK;
		
		@FindBy(xpath="//*[@id='secondary']/div/h3")
		public WebElement your_order_Gettext;	
		
		@FindBy(xpath="//*[@id='secondary']/div/div[1]/div/div[1]/a")
		public WebElement ProductImg_LK;
		
		@FindBy(xpath="//*[@id='secondary']/div/div[1]/div/div[2]/a")
		public WebElement ProductName_LK;
		
		// get text form PDP and your order section
		
		@FindBy(xpath="//*[@id='product-content']/h1[2]")
		public WebElement ProductNamePDP_gettext;
		
		@FindBy(xpath="//*[@id='product-image-carousel']/div/div/div[3]/figure/a/img")
		public WebElement ProductImgPDP_gettext;
		
		@FindBy(xpath="//*[@id='secondary']/div/div[1]/div/div[2]/a")
		public WebElement ProductNameShipping_gettext;
		
		@FindBy(xpath="//*[@id='secondary']/div/div[1]/div/div[1]/a/img")
		public WebElement ProductImgShipping_gettext;
		
		//Error message when fields are blank for shipping page 
		@FindBy(xpath="//div[@id='shipping-form-fields']//span[@for='dwfrm_singleshipping_shippingAddress_addressFields_firstName']")
		public WebElement ErrorMsgfirstname_gettext;
		
		@FindBy(xpath="//div[@id='shipping-form-fields']//span[@for='dwfrm_singleshipping_shippingAddress_addressFields_lastName']")
		public WebElement ErrorMsglastname_gettext;
		
		@FindBy(xpath="//div[@id='shipping-form-fields']//span[@for='dwfrm_singleshipping_shippingAddress_addressFields_address1']")
		public WebElement ErrorMsgaddress1_gettext;
		
		@FindBy(xpath="//div[@id='shipping-form-fields']//span[@for='dwfrm_singleshipping_shippingAddress_addressFields_city']")
		public WebElement ErrorMsgCity_gettext;
		
		@FindBy(xpath="//div[@id='shipping-form-fields']//span[@for='dwfrm_singleshipping_shippingAddress_addressFields_states_state']")
		public WebElement ErrorMsgState_gettext;
		
		@FindBy(xpath="//div[@id='shipping-form-fields']//span[@for='dwfrm_singleshipping_shippingAddress_addressFields_zip']")
		public WebElement ErrorMsgZip_gettext;
		
		@FindBy(xpath="//div[@id='shipping-form-fields']//span[@for='dwfrm_singleshipping_shippingAddress_addressFields_phone']")
		public WebElement ErrorMsgphonenumber_gettext;
		               
		@FindBy(xpath="//*[@id='shipping-form-fields']/div[1]/div/div")
		public WebElement GetStateName_gettext;
		
		///////////////////////////////////******************************************************//////////////////////////////////
		
		//Error message when fields are blank for billing page 
		
		@FindBy(xpath="//div[@id='payment-method-fields']//span[@for='dwfrm_billing_paymentMethods_creditCard_owner']")
		public WebElement erromessageCardName_gettext;
		
		@FindBy(xpath="//div[@id='payment-method-fields']//span[@for='dwfrm_billing_paymentMethods_creditCard_number']")
		public WebElement erromessageCardNumber_gettext;
		
		@FindBy(xpath="//div[@id='payment-method-fields']//span[@for='dwfrm_billing_paymentMethods_creditCard_cvn']")
		public WebElement erromessageforCVV_gettext;
						
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_addressFields_firstName']")
		public WebElement erromessageforfirstName_gettext;
		
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_addressFields_lastName']")
		public WebElement erromessageforLastName_gettext;
		
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_addressFields_address1']")
		public WebElement erromessageforAddress1_gettext;
		                
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_addressFields_city']")
		public WebElement errormessageforcity_gettext;
		
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_addressFields_states_state']")
		public WebElement GetStateNamefromBilling_gettext;
		
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_addressFields_zip']")
		public WebElement errormessageforpostal_gettext;
		
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_email_emailAddress']")
		public WebElement errorMessageforEmail_gettext;
		
		@FindBy(xpath="//div[@id='billing-form-fields']//span[@for='dwfrm_billing_billingAddress_addressFields_phone']")
		public WebElement errormessageforPhone_gettext;
		                
		@FindBy(xpath="//*[@id='addresses']/section[2]/ul/li[2]/div/address")
		public WebElement Boxget_text;
		
		@FindBy(xpath="//div[@id='addresses']/section[2]/ul/li[@class='first default col-xs-12 col-sm-6 col-md-4 col-lg-3']/div/address")
		public WebElement MyAcc_AddressBook_Address1_TV;
		
		@FindBy(xpath="//*[@id='addresses']/section[2]/ul/li[2]/div/a")
		public WebElement Delete_LK;
		
		@FindBy(name="dwfrm_profile_address_remove")
		public WebElement Delete_BTN;
		
		//*************************Shipping methods for US Address********************
		@FindBy(xpath="//div[contains(@class,'shippingMethod atg_radiobox_div_hli')]//label[@class='atg_store_shippingMethodTitle']")
		public WebElement shipping_SelectedMethod_TV;
		
		@FindBy(xpath="//div[contains(@class,'shippingMethod')]//label[contains(text(),'Ground')]")
		public WebElement shipping_Methods_Ground_LK;
		
		@FindBy(xpath="//div[contains(@class,'shippingMethod')]//label[contains(text(),'Two Business Day')]")
		public WebElement shipping_Methods_TwoBusinessDay_LK;
		                
		@FindBy(xpath="//div[contains(@class,'shippingMethod')]//label[contains(text(),'Next Business Day')]")
		public WebElement shipping_Methods_NextBusinessDay_LK;
		
		@FindBy(xpath="//div[contains(@class,'shippingMethod')]//label[contains(text(),'Same Day')]")
		public WebElement shipping_Methods_SameDay_LK;
		
		@FindBy(xpath="//ul[@class='atg_store_orderSubTotals']/li[2]/span[2]")
		public WebElement shipping_OrderSubTotals_ShippingCharges_TV;
		
	//*********************Shipping Methods for India address********************************		                
		@FindBy(xpath="//*[@id='shipping-method-list']/fieldset/div[2]/label")
		public WebElement Aramex_LK;
		
		@FindBy(xpath="//*[@id='shipping-method-list']/fieldset/div[3]/label")
		public WebElement DHLExpress_LK;
		
		
		@FindBy(xpath="//*[@id='secondary']/div/div[2]/table/tbody/tr[3]/td[2]")
		public WebElement ShippingChargesForIndia_gettext;

		//*********************Shipping Methods for Canada address********************************
		@FindBy(xpath="//*[@id='secondary']/div[1]/div[2]/table/tbody/tr[3]/td[2]")
		public WebElement ShippingChargesForCanada_gettext;
		
		
		//********************************Gift Wrap ***************************************************          
		@FindBy(xpath="//label[@id='gift_wrap_main']/a")
		public WebElement shipping_AddGiftWrap_LK ;
		
		@FindBy(xpath="//div[@class='addGiftWrapCkh']/div/a")
		public WebElement shipping_AddGiftWrap_CB ;
		
		@FindBy(xpath="//div[@class='addGiftMessageChk']/div/a")
		public WebElement shipping_AddGiftMessage_CB ;
		
		//***************************Gift message section****************************************
		
		@FindBy(xpath="//div[@class='gift_fields giftCardMessageDiv']//input[@name='giftMessage1']")
		public WebElement shipping_Messageline1_TB ;
		
		@FindBy(xpath="//div[@class='gift_fields giftCardMessageDiv']//input[@name='giftMessage2']")
		public WebElement shipping_Messageline2_TB ;
		
		@FindBy(xpath="//div[@class='gift_fields giftCardMessageDiv']//input[@name='giftMessage3']")
		public WebElement shipping_Messageline3_TB ;
		
		@FindBy(xpath="//div[@class='gift_fields giftCardMessageDiv']//input[@name='giftMessage4']")
		public WebElement shipping_Messageline4_TB ;
		
		@FindBy(xpath="//div[@class='gift_fields giftCardMessageDiv']//input[@name='giftMessage5']")
		public WebElement shipping_Messageline5_TB ;
		
		@FindBy(xpath="//div[@class='gift_fields giftCardMessageDiv']//input[@name='giftMessage6']")
		public WebElement shipping_Messageline6_TB ;
		
		@FindBy(xpath="//div[@id='card_preview']//div[@class='card-text']")
		public WebElement shipping_GiftMessage_Cardpriview ;
	
		
}
