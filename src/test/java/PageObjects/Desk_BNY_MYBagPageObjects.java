package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_MYBagPageObjects {
	
	@FindBy(id="atg_store_countryNameSelect")
	public WebElement MYBag_Country_DD;
	
	@FindBy(xpath="//select[@id='atg_store_countryNameSelect']")
	public WebElement MYBag_Country_Domestic_DD;
	
	@FindBy(xpath="//div[@class='atg_store_cartActions']/div[@class='domesticCheckout']/input[@id='atg_store_checkout']")
	public WebElement MYBag_CHECKOUT_BTN;
	
	//*******login pop up after clicking on checkout button from cart page.************ 
	 
	 @FindBy(xpath="//div[@id='checkoutLoginPopup']")
	 public WebElement Cartloginpopup;
	 
	@FindBy(id="atg_store_emailInput")
	public WebElement myBag_LoginPopup_loginEmail_TB;
	
	@FindBy(id="atg_store_passwordInput")
	public WebElement myBag_LoginPopup_loginPassword_TB;
	
	@FindBy(id="atg_store_checkoutLoginButton")
	public WebElement myBag_LoginPopup_login_BTN;
	
	@FindBy(xpath="//input[@type='submit'][@value='Continue as Guest']")
	public WebElement myBag_loginPopup_continueAsGuest_BTN;
		
//---------------------------------------------------------------------------------------------
	
	
	@FindBy(xpath="//div[@id='primary']/div[@class='error-form col-xs-12']")
	public WebElement MYBag_RestrictionErrorMessage_Top_TV;
	
	@FindBy(xpath="//div[@class='item-quantity']/div/span")
	public WebElement MYBag_RestrictionErrorMessage_belowQnty_TV;
	
	@FindBy(xpath="//span[@class='price-sales']")
	public WebElement MYBag_ProductSalesPrice_TV;

	@FindBy(xpath="//form[@id='cart-items-form']/fieldset/div/div/table/tbody/tr[4]/td[2]")
	public WebElement MYBag_EstimatedTotal_LB;
	
	@FindBy(xpath="//div[@id='atg_store_contentHeader']/h1[contains (text(),'My Bag')]")
	public WebElement MYBag_Title_LB;
	
	@FindBy(name="dwfrm_cart_coupons_i0_deleteCoupon")
	public WebElement MYBag_PromoDelete_LK;
	
	@FindBy(xpath="//table[@class='order-totals-table']/tbody/tr[1]/td[2]")
	public WebElement MYBag_SubTotal_LB;
	
	@FindBy(xpath="//a[@class='removeCartItem']")
	public WebElement MYBag_productRemove_LK;
	
	@FindBy(xpath="//li [@class='mybag-ul my-bag-link']/a/span")
	public WebElement homePage_UtilityNav_MyBag_LK;
	
	@FindBy(xpath="//a[@class='bny-black-button btn-block '][contains(text(),'Checkout')]")
	public WebElement miniBagPopup_Checkout_BTN;

	@FindBy(xpath="//div[@class='favorites-icon']/span[@class='pdp-fav-hearticon']/a")
	public WebElement MYBag_fav_BTN;
	
	@FindBy(xpath="//div[@id='atg_store_shoppingCart']/div/h3")
	public WebElement Cart_EmptyBag_Message;
	
	@FindBy(xpath="//div[@class='order-summary col-md-3 col-xs-12 customer-box']//h3")
	public WebElement Cart_CustomerServiceBox;
	
	@FindBy(xpath="//a[@href='http://profile.stellaservice.com/barneys.com']")
	public WebElement Cart_Stellalogo;
	
	@FindBy(xpath="//div[@class='propertyContainer']//li/a[contains(text(),'Edit')]")
	public WebElement MyBag_ProductQuickview_Edit_LK;
	
	@FindBy(id="cartQuickView")
	public WebElement ProductQuickView_PP;
	
	@FindBy(xpath="//div[@class='ajaxQuickViewContainer']//p/a[contains(text(),'View Product Details')]")
	public WebElement PDPPageLinkOnQickViewPage_LK;
	
	@FindBy(xpath="//div[@class='propertyContainer']//ul/li/a[contains(text(),'Remove')]")
	public WebElement MyBagRemove_LK;
		
	@FindBy(xpath="//a[@class='mini-cart-link']/span")
	public WebElement MyBagTopLinkFromHeader;

	

	@FindBy(xpath="//*[@id='cart-items-form']/fieldset/div[2]")
	public WebElement inventoryMesg_gettext;
	
	@FindBy(xpath="//*[@id='top-nav']/div[1]/ul/li[2]/a")
	public WebElement WAREHOUSEWomenFromHeader_LK;
	
	@FindBy(xpath="(//ul[@class='topnav-level-1']/li/a[contains(text(),'Women')]")
	public WebElement WomenFromHeader_LK;
	
	
	@FindBy(xpath="//li[@class='activeNav']/div/div/div/ul/li/ul/li/a[contains(text(),'Shoes')]")
	public WebElement Shoes_LK;
	
	@FindBy(xpath="//*[@id='category-level-1']/li[2]/a")
	public WebElement WarShoes_LK;
	
	@FindBy(css="#bc5bwiaaiyjGwaaadkSiUg4pmi > div.product-image.grid_flexH > a.thumb-link > img.gridImg")
	public WebElement Shoesimg_LK;
	
	@FindBy(css="#bdofoiaaiy57aaaadiAil2b9I1 > div.product-image.grid_flexH > a > img")
	public WebElement WarShoesimg_LK;
	
	
	@FindBy(css="a.swatchanchor > img[alt=\"Prada Suede Wingtip Derbys - Shoes - Barneys.com\"]")
	public WebElement Shoesimgcolor_LK;
  
	@FindBy(linkText="6.5")
	public WebElement ShoesSize_LK;
	
	@FindBy(linkText="10")
	public WebElement WarShoesSize_LK;
	
	//div[@id='primary']/div[@class='col-lg-12 col-xs-12 checkout-header']/h1
	@FindBy(xpath="//div[@id='atg_store_contentHeader']/h1[contains (text(),'My Bag')]")
	public WebElement CartPage_text;
	
	@FindBy(xpath="//input[@id='atg_behavior_addItemToCart']")
	public WebElement AddtoCart_BTN;
		
	@FindBy(xpath="//a[contains(text(),'Checkout')]")
	public WebElement Checkout_BTN;
	
	//div[@class='product-list-item']/div[@class='name']/div/a

	
	//span[@id='ui-dialog-title-QuickViewDialog']

  
	@FindBy(xpath="//*[@id='product-content']/h1[1]/a")
	public WebElement BrandfromQuickview_LK;
	
	@FindBy(id="Quantity")
	public WebElement Quantity_TB;
	
	@FindBy(id="add-to-cart")
	public WebElement UpdateCart_BTN;
	
	//div[@id='quickView']//a[contains(text(),'View Product Details')]

	
		
	//div[@id='mini-cart']//a[@class='mini-cart-link']/span
	@FindBy(xpath="//a[@class='mini-cart-link']/span")
	public WebElement CartPage_LK;
	
	@FindBy(id="atg_store_countryNameSelect")//dwfrm_cart_country
	public WebElement Country_DD;
	
	@FindBy(xpath="//*[@id='dwfrm_cart_country']/option[101]")
	public WebElement Countryvalue_DD;
	                
	@FindBy(xpath="//*[@id='dwfrm_cart_country']/option[46]")
	public WebElement CountryvalueIndia_DD;
	
	@FindBy(xpath="//select[@id='atg_store_countryNameSelect']/option[@value='CA']")
	public WebElement CountryvalueCanada_DD;
	
	@FindBy(xpath="//*[@id='dwfrm_cart_country']/option[17]")
	public WebElement ShippingChargesForCanada_gettext;
	
	//@FindBy(name="dwfrm_cart_shipments_i0_items_i0_quantity")
	@FindBy(xpath="//input[@id='qty-text']")
	public WebElement Quantity1_TB;
	
	/*@FindBy(name="dwfrm_cart_shipments_i0_items_i0_quantity")
	public WebElement Quantity1_TB;*/
	
	//@FindBy(name="dwfrm_cart_updateCart")
	@FindBy(xpath="//input[@id='atg_store_update']")
	public WebElement UpdateQuantity_LK;	
				   
	@FindBy(css="span.price-sales")
	public WebElement PriceFromPDP_gettext;
	
	@FindBy(css="span.mini-cart-price.mini-cart-saleprice")
	public WebElement warPriceFromPDP_gettext;
	
	@FindBy(xpath="//div[@id='productInfoContainer']/h2")
	public WebElement ProductNameFromPDP_gettext;
	
	//div[@id='product-content']/h1[@class='product-name']
	@FindBy(xpath="//div[@id='quickView']//h2[@class='product-title']")
	public WebElement ProductNameFromQuickView_TV;
	                 
	//div[@id='product-content']//span[@class='price-sales']
	@FindBy(xpath="//div[@id='atg_store_picker']//div[@class='atg_store_productPrice']")
	public WebElement ProductPriceFromQuickView_TV;
	
	@FindBy(xpath="//*[@id='cart-table']/tbody/tr/td[2]/div[1]/button/span")
	public WebElement Remove_LK;
	
	@FindBy(xpath="//span[contains(text(),'Continue Shopping')]")
	public WebElement ContinueShoppingEmptyCart_LK;
	
	@FindBy(xpath="//*[@id='primary']/div[2]/p")
	public WebElement EmptyCart_gettext;
	
	@FindBy(xpath="//*[@id='checkout-form']/fieldset/button[2]")
	public WebElement CheckoutfromCart_BTN;
	
	@FindBy(name="dwfrm_cart_continueShopping")
	public WebElement ContinueShoppingfullCart_BTN;
	
	@FindBy(xpath="//div[@class='popup mini-cart']//a[contains(text(),'Continue Shopping')]")
	public WebElement ContinueShoppingfromPDP_BTN;
	
	@FindBy(xpath="//*[@id='mini-cart']/div[2]/div[1]/div[3]/div/a[2]")
	public WebElement WarContinueShoppingfromPDP_BTN;
	
	@FindBy(xpath="//*[@id='mini-cart']/div[1]/span[2]")
	public WebElement bagdropdownfrom_header_LK;
	
	@FindBy(xpath="//div[@class='mini-cart-bag']//div[@class='mini-cart-header']")
	public WebElement MybagfromPDP_gettext;
	               
	@FindBy(xpath="//*[@id='rrMiniBag']/div/span")
	public WebElement RECOMMENDED_gettext;
	
	@FindBy(xpath="//*[@id='mini-cart']/div[2]/div[1]/div[3]/div/span[2]")
	public WebElement subtotal_gettext;
	
	@FindBy(xpath="//*[@id='mini-cart']/div[2]/div[1]/div[3]/div/span[2]")
	public WebElement Editremovepopup_LK;
	
	// Go to shopping cart page from Shipping page
					
	@FindBy(xpath="//*[@id='secondary']/div/div[2]/a")
	public WebElement ShippingCartpage_LK;
	
	
	@FindBy(name="dwfrm_cart_shipments_i0_items_i0_quantity")
	public WebElement CartPAgeQuantity_TB;
	
	@FindBy(name="dwfrm_cart_updateCart")
	public WebElement update_LK;
	
    @FindBy(xpath="//a[@class='mini-cart-link']")
	public WebElement MyBagTopLinkWithQuantity_TV;

	@FindBy(id="qty-text")
	public WebElement CartPageQty_TB;
	
	@FindBy(id="atg_store_update")
	public WebElement CartPageUpdate_LK;
	
	@FindBy(id="showMiniCart")
	public WebElement MiniBag_PP;
	
	

}
