package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class Desk_BNY_MyAccountPersonalInfoPageObject 
{
//================================OC page objects==========================================
	@FindBy(xpath="//span/span[@class='atg_store_loggedInUserName']")
	public WebElement Home_UtilityNav_LoggedInUserName_LK;
	
	@FindBy(xpath="//span[contains(text(),'View/Edit')]")
	public WebElement MYAccount_PersonalInfo_Edit_LK;
	              
	@FindBy(xpath="//ul[@class='atg_store_infoList marginT32']")
	public WebElement MYAccount_PersonalInfo_PANEL;
	
	@FindBy(xpath="//ul[@class='atg_store_infoList marginT32']/li[2]")
	public WebElement MYAccount_PersonalInfo_Email_TXT;
	
	@FindBy(id="atg_store_profileMyInfoEditFirstName")
	public WebElement MYAccount_PersonalInfo_FirstName_TB;
	
	@FindBy(id="atg_store_profileMyInfoEditLastName")
	public WebElement MYAccount_PersonalInfo_LastName_TB;
	
	@FindBy(id="atg_store_profileMyInfoEditGender")
	public WebElement MYAccount_PersonalInfo_Gender_DD;
	
	@FindBy(id="atg_store_eventMonth")
	public WebElement MYAccount_PersonalInfo_DOB_Month_DD;
	
	@FindBy(id="atg_store_eventDay")
	public WebElement MYAccount_PersonalInfo_DOB_Day_DD;
	
	@FindBy(id="atg_store_eventYear")
	public WebElement MYAccount_PersonalInfo_DOB_Year_DD;
	
	@FindBy(id="atg_store_profileMyInfoEditSubmit_editProfile")
	public WebElement MYAccount_PersonalInfo_Submit_BTN;
	
	@FindBy(xpath="//a[@class='changeEmaillink']")
	public WebElement PersonalInfoPage_ChangeEmail_LK;
	
	@FindBy(id="atg_store_profileEmailEditNewEmail")
	public WebElement PersonalInfoPage_ChangeEmailAddress_TB;
	
	@FindBy(id="atg_store_profileEmailEditConfirmEmail")
	public WebElement PersonalInfoPage_ConfirmEmailAddress_TB;
	
	@FindBy(id="atg_store_profileEditEmailPassword")
	public WebElement PersonalInfoPage_Password_TB;

	@FindBy(id="atg_store_profilePasswordEditSubmit")
	public WebElement PersonalInfoPage_Apply_BTN;

	@FindBy(xpath="//ul[@class='atg_store_infoList marginT32']/li[2]")
	public WebElement MYAccount_PersonalInfoEmail_gettext;
	
	@FindBy(id="atg_store_profileMyInfoEditFirstName-error")
	public WebElement firstnameErrorMeg_gettext;
	
	@FindBy(id="atg_store_profileMyInfoEditLastName-error")
	public WebElement LastnameErrorMeg_gettext;
	

	@FindBy(id="atg_store_profileEmailEditNewEmail-error")
	public WebElement EmailErrorMeg_gettext;
	
	@FindBy(id="atg_store_profileEditEmailPassword-error")
	public WebElement PasswordErrorMeg_gettext;
	
	@FindBy(id="atg_store_profileEmailEditConfirmEmail-error")
	public WebElement ConfirmEmailErrorMeg_gettext;
	
	@FindBy(id="atg_store_profilePasswordEditOldPassword-error")
	public WebElement oldpasswordErrorMeg_gettext;
	
	@FindBy(id="atg_store_profilePasswordEditNewPassword-error")
	public WebElement NewPasswordErrorMeg_gettext;
	
	@FindBy(id="atg_store_profilePasswordEditRetypePassword-error")
	public WebElement ConfirmPasswordErrorMeg_gettext;
	
	@FindBy(xpath="//div[@id='atg_store_myAccountNav']//li/a[contains(text(),'Personal Info')]")
	public WebElement PersonalInfor_leftNav;
	
	
//=========================================old Page Objects===============================================
	
	
	@FindBy(xpath="(//a[contains(text(),'Personal Info')])[2]")
	public WebElement MYAccount_PersonalInfo_LK;
	
	// Change password
	
	
}
