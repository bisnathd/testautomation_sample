package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_RegistrationPageObject {
	//====================================OC Page objects=====================================
	
	@FindBy(id="atg_store_newCustomerLogin")
	public WebElement RegisterPage_RegisterPanel_Form;
	
	@FindBy(id="atg_store_registerEmailAddress")
	public WebElement RegisterPage_RegisterPanel_Email_TB;
	
	@FindBy(id="atg_store_registerPassword")
	public WebElement RegisterPage_RegisterPanel_Password_TB;
	
	@FindBy(id="atg_store_registerRetypePassword")
	public WebElement RegisterPage_RegisterPanel_ConfirmPassword_TB;
	
	@FindBy(id="atg_store_createMyAccount")
	public WebElement RegisterPage_RegisterPanel_LogIN_BTN;
	
	@FindBy(xpath="//a[@class='collapsed-hide']")
	public WebElement Home_TopRegister_LK;
	
}
