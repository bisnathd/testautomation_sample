package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_OrderConfirmationPageObject {
	
	@FindBy(xpath="//div[@class='order-paragraphs']/p/b")
	public WebElement OrderCOnfirmation_OrderRefPara_TV;
	
	//Locator for Size section
	@FindBy(xpath="//div[@class='order-paragraphs']/p")
	public WebElement OrderCOnfirmation_OrderRefNumber_TV;
	
}
