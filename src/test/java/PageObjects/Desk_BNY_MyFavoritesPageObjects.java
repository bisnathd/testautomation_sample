package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Desk_BNY_MyFavoritesPageObjects {
	
	//================================OC Page Objects==========================================
	
	//My Favorites Link from Left Nav under My Account page
	@FindBy(xpath="//ul[@class='left-nav']//a[contains(text(),'My Favorites')]")
	public WebElement MyAcc_LeftNav_MyFavorites_LK;
	
	//List Tab under My Favorites page
	@FindBy(xpath="//div[@class='list-url']//a[contains(text(),'Lists')]")
	public WebElement MYFAVPage_ListTab_BTN;
	
	//First Custom List Box
	@FindBy(xpath="//div[@id='fav-product-list']/ul[2]//div[@class='bordered-content clearfix ']/div[@class='row col-xs-12 col-sm-4 col-md-4 col-lg-3 bordered-description']")
	public WebElement MYFAVPage_FirstCustomListPage_BOX;
	
	//Div of First Custom List Box
	@FindBy(xpath="//div[@id='fav-product-list']/ul[2]//div[@class='bordered-content clearfix ']/div")
	public WebElement MYFAVPage_FirstCustomListPage_BOX_Div;
		
	//List Name of the First Custom List Box
	@FindBy(xpath="//div[@id='fav-product-list']/ul[2]")
	public WebElement MYFAVPage_ListName_FirstCustomListPage_BOX;	
	
	//View List link of First Custom List Box
	@FindBy(xpath="//div[@id='fav-product-list']/ul[2]//a[contains(text(), 'View List')]")
	public WebElement MYFAVPage_FirstCustomListPage_ViewList_LK;
	
	//Delete icon from First Custom List Box
	@FindBy(xpath="//div[@id='fav-product-list']/ul[2]//span[@class='fav-list-close delete-popup-button']")
	public WebElement MYFAVPage_ListName_FirstCustomListPage_Delete_ICON;
	
	//Yes Button of the Delete This List Pop Up
	@FindBy(xpath="//div[@id='favourites-List-Deletion']//a[contains(text(),'Yes')]")
	public WebElement MYFAVPage_DeleteThisList_YES_BTN;
	
	//Clicking on Add List
	@FindBy(xpath="//div[@class='create-new-registry']//a[contains(text(),'Add List +')]")
	public WebElement MYFAVPage_AddList_BTN;
	
	//List Name TextBox
	@FindBy(xpath="//input[@name='addlist']")
	public WebElement MYFAVPage_AddList_ListName_TB;
	
	//List Description
	@FindBy(xpath="//textarea[@class='favourites-textarea']")
	public WebElement MYFAVPage_AddList_ListDesc_TA;
	
	//Add List - Save Button
	@FindBy(xpath="//input[@class='bny-Button sub-btn']")
	public WebElement MYFAVPage_AddList_Save_BTN;		
	
	//First Product under Product Tabs
	@FindBy(xpath="//div[@class='custom-product-list row search-result-content']/ul/li")
	public WebElement MYFAVPage_ProductsTab_FirstProduct;	
	
	//First Product Copy
	@FindBy(xpath="//div[@class='custom-product-list row search-result-content']/ul/li[1]//div[@class='wrap-desc']")
	public WebElement MYFAVPage_FirstProduct_Copy;	
	
	//Second Product Copy
	@FindBy(xpath="//div[@class='custom-product-list row search-result-content']/ul/li[2]//div[@class='wrap-desc']")
	public WebElement MYFAVPage_SecondProduct_Copy;
	
	//Custom List Page Title
	@FindBy(xpath="//div[@class='pageview-result']/div/div/span")
	public WebElement MYFAVPage_CustomListPage_Title;

	
	
	//================================Old Page Objects==========================================
	
		//form[@id='dwfrm_giftregistry']//a[contains(text(),'Lists')]
		@FindBy(xpath="//div[@class='favorites-subnav']//a[contains(text(),'Lists')]")
		public WebElement MYFAVPage_ListButton_BTN;
	
		@FindBy(xpath="//ul[@class='bordered-list']/li//a/img")
		public WebElement MYFAV_MyFavoritesList_BOX_Element;
		
		@FindBy(xpath="//ul[@class='bordered-list']/li//div[@class='bordered-content row']/span/a[contains(text(),'View')]")
		public WebElement MYFAV_MyFavoritesList_BOX_Edit_LK;
		
		//a[@class='name-link']
		@FindBy(xpath="//div[@class='custom-product-list row search-result-content']//li[1]//a[@class='name-link']")
		public WebElement MYFAV_MyFavoritesListPage_1stProductName_LK;
		
        @FindBy(xpath="//div[@class='favorites-subnav']//div[@class='product-url']")
        public WebElement MYFAV_Product_BTN;
                
        @FindBy(xpath="//div[@class='clearfix']/div[1]/a[1]")
        public WebElement PDP_LoggedIn_Hearticon_LK;        
      
        @FindBy(xpath="//div[@id='atg_store_prodList']/ul/li[1]//a[1]/img")
        public WebElement CatBrowse_FirstImage_LK;                
      
        @FindBy(xpath="//div[@id='atg_store_prodList']/ul/li[1]//span[@class='pdp-fav-hearticon']/a")
        public WebElement CatBrowse_FirstImage_HeartIcon_LK;        
       
        @FindBy(xpath="//div[@class='favorites-icon']/a")
        public WebElement CatBrowse_Guest_FirstImage_HeartIcon_LK;        
      
        @FindBy(xpath="//div[@id='atg_store_prodList']//div[@class='product-name']")
        public WebElement CatBrowse_FirstImage_ProductName_LK;                
     
        @FindBy(xpath="//span[@class='pdp-fav-hearticon']")
        public WebElement MyBag_FirstImage_HeartIcon_LK;     
      
        @FindBy(xpath="//div[@class='cart-faves']/div[@class='favorites-icon']/a")
        public WebElement Guest_MyBag_FirstImage_HeartIcon_LK;
                
      //div[@class='product-list-item']/div[2]/div[1]/a
        @FindBy(xpath="//div[@class='propertyContent']/div")
        public WebElement MyBag_FirstImage_ProductName_LK;
        
      //div[@id='search-result-items']/div/div/div[2]/div[1]/a
        @FindBy(xpath="//div[@class='custom-product-list row search-result-content']//li[1]//div[@class='product-name']/a[1]")
        public WebElement MYFAV_FirstItem_ProductName_LB;
                
        @FindBy(xpath="//form[@id='cart-items-form']/fieldset/table/tbody/tr/td[1]/a[1]")
        public WebElement MyBag_Login_HeartIcon_LK;
        
        @FindBy(xpath="//form[@id='cart-items-form']/fieldset/table/tbody/tr/td[1]/a[2]")
        public WebElement MyBag_Guest_HeartIcon_LK;

      //div[@class='primary-content']/div[1]/h1
        @FindBy(xpath="//div[@class='primary-content']//div[@id='atg_store_registerIntro']")
        public WebElement LoginPage_Title;
        
      //div[@class='name']/div[1]/a
        @FindBy(xpath="//div[@class='propertyContent']/div[@class='itemProperty']")
        public WebElement MyBag_Fav_ProductName;
        
      //div[@class='pdp-faves']/a[2]  //div[@class='pdp-faves product-tile']//span[@class='favorites-arrow  hidden-xs hidden-md hidden-sm']
        @FindBy(xpath="//div[@class='pdp-faves product-tile']//span[2]")
        public WebElement PDP_Fav_Arrow_LK;        
      
      //div[@class='clearfix']/div[1]/ul/li[2]/a
        @FindBy(xpath="//div[@class='last']/a")
        public WebElement PDP_FavDropDown_AddList_DD;
        
      
      //span[@class='product-list-name productListName']
        @FindBy(xpath="//div[@id='atg_store_content']//span[contains(text(),'Edit List')]")
        public WebElement MYFAV_EditList_Title_LB;
        
        @FindBy(xpath="//input[@name='addlist']")
        public WebElement MYFAV_EditList_ListName_TB;
        
        @FindBy(xpath="//textarea[@class='favourites-textarea']")
        public WebElement MYFAV_EditList_ListDescription_TB;
        
      
        @FindBy(xpath="//div[@class='btn-add-list']")
        public WebElement MYFAV_EditList_Save_BTN;
        
      
        @FindBy(xpath="//div[@class='list-url']/div/a")
        public WebElement MYFAV_List_BTN;

      
        @FindBy(xpath="//div[@id='fav-product-list']/ul[2]/li/div/div[1]/b")
        public WebElement MYFAV_List_NewListCreated_LB;
        
        @FindBy(xpath="//div[@id='fav-product-list']//b[@class='event-name'][contains(text(),'')]/parent::div/following-sibling::div[2]//a[@title='View List']")
        public WebElement MYFAV_List_NewListCreated_View_LK;
        
      
        @FindBy(xpath="//div[@class='product-name']/a")
        public WebElement MYFAV_NewListCreated_Product_LB;


        @FindBy(xpath="//span[@class='edit-list-name']")
        public WebElement MYFAV_NewListCreated_List_LB;

      
    	@FindBy(xpath="(//a[contains(text(),'My Favorites')])[3]")
    	public WebElement MyAccountFavorites_LK;
    	
    	//for assert testing 
    	@FindBy(css="p.event-product-count.standard-p")
    	public WebElement zero_products_gettext;
    	
    	//go to category pages women 
    	
    	@FindBy(xpath="//*[@id='top-nav']/div[1]/ul/li[2]/a")
    	public WebElement CatWomen_LK;
    	
    	@FindBy(xpath="(//a[contains(text(),'Jewelry')])[4]")
    	public WebElement Jewelry_LK;
    	
    	
    	@FindBy(linkText="Add to Favorites")
    	public WebElement AddtoFavorites_product1_LK;
    	
    	@FindBy(xpath="//*[@id='bdcjwiaaiyxKIaaadiDdt2b9IJ']/div[1]/a/img")
    	public WebElement Product_img_LK;
    	
    	@FindBy(css="a.wl-action.dropdown-toggle")
    	public WebElement AddtoFavoritFromPDP_DD;
    	
    	@FindBy(linkText="Add to Favorites")
    	public WebElement AddtoFavorites_PDP_LK;
    	
    	@FindBy(linkText="Go To Favorites")
    	public WebElement GotoFavorit_PDP_LK;
    	
    	@FindBy(xpath="//form[@id='atg_store_registerLoginForm']/span/a[contains(text(),'Register Here')]")
    	public WebElement Register_LK;
    	
}
