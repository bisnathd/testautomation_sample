package actionsAndSetup;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import atu.testng.reports.ATUReports;


public class MultipleBrowser {	
	{
		System.setProperty("atu.reporter.config", "atu.properties");
	}
	public  static WebDriver Driver;
	public static EventFiringWebDriver driver;
	DesiredCapabilities capabilities;
	
	public void setuptest(String Browser){
		ATUReports.indexPageDescription="<b>My Project Description <br/> "
				+ "<b>Can include Full set of HTML Tags</b>";
		ATUReports.currentRunDescription="This is run description in Bold";
		
			if(Browser.equals("FF")){
				System.setProperty("webdriver.gecko.driver", "src\\test\\java\\lib\\geckodriver.exe");
				//Driver = new FirefoxDriver();
				//System.setProperty("webdriver.firefox.marionette","src\\test\\java\\lib\\geckodriver.exe");
				Driver= new FirefoxDriver();
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
				driver.manage().window().maximize();
			}
			else if(Browser.equals("CH")){
				String path ="src/test/java/lib/chromedriver.exe";
				System.setProperty("webdriver.chrome.driver", path);
				ChromeOptions options = new ChromeOptions();
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				
				options.addArguments("--disable-extensions");				
		        capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);	
		        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		        //capabilities.setCapability("chrome.options", Arrays.asList("--disable-extensions"));
				Driver = new ChromeDriver(capabilities);
				Driver.manage().deleteAllCookies();
				driver = new EventFiringWebDriver(Driver); 
				EventHandler handler = new EventHandler();
				driver.register(handler);
				driver.manage().window().maximize();
			}else if (Browser.equals("IE")){
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer();   
				caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "");			
				String path = "src/test/java/lib/IEDriverServer_64.exe";
				System.setProperty("webdriver.ie.driver", path);
				Driver = new InternetExplorerDriver(caps);	
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
				driver.manage().window().maximize();
			}else if (Browser.equals("SA")){
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer(); 
				caps.setCapability("safariIgnoreFraudWarning", true);
				SafariOptions options = new SafariOptions();
				options.setUseCleanSession(true);
				Driver = new SafariDriver(caps);
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
				driver.manage().window().maximize();
			}else if (Browser.equals("UNIT")){				
				HtmlUnitDriver HDriver =  new HtmlUnitDriver();
				driver = new EventFiringWebDriver(HDriver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
				//driver.manage().deleteAllCookies();
			}
			ATUReports.setWebDriver(driver);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
}
