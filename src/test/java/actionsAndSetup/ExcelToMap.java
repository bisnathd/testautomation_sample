package actionsAndSetup;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ExcelToMap{
	String propFilePath;
	WebDriver driver;
	String sheetName;
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell locatorNameCell;
	private static XSSFCell locatorTypeCell;
	private static XSSFCell locatorValueCell;
	static Xpath xpath;
	
//---------Constructor--------------------------
	public ExcelToMap(WebDriver driver,String propFilePath,String sheetName){
		this.propFilePath=propFilePath;
		this.driver=driver;
		this.sheetName=sheetName;
	}
	
	public ExcelToMap(){
	
	}
//-------------------------------------------------	
	public Map<String, Xpath> readExcelFileAsMap(String filename,String sheetName){
			  
		  Map<String, Xpath> map = new HashMap<String, Xpath>();
		 try {
		  FileInputStream ExcelFile;
		
			ExcelFile = new FileInputStream(filename);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
		   ExcelWSheet = ExcelWBook.getSheet(sheetName);
		   int startRow = 1;
		   int ci=1;
		   int totalRows = ExcelWSheet.getLastRowNum();
		   
		   for (int i=startRow;i<=totalRows;i++, ci++) {           	   			   
		   	   locatorNameCell= ExcelWSheet.getRow(i).getCell(0);
		   	   locatorTypeCell= ExcelWSheet.getRow(i).getCell(1);
			   locatorValueCell= ExcelWSheet.getRow(i).getCell(2);
			   xpath=new Xpath(locatorTypeCell.getStringCellValue(), locatorValueCell.getStringCellValue());
			   map.put(locatorNameCell.getStringCellValue(), xpath);	   
			}
		}catch(FileNotFoundException fne){
			fne.printStackTrace();
		}catch (IOException ioe) {
			ioe.printStackTrace();
		}

		return map;
		//System.out.println(map.get("home_header_login_LK").locatorType);
		//System.out.println(map.get("home_header_login_LK").LocatorValue);
	}
		  

	public WebElement getWebElement(String key){
		ExcelToMap excelTomap=new ExcelToMap();
		Map<String, Xpath> objectMap=null;
		WebElement elementToreturn=null;
		
		try {
			objectMap=excelTomap.readExcelFileAsMap(propFilePath,sheetName);			
			//System.out.println(objectMap.keySet());	
	    String methodName="Clear()";
		    switch (objectMap.get(key).locatorType) {
			case "xpath":
				elementToreturn=driver.findElement(By.xpath(objectMap.get(key).LocatorValue));
				
				break;
			case "id":
				elementToreturn=driver.findElement(By.id(objectMap.get(key).LocatorValue));
				break;
			case "className":
				elementToreturn=driver.findElement(By.className(objectMap.get(key).LocatorValue));
				break;
			case "linkText":
				elementToreturn=driver.findElement(By.linkText(objectMap.get(key).LocatorValue));
				break;
			case "name":
				elementToreturn=driver.findElement(By.name(objectMap.get(key).LocatorValue));
				break;
			case "partialLinkText":
				elementToreturn=driver.findElement(By.partialLinkText(objectMap.get(key).LocatorValue));
				break;
			case "cssSelector":
				elementToreturn=driver.findElement(By.cssSelector(objectMap.get(key).LocatorValue));
				break;
			case "tagName":
				elementToreturn=driver.findElement(By.tagName(objectMap.get(key).LocatorValue));
				break;
			default:				
				break;
			}		    
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return elementToreturn;
	}	
}

class Xpath{
	String locatorType;
	String LocatorValue;
	
	public Xpath(String locatorType,String LocatorValue){
		this.locatorType=locatorType;
		this.LocatorValue=LocatorValue;
	}	
}
