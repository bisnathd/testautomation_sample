package actionsAndSetup.reporterAndListner.mailConfig;

import java.io.IOException;

import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.BitmapEncoder.BitmapFormat;
import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.style.PieStyler.AnnotationType;
import org.knowm.xchart.style.Styler.ChartTheme;

public class PieChart3 {

	  public void generatePieChart(int passed, int failed, int skipped){

	    // Create Chart
	    PieChart chart = new PieChartBuilder().width(400).height(300).title("Test Execution Status").theme(ChartTheme.GGPlot2).build();

	    // Customize Chart
	    chart.getStyler().setLegendVisible(false);
	    chart.getStyler().setAnnotationType(AnnotationType.LabelAndPercentage);
	    chart.getStyler().setAnnotationDistance(1.15);
	    chart.getStyler().setPlotContentSize(.7);
	    chart.getStyler().setStartAngleInDegrees(90);

	    // Series
	    chart.addSeries("Failed", failed);
	    chart.addSeries("Skipped", skipped);
	    chart.addSeries("Passed", passed);	    

	    // Show it
	    //new SwingWrapper(chart).displayChart();

	    // Save it
	    try {
			BitmapEncoder.saveBitmap(chart, "./TestResult_Chart", BitmapFormat.PNG);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    // or save it in high-res
	    //BitmapEncoder.saveBitmapWithDPI(chart, "./Sample_Chart_300_DPI", BitmapFormat.PNG, 300);
	  }

	}