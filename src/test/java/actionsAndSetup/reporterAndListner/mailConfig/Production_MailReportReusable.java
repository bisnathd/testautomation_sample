package actionsAndSetup.reporterAndListner.mailConfig;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;

import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;

import actionsAndSetup.reporterAndListner.mailConfig.Production_MailReportReusable;

import javax.activation.*;

public class Production_MailReportReusable {
	 
	
	public static void sendreportinMail(String resultPath,int totalPassed, int totalFailed, int totalSkipped) {
		PieChart3 pieChart=new PieChart3();
		  final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		  
		  //---------Generate Pie Chart--------------
			pieChart.generatePieChart(totalPassed, totalFailed, totalSkipped);
		 
		  	 String status="Pass";
		  	 String reportFilePath = null;
		     Properties props = System.getProperties();
		     props.setProperty("mail.smtp.host", "smtp.gmail.com");
		     props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		     props.setProperty("mail.smtp.socketFactory.fallback", "false");
		     props.setProperty("mail.smtp.port", "465");
		     props.setProperty("mail.smtp.socketFactory.port", "465");
		     props.put("mail.smtp.auth", "true");
		     props.put("mail.debug", "true");
		     props.put("mail.store.protocol", "pop3");
		     props.put("mail.transport.protocol", "smtp");
		     final String username = "testadapty@gmail.com";//
		     final String password = "testadapty@1";
		     
		     if (totalFailed>0) {
	            status="Fail";
			 }
		     		     
		     try{
		     Session session = Session.getDefaultInstance(props, 
		                          new Authenticator(){
		                             protected PasswordAuthentication getPasswordAuthentication() {
		                                return new PasswordAuthentication(username, password);
		                             }});

		   // -- Create a new message --
		     Message message = new MimeMessage(session);

// ------------------------- Set the FROM and TO fields ---------------------------
		     message.setFrom(new InternetAddress("adaptyautomationreporter@gmail.com"));
		     
		     String[] to = {"govind.varma@adapty.com"};
		     InternetAddress[] addressTo = new InternetAddress[to.length];
		     for (int i = 0; i < to.length; i++){
               addressTo[i] = new InternetAddress(to[i]);
		     }
		     message.setRecipients(Message.RecipientType.TO, addressTo);
		     
		     String[] toBCC = {"govind.varma@adapty.com"};
		     InternetAddress[] addressToCC = new InternetAddress[toBCC.length];
		     for (int i = 0; i < toBCC.length; i++){
	             addressToCC[i] = new InternetAddress(toBCC[i]);
	         }
		     message.setRecipients(Message.RecipientType.CC, addressToCC); 
		     
		     
//============================================================================================================== 
		     String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
		     message.setSubject(status+": Production Automation Report :"+ timeStamp); 
		     
		  // This mail has 2 part, the BODY and the embedded image
		     Multipart multipart = new MimeMultipart("related");
	        
	         // This is the HTML part
		     BodyPart messageBodyPart = new MimeBodyPart();
	         String htmlText = "</br>"
	         		+ "</br>"
	         		+ "<p>Please  find  the  sanity  Report  for  Barneys  &  Warehouse Production  site,</p>"
	         		+ "</br>"
	         		+ "</br>"
         		+ "<table style=\"height: 60px; border-color: #000000;\" width=\"58\">"
	         		+ "<tbody>"
	         			+ "<tr>"
	         				+ "<td>"
	         					+ "<span style=\"color: #07af47;\">Passed</span>"
	         				+ "</td>"
	         				+ "<td>"+totalPassed+"</td>"
	         			+ "</tr>"
	         			+ "<tr>"
	         				+ "<td>"
	         					+ "<span style=\"color: #e20929;\">Fail</span>"
	         				+ "</td>"
	         				+ "<td>"+totalFailed+"</td>"
	         			+ "</tr>"
	         			+ "<tr>"
	         				+ "<td>"
	         					+ "<span style=\"color: #e29a09;\">Skip</span>"
	         				+ "</td>"
	         				+ "<td>"+totalSkipped+"</td>"
	         			+ "</tr>"
	         			+"<tr>"
         				+ "<td>"
         					+ "<span style=\"color: #e29a09;\">-</span>"
         				+ "</td>"
         				+ "<td>"
         				+"<img src=\"cid:image\">"
         				+"</td>"
         			+ "</tr>"	  
 					+ "</tbody>"
				+ "</table>";
	         
	         messageBodyPart.setContent("Hello Team," + htmlText, "text/html");
	         multipart.addBodyPart(messageBodyPart);
	         
	         
	         //second part (the embedded image pie chart)
	         messageBodyPart = new MimeBodyPart();
	         DataSource fds = new FileDataSource("./TestResult_Chart.png");
	         messageBodyPart.setDataHandler(new DataHandler(fds));
	         messageBodyPart.setHeader("Content-ID", "<image>");
	         multipart.addBodyPart(messageBodyPart);
	         
	         //Attachment 2(result file) 
	         	//-------Get Path of latest report-------------
	         File fileToSend = null;
	         fileToSend=Production_MailReportReusable.getLastReportExcel(resultPath);
	         reportFilePath=fileToSend.getPath();
	         	//Attach the report
	         messageBodyPart = new MimeBodyPart();	       	          
	         fds = new FileDataSource(reportFilePath);
	         messageBodyPart.setDataHandler(new DataHandler(fds));
	         messageBodyPart.setFileName("Prod_Test_Report.xls");
	         multipart.addBodyPart(messageBodyPart);
	         
	         message.setContent(multipart );
		     Transport.send(message);
		     System.out.println("=============Message sent============");
		  }catch (MessagingException e){ 
			  System.out.println("Error Cause " + e);
		  }
		}
	
	
	public static File getLastReportExcel(String ResultsPath) {
		File lastReportFolder=Production_MailReportReusable.listModifiedDirectory(ResultsPath);
		
	    File fl = new File(lastReportFolder.getPath());
	    long millisec;
	    long milliSecCreated;
	    String path;
	    File[] files = fl.listFiles(new FileFilter() {          
	        public boolean accept(File file) {
	            return file.isFile();
	        }
	    });
	    
	    long lastMod = Long.MIN_VALUE;
	    File choice = null;
	    for (File file : files) {
	    	millisec=file.lastModified();
	    	
	    	Date dt = new Date(millisec);
	    	 path = file.getPath();

	        if (file.lastModified() > lastMod) {
	            choice = file;
	            lastMod = file.lastModified();


	            Path p = Paths.get(file.getAbsoluteFile().toURI());
	            
	            
	        }
	    }
	    //System.out.println(choice.getPath());
	    return choice;
	}
	
	public static File listModifiedDirectory(String dir) {
	    File file = new File(dir);
	    File directoryFile;
	    String[] names = file.list();
	    String folderPath;
	    long milliseconds;
	    String path;
	    File choice = null;
	    long lastMod = Long.MIN_VALUE;

	    for(String name : names){
	    	directoryFile=new File(dir+name);
	        if (directoryFile.isDirectory()){
	        	milliseconds=directoryFile.lastModified();	        	
	    		Date dt = new Date(milliseconds);
	    		path = directoryFile.getPath();
		    	 
	    	 if (directoryFile.lastModified() > lastMod) {
		            choice = directoryFile;
		            lastMod = directoryFile.lastModified();
		      }		    			
	        }	        
	    }
	    System.out.println(choice.getPath());
	    return choice;
	}

	 
	 
}
