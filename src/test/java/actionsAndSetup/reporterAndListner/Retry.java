package actionsAndSetup.reporterAndListner;

	import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

	public class Retry implements IRetryAnalyzer {
	
	    private int retryCount = 0;
	    private int maxRetryCount = 1;//Integer.parseInt(System.getProperty("retryCount"));

	// Below method returns 'true' if the test method has to be retried else 'false' 
	//and it takes the 'Result' as parameter of the test method that just ran
	  /*  public boolean retry(ITestResult result) {
	        if (retryCount < maxRetryCount) {
	            System.out.println("Retrying test " + result.getName() + " with status "
	                    + getResultStatusName(result.getStatus()) + " for the " + (retryCount+1) + " time(s).");
	            retryCount++;
	            return true;
	        }
	        return false;
	    }*/
	    
	    public boolean retry(ITestResult iTestResult) {
	        if (!iTestResult.isSuccess()) {                      //Check if test not succeed
	            if (retryCount < maxRetryCount) {                //Check if maxtry count is reached
	            	//System.out.println("Retrying test " + iTestResult.getName() + " with status "+ getResultStatusName(iTestResult.getStatus()) + " for the " + (retryCount+1) + " time(s).");		            
	            	retryCount++;                                //Increase the maxTry count by 1
	                iTestResult.setStatus(ITestResult.FAILURE);  //Mark test as failed
	                return true;                                 //Tells TestNG to re-run the test
	            } else {
	                iTestResult.setStatus(ITestResult.FAILURE);  //If maxCount reached,test marked as failed
	            }
	        } else {
	            iTestResult.setStatus(ITestResult.SUCCESS);      //If test passes, TestNG marks it as passed
	        }
	        return false;
	    }
	    
	    
	    /*public String getResultStatusName(int status) {
	    	String resultName = null;
	    	if(status==1)
	    		resultName = "SUCCESS";
	    	if(status==2)
	    		resultName = "FAILURE";
	    	if(status==3)
	    		resultName = "SKIP";
			return resultName;
	    }*/
}
