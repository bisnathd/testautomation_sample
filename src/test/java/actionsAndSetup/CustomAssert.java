package actionsAndSetup;

import org.testng.Assert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class CustomAssert {

	public static void assertAndReport(String expected,String actual,String stepDescription,String customMessage,String data){
		if (expected.equals(actual)){
			ATUReports.add(stepDescription, customMessage+" "+expected, customMessage+" "+actual,LogAs.PASSED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}else{
			ATUReports.add(stepDescription, customMessage+" "+expected, customMessage+" "+actual,LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			Assert.assertEquals(actual,expected,actual+" and "+expected+" did not match");
		}
	}
	

	public static void assertAndReport(int expected,int actual,String stepDescription,String customMessage,String data){
		if (expected==actual){
			ATUReports.add(stepDescription, customMessage+" "+expected, customMessage+" "+actual,LogAs.PASSED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}else{
			ATUReports.add(stepDescription, customMessage+" "+expected, customMessage+" "+actual,LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			Assert.assertEquals(actual,expected,actual+" and "+expected+" did not match");
		}
	}
	
	public static void assertAndReport(boolean expected,boolean actual,String stepDescription,String customMessage,String data){
		if (expected==actual){
			ATUReports.add(stepDescription, customMessage+" "+expected, customMessage+" "+actual,LogAs.PASSED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}else{
			ATUReports.add(stepDescription, customMessage+" "+expected, customMessage+" "+actual,LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			Assert.assertEquals(actual,expected,actual+" and "+expected+" did not match");
		}
	}
}
