package actionsAndSetup.testCaseReflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import actionsAndSetup.MultipleBrowser;
import atu.testng.reports.ATUReports;

public class ReflectionClass{
	
	
public void runReflectionMethod(String strClassName, String strMethodName,Object... inputArgs){
		
		Class<?> params[] = new Class[inputArgs.length];		
		for (int i = 0; i < inputArgs.length; i++) {			
			if (inputArgs[i] instanceof String) {
				params[i] = String.class;				
			}
		}
		
		try {
			Class<?> cls = Class.forName(strClassName);
			Constructor<?> ctr=cls.getConstructor(WebDriver.class);
			
				Object _instance = ctr.newInstance(MultipleBrowser.driver);
				Method myMethod = cls.getDeclaredMethod(strMethodName, params);
				myMethod.invoke(_instance, inputArgs);
						
		} catch (ClassNotFoundException e) {
			System.err.format(strClassName + ":- Class not found%n");
		} catch (IllegalArgumentException e) {
			System.err.format("Method invoked with wrong number of arguments%n");
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			System.err.format("In Class " + strClassName + "::" + strMethodName
					+ ":- method does not exists%n");
		} catch (IllegalAccessException e) {
			System.err.format("Can not access a member of class with modifiers private%n");
			e.printStackTrace();
		} catch (InstantiationException e) {
			System.err.format("Object cannot be instantiated for the specified class using the newInstance method%n");
		}catch (InvocationTargetException e) {
			System.out.println(e.getCause());
			
			Assert.assertEquals("invocationexception:","Should not throw exception", e.getCause().getLocalizedMessage().toString());
		}
	}

	public void invokeReusableMethods(String excelPath,String sheetName){
		ReflectionClass exeKey = new ReflectionClass();
		TestCaseExcelRead excelSheet = new TestCaseExcelRead();
		excelSheet.openSheet(excelPath,sheetName);
		ATUReports.setAuthorInfo("Govind", "20th July", "1");
		ATUReports.setTestCaseReqCoverage("This is for positive Login and Password Test case");
		
		for (int row = 1; row < excelSheet.getRowCount(); row++) {
			List<Object> myParamList = new ArrayList<Object>();
			String methodName = excelSheet.getValueFromCell(0, row);
			String className = excelSheet.getValueFromCell(1, row);
			
			for (int col = 2; col < excelSheet.getColumnCount(); col++) {
				if (!excelSheet.getValueFromCell(col, row).isEmpty() & !excelSheet.getValueFromCell(col, row).equals("null")) {
					myParamList.add(excelSheet.getValueFromCell(col, row));
				}
			}
	
			Object[] paramListObject = new String[myParamList.size()];
			paramListObject = myParamList.toArray(paramListObject);
	
			exeKey.runReflectionMethod(className,methodName, paramListObject);
		}
	}
}
