package setup;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import actions.EventHandler;


public class MultipleBrowser {
	{
		System.setProperty("atu.reporter.config", "atu.properties");		
	}
	
	public String Browser;
	public String Device;
	public static WebDriver Driver;
	public EventFiringWebDriver driver;
	DesiredCapabilities capabilities;
	
	public void setuptest(String Device,String Browser){
		
		if (Device.equals("Desktop")) {
			
			if(Browser.equals("FF")){
				System.setProperty("webdriver.firefox.marionette","src\\test\\java\\lib\\geckodriver.exe");
				Driver= new FirefoxDriver();
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
			}
			else if(Browser.equals("CH")){
				String path ="src/test/java/lib/chromedriver.exe";
				System.setProperty("webdriver.chrome.driver", path);
				ChromeOptions options = new ChromeOptions();
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				
				options.addArguments("--disable-extensions");				
		        capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);	
		        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		        //capabilities.setCapability("chrome.options", Arrays.asList("--disable-extensions"));
				Driver = new ChromeDriver(capabilities);
				Driver.manage().deleteAllCookies();
				driver = new EventFiringWebDriver(Driver); 
				EventHandler handler = new EventHandler();
				driver.register(handler);
			}
			else if (Browser.equals("IE"))
			{
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer();   
				caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "");			
				String path = "src/test/java/lib/IEDriverServer_64.exe";
				System.setProperty("webdriver.ie.driver", path);
				Driver = new InternetExplorerDriver(caps);	
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
			}else if (Browser.equals("SA")){
				DesiredCapabilities caps = DesiredCapabilities.internetExplorer(); 
				caps.setCapability("safariIgnoreFraudWarning", true);
				SafariOptions options = new SafariOptions();
				options.setUseCleanSession(true);
				Driver = new SafariDriver(caps);
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
				//driver.manage().deleteAllCookies();
			}
						
		} else if(Device.equals("iPad")) {
			capabilities= new DesiredCapabilities();
			capabilities.setCapability("browserName","Safari");
			capabilities.setCapability("platform","iOS");
			capabilities.setCapability("platformName", "9.1");
			capabilities.setCapability("deviceName", "iPad 2");
			//capabilities.setCapability("deviceName", "iPad Air");
			capabilities.setCapability("safariIgnoreFraudWarning", true);
			
			//iPad Mini
			//capabilities.setCapability("udid", "bf071cb42993df88d7cb42e24594d3e8483dbb20");
			// /Applications/Appium.app/Contents/Resources/node_modules/appium/bin/ios-webkit-debug-proxy-launcher.js -c bf071cb42993df88d7cb42e24594d3e8483dbb20:27753 -d
			
			//Old iPad
			capabilities.setCapability("udid", "00fb842363f4aa82e3ec3c5a4a17d72b8d6a2c64");
			// /Applications/Appium.app/Contents/Resources/node_modules/appium/bin/ios-webkit-debug-proxy-launcher.js -c 00fb842363f4aa82e3ec3c5a4a17d72b8d6a2c64:27753 -d
				try {
					Driver=new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities) ;
					driver = new EventFiringWebDriver(Driver);
					EventHandler handler = new EventHandler();
					driver.register(handler);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			
		}else if(Device.equals("iPhone")){
			capabilities= new DesiredCapabilities();
			capabilities.setCapability("platform","iOS");
			capabilities.setCapability("platformName", "9.3.1");
			capabilities.setCapability("deviceName", "iPhone 5");
			capabilities.setCapability("browserName","Safari");
			capabilities.setCapability("safariIgnoreFraudWarning", true);
			capabilities.setCapability("noReset", true);
			//iPhone
			//capabilities.setCapability("udid", "3a3bdb476adcedfaaba85db3ca71bbe9fdabfe3f");
			//72f3617143654479cfb558cdddeb0d3aca7c4006   ipod
			capabilities.setCapability("udid", "72f3617143654479cfb558cdddeb0d3aca7c4006");
			//   /Applications/Appium.app/Contents/Resources/node_modules/appium/bin/ios-webkit-debug-proxy-launcher.js -c 72f3617143654479cfb558cdddeb0d3aca7c4006:27753 -d 
			//   ios_webkit_debug_proxy -c 72f3617143654479cfb558cdddeb0d3aca7c4006:27753 -d
			//    
			try {
				Driver=new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities) ;
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}else if(Device.equals("android")){

			DesiredCapabilities capabilities= new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.BROWSER_NAME,"Chrome");
			capabilities.setCapability("platform","Android");
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("deviceName", "micromax-a250-0123456789ABCDEF");
			//micromax-a250-0123456789ABCDEF
//			oneplus-one_e1003-1ac620a2
			capabilities.setCapability("version","4.2.1");
			try{
				Driver=new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities) ;
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
				
			} catch(Exception e) {
				//TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(Device.equals("selendroid")){

			DesiredCapabilities capabilities= new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.BROWSER_NAME,"Chrome");
			capabilities.setCapability("platform","Android");
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("deviceName", "oneplus-one_e1003-1ac620a2");
			//micromax-a250-0123456789ABCDEF
//			oneplus-one_e1003-1ac620a2
			capabilities.setCapability("version","4.2.1");
			try{
				Driver=new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities) ;
				driver = new EventFiringWebDriver(Driver);
				EventHandler handler = new EventHandler();
				driver.register(handler);
				
			} catch(Exception e) {
				//TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
