package reusableModules.iPhone;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_MobileBNYLogin_Reusable {
	
	Desk_BNY_HomePageObject LoginToBNY_Objects;
	String CurrentPageTitle;
	GenericMethods genericMethods;
	String Popup;
	String _className;
	String _methodName;



	public void Login(WebDriver driver,String userEmail, String password) throws InterruptedException {
		_className=this.getClass().getName();
		_methodName=Thread.currentThread().getStackTrace()[1].getMethodName();
		
		genericMethods=new GenericMethods();
		LoginToBNY_Objects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		
		
		CurrentPageTitle=genericMethods.getTitle(driver);
		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse"))
		{			
			Assert.assertEquals(genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK), "Log In");
		}else
		{
			Assert.assertEquals(genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK), "LOG IN");
		}
		
		genericMethods.Click(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK,"TopLogin Link");
		ATUReports.add("Check Type of User", "Should be guest", "Guest User", true,LogAs.INFO);
		
		
	Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
		if (Popup.equalsIgnoreCase("notpresent"))
		{
			ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
			takeMobileScreenShot(driver, _className, _methodName);
		}
		else
		{
			ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login popup is  displayed", true,LogAs.FAILED);
			Assert.assertEquals("Page present", "Popup present");		
		}
		
		genericMethods.inputValue(driver,LoginToBNY_Objects.LoginPage_LoginPanel_Email_TB, userEmail,"Email Textbox");
		genericMethods.inputValue(driver,LoginToBNY_Objects.LoginPage_LoginPanel_Password_TB, password,"Password Textbox");
		ATUReports.add("Login Popup",true, LogAs.INFO);
		genericMethods.Click(driver,LoginToBNY_Objects.LoginPage_LoginPanel_LOGIN_BTN,"Login BTN");
		genericMethods.waitForPageLoaded(driver);
		if ((genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK)).equalsIgnoreCase("LOG IN")) {
			ATUReports.add("User should be logged in successfully", "Unable to login",true,LogAs.FAILED);
			Assert.assertNotEquals((genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK)), "LOG IN");
			
		}else
		{	ATUReports.add("User should be logged in successfully", "Login is Success",true,LogAs.PASSED);
			Assert.assertNotEquals((genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK)), "LOG IN");					
		}			
 }
	
	/*public void forGotPassword(String emailType,WebDriver driver,String userEmail) {
		genericMethods=new GenericMethods();
		LoginToBNY_Objects = PageFactory.initElements(driver, HomePageObject.class);
		genericMethods =  new GenericMethods();
		String messageAfterSend;
		
	CurrentPageTitle=genericMethods.getTitle(driver);		
		genericMethods.Click(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK);
		genericMethods.Click(driver,LoginToBNY_Objects.LoginPopup_ForgotPassword_LK);
		genericMethods.inputValue(driver,LoginToBNY_Objects.LoginPopup_ForgotPassword_Email_TB,userEmail);
		genericMethods.Click(driver,LoginToBNY_Objects.LoginPopup_ForgotPassword_Send_BTN);
		GenericMethods.Waitformilliseconds(1000);
		

		if(emailType.equalsIgnoreCase("empty"))
		{
			messageAfterSend=genericMethods.getText(driver,LoginToBNY_Objects.LoginPopup_ForgotPassword_Validation_TV);
			if(messageAfterSend.equalsIgnoreCase("Please provide a valid login."))
			{
				ATUReports.add("Verify Error Message", "Error message should be 'Please provide a valid login.'", "Error Message is :" +messageAfterSend, true, LogAs.PASSED);
				Assert.assertEquals(messageAfterSend, "Please provide a valid login.");
			}else
			{
				ATUReports.add("Verify Error Message", "Error message should be 'Please provide a valid login.'", "Error Message is :" +messageAfterSend, true, LogAs.FAILED);
				Assert.assertEquals(messageAfterSend, "Please provide a valid login.");
			}
			
		}else if (emailType.equalsIgnoreCase("invalid")) {
			messageAfterSend=genericMethods.getText(driver,LoginToBNY_Objects.LoginPopup_ForgotPassword_Validation_TV);
			if(messageAfterSend.equalsIgnoreCase("Please provide a valid login."))
			{
				ATUReports.add("Verify Error Message", "Error message should be 'Please provide a valid login.'", "Error Message is :" +messageAfterSend, true, LogAs.PASSED);
				Assert.assertEquals(messageAfterSend, "Please provide a valid login.");
			}else
			{
				ATUReports.add("Verify Error Message", "Error message should be 'Please provide a valid login.'", "Error Message is :" +messageAfterSend, true, LogAs.FAILED);
				Assert.assertEquals(messageAfterSend, "Please provide a valid login.");
			}
			
		}else if (emailType.equalsIgnoreCase("valid")) {
			messageAfterSend=genericMethods.getText(driver,LoginToBNY_Objects.LoginPopup_ForgotPassword_MesgAfterSuccess_TV);
			if(messageAfterSend.equalsIgnoreCase("Request to Reset Your Password Received"))
			{
				ATUReports.add("Verify Message", "Error message should be 'Request to Reset Your Password Received'", "Error Message is :" +messageAfterSend, true, LogAs.PASSED);
				Assert.assertEquals(messageAfterSend, "REQUEST TO RESET YOUR PASSWORD RECEIVED");
				genericMethods.Click(driver,LoginToBNY_Objects.LoginPopup_ForgotPassword_MesgAfterSuccess_GoToHome_LK);
				ATUReports.add("Page Displayed", "Should Navigate to Home Page'", "User is on Home Page", true, LogAs.PASSED);
			}else
			{
				ATUReports.add("Verify  Message", "Error message should be 'Request to Reset Your Password Received'", "Error Message is :" +messageAfterSend, true, LogAs.FAILED);
				Assert.assertEquals(messageAfterSend, "REQUEST TO RESET YOUR PASSWORD RECEIVED");
			}
		}
	
	}*/
	
	
	



	public void GeneralLogin(WebDriver driver,String userEmail, String password) {
		genericMethods=new GenericMethods();
		LoginToBNY_Objects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();

		
		
	/*	CurrentPageTitle=genericMethods.getTitle(driver);
		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse"))
		{			
			Assert.assertEquals(genericMethods.getText(LoginToBNY_Objects.BNY_Home_TopLogin_LK), "Log In");
		}else
		{
			Assert.assertEquals(genericMethods.getText(LoginToBNY_Objects.BNY_Home_TopLogin_LK), "LOG IN");
		}
		
		genericMethods.Click(LoginToBNY_Objects.BNY_Home_TopLogin_LK);
		ATUReports.add("Check Type of User", "Should be guest", "Guest User", true,LogAs.INFO);
		*/
		genericMethods.inputValue(driver,LoginToBNY_Objects.LoginPage_LoginPanel_Email_TB, userEmail,"Email Textbox");
		genericMethods.inputValue(driver,LoginToBNY_Objects.LoginPage_LoginPanel_Password_TB, password,"Password Textbox");
		ATUReports.add("Login Popup",true, LogAs.INFO);
		genericMethods.Click(driver,LoginToBNY_Objects.LoginPage_LoginPanel_LOGIN_BTN,"Login BTN");
		
		if ((genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK)).equalsIgnoreCase("LOG IN")) {
			ATUReports.add("User should be logged in successfully", "Unable to login",true,LogAs.FAILED);
			Assert.assertNotEquals((genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK)), "LOG IN");
			
		}else
		{	ATUReports.add("User should be logged in successfully", "Login is Success",true,LogAs.PASSED);
			Assert.assertNotEquals((genericMethods.getText(driver,LoginToBNY_Objects.Home_UtilityNav_TopLogin_LK)), "LOG IN");					
		}				
 }
	
	public void takeMobileScreenShot(WebDriver driver,String className,String methodName)
	{
		//String scrFolder1 = "c:/Result/"+ new SimpleDateFormat("yyyy_MM_dd_HHmmss").format(Calendar.getInstance().getTime()).toString();
		String scrFolder="C:\\MobileScreens\\" + className+"\\"+methodName;
		new File(scrFolder).mkdir();
	    
	  //get the screenshot folder location from environment variable set in beginning of test
		driver = new Augmenter().augment(driver);
	    File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	    //copy screenshot to screenshot folder
	    try {
			FileUtils.copyFile(scrFile,new File(scrFolder+ "/screenshot" + new SimpleDateFormat("HHmmss").format(Calendar.getInstance().getTime()).toString()+ ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
