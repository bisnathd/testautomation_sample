package reusableModules.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import PageObjects.desktop.Desk_BNY_MyAccountPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_MyAccountLanding_Reusable {
	GenericMethods genericMethods;
	Desk_BNY_MyAccountPageObject myAccount_PageObjects;
	
	public void navigateUsingLeftNav(WebDriver driver, String leftNavText){
		
		genericMethods=new GenericMethods();
		myAccount_PageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
		
		switch (leftNavText) {
	     	case "PersonalInfo": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_PersInfo_LK,"Left Nav Personal Info Link");
	  		  ATUReports.add("Clicked on Personal Info link",true,LogAs.INFO);
	  		  break;
	  		
	      case "AddressBook": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_AddresssBook_LK,"Left Nav Address book Link");	
	    	  ATUReports.add("Clicked on Address book link",true,LogAs.INFO);
	    	  break;
	    	  
	      case "PaymentOptions": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_PaymentOpt_LK,"Left Nav Payment Options Link");
	    	  ATUReports.add("Clicked on Payment Options link",true,LogAs.INFO);
	    	  break;
	    		
	      case "MyOrders": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_MyOrders_LK,"Orders Link");
	    	  ATUReports.add("Clicked on My Orders link",true,LogAs.INFO);
			  break;
	    		
	      case "EmailPreferences": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_EmailPref_LK,"Left Nav Email Preferences Link");
	    	  ATUReports.add("Clicked on Email Preferences link",true,LogAs.INFO);
	    	  break;
	    		
	      case "MyFavorites": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_MyFavorites_LK,"Favorites Link");
	    	  ATUReports.add("Clicked on My Favorites link",true,LogAs.INFO);
	    	  break;
	    	  
	      case "MyDesigners": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_MyDesigners_LK,"Designers Link");
	    	  ATUReports.add("Clicked on My Designers link",true,LogAs.INFO);
	    	  break;
	   
	      default:
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_LeftNav_MyAccount_LK,"Account Link");
	  		  ATUReports.add("Clicked on My Account link",true,LogAs.INFO);
	          break;
		  }		
	}
	
	public void clickViewEditLinksOnMyAccount(WebDriver driver, String moduleName){
		
		genericMethods=new GenericMethods();
		myAccount_PageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
		
		switch (moduleName) {
	     	case "PersonalInfo": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_PersInfo_ViewEdit_LK,"Personal Info View/Edit Link");
	  		  ATUReports.add("Clicked on Personal Info View/Edit link",true,LogAs.INFO);
	  		  break;
	  		
	      case "AddressBook": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_Address_ViewEdit_LK,"Address book View/Edit Link");	
	    	  ATUReports.add("Clicked on Address book View/Edit link",true,LogAs.INFO);
	    	  break;
	    	  
	      case "PaymentOptions": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_Payments_ViewEdit_LK,"Payment Options View/Edit Link");
	    	  ATUReports.add("Clicked on Payment Options View/Edit link",true,LogAs.INFO);
	    	  break;
	    		
	      case "MyOrders": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_Orders_ViewEdit_LK,"My Orders View/Edit Link");
	    	  ATUReports.add("Clicked on My Orders View/Edit link",true,LogAs.INFO);
			  break;
	    		
	      case "EmailPreferences": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_EmailPref_ViewEdit_LK,"Email Preferences View/Edit Link");
	    	  ATUReports.add("Clicked on Email Preferences View/Edit link",true,LogAs.INFO);
	    	  break;
	    		
	      case "MyFavorites": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_Favorites_ViewEdit_LK,"Favorites View/Edit Link");
	    	  ATUReports.add("Clicked on My Favorites View/Edit link",true,LogAs.INFO);
	    	  break;
	    	  
	      case "MyDesigners": 
	    	  genericMethods.Click(driver, myAccount_PageObjects.MyAccLanding_Designers_ViewEdit_LK,"My Designers View/Edit Link");
	    	  ATUReports.add("Clicked on My Designers View/Edit link",true,LogAs.INFO);
	    	  break;
		  }		
	}
	
	public void checkLandingPageAddressMyaccount(WebDriver driver,String addresstext) {
	  	myAccount_PageObjects= PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
		genericMethods = new GenericMethods();
		
		ATUReports.add("Customer is on Address book page", "System should display Addrss book page","Address book page open", true, LogAs.INFO);
		String landingPageAddress = genericMethods.getText(driver, myAccount_PageObjects.MyAccLanding_AddressSection_DefaultAddress_TV);
		if (landingPageAddress.contains(addresstext)){
		    ATUReports.add("System displaying address added on shipping page","  ", "System should display shipping address in my account section added on shipping page","address on my account section : " +landingPageAddress,  true, LogAs.PASSED);
		}else{
		   	ATUReports.add("System is not displaying address added on shipping page","  ", "System should not display shipping address in my account section added on shipping page","system not displaying address on my account section : " +landingPageAddress,  true, LogAs.PASSED);
		    Assert.assertEquals("Entered Address does not contains"+addresstext, "Entered Address contains"+addresstext);
	    } 					
	}

}
