package reusableModules.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import PageObjects.desktop.Desk_BNY_MYBagPageObjects;
import setup.MultipleBrowser;
import PageObjects.desktop.Desk_BNY_MyFavoritesPageObjects;
import PageObjects.desktop.Desk_BNY_MyAccountPersonalInfoPageObject;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_PDPPageObjects;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import reusableModules.desktop.Desk_CategoryProductClick_Reusable;

public class Desk_MyAccMyFavorites_Resuable extends MultipleBrowser {
	
	WebDriverWait xplicitWait ;
	Desk_PDP_Reusable pdp_Reusable;
	Desk_BNY_PDPPageObjects pdpPageObjects;
	Desk_BNY_NavigationPageObject navigationPageObjects;
	Desk_BNY_MyFavoritesPageObjects myFavoritesPageObjects;
	Desk_LoginRegister_Reusable loginRegisterPageObjects;
	Desk_BNY_MyFavoritesPageObjects myAccountFavPageObjects;
	Desk_BNY_MyAccountPersonalInfoPageObject myAccountPersonalInfoPageObjects;	
	Desk_LoginRegister_Reusable loginRegisterReusable;
	Desk_BNY_NavigationPageObject globalNav_Objects;
	Desk_MYBagCheckout_Reusable CheckOutMyBag;
	Desk_SearchResultsAndFilters_Reusable searchResultsAndFilters;
	Desk_Logout_Reusable logout;
	Desk_GlobalNav_Reusable globalNavModule;
	Desk_BNY_PDPPageObjects PDPObjects;
	Desk_BNY_HomePageObject homePageObj;
	Desk_BNY_MyFavoritesPageObjects myFavPageObj;
	Desk_CategoryProductClick_Reusable productClick;
	Desk_BNY_MYBagPageObjects mybagPageObjects;
	String Category="Women";
	String SubCategroy="Hats";
	String Emailid="a1@yopmail.com";
	String Password="12345678";
	static	String login, product_heartIcon_clicked, product_added,PDPPage;
	String ListName, ListDescription;
	WebElement filledHeartIcon;
	boolean isFilledHeartIconPresent;
	String firstProductCopy;
	String secondProductCopy;
	boolean isFirstCustomListPresent;
	WebElement isViewListLinkPresent;
	WebElement customListDeleteIcon;
	Actions action;
	public String ProductName;
	public String FavProductName;
	String GuestUser, FavItem;
	GenericMethods genericMethods;
	
	public void goToListsTab(WebDriver driver) {
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myFavPageObj = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods = new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 20);
		
		//xplicitWait.until(ExpectedConditions.visibilityOf(homePageObj.HomePage_SiteHeader_HeartIcon));
		//genericMethods.Click(driver, homePageObj.HomePage_SiteHeader_HeartIcon);
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"utlity nav logged in Username");
		xplicitWait=new WebDriverWait(driver, 10);
		genericMethods.Click(driver, myFavPageObj.MyAcc_LeftNav_MyFavorites_LK,"My Favorites Left Nav link");
		ATUReports.add("Verify My Favorites link is clicked", "My Favorites link should be clicked", "My Favorites link is clicked", true,LogAs.PASSED);
		xplicitWait=new WebDriverWait(driver, 20);
		genericMethods.Click(driver, myFavPageObj.MYFAVPage_ListTab_BTN,"My Fav List tab");
		ATUReports.add("Verify Lists tab is clicked", "Lists tab should be clicked", "Lists Tab is clicked", true,LogAs.PASSED);
	}
	
	public void goToSpecificListPage(WebDriver driver) {
		myFavPageObj = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		
		isFirstCustomListPresent = genericMethods.isElementPresentforMouseHover(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX);
		
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"utlity nav logged in Username");
		GenericMethods.Waitformilliseconds(1000);
		genericMethods.Click(driver, myFavPageObj.MyAcc_LeftNav_MyFavorites_LK,"My Favorites Left Nav link");
		GenericMethods.Waitformilliseconds(1000);
		genericMethods.Click(driver, myFavPageObj.MYFAVPage_ListTab_BTN,"My Fav List tab");
		
		xplicitWait = new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX));
		
		if (isFirstCustomListPresent = true) {
			ATUReports.add("Verify First Custom List Box is Present", "First Custom List Box should be Present", "First Custom List Box is Present", true,LogAs.PASSED);
			//genericMethods.Click(driver, myFavPageObj.MYFAVPage_FirstCustomListPage_BOX);
			/*action = new Actions(driver);
			action.moveToElement(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX).build().perform();*/
			
			genericMethods.mouseHover(driver, myFavPageObj.MYFAVPage_FirstCustomListPage_BOX,"My Fav Custom list 1 box");
			isViewListLinkPresent = driver.findElement(By.xpath("//div[@id='fav-product-list']/ul[2]//a[contains(text(), 'View List')]"));

			((JavascriptExecutor)driver).executeScript("arguments[0].click();", isViewListLinkPresent);
			ATUReports.add("Verify View List link inside First Custom List Box is Present", "View List link inside First Custom List Box should be Present", "View List link inside First Custom List Box is Present", true,LogAs.PASSED);
		} else {
			ATUReports.add("Verify First Custom List Box is Present", "First Custom List Box should be Present", "First Custom List Box is not Present", true,LogAs.PASSED);
			Assert.assertEquals(isFirstCustomListPresent, true);
		}
	}
	
	public void createListThroughAddList(WebDriver driver, String listName, String listDesc) {
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myFavPageObj = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods = new GenericMethods();
		//genericMethods.Click(driver, homePageObj.HomePage_SiteHeader_HeartIcon);
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"Utlity Nav top Login link");
		xplicitWait=new WebDriverWait(driver, 10);
		genericMethods.Click(driver, myFavPageObj.MyAcc_LeftNav_MyFavorites_LK,"Left Nav My Favorites LK");
		ATUReports.add("Verify Heart Icon of the Site Header is clicked", "Heart Icon of the Site Header should be clicked", "Heart Icon of the Site Header is clicked", true,LogAs.PASSED);
		xplicitWait=new WebDriverWait(driver, 20);
		
		genericMethods.Click(driver, myFavPageObj.MYFAVPage_AddList_BTN,"My Fav Add List button");
		genericMethods.inputValue(driver, myFavPageObj.MYFAVPage_AddList_ListName_TB, listName,"List Name");
		genericMethods.inputValue(driver, myFavPageObj.MYFAVPage_AddList_ListDesc_TA, listDesc,"List Description");
		genericMethods.Click(driver, myFavPageObj.MYFAVPage_AddList_Save_BTN,"List Save Button");
		xplicitWait=new WebDriverWait(driver, 20);
		
		genericMethods.Click(driver, myFavPageObj.MYFAVPage_ListTab_BTN,"My Fav List Tab");
		xplicitWait=new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX));
		isFirstCustomListPresent = genericMethods.isElementPresentforMouseHover(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX);
		String firstCustomListBox = driver.findElement(By.xpath("//div[@id='fav-product-list']/ul[2]/li/div/div/b[@class='event-name']")).getText();
		
		if (isFirstCustomListPresent = true) {
			ATUReports.add("Verify First Custom List Box is Present", "First Custom List Box should be Present", "First Custom List Box is Present", true,LogAs.PASSED);
			if (listName == firstCustomListBox) {
				ATUReports.add("Verify Custom List has been created", "Custom List should be created", "Custom List is created", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify Custom List has been created", "Custom List should be created", "Custom List is not created", true,LogAs.PASSED);
				Assert.assertEquals(listName, firstCustomListBox);
			}
		}	
	}
	
	public void deleteListfromListsPage(WebDriver driver) {
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myFavPageObj = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods = new GenericMethods();
		//genericMethods.Click(driver, homePageObj.HomePage_SiteHeader_HeartIcon);
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"Logged In USername ");
		GenericMethods.Waitformilliseconds(5000);
		
		xplicitWait = new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavPageObj.MyAcc_LeftNav_MyFavorites_LK));
		genericMethods.Click(driver, myFavPageObj.MyAcc_LeftNav_MyFavorites_LK,"My Favorites Left Nav Link");
		ATUReports.add("Verify My Favorites link from Left Nav is clicked", "My Favorites link from Left Nav should be clicked", "My Favorites link from Left Nav is clicked", true,LogAs.PASSED);
		xplicitWait = new WebDriverWait(driver, 30);
		
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavPageObj.MYFAVPage_ListTab_BTN));
		genericMethods.Click(driver, myFavPageObj.MYFAVPage_ListTab_BTN,"My Fav list tab link");
		ATUReports.add("Verify Lists tab is clicked", "Lists tab should be clicked", "Lists Tab is clicked", true,LogAs.PASSED);
		
		boolean isFirstCustomListPresent = genericMethods.isElementPresentforMouseHover(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX);
		boolean isDeleteIconOnFirstListPresent = genericMethods.isElementPresentforMouseHover(myFavPageObj.MYFAVPage_ListName_FirstCustomListPage_Delete_ICON);
		String firstCustomListName = driver.findElement(By.xpath("//div[@id='fav-product-list']/ul[2]//b[@class='event-name']")).getText();
		String firstCustomListDiv = genericMethods.getText(driver, myFavPageObj.MYFAVPage_FirstCustomListPage_BOX_Div);
		
		xplicitWait = new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX));
		if (isFirstCustomListPresent = true) {
			genericMethods.mouseHover(driver, myFavPageObj.MYFAVPage_FirstCustomListPage_BOX,"My Fav first Custom list box");
			
			ATUReports.add("Verify First Custom List Box is Present", "First Custom List Box should be Present", "First Custom List Box is Present", true,LogAs.PASSED);
				customListDeleteIcon = driver.findElement(By.xpath("//div[@id='fav-product-list']/ul[2]//div[@class='bordered-content clearfix ']//span[@class='fav-list-close delete-popup-button']"));
				((JavascriptExecutor)driver).executeScript("arguments[0].click();", customListDeleteIcon);
				//genericMethods.Click(driver, myFavPageObj.MYFAVPage_ListName_FirstCustomListPage_Delete_ICON);
				ATUReports.add("Verify Delete Icon of First Custom List Box is Present", "Delete Icon of inside First Custom List Box should be Present", "Delete Icon of inside First Custom List Box is Present", true,LogAs.PASSED);
				genericMethods.Click(driver, myFavPageObj.MYFAVPage_DeleteThisList_YES_BTN,"Delete List 'YES' link");
				ATUReports.add("Verify Yes Button of Delete This List is clicked", "Yes Button of Delete This List should be clicked", "Yes Button of Delete This List is Present", true,LogAs.PASSED);
				if (firstCustomListDiv.contains(firstCustomListName)) {
					ATUReports.add("Verify the First Custom List has been deleted", "The First Custom List should be deleted", "The First Custom List is not deleted", true,LogAs.FAILED);
					Assert.assertNotEquals(firstCustomListDiv, firstCustomListName);
				}else {
					ATUReports.add("Verify the First Custom List has been deleted", "The First Custom List should be deleted", "The First Custom List is deleted", true,LogAs.PASSED);
					Assert.assertNotEquals(firstCustomListDiv, firstCustomListName);
				}
		}else {
			ATUReports.add("Verify First Custom List Box is Present", "First Custom List Box should be Present", "First Custom List Box is not Present", true,LogAs.PASSED);
			Assert.assertEquals(firstCustomListName, true);
		}
	}
	
	public void removeProductFromProductsTab(WebDriver driver) {
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myFavPageObj = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods = new GenericMethods();
		
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"Utility Nav Top User name");
		xplicitWait=new WebDriverWait(driver, 10);
		genericMethods.Click(driver, myFavPageObj.MyAcc_LeftNav_MyFavorites_LK,"My Fav left Nav link");
		ATUReports.add("Verify My Favorites Link is clicked from Left Nav is clicked", "My Favorites Link is clicked from Left Nav should be clicked", "My Favorites Link is clicked from Left Nav is clicked", true,LogAs.PASSED);
		xplicitWait=new WebDriverWait(driver, 20);
		
		secondProductCopy = genericMethods.getText(driver, myFavPageObj.MYFAVPage_SecondProduct_Copy);
		System.out.println("This is Copy before removing fav product " + secondProductCopy);
		
		GenericMethods.Waitformilliseconds(5000);
		
		genericMethods.mouseHover(driver, myFavPageObj.MYFAVPage_ProductsTab_FirstProduct,"Products Tab forst product");
		filledHeartIcon = driver.findElement(By.xpath("//a[@class='favorite-status delete-product']"));
		isFilledHeartIconPresent = genericMethods.isElementPresentforMouseHover(filledHeartIcon);
		if (isFilledHeartIconPresent == true) {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", filledHeartIcon);
		ATUReports.add("Verify Filled Heart Icon of the First Product is clicked", "Filled Heart Icon of the First Product should be clicked", "Filled Heart Icon of the First Product is clicked", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Filled Heart Icon of the First Product is clicked", "Filled Heart Icon of the First Product should be clicked", "Filled Heart Icon of the First Product is not clicked", true,LogAs.PASSED);
			Assert.assertEquals(isFilledHeartIconPresent, true);
		}
		
		GenericMethods.Waitformilliseconds(10000);
		
		firstProductCopy = genericMethods.getText(driver, myFavPageObj.MYFAVPage_FirstProduct_Copy);
		System.out.println("This is Copy after removing fav product " + firstProductCopy);
		
		if (secondProductCopy == firstProductCopy) {
			ATUReports.add("Verify first product has been removed", "First product should be removed", "First product is removed", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify first product has been removed", "First product should be removed", "First product is not removed", true,LogAs.PASSED);
			Assert.assertEquals(firstProductCopy, secondProductCopy);
		}
		
	}
	
	public void removeProductFromCustomList(WebDriver driver) {
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myFavPageObj = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods = new GenericMethods();
		
		isFirstCustomListPresent = genericMethods.isElementPresentforMouseHover(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX);
		
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"Unitlity Nav logged in User name");
		xplicitWait=new WebDriverWait(driver, 10);
		genericMethods.Click(driver, myFavPageObj.MyAcc_LeftNav_MyFavorites_LK,"My Favorites Left Nav");
		ATUReports.add("Verify My Favorites Link is clicked from Left Nav is clicked", "My Favorites Link is clicked from Left Nav should be clicked", "My Favorites Link is clicked from Left Nav is clicked", true,LogAs.PASSED);
		xplicitWait=new WebDriverWait(driver, 20);
		genericMethods.Click(driver, myFavPageObj.MYFAVPage_ListTab_BTN,"My Favorites List Tab");
		
		xplicitWait = new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavPageObj.MYFAVPage_FirstCustomListPage_BOX));
		
		if (isFirstCustomListPresent = true) {
			ATUReports.add("Verify First Custom List Box is Present", "First Custom List Box should be Present", "First Custom List Box is Present", true,LogAs.PASSED);
			genericMethods.mouseHover(driver, myFavPageObj.MYFAVPage_FirstCustomListPage_BOX,"First Custom List Box");
			isViewListLinkPresent = driver.findElement(By.xpath("//div[@id='fav-product-list']/ul[2]//a[contains(text(), 'View List')]"));

			((JavascriptExecutor)driver).executeScript("arguments[0].click();", isViewListLinkPresent);
			ATUReports.add("Verify View List link inside First Custom List Box is Present", "View List link inside First Custom List Box should be Present", "View List link inside First Custom List Box is Present", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify First Custom List Box is Present", "First Custom List Box should be Present", "First Custom List Box is not Present", true,LogAs.PASSED);
			Assert.assertEquals(isFirstCustomListPresent, true);
		}
		
		xplicitWait = new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavPageObj.MYFAVPage_CustomListPage_Title));
		
		GenericMethods.Waitformilliseconds(2000);
		
		secondProductCopy = genericMethods.getText(driver, myFavPageObj.MYFAVPage_SecondProduct_Copy);
		System.out.println("This is Copy before removing fav product " + secondProductCopy);
		
		genericMethods.mouseHover(driver, myFavPageObj.MYFAVPage_ProductsTab_FirstProduct,"Products tab first product");
		filledHeartIcon = driver.findElement(By.xpath("//a[@class='favorite-status delete-product']"));
		isFilledHeartIconPresent = genericMethods.isElementPresentforMouseHover(filledHeartIcon);
		if (isFilledHeartIconPresent == true) {
			((JavascriptExecutor)driver).executeScript("arguments[0].click();", filledHeartIcon);
			ATUReports.add("Verify Filled Heart Icon of the First Product is clicked", "Filled Heart Icon of the First Product should be clicked", "Filled Heart Icon of the First Product is clicked", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Filled Heart Icon of the First Product is clicked", "Filled Heart Icon of the First Product should be clicked", "Filled Heart Icon of the First Product is not clicked", true,LogAs.PASSED);
			Assert.assertEquals(isFilledHeartIconPresent, true);
		}
		
		firstProductCopy = genericMethods.getText(driver, myFavPageObj.MYFAVPage_FirstProduct_Copy);
		System.out.println("This is Copy after removing fav product " + firstProductCopy);
		
		if (secondProductCopy == firstProductCopy) {
			ATUReports.add("Verify first product has been removed", "First product should be removed", "First product is removed", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify first product has been removed", "First product should be removed", "First product is not removed", true,LogAs.PASSED);
			Assert.assertEquals(firstProductCopy, secondProductCopy);
		}
	}
	
//===============================================My Favorites ============================================
	public void ClickHeartIconPDP(WebDriver driver) {
		productClick = new Desk_CategoryProductClick_Reusable();
		productClick.browseProductandClickonBNY(driver,"2500");
		ATUReports.add("User is navigated to PDP page", true, LogAs.INFO);
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		myFavoritesPageObjects=PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		genericMethods=new GenericMethods();
		ProductName= genericMethods.getText(driver, pdpPageObjects.PDP_ProductName_LB);
		GuestUser= genericMethods.isElementPresent(driver, "//span[@class='icon dropdown-toggle hidden-xs dropdown-button']");
		if (GuestUser.equalsIgnoreCase("notpresent")){
			genericMethods.Click(driver, pdpPageObjects.PDP_AddToFavForGuest_icon_LK,"Add To Favorites icon");
			
			//driver.get("https://dev.barneys.com/account/login.jsp");
			
			String LoginPage = genericMethods.getText(driver, myFavoritesPageObjects.LoginPage_Title);
			if (LoginPage.equalsIgnoreCase("Log in to your account")){
				ATUReports.add("Guest user clicks on the heart icon from PDP page", "Heart icon should be clicked and login page should appear", "Heart icon is clicked and log in page appears",true,LogAs.PASSED);			
			}else{
				ATUReports.add("Guest user clicks on the heart icon from PDP page", "Heart icon should be clicked and login page should appear", "Heart icon is clicked and log in page does not appear",true,LogAs.FAILED);
				Assert.assertEquals("Log in to your account", LoginPage);
			}			
		}else{
			genericMethods.Click(driver, pdpPageObjects.PDP_AddToFavForLoggedIn_icon_LK,"Add To Favorites icon");	
			ATUReports.add("Logged in user clicks on the heart icon from PDP page", "Heart icon should be clicked", "Heart icon is clicked",true,LogAs.PASSED);
		}	
	}
	
	public void ClickHeartIconCatBrowse(WebDriver driver) {

		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods = new GenericMethods();
		genericMethods.mouseHover(driver, myFavoritesPageObjects.CatBrowse_FirstImage_LK,"Category Browse First image");
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", myFavoritesPageObjects.CatBrowse_FirstImage_HeartIcon_LK);
		ProductName= genericMethods.getText(driver, myFavoritesPageObjects.CatBrowse_FirstImage_ProductName_LK);	
	}
	
	public void GuestUserClickHeartIconCatBrowse(WebDriver driver) {
		genericMethods= new GenericMethods();
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);
		ProductName= genericMethods.getText(driver, myFavoritesPageObjects.CatBrowse_FirstImage_ProductName_LK);	
		genericMethods.mouseHover(driver, myFavoritesPageObjects.CatBrowse_FirstImage_LK,"Category Browse First image");
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", myFavoritesPageObjects.CatBrowse_Guest_FirstImage_HeartIcon_LK);		
	}
		
	public void ClickHeartIconMyBag(WebDriver driver,String userStatus) {
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);
		mybagPageObjects=PageFactory.initElements(driver,Desk_BNY_MYBagPageObjects.class);
		genericMethods = new GenericMethods();
		productClick = new Desk_CategoryProductClick_Reusable();
		pdp_Reusable = new Desk_PDP_Reusable();
		mybagPageObjects = new Desk_BNY_MYBagPageObjects();
		
		
		
		//**********Select Color and size on PDP and move to Cart page
		pdp_Reusable.selectAvailableColorOnPDP(driver);
		pdp_Reusable.selectAvailableSizeOnPDP(driver);
		GenericMethods.waitForPageLoaded(driver);
		pdp_Reusable.AddtoCartButtononPDP(driver);
		pdp_Reusable.miniBagPopuponPDP(driver);
		pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
		
		genericMethods.mouseHover(driver, pdpPageObjects.ProductImage_OnMyBag,"Product Image on MyBAG" );
		//genericMethods.findVisibleElementandClick(driver, "//div[@class='favorites-icon']/a");
		if (userStatus.equalsIgnoreCase("login")) {
			genericMethods.javascriptClick(driver,  "//div[@class='favorites-icon']/span[@class='pdp-fav-hearticon']/a", "ClickHeartIconLogin");
		}else {
			genericMethods.javascriptClick(driver,"//div[@class='cart-faves']/div[@class='favorites-icon']","ClickHeartIconGuest");
		}
		
		//genericMethods.Click(driver, myFavoritesPageObjects.MyBag_FirstImage_HeartIcon_LK,"My Bag Product Heart icon");
			
		ProductName= genericMethods.getText(driver, myFavoritesPageObjects.MyBag_FirstImage_ProductName_LK);	
		GuestUser= genericMethods.isElementPresent(driver, "//span[@class='icon dropdown-toggle hidden-xs dropdown-button']");//b[@class='caret']
		if (GuestUser.equalsIgnoreCase("notpresent")){
			genericMethods.javascriptClick(driver,"//div[@class='cart-faves']/div[@class='favorites-icon']/a","My Bag Product Heart icon");
			//driver.get("https://dev.barneys.com/account/login.jsp?loginFromHeader=yes");
			String LoginPage = genericMethods.getText(driver, myFavoritesPageObjects.LoginPage_Title);
			if (LoginPage.equalsIgnoreCase("Log in to your account")){
			ATUReports.add("Guest user clicks on the heart icon from PDP page", "Heart icon should be clicked and login page should appear", "Heart icon is clicked and log in page appears",true,LogAs.PASSED);			
			}else{
				ATUReports.add("Guest user clicks on the heart icon from PDP page", "Heart icon should be clicked and login page should appear", "Heart icon is clicked and log in page does not appear",true,LogAs.FAILED);
				Assert.assertEquals("Log in to your account", LoginPage);
			}			
		}
		
		else{
			ATUReports.add("Logged in user clicks on the heart icon from MY BAG page", "Heart icon should be clicked", "Heart icon is clicked",true,LogAs.PASSED);
		}		
	}
	
	public void UnclickHeartIconFromPDP(WebDriver driver){
		
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);		
		genericMethods.Click(driver, pdpPageObjects.PDP_AddToFavForLoggedIn_icon_LK,"Add to favorites icon on PDP");	
		ATUReports.add("Logged in user unclicks on the heart icon from PDP page", "Heart icon should be unclicked", "Heart icon is unclicked",true,LogAs.PASSED);
	}
	
	
	public void UnclickHeartIconFromMyBag(WebDriver driver){	
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);		
		genericMethods.Click(driver, myFavoritesPageObjects.MyBag_FirstImage_HeartIcon_LK,"My Bag Product Heart icon");
		ATUReports.add("Logged in user unclicks on the heart icon from My Bag page", "Heart icon should be unclicked", "Heart icon is unclicked",true,LogAs.PASSED);
	}
	
	public void UnclickHeartIconFromCatBrowse(WebDriver driver){
	
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods.mouseHover(driver, myFavoritesPageObjects.CatBrowse_FirstImage_LK,"Browse first image heart icon");
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", myFavoritesPageObjects.CatBrowse_FirstImage_HeartIcon_LK);
	}
	
	public void VerifyProductAddedToFavorites(WebDriver driver) {
		
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);
		xplicitWait = new WebDriverWait(driver,30);
		
		genericMethods.findVisibleElementandClick(driver, "//ul[@class='hidden-xs myBag']/li/a[@id='myWishList']");
		GenericMethods.Waitformilliseconds(2000);
		genericMethods.Click(driver, myFavoritesPageObjects.MYFAV_Product_BTN,"My Favorites product Button");
		GenericMethods.Waitformilliseconds(2000);
		xplicitWait.until(ExpectedConditions.visibilityOf(myFavoritesPageObjects.MYFAV_FirstItem_ProductName_LB));
		FavProductName= genericMethods.getText(driver, myFavoritesPageObjects.MYFAV_FirstItem_ProductName_LB);	
		
	}
	
	public void VerifyProductNotAddedToFavorites(WebDriver driver){

		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);		
		genericMethods.Click(driver, navigationPageObjects.TopNavigation_HeartIcon_LK,"Utility Nav Heart Icon");
		GenericMethods.Waitformilliseconds(2000);
		genericMethods.Click(driver, myFavoritesPageObjects.MYFAV_Product_BTN,"My Favorites product Button");
		FavItem= genericMethods.isElementPresent(driver, "//div[@id='search-result-items']/div/div/div[2]/div[1]/a");
		if (FavItem.equalsIgnoreCase("notpresent")){
			ATUReports.add("Verify Product is removed from favorites", "Product should be removed from Favorites", "Product is removed from Favorites", true, LogAs.PASSED);			
		}else{
			ATUReports.add("Verify Product is removed from favorites", "Product should be removed from Favorites", "Product is not removed from Favorites", true, LogAs.FAILED);
			Assert.assertEquals("notpresent", FavItem);
		}		
	}
	
	public void AddingFavoriteListFromPDP(WebDriver driver){
		genericMethods=new GenericMethods();
		productClick = new Desk_CategoryProductClick_Reusable();
		productClick.browseProductandClickonBNY(driver,"2500");
		ATUReports.add("User is navigated to PDP page", true, LogAs.INFO);
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);		
		genericMethods.Click(driver, myFavoritesPageObjects.PDP_Fav_Arrow_LK,"My Favorites Arrow link");
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", myFavoritesPageObjects.PDP_FavDropDown_AddList_DD);
		ATUReports.add("User Clicks on Favorite arrow and selects add a new list", true, LogAs.INFO);
		GenericMethods.Waitformilliseconds(2000);
		
		//driver.get("https://dev.barneys.com/account/addFavouriteList.jsp?productId=504213370&saveGiftlistSuccessURL=%2Fcategory%2Fwomen%2Fclothing%2Fdresses%2FN-1k836i8");
		
		String EditListPageTitle= genericMethods.getText(driver, myFavoritesPageObjects.MYFAV_EditList_Title_LB);
		
		if (EditListPageTitle.equalsIgnoreCase("EDIT LIST")){
			ATUReports.add("Verify user is redirected to Edit list page", "User should be redirected to edit list page", "User is redirected to edit list page", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify user is redirected to Edit list page", "User should be redirected to edit list page", "User is not redirected to edit list page", true,LogAs.FAILED);
			Assert.assertEquals("EDIT LIST", EditListPageTitle);
		}
	}

	public void MyFavCreateNewList(WebDriver driver, String ListName, String ListDescription){
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);		
		genericMethods.inputValue(driver, myFavoritesPageObjects.MYFAV_EditList_ListName_TB, ListName,"My Fav edit list Name");
		genericMethods.Click(driver, myFavoritesPageObjects.MYFAV_EditList_Save_BTN,"My Fav edit list Save button");
		ATUReports.add("User enters list name and list description and clicks on Save button",  true,LogAs.INFO);
	}

	public void VerifyProductAddedInNewList(WebDriver driver,String  ListName) {
		
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		navigationPageObjects= PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		myFavoritesPageObjects=PageFactory.initElements(driver,Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods.Click(driver, navigationPageObjects.TopNavigation_HeartIcon_LK, "Top Nav heart icon");
		genericMethods.Click(driver, myFavoritesPageObjects.MYFAV_List_BTN,"My Fav list button");
		String newListPresent= genericMethods.isElementPresent(driver,"//div[@id='fav-product-list']//b[contains(text(),'"+ListName+"')]");//div[@id='fav-product-list']/ul[2]/li/div/div[1]/b[contains(text(),'"+ListName+"')]"
		System.out.println(newListPresent);
		if (newListPresent.equalsIgnoreCase("present")){
			ATUReports.add("Verify new list is created or not", "New list should be created", "New list is created", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify new list is created or not", "New list should be created", "New list is not created", true,LogAs.FAILED);
			//Assert.assertEquals("present", newListPresent);
		}
		
		genericMethods.mouseHoverJScript(driver, myFavoritesPageObjects.MYFAV_List_NewListCreated_LB);		
		//((JavascriptExecutor)driver).executeScript("arguments[0].click();", myFavoritesPageObjects.MYFAV_List_NewListCreated_View_LK);
		
		genericMethods.javascriptClick(driver, "//div[@id='fav-product-list']//b[@class='event-name'][contains(text(),'" + ListName + "')]/parent::div/following-sibling::div[2]//a[@title='View List']", "MYFAV_NewList_View_LK");
		
		genericMethods.waitForPageLoaded(driver);
		String newListName= genericMethods.getText(driver, myFavoritesPageObjects.MYFAV_NewListCreated_List_LB);
		
		if (newListName.equalsIgnoreCase(ListName)){
			ATUReports.add("Verify user is redicted to " + ListName + " page", "User should be redicted to "+ ListName + " page", "User is redirected to " +newListName+ " page", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify user is redicted to " + ListName + " page", "User should be redicted to "+ ListName + " page", "User is redirected to " +newListName+ " page", true,LogAs.PASSED);
			Assert.assertEquals(ListName, newListName);
		}	
		FavProductName= genericMethods.getText(driver, myFavoritesPageObjects.MYFAV_NewListCreated_Product_LB);	
	}
	
	public void AddproductintoFavorites(WebDriver driver){		
		myAccountFavPageObjects = PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);			
		myAccountPersonalInfoPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
		genericMethods =  new GenericMethods();		
		genericMethods.Click(driver, myAccountPersonalInfoPageObjects.Home_UtilityNav_LoggedInUserName_LK,"Utility Nav Logged In User name");
		genericMethods.Click(driver, myAccountFavPageObjects.MyAccountFavorites_LK,"Favorites Link");
					
		String element = genericMethods.getText(driver, myAccountFavPageObjects.zero_products_gettext);
		System.out.println(element);
		if (element.contains("0 Products")){
			ATUReports.add("My Favorites list is empty", "My Favorites empty",true,LogAs.PASSED);
			Assert.assertNotEquals((genericMethods.getText(driver, myAccountFavPageObjects.zero_products_gettext)), " 0 Products");
			System.out.println("My Favorites list is empty");
		}else{
			ATUReports.add("My Favorites list is not empty", "My Favorites empty",true,LogAs.FAILED);
			Assert.assertNotEquals((genericMethods.getText(driver, myAccountFavPageObjects.zero_products_gettext)), " 0 Products");
			System.out.println("My Favorites list is not empty");
		}
				
		try {
			genericMethods.Click(driver, myAccountFavPageObjects.CatWomen_LK,"Women Category");
			genericMethods.Click(driver, myAccountFavPageObjects.Jewelry_LK,"Jewelry Link");
			genericMethods.Click(driver, myAccountFavPageObjects.AddtoFavorites_product1_LK,"Add to Favorites Link");
			genericMethods.Click(driver, myAccountFavPageObjects.Product_img_LK,"Product Image link");
			genericMethods.Click(driver, myAccountFavPageObjects.AddtoFavoritFromPDP_DD,"Add to Favorites from PDP Dropdown");
			genericMethods.Click(driver, myAccountFavPageObjects.AddtoFavorites_PDP_LK,"Add to favorites link");
			genericMethods.Click(driver, myAccountFavPageObjects.GotoFavorit_PDP_LK,"Goto favorites link");				
		} catch (Exception e){
			e.printStackTrace();
		}			
	}
}
