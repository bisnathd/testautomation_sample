package reusableModules.desktop;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import setup.WinAuth;
import PageObjects.desktop.Desk_BNY_MYBagPageObjects;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_PDPPageObjects;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_MYBagCheckout_Reusable {
	
	Desk_BNY_MYBagPageObjects myBagPageObjects;
	Desk_BNY_PDPPageObjects pdpPageObjects;
	Desk_BNY_HomePageObject homePageLoginPageObjects;
	Desk_GlobalNav_Reusable globalNavReusable;
	Desk_CategoryProductClick_Reusable categoryProductClick_Reusable;
	Desk_MYBagCheckout_Reusable myBagCheckout_Reusable;
	Desk_PDP_Reusable pdp_Reusable;
	WebDriverWait xplicitWait ;
	GenericMethods genericMethods;	
	String myBagLinkText;
	
	//This method selects country from Country Dropdown on Cart Page 
	public  void selectCountry(WebDriver driver, String country) {
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		genericMethods =  new GenericMethods();
		
		genericMethods.selectDropdown(driver, myBagPageObjects.MYBag_Country_Domestic_DD, country, "Country Dropdown");
		GenericMethods.Waitformilliseconds(2000);
	}
	
	public  void clickCheckoutButton(WebDriver driver) {
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		genericMethods =  new GenericMethods();
		
		genericMethods.Click(driver, myBagPageObjects.MYBag_CHECKOUT_BTN,"MyBag Checkout BTN");	
		GenericMethods.waitForPageLoaded(driver);		
	}

	public void emptyMyBag(WebDriver driver,String site) {
		//ATUReports.setTestCaseReqCoverage("This Test Case Clicks top Login Link");
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		homePageLoginPageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		boolean isCheckoutButtonPresent;
		List<WebElement> removeLink;
		
		myBagLinkText=genericMethods.getText(driver, "//li [@class='mybag-ul my-bag-link']/a/span");
		if (myBagLinkText.equalsIgnoreCase("My Bag 0")) {
			ATUReports.add("Verify Items in the Cart", "","There are no items in the cart",true,LogAs.INFO);
		} else {
			genericMethods.Click(driver, myBagPageObjects.homePage_UtilityNav_MyBag_LK, "My bag link");
			WinAuth.aitoIT_AuthHandling(driver, "storefornt", "barneys");
			isCheckoutButtonPresent=genericMethods.isElementPresentforMouseHover(myBagPageObjects.MYBag_CHECKOUT_BTN);
			if(isCheckoutButtonPresent==true){			
				removeLink=driver.findElements(By.xpath("//a[@class='removeCartItem']"));
				ATUReports.add("Verify Items in the Cart", "","There are "+(removeLink.size()+1)+" items in the cart",true, LogAs.INFO);
				
				for (int i = 1; i <= removeLink.size()/2; i++) {
					genericMethods.Click(driver, myBagPageObjects.MYBag_productRemove_LK,"MyBag ProductRemove Link");
				}				
			}else {
				ATUReports.add("Verify Page displayed", "My Bag Page should be displayed","My Bag page is not displayed",true, LogAs.FAILED);
			}
		}			
	}

	public void clickMyBag(WebDriver driver,String site){
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		genericMethods =  new GenericMethods();
		
		genericMethods.Click(driver, myBagPageObjects.MyBagTopLinkFromHeader,"MY_BAG_LINK");	
	}
//----------------------------------------------------------------------------------------------------------

	
//GV: Break this reusable modules into Multiple small modules
	public void verifyProductDetailsOnMyCart(WebDriver driver,String urlFromXML,String BarneysUrl1 , String WareHouseUrl2) {
		genericMethods =  new GenericMethods(); 
	    myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	    pdpPageObjects=PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
	    xplicitWait = new WebDriverWait(driver, 15);
	    String popupName;
	    String pdpProductName;
	    String pdpSalesPrice;
	    String productSalesPriceQuickview;
	    String productNameQuickview;
	    String miniCart_ProductSales_Price;
	    String miniCart_productName ;
	    String pagename;
	    String updatedProductQuantity;
	    String cart_item;
	   
	        
	    xplicitWait.until(ExpectedConditions.visibilityOf(pdpPageObjects.PDP_ProductName_LB));
	    pdpProductName=genericMethods.getText(driver, pdpPageObjects.PDP_ProductName_LB);
	    pdpSalesPrice=genericMethods.getText(driver, pdpPageObjects.PDP_SalesPrice_TV);
	    genericMethods.Click(driver, myBagPageObjects.AddtoCart_BTN,"AddtoCart BTN");
	    
	    GenericMethods.Waitformilliseconds(4000);
	    popupName = genericMethods.getText(driver, myBagPageObjects.MybagfromPDP_gettext);
	    System.out.println(popupName);
	    if (popupName.contains("MY BAG")) {
	    	ATUReports.add("Verify Mini Cart", "MY BAG\nTotal 1 item",popupName, true, LogAs.PASSED);
	    	//Assert.assertEquals(popupName, "MY BAG\nTotal 1 item");
		} else {
			ATUReports.add("Verify Mini Cart", "MY BAG\nTotal 1 Item",popupName, true, LogAs.FAILED);
			//Assert.assertEquals(popupName, "MY BAG\nTotal 1 item");
		}
	   
	    miniCart_ProductSales_Price=genericMethods.getText(driver, pdpPageObjects.MiniCart_ProductSalesPrice_TV);
	    System.out.println(miniCart_ProductSales_Price);
	    System.out.println(pdpSalesPrice);
	    if(miniCart_ProductSales_Price.contains(pdpSalesPrice)){ 
	    	ATUReports.add("Compare Price from PDP and Mini Cart","mini Crat Price is :"+miniCart_ProductSales_Price,"PDP Sales Price is : "+ pdpSalesPrice, true, LogAs.PASSED);
	    }else{
	    	ATUReports.add("Compare Price from PDP and Mini Cart","mini Crat Price is :"+miniCart_ProductSales_Price,"PDP Sales Price is : "+ pdpSalesPrice, true, LogAs.PASSED);
	    	//Assert.assertEquals(miniCart_ProductSales_Price, pdpSalesPrice);
	    }
	    
	    miniCart_productName = genericMethods.getText(driver, myBagPageObjects.ProductNameFromPDP_gettext); 
	    if (miniCart_productName.equalsIgnoreCase(pdpProductName)) {
	    	ATUReports.add("Compare Product Name from PDP and Mini Cart","mini Crat Product Name  is :"+miniCart_productName,"PDP Product Name is : "+ pdpProductName, true, LogAs.PASSED);
		} else {
			ATUReports.add("Compare Product Name from PDP and Mini Cart","mini Crat Product Name  is :"+miniCart_productName,"PDP Product Name is : "+ pdpProductName, true, LogAs.PASSED);
			Assert.assertEquals(miniCart_productName, pdpProductName);
		}
//Verification on Mini Cart Ends
	    
	    genericMethods.Click(driver, myBagPageObjects.Checkout_BTN,"Checkout BTN");//Take Product name from PDP and compare it here
	    GenericMethods.Waitformilliseconds(1000);
	    xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.CartPage_text));
	    
	    pagename= genericMethods.getText(driver, myBagPageObjects.CartPage_text); 
	    if (pagename.contains("MY BAG")){
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is on MY BAG page",true,LogAs.PASSED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		 }else {
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is not on MY BAG page",true,LogAs.FAILED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		 }   
	    
	    genericMethods.Click(driver, myBagPageObjects.MyBag_ProductQuickview_Edit_LK,"Product Quick View Link");
	    xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.ProductQuickView_PP));
	    ATUReports.add("Verify If Product Quick View is open","Product Quick View Should open","Product QUick View is Open", true, LogAs.PASSED);
	    
	    productSalesPriceQuickview = genericMethods.getText(driver, myBagPageObjects.ProductPriceFromQuickView_TV);
	    if(productSalesPriceQuickview.equalsIgnoreCase(miniCart_ProductSales_Price)){ 
	    	ATUReports.add("Compare Price from Mini Cart and Product Quick View","Product Quick View Price is :"+productSalesPriceQuickview,"Mini Cart Sales Price is : "+ pdpSalesPrice, true, LogAs.PASSED);
	    }else{
	    	ATUReports.add("Compare Price from Mini Cart and Product Quick View","Product Quick View Price is :"+productSalesPriceQuickview,"Mini Cart Sales Price is : "+ pdpSalesPrice, true, LogAs.PASSED);
	    	Assert.assertEquals(productSalesPriceQuickview, pdpSalesPrice);
	    }
	    
	    productNameQuickview = genericMethods.getText(driver, myBagPageObjects.ProductNameFromQuickView_TV); 
	    if (productNameQuickview.equalsIgnoreCase(pdpProductName)) {
	    	ATUReports.add("Compare Product Name from PDP and Product Quick View","Product Quick View Product Name  is :"+productNameQuickview,"PDP Product Name is : "+ pdpProductName, true, LogAs.PASSED);
		} else {
			ATUReports.add("Compare Product Name from PDP and Product Quick View","Product Quick View Product Name  is :"+productNameQuickview,"PDP Product Name is : "+ pdpProductName, true, LogAs.PASSED);
			Assert.assertEquals(productNameQuickview, pdpProductName);
		}
	   
	    genericMethods.Click(driver, myBagPageObjects.PDPPageLinkOnQickViewPage_LK,"Quick View Link");
	    //xplicitWait.until(ExpectedConditions.visibilityOf(pdpPageObjects.PDP_BUY_BTN));   
	    ATUReports.add("Verify Page Displayed","User should be on PDP page", "user is on PDP page",true,LogAs.INFO); 
	    
	    genericMethods.Click(driver, myBagPageObjects.CartPage_LK,"CartPage Link");
	    genericMethods.Click(driver, myBagPageObjects.Checkout_BTN, "Checkout BTN");
	    pagename = genericMethods.getText(driver, myBagPageObjects.CartPage_text); 
	    if (pagename.contains("MY BAG")){
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is on MY BAG page",true,LogAs.PASSED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		 }else {
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is not on MY BAG page",true,LogAs.FAILED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		 }  
	    
	    //genericMethods.selectDropdown(driver, myBagPageObjects.Country_DD, "India","Country Dropdown");
	    genericMethods.selectDropdown(driver, myBagPageObjects.Country_DD, "Canada","Country Dropdown");
	    
	    genericMethods.Click(driver, myBagPageObjects.Quantity1_TB,"Quantity Textbox");
	    genericMethods.inputValue(driver, myBagPageObjects.Quantity1_TB, "10","Quantity Textbox");
	    genericMethods.Click(driver, myBagPageObjects.UpdateQuantity_LK,"Update Qty Link");
	    GenericMethods.Waitformilliseconds(5000);
	    updatedProductQuantity=genericMethods.getText(driver, myBagPageObjects.MyBagTopLinkWithQuantity_TV);
	    if (updatedProductQuantity.equalsIgnoreCase("MY BAG 10")) {
	    	ATUReports.add("Verify Updated Quantity","QUantity Should be : MY BAG 10", "New Quantity is "+updatedProductQuantity,true,LogAs.INFO); 
		} else {
			ATUReports.add("Verify Updated Quantity","QUantity Should be : MY BAG 10", "New Quantity is "+updatedProductQuantity,true,LogAs.INFO);
			Assert.assertEquals(updatedProductQuantity, "MY BAG 10");
		}
	   
	    genericMethods.Click(driver, myBagPageObjects.Remove_LK,"Remove Link");
	    GenericMethods.Waitformilliseconds(5000);
	    updatedProductQuantity=genericMethods.getText(driver, myBagPageObjects.MyBagTopLinkWithQuantity_TV);
	    if (updatedProductQuantity.equalsIgnoreCase("MY BAG 0")) {
	    	ATUReports.add("Verify Updated Quantity","QUantity Should be : MY BAG 0", "New Quantity is "+updatedProductQuantity,true,LogAs.INFO); 
		}else {
			ATUReports.add("Verify Updated Quantity","QUantity Should be : MY BAG 0", "New Quantity is "+updatedProductQuantity
		    ,true,LogAs.INFO);
			Assert.assertEquals(updatedProductQuantity, "MY BAG 0");
		}	    
   
	    cart_item= genericMethods.getText(driver, myBagPageObjects.EmptyCart_gettext);
	    if(cart_item.contains("Your shopping bag is empty.")){
	    	ATUReports.add("Verify Position of Continue shopping Button","Continue shopping  button should be at botton and checkout button should be not be present ", "Continue shopping button is at botton and Checkout button is not present",true,LogAs.INFO);
	    	genericMethods.Click(driver, myBagPageObjects.ContinueShoppingEmptyCart_LK,"Continue Shopping Link");
	     
	    	String  CurrentPageTitle=GenericMethods.getMyPagetitle(driver);			
			if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){
				GenericMethods.Waitformilliseconds(2000);
				driver.get(urlFromXML+WareHouseUrl2);		
			}else{
				GenericMethods.Waitformilliseconds(2000);
				driver.get(urlFromXML+BarneysUrl1);				
			}  
	     
			GenericMethods.Waitformilliseconds(2000);
			xplicitWait.until(ExpectedConditions.visibilityOf(pdpPageObjects.PDP_BUY_BTN));   
			ATUReports.add("Verify Page Displayed","User should be on PDP page", "user is on PDP page",true,LogAs.INFO); 	    	     
			GenericMethods.Waitformilliseconds(1000);
			genericMethods.Click(driver, myBagPageObjects.AddtoCart_BTN,"AddtoCart BTN");
			GenericMethods.Waitformilliseconds(1000);
			popupName = genericMethods.getText(driver, myBagPageObjects.MybagfromPDP_gettext);
		    if (popupName.contains("MY BAG")) {
		    	ATUReports.add("Verify Mini Cart", "MY BAG\nTotal 1 Item",popupName, true, LogAs.PASSED);
			} else {
				ATUReports.add("Verify Mini Cart", "MY BAG\nTotal 1 Item",popupName, true, LogAs.PASSED);
				Assert.assertEquals(popupName, "MY BAG\nTotal 1 Item");
			}	       
	    }else{
	    	 ATUReports.add("Verify Position of Continue shopping Button","Continue shopping  button should be at botton and checkout button should be not be present ", "Continue shopping button is at botton and Checkout button is not present",true,LogAs.INFO);
	    	 ATUReports.add("Verify Updated Quantity","QUantity Should be : MY BAG 0", "New Quantity is "+updatedProductQuantity,true,LogAs.INFO);
	    	 Assert.assertEquals(updatedProductQuantity, "MY BAG 0");
	    }
	  }
	
	
	public void addProductFromPDPandVerifyCartQuantityOnTop(WebDriver driver) {
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	    genericMethods =  new GenericMethods();
	    xplicitWait=new WebDriverWait(driver, 15);
	    pdpPageObjects=PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
	    String pagename;
	    GenericMethods.Waitformilliseconds(3000);
	       genericMethods.Click(driver, myBagPageObjects.AddtoCart_BTN,"Addtocart BTN");
	    ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);
	    GenericMethods.Waitformilliseconds(2000);
	    //genericMethods.isElementPresentforMouseHover(myBagPageObjects.MybagfromPDP_gettext);
	    String Popupname = genericMethods.getText(driver, myBagPageObjects.MybagfromPDP_gettext);
	    System.out.println(Popupname);
	    if (Popupname.contains("MY BAG")){
	   ATUReports.add("System displaying Mini Cart pop up", "Customer should be on Mini Cart pop up","Customer is on MY BAG pop up",true,LogAs.PASSED);
	   Assert.assertEquals(Popupname, "MY BAG\nTotal 1 item");     
	    }else{
	   ATUReports.add("Customer is not on Mini Cart pop up", "Customer should be on Mini Cart pop up","Customer is not on MY BAG pop up",true,LogAs.FAILED);
	   Assert.assertEquals(Popupname, "MY BAG\nTotal 1 item");     
	    }
		 
	/*	  
	 * String RECOMMENDED = genericMethods.getText(driver, myBagPageObjects.RECOMMENDED_gettext);
		  System.out.println(RECOMMENDED);
		  String subtotal = genericMethods.getText(driver, myBagPageObjects.subtotal_gettext);
		  System.out.println(subtotal);
		 */ 
		  
		  genericMethods.Click(driver, myBagPageObjects.Checkout_BTN,"Checkout BTN");
		  GenericMethods.Waitformilliseconds(1000);
		  
		  //genericMethods.Click(driver, myBagPageObjects.CartPage_LK,"CartPage Link");
		  pagename = genericMethods.getText(driver, myBagPageObjects.CartPage_text); 
		  if (pagename.contains("MY BAG")){
			  ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is on MY BAG page",true,LogAs.PASSED);
			  Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		  }else {
			  ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is not on MY BAG page",true,LogAs.FAILED);
			  Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		  } 
	    
		  genericMethods.Click(driver, myBagPageObjects.MyBag_ProductQuickview_Edit_LK,"Product Quick View Link");
		  xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.ProductQuickView_PP));
		  ATUReports.add("Verify If Product Quick View is open","Product Quick View Should open","Product QUick View is Open", true, LogAs.PASSED);
		    
		  genericMethods.Click(driver, myBagPageObjects.PDPPageLinkOnQickViewPage_LK,"Product Quick View Link");
		  GenericMethods.Waitformilliseconds(2000);
		  xplicitWait.until(ExpectedConditions.visibilityOf(pdpPageObjects.PDP_BUY_BTN));   
		  ATUReports.add("Verify Page Displayed","User should be on PDP page", "user is on PDP page",true,LogAs.INFO); 
		  
		  genericMethods.Click(driver, myBagPageObjects.CartPage_LK,"CartPage Link");
		  genericMethods.Click(driver, myBagPageObjects.Checkout_BTN, "Checkout BTN");
		  pagename = genericMethods.getText(driver, myBagPageObjects.CartPage_text); 
		  if (pagename.contains("MY BAG")){
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is on MY BAG page",true,LogAs.PASSED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		  }else {
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is not on MY BAG page",true,LogAs.FAILED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		  } 
	}
	
	public void addtocart(WebDriver driver) {
			genericMethods = new GenericMethods();
		 	myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		 	globalNavReusable=new Desk_GlobalNav_Reusable();
		 	categoryProductClick_Reusable=new Desk_CategoryProductClick_Reusable();
		 	pdp_Reusable=new Desk_PDP_Reusable();
		 	
		 	GenericMethods.Waitformilliseconds(2000);
		 	//----------------------Category Browse--------------------------
	 		globalNavReusable.CategoryMouseHover(driver, "Women");
			globalNavReusable.clickSubCategoryName(driver, "Women","Dresses");	
			ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
			categoryProductClick_Reusable.browseProductandClickonBNY(driver, "2000");
	 			
	 		//----------------------------PDP---------------------------------
			pdp_Reusable.selectAvailableColorOnPDP(driver);
			pdp_Reusable.selectAvailableSizeOnPDP(driver);
			pdp_Reusable.AddtoCartButtononPDP(driver);
			pdp_Reusable.miniBagPopuponPDP(driver);
			pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
			GenericMethods.Waitformilliseconds(4000);
			ATUReports.add("Customer is on Cart page", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);
			genericMethods.Click(driver,myBagPageObjects.Country_DD,"Country Dropdown");
			genericMethods.Click(driver,myBagPageObjects.Countryvalue_DD,"Country Value Dropdown");
			genericMethods.Click(driver,myBagPageObjects.CheckoutfromCart_BTN,"Checkout BTN");
			GenericMethods.Waitformilliseconds(2000);		
		}
	  
	public void addtocartCat(WebDriver driver) {
		  myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		  genericMethods =  new GenericMethods();
		  
		  String women = genericMethods.isElementPresent(driver, "//*[@id='top-nav']/div[1]/ul/li[2]/a");
		  if(women.equals("present")){
			  genericMethods.Click(driver,myBagPageObjects.WAREHOUSEWomenFromHeader_LK,"Women Category");
			  selectSizeAndColor_AddtoCartOnWHS(driver);
		  }else{
			  genericMethods.Click(driver,myBagPageObjects.WomenFromHeader_LK,"Women Category"); 
			  selectSizeAndColor_AddtoCartOnBNY(driver);
		  }
	  }
	  
	
	public void GotoShippingPage(WebDriver driver) {
		  	myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	      	genericMethods = new GenericMethods();
	      	globalNavReusable= new Desk_GlobalNav_Reusable();
	      	pdp_Reusable =new Desk_PDP_Reusable();
	      	myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
	   
	      	//----------------------Category Browse--------------------------
	      	globalNavReusable.CategoryMouseHover(driver, "Women");
			globalNavReusable.clickSubCategoryName(driver,"Women", "Dresses");	
			ATUReports.add("Check Expanded Navigation", "Should be expanded", "Its Expanded", true,LogAs.PASSED);
			categoryProductClick_Reusable.browseProductandClickonBNY(driver,"2000");
			
		//----------------------------PDP---------------------------------			
			pdp_Reusable.selectAvailableColorOnPDP(driver);
			pdp_Reusable.selectAvailableSizeOnPDP(driver);
			pdp_Reusable.AddtoCartButtononPDP(driver);
			pdp_Reusable.miniBagPopuponPDP(driver);
			pdp_Reusable.miniBagPopupclickOnCheckout_BT(driver);
	      
			//------------------My Bag(Guest and Logged In User)---------------------------------
			myBagCheckout_Reusable.selectCountry(driver, "United States");
	  }
	
	/* 
	public void Changeshippingto_India(WebDriver driver,String URLfronXML,String BarneysUrl1,String WareHouseUrl2){	 
		  myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	      genericMethods = new GenericMethods();
	      myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
	      String iscartEmpty;
	      String isMybaglink;
	      
	      isMybaglink = genericMethods.isElementPresent(driver, "//*[@id='secondary']/div/div[2]/a"); 
	      System.out.println(isMybaglink);
	      if (isMybaglink.equals("present")){
	    	  genericMethods.Click(driver,myBagPageObjects.ShippingCartpage_LK,"ShippingCartpage Link");	    	  
	      }else{
	    	  genericMethods.Click(driver,myBagPageObjects.CartPage_LK,"CartPage Link");
	      }
	      
	      iscartEmpty= genericMethods.isElementPresent(driver, "//*[@id='primary']/div[2]/a/span"); 
	      System.out.println(iscartEmpty);
	      if (iscartEmpty.equals("present")){	    	
	    	  myBagCheckout_Reusable.GotoPDP(driver, URLfronXML,BarneysUrl1, WareHouseUrl2);
		  	genericMethods.Click(driver,myBagPageObjects.AddtoCart_BTN,"Add to Cart button");
			GenericMethods.Waitformilliseconds(2000);
			ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);
			genericMethods.Click(driver,myBagPageObjects.CartPage_LK,"CartPage Link");				
			ATUReports.add("Customer is on Cart page", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);	    	 	    	  
	      }else{
	    	  ATUReports.add("Customer Cart is not empty", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);			  
	      }
	      genericMethods.Click(driver,myBagPageObjects.CountryvalueIndia_DD,"Country Value Dropdown");
		  ATUReports.add("Customer is selecting Country as India", "System should select Shipping country as india","Country as india", true, LogAs.INFO);
	      genericMethods.Click(driver,myBagPageObjects.CheckoutfromCart_BTN,"Checkout BTN");
	      ATUReports.add("Customer is on Shipping page", "System should display Shipping page","Shipping page open", true, LogAs.INFO);
	 	}
		  
	public void Changeshippingto_US(WebDriver driver,String URLfronXML, String BarneysUrl1,String WareHouseUrl2) {
		 
		  myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	      genericMethods = new GenericMethods();
	      myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
	      xplicitWait=new WebDriverWait(driver, 15);
	      String myBagLinkText;

	      myBagLinkText=driver.findElement(By.xpath("//div[@id='mini-myBagPageObjects']/div[@class='mini-myBagPageObjects-total']/a[@class='mini-myBagPageObjects-link']")).getText();
	      if (myBagLinkText.equalsIgnoreCase("My Bag 0")){	    	
	    	  myBagCheckout_Reusable.GotoPDP(driver, URLfronXML, BarneysUrl1, WareHouseUrl2);
		    GenericMethods.Waitformilliseconds(3000);
		  	genericMethods.Click(driver,myBagPageObjects.AddtoCart_BTN,"AddtoCart BTN");
			GenericMethods.Waitformilliseconds(2000);
			ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);
			genericMethods.Click(driver,myBagPageObjects.CartPage_LK,"CartPage Link");
			ATUReports.add("Customer is on Cart page", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);	    	  
	      }else{
	    	  ATUReports.add("Customer Cart is not empty", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);			  
	      }
	      //genericMethods.Click(driver,myBagPageObjects.Country_DD);
		  //genericMethods.Click(driver,myBagPageObjects.Countryvalue_DD);
		  genericMethods.selectDropdown(driver, myBagPageObjects.Country_DD, "United States","Country Value Dropdown");
		  ATUReports.add("Customer is selecting Country as US", "System should select Shipping country as US","Country as US", true, LogAs.INFO);
	      genericMethods.Click(driver,myBagPageObjects.CheckoutfromCart_BTN,"Checkout BTN");
	      ATUReports.add("Customer is on Shipping page", "System should display Shipping page","Shipping page open", true, LogAs.INFO);
	 }  
		  
	public void Changeshippingto_Canada(WebDriver driver,String URLfronXML, String BarneysUrl1,String WareHouseUrl2) {
		  myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);			  
		  genericMethods = new GenericMethods();
		  myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
	      String iscartEmpty;
		  String isMybaglink;
	      
  //GV:correct xpath
	  isMybaglink= genericMethods.isElementPresent(driver, "//*[@id='secondary']/div/div[2]/a"); 
	  if (isMybaglink.equals("present")){
		  genericMethods.Click(driver,myBagPageObjects.ShippingCartpage_LK,"ShippingCartpage Link");	    	  
	  }else{
		  genericMethods.Click(driver,myBagPageObjects.CartPage_LK,"CartPage Link");
		  }
  //GV:correct xpath	 	      		 	  
	  iscartEmpty= genericMethods.isElementPresent(driver, "//*[@id='primary']/div[2]/a/span"); 
	  if (isMybaglink.equals("present")){	    	
		  myBagCheckout_Reusable.GotoPDP(driver, URLfronXML, BarneysUrl1, WareHouseUrl2);
			genericMethods.Click(driver,myBagPageObjects.AddtoCart_BTN,"AddtoCart Button");
			//GenericMethods.Waitformilliseconds(2000);
			ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);
			genericMethods.Click(driver,myBagPageObjects.CartPage_LK,"CartPage Link");
			ATUReports.add("Customer is on Cart page", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);	    	  
	  }else{
	  		ATUReports.add("Customer Cart is not empty", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);			  
	  }
	  
	  genericMethods.inputValue(driver, myBagPageObjects.Quantity1_TB,"1","2");
	  genericMethods.Click(driver,myBagPageObjects.UpdateQuantity_LK,"Update Qty link");
	  genericMethods.Click(driver,myBagPageObjects.Country_DD,"Country Dropdown");
	  genericMethods.Click(driver,myBagPageObjects.ShippingChargesForCanada_gettext,"ShippingChargesForCanada");
	  ATUReports.add("Customer is selecting Country as US", "System should select Shipping country as US","Country as US", true, LogAs.INFO);
	  genericMethods.Click(driver,myBagPageObjects.CheckoutfromCart_BTN,"Checkout BTN");
	  ATUReports.add("Customer is on Shipping page", "System should display Shipping page","Shipping page open", true, LogAs.INFO);
	}
		  
	*/
	
	public void selectSizeAndColor_AddtoCartOnBNY(WebDriver driver)  {
		  genericMethods.Click(driver,myBagPageObjects.Shoes_LK,"BNY_Shoes Link");
		  ATUReports.add("Verify Navigation", "Selecting a category", true, LogAs.INFO);
		  genericMethods.Click(driver,myBagPageObjects.Shoesimg_LK,"BNY_Shoes img Link");
		  genericMethods.Click(driver,myBagPageObjects.ShoesSize_LK,"BNY_Shoes Size Link");
		  ATUReports.add("Verify Colour and Size selection on PDP", "Colour and Size should be selected","Colour and Size are selected", true, LogAs.INFO);
		  genericMethods.Click(driver,myBagPageObjects.AddtoCart_BTN,"BNY_AddtoCart Button");
		  GenericMethods.Waitformilliseconds(2000);
		  ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);	  
	}

	public void selectSizeAndColor_AddtoCartOnWHS(WebDriver driver) {
		  genericMethods = new GenericMethods();
		  
		  genericMethods.Click(driver,myBagPageObjects.WarShoes_LK,"WHS_Shoes Link");
		  ATUReports.add("Verify Navigation", "Selecting a category", true, LogAs.INFO);
		  genericMethods.Click(driver,myBagPageObjects.WarShoesimg_LK,"WHS_Shoes Image");
		  genericMethods.Click(driver,myBagPageObjects.WarShoesSize_LK,"WHS_Shoes Size");
		  ATUReports.add("Verify Colour and Size selection on PDP", "Colour and Size should be selected","Colour and Size are selected", true, LogAs.INFO);
		  genericMethods.Click(driver,myBagPageObjects.AddtoCart_BTN,"WHS_AddtoCart Button");
		  ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);		  
	}

	public void addToCarfromPDPt_MyBag_AndgotoShipping(WebDriver driver){
		 myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	     genericMethods = new GenericMethods();
	     xplicitWait=new WebDriverWait(driver, 15);
	     //xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.Addtocart_BTN));
	     
		genericMethods.Click(driver,myBagPageObjects.AddtoCart_BTN,"AddtoCart Button");
		ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);
		genericMethods.Click(driver,myBagPageObjects.Checkout_BTN,"Checkout BTN");
		genericMethods.Click(driver,myBagPageObjects.Country_DD,"Country Dropdown");
		genericMethods.Click(driver,myBagPageObjects.Countryvalue_DD,"Country Value Dropdown");
		ATUReports.add("Customer is selecting Country as US", "System should select Shipping country as US","Country as US", true, LogAs.INFO);
		genericMethods.Click(driver,myBagPageObjects.CheckoutfromCart_BTN,"Checkout BTN");
	
	}

	public void GotoPDP(WebDriver driver,String URLfromXML, String bnyProductURL , String whsProductURL ) {
		myBagCheckout_Reusable=new Desk_MYBagCheckout_Reusable();
		//String  CurrentPageTitle=GenericMethods.getMyPagetitle(driver);			
		
		/*if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){				
			myBagCheckout_Reusable.emptyMyBag(driver, "WHS");
			driver.get(URLfromXML+whsProductURL);		
		}else{*/
			//myBagCheckout_Reusable.emptyMyBag(driver, "BNY");
			driver.get(URLfromXML+bnyProductURL);				
		//}  
		ATUReports.add("Verify customer is on PDP page", "PDP should open","System should display PDP", true, LogAs.INFO);	
	}	
	
	public void myBagLinkAndArrowonHeader(WebDriver driver) {
		homePageLoginPageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		
		
	}
	
	public void elementDisplayedOnEmptyCartPage (WebDriver driver) {
		
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		homePageLoginPageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();

		String emptyMessage;
		Boolean custServiceBox;
		Boolean stellaLogo;
		
		myBagLinkText=driver.findElement(By.xpath("//a[@class='mini-cart-link']/span")).getText();
		System.out.println(myBagLinkText);
		ATUReports.add("Verify the my bag from header","My bag should be empty","My bag is empty",true, LogAs.PASSED);
		if (myBagLinkText.equalsIgnoreCase("My Bag 0")) {
			ATUReports.add("Verify Items in the Cart", "","There are no items in the myBagPageObjects",true, LogAs.INFO);
			genericMethods.Click(driver, homePageLoginPageObjects.BNY_Banner_MyBag_LK, "My bag link");
			GenericMethods.waitForPageLoaded(driver);
//*************************************Verifying the entire cart page************************************************************
			emptyMessage = genericMethods.getText(driver, myBagPageObjects.Cart_EmptyBag_Message);
			custServiceBox = genericMethods.isElementPresentforMouseHover(myBagPageObjects.Cart_CustomerServiceBox);
			stellaLogo =  genericMethods.isElementPresentforMouseHover(myBagPageObjects.Cart_CustomerServiceBox);
			System.out.println(emptyMessage + custServiceBox + stellaLogo);
			if (emptyMessage.equalsIgnoreCase("Your shopping bag is empty.") && custServiceBox == true && stellaLogo == true){
				ATUReports.add("Verify the the Cart page","Cart page should display :"+ emptyMessage + "&" + custServiceBox +"&" + stellaLogo,emptyMessage + "&" + custServiceBox +"&" + stellaLogo + "are displayed",true, LogAs.PASSED);
			}else {
				ATUReports.add("Verify the the Cart page","Cart page should display :"+ emptyMessage + "&" + custServiceBox +"&" + stellaLogo,emptyMessage + "&" + custServiceBox +"&" + stellaLogo + "are not displayed",true, LogAs.PASSED);
				Assert.assertEquals(emptyMessage, "Your shopping bag is empty.");
		}
	}else{
		Assert.assertEquals(myBagLinkText, "My Bag 0");
		System.out.println(myBagLinkText);
		ATUReports.add("Verify the my bag from header","My bag should be empty","My bag is not empty",true, LogAs.FAILED);
		}
			
	}
	

	public void EditQuickViewPpoUp(WebDriver driver) {
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		  genericMethods =  new GenericMethods();
		  xplicitWait=new WebDriverWait(driver, 15);
		 
		  GenericMethods.Waitformilliseconds(3000);
		  
//********************To Verify quick view pop up on cart page.********************************************************************
		  genericMethods.Click(driver, myBagPageObjects.MyBag_ProductQuickview_Edit_LK,"Product Quick View Link");
		  xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.ProductQuickView_PP));
		  ATUReports.add("Verify If Product Quick View is open","Product Quick View Should open","Product QUick View is Open", true, LogAs.PASSED);
		  
	}


//*****************************************To click on view product detail link on quick view pop up.******************************	  
	public void ClickOnViewProductDetail_LK_fromPopUp(WebDriver driver) {
		myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		  genericMethods =  new GenericMethods();
		  xplicitWait=new WebDriverWait(driver, 15);
		  String pagename;
		  GenericMethods.Waitformilliseconds(3000);

//*****Clicking on my bag link from PDP page after visiting from view product details link.********* 		  	  
		  genericMethods.Click(driver, myBagPageObjects.MyBagTopLinkFromHeader,"My bag link from header");
		  pagename = genericMethods.getText(driver, myBagPageObjects.CartPage_text); 
		  if (pagename.contains("MY BAG")){
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is on MY BAG page",true,LogAs.PASSED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		  }else {
		    ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is not on MY BAG page",true,LogAs.FAILED);
		    Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
		  } 
		  
	}
	
	
public void addProductFromPDPandVerifyCartPage(WebDriver driver) {
	myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	  genericMethods =  new GenericMethods();
	  pdp_Reusable = new Desk_PDP_Reusable();
	  xplicitWait=new WebDriverWait(driver, 15);
	  pdpPageObjects=PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
	  String pagename;
	  boolean removeLink;
	  boolean EditLink;
	  GenericMethods.Waitformilliseconds(3000);
	  
//Reads the text on cart page.
	  pagename = genericMethods.getText(driver, myBagPageObjects.CartPage_text); 
	  if (pagename.contains("MY BAG")){
		  ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is on MY BAG page",true,LogAs.PASSED);
		  Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
	  }else {
		  ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is not on MY BAG page",true,LogAs.FAILED);
		  Assert.assertEquals((genericMethods.getText(driver, myBagPageObjects.CartPage_text)), "MY BAG");    
	  } 
//To verify remove link on cart page.	  
	  removeLink = genericMethods.isElementPresentforMouseHover(myBagPageObjects.MyBagRemove_LK);
	  if (removeLink = true){
		  ATUReports.add("To verify remove link", "Remove link should be displayed","Remove link is displayed",true,LogAs.PASSED);
		  System.out.println(removeLink);
		  Assert.assertEquals(removeLink, true);
	  }else{
		  ATUReports.add("To verify remove link", "Remove link should be displayed","Remove is not displayed",true,LogAs.FAILED);
		  Assert.assertEquals(removeLink, false);
	  }
//To verify edit link on cart page. 
	  EditLink = genericMethods.isElementPresentforMouseHover(myBagPageObjects.MyBag_ProductQuickview_Edit_LK);
	  if (EditLink = true){
		  ATUReports.add("To verify Edit link", "Edit link should be displayed","Edit link is displayed",true,LogAs.PASSED);
		  System.out.println(EditLink);
		  Assert.assertEquals(EditLink, true);
	  }else{
		  ATUReports.add("To verify Edit link", "Edit link should be displayed","Edit link is not displayed",true,LogAs.FAILED);
		  Assert.assertEquals(EditLink, false);
	  }
	  
}
	
	
public void removeProductonCartPage(WebDriver driver) {
	  pdpPageObjects=PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
	  myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
	 // pdp_Reusable = new Desk_PDP_Reusable();
	  genericMethods =  new GenericMethods();
	  xplicitWait = new WebDriverWait(driver, 20);
	  String emptyMessage;

//***********************************Remove product from cart page.****************************************************************************************
	  genericMethods.Click(driver, myBagPageObjects.MyBagRemove_LK, "Remove link on my bag page");

	  
	  xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.Cart_EmptyBag_Message));
	  emptyMessage = genericMethods.getText(driver,myBagPageObjects.Cart_EmptyBag_Message);
	 
	  if (emptyMessage.equalsIgnoreCase("Your shopping bag is empty.")){
			ATUReports.add("Verify the the Cart page","Cart page should display message :"+ emptyMessage ,emptyMessage + " is displayed",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify the the Cart page","Cart page should display message :"+ emptyMessage ,emptyMessage + " is not displayed",true, LogAs.FAILED);
			Assert.assertEquals(emptyMessage, "Your shopping bag is empty.");
	}
}

//********************************Update quantity from cart page. *******************************************************************************************
	public void UpateQtyOnCartPage(WebDriver driver,String qtyNumber) {
	  	myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class); 
	  	categoryProductClick_Reusable=new Desk_CategoryProductClick_Reusable();
	  	
	  	pdp_Reusable=new Desk_PDP_Reusable();
	  	genericMethods = new GenericMethods();
	 	globalNavReusable=new Desk_GlobalNav_Reusable();
	 	xplicitWait = new WebDriverWait(driver, 20);
		  	
	 	boolean isQtyBoxPresent = genericMethods.isElementVisible(driver, By.id("qty-text"));
	 	System.out.println(isQtyBoxPresent);
	 	if(isQtyBoxPresent=true){
	 		ATUReports.add("Customer is on Cart page", "System should display myBagPageObjects page","Cart page open", true, LogAs.INFO);
			   		
	 	}
	      genericMethods.Click(driver, myBagPageObjects.CartPageQty_TB, "QTY textbbox");
		  genericMethods.inputValue(driver, myBagPageObjects.CartPageQty_TB, qtyNumber, "updated qty");
		  GenericMethods.Waitformilliseconds(2000);
		  genericMethods.Click(driver, myBagPageObjects.CartPageUpdate_LK, "update link below QTY Textbox");
		  
		 // xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.Cart_EmptyBag_Message));
		  xplicitWait.until(ExpectedConditions.visibilityOf(myBagPageObjects.CartPageQty_TB));
		  String qtytext = genericMethods.getText(driver, myBagPageObjects.CartPageQty_TB);
		  System.out.println("Qyt number" + qtyNumber);
		  System.out.println("Qua text box" + qtytext);
		  String mybagLinkFromheader = genericMethods.getText(driver, myBagPageObjects.MyBagTopLinkFromHeader);
		  System.out.println("fdkfjdkfjdkfdkfjkdjfkdjfk" + mybagLinkFromheader);
		  if(mybagLinkFromheader.equalsIgnoreCase("My bag " +qtyNumber)){
			 ATUReports.add("Verify the quantity on cart page","Qty should be updated with: " +qtyNumber ,qtytext+ " is displayed",true, LogAs.PASSED);
		  }else{
			  
		     ATUReports.add("Verify the quantity on cart page","Qty should be updated with: " +qtyNumber,qtytext+ " Quantity is not updated",true, LogAs.FAILED);
			 Assert.assertEquals(mybagLinkFromheader,"MY BAG 2");
		  }
 	}

//********Method to continue as a logged in user/Guest user on checkout	
	public void CheckoutLogInPopupOncart (WebDriver driver,String LoginType,String emailid,String password) {
	  	myBagPageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class); 
	  	xplicitWait = new WebDriverWait(driver, 30);
	  	
	  	//xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='checkoutLoginPopup']")));
	  	boolean Popup = genericMethods.isElementVisible(driver, By.xpath("//div[@id='checkoutLoginPopup']"));
	  	if (Popup=true){
	  		ATUReports.add("To verify if login pop up is displayed","Login pop should be displayed","Login pop up is displayed",true, LogAs.PASSED);
	  		
	  		if(LoginType.equalsIgnoreCase("login")){
	  			genericMethods.inputValue(driver, myBagPageObjects.myBag_LoginPopup_loginEmail_TB, emailid, "Username");
	  			genericMethods.inputValue(driver, myBagPageObjects.myBag_LoginPopup_loginPassword_TB, password, "password");
	  			genericMethods.Click(driver, myBagPageObjects.myBag_LoginPopup_login_BTN, "Login button");
	  			ATUReports.add("To verify if user is logged in","User is now logged in","Logged in successfully",true, LogAs.PASSED);
	  			
	  		}else if(LoginType.equalsIgnoreCase("guest")){
	  			genericMethods.Click(driver, myBagPageObjects.myBag_loginPopup_continueAsGuest_BTN, "guest continue button");
	  			ATUReports.add("To verify if user is continued as a guest","User has clicked on continue button to checkout as guest","Guest checkout",true, LogAs.PASSED);
	  		}
	  		
	  	}else {
	  		ATUReports.add("To verify if login pop up is displayed","Login pop should be displayed","Login pop up is not displayed",true, LogAs.FAILED);
	  		Assert.assertEquals(Popup, true);
		}
	  	
	
	  	
	  	
	}

}
