package reusableModules.desktop;
/*
 * 
 List Of reusable methods in this Class file:
======================Ticker================================
1.rightTickerArrowClick(WebDriver driver)
2.rightTickerArrowClick(WebDriver driver)
===================HomePage Utility Nav links===============
3.verifyWarehouseLinkOnUtilityNav(WebDriver driver)
4.verifyTheWindowLinkOnUtilityNav(WebDriver driver)
5.verifyTopLoginLinkClickable(WebDriver driver)
6.verifyTopRegisterLinkClickable(WebDriver driver)
7.verifyTopFavoriteArrowLinkClickable(WebDriver driver)
8.verifyAllSiteHeaderElements(WebDriver driver)
===================Global Nav===============================
9.globalNavElements(WebDriver driver)
===================Hero Image===============================
10.heroBannerRightArrowClick(WebDriver driver)
11.heroBannerLeftArrowClick(WebDriver driver)
=================Content Spots==============================
12.verifyPresenceOfAllContentSpot(WebDriver driver)
13.verifyPresenceOfSecondryCOntentSpot(WebDriver driver)
14.verifyPresenceOfWeAdoreContentSpot(WebDriver driver)
15.public void personalizedContentSpot(WebDriver driver)
 * 
 */

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_MyAccountPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import java.util.List;
import org.testng.Assert;
import setup.WinAuth;


public class Desk_HomePage_Reusable {
	
	WebDriverWait xplicitWait;
	Desk_BNY_HomePageObject home_PageObject;
	Desk_BNY_MyAccountPageObject myAccount_PageObjects;
	Desk_LoginRegister_Reusable loginRegisterRegister_Reusable;
	String windowHandle;
	WebElement firstSuggestionElement;
	GenericMethods genericMethods;

//=====================================================Ticker functionality==========================================================
	/*This method checks 
	 * whether more than one ticker are present.
	 * If yes, then whether the arrows of the ticker are clickable
	*/
	public void leftTickerArrowClick(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods=new GenericMethods();
		String tickerTextBeforeClick;
		String tickerTextAfterClick;
		boolean isTickerLeftArrowPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_Ticker_LeftArrow);
		
		if (isTickerLeftArrowPresent = true) {
			ATUReports.add("Verify Ticker's Left Arrow is present",  "Ticker's Left Arrow should be present", "Ticker's Left Arrow is present", true,LogAs.PASSED);
			tickerTextBeforeClick=genericMethods.getText(driver,home_PageObject.HomePage_Ticker_ActiveTicker_TV);
			genericMethods.Click(driver, home_PageObject.HomePage_Ticker_LeftArrow,"Ticker Left Arrow");
			GenericMethods.Waitformilliseconds(1000);
			tickerTextAfterClick=genericMethods.getText(driver,home_PageObject.HomePage_Ticker_ActiveTicker_TV);
			 
			//Check if ticker text after and before click are different
			if (tickerTextBeforeClick.equalsIgnoreCase(tickerTextAfterClick)) {
				ATUReports.add("Check if left ticker arrow was click", "left ticker arrow should be clicked","left ticker arrow was not clicked",true,LogAs.FAILED);
				Assert.assertNotEquals(tickerTextBeforeClick, tickerTextAfterClick);
			} else {
				ATUReports.add("Check if left ticker arrow was click", "left ticker arrow should be clicked","left ticker arrow was clicked",true,LogAs.PASSED);
			}
		}else {
			ATUReports.add("Verify Ticker's Left Arrow is present",  "Ticker's Left Arrow should be present", "Ticker's Left Arrow is not present", true,LogAs.FAILED);
			Assert.assertEquals(isTickerLeftArrowPresent, true);
		}
	}
	
	public void rightTickerArrowClick(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods=new GenericMethods();
		String tickerTextBeforeClick;
		String tickerTextAfterClick;
		boolean isTickerRightArrowPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_Ticker_RightArrow);
		GenericMethods.Waitformilliseconds(1000);
		
		if (isTickerRightArrowPresent = true) {
			ATUReports.add("Verify Ticker's Right Arrow is present",  "Ticker's Right Arrow should be present", "Ticker's Right Arrow is present", true,LogAs.PASSED);
			tickerTextBeforeClick=genericMethods.getText(driver,home_PageObject.HomePage_Ticker_ActiveTicker_TV);
			genericMethods.Click(driver, home_PageObject.HomePage_Ticker_RightArrow,"Ticker Right Arrow");
			GenericMethods.Waitformilliseconds(1000);
			tickerTextAfterClick=genericMethods.getText(driver,home_PageObject.HomePage_Ticker_ActiveTicker_TV);
			
			//Check if ticker text after and before click are different
			if (tickerTextBeforeClick.equalsIgnoreCase(tickerTextAfterClick)) {
				ATUReports.add("Check if right ticker arrow was click", "right ticker arrow should be clicked","right ticker arrow was not clicked",true,LogAs.FAILED);
				Assert.assertNotEquals(tickerTextBeforeClick, tickerTextAfterClick);
			} else {
				ATUReports.add("Check if right ticker arrow was click", "right ticker arrow should be clicked","right ticker arrow was clicked",true,LogAs.PASSED);
			}			
		}else {
			ATUReports.add("Verify Ticker's Right Arrow is present",  "Ticker's Right Arrow should be present", "Ticker's Right Arrow is not present", true,LogAs.FAILED);
			Assert.assertEquals(isTickerRightArrowPresent, true);
		}		
		GenericMethods.Waitformilliseconds(2000);
	}
		
	//Verify the Warehouse link
	public void verifyWarehouseLinkOnUtilityNav(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		windowHandle = driver.getWindowHandle();		
		String newTabTitle;
		
		
		boolean isWhsCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_WHS_LK);		
		if (isWhsCtaPresent = true) {
			ATUReports.add("Verify Warehouse link is present",  "Warehouse link should be present", "Warehouse link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.HomePage_SiteHeader_WHS_LK,"Warehouse link on Site Header");	
			ATUReports.add("Verify Warehouse link is clickable",  "Warehouse link should be clickable", "Warehouse link is clickable", true,LogAs.PASSED);
			
			//newTabTitle=genericMethods.switchTabAndgetH1text(driver);
			newTabTitle=genericMethods.switchTabAndgetPageTitle(driver);
			WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
			System.out.println(newTabTitle);
			if (newTabTitle.equalsIgnoreCase("Designer Clothing, Shoes, Bags & Accessories | Barneys Warehouse")) {
				ATUReports.add("Verify if Warehouse link is working", "Warehouse site should open in a new tab","Warehouse site opened in a new tab",true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify if Warehouse link is working", "Warehouse site should open in a new tab","Warehouse site did not open(in a new tab)",true, LogAs.FAILED);
				Assert.assertEquals(newTabTitle, "Designer Clothing, Shoes, Bags & Accessories | Barneys Warehouse");
			}
		}else{
			ATUReports.add("Verify Warehouse link is present",  "Warehouse link should be present", "Warehouse link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isWhsCtaPresent, true,"Warehouse link on Barneys header is not present");
		}	
	}
	
	//Verify the Window link
	public void verifyTheWindowLinkOnUtilityNav(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		windowHandle = driver.getWindowHandle();		
		String newTabTitle;
				
		boolean isTheWindowCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_THEWINDOW_LK);		
		if (isTheWindowCtaPresent = true) {
			ATUReports.add("Verify The Window link is present",  "The Window link should be present", "The Window link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.HomePage_SiteHeader_THEWINDOW_LK,"The Window Link");	
			
			newTabTitle=genericMethods.switchTabAndgetPageTitle(driver);
			if (newTabTitle.equals("The Window | Barneys New York")) {
				ATUReports.add("Verify if The Window link is working", "The Window site should open in a new tab","The Window site opened in a new tab",true, LogAs.PASSED);
			}else {
				ATUReports.add("Verify if The Window link is working", "The Window site should open in a new tab","The Window site did not open in a new tab",true, LogAs.FAILED);
				Assert.assertEquals(newTabTitle, "The Window | Barneys New York");
			}
			ATUReports.add("Verify The Window link is clickable",  "The Window link should be clickable", "The Window link is clickable", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify The Window link is present",  "The Window link should be present", "The Window link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isTheWindowCtaPresent, true);
		}		
	}
	
	//Verify Login Link is working and login Page is displayed	and is clickable
	public void verifyTopLoginLinkClickable(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		windowHandle = driver.getWindowHandle();		
		String pageHeaderText;
		
		boolean isLoginCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.Home_UtilityNav_TopLogin_LK);
		if (isLoginCtaPresent = true) {
			ATUReports.add("Verify Log In link is present",  "The Log In link should be present", "The Log In link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.Home_UtilityNav_TopLogin_LK,"UtilityNav Top Login");
			WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
			
			pageHeaderText=genericMethods.getText(driver, home_PageObject.Home_Login_Header_TX);
			System.out.println(pageHeaderText);
			if (pageHeaderText.equalsIgnoreCase("LOG IN TO YOUR ACCOUNT")) {
				ATUReports.add("Verify page Displayed",  "The Log In link should be clickable and Login page should be displayed", "The Log In link is clickable and Login page is displayed", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify page Displayed",  "The Log In link should be clickable and Login page should be displayed", "The Login Page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(pageHeaderText, "LOG IN TO YOUR ACCOUNT");
			}			
			//genericMethods.Click(driver, home_PageObject.HomePage_BNY_SiteHeader_LOGO, "Home Page Logo");
			//ATUReports.add("Verify User is navigated to HomePage",  "User should be navigated to HomePage", "User is navigated to HomePage", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Log In link is present",  "The Log In link should be present", "The Log In link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isLoginCtaPresent, true);
		}		
	}
	
	//Verify Register Link is working and register Page is displayed	
	public void verifyTopRegisterLinkClickable(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		windowHandle = driver.getWindowHandle();
		String pageHeaderText;
		
		boolean isRegisterCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.Home_UtilityNav_TopRegister_LK);				
		if (isRegisterCtaPresent = true) {
			ATUReports.add("Verify Register link is present",  "The Register link should be present", "The Register link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.Home_UtilityNav_TopRegister_LK,"Utility Nav register link");	
			WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
			pageHeaderText=genericMethods.getText(driver, home_PageObject.Home_Register_Header_TX);
			if (pageHeaderText.equalsIgnoreCase("CREATE AN ACCOUNT")) {
				ATUReports.add("Verify page Displayed",  "The Register link should be clickable and Register page should be displayed", "The Register link is clickable and Login page is displayed", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify page Displayed",  "The Register link should be clickable and Register page should be displayed", "The Register Page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(pageHeaderText, "CREATE AN ACCOUNT");
			}
		}else {
			ATUReports.add("Verify Register link is present",  "The Register link should be present", "The Register link is present", true,LogAs.FAILED);
			Assert.assertEquals(isRegisterCtaPresent, true);
		}	
	}
	
	//Verify favourite Heart icon 
	public void verifyTopFavoriteArrowLinkClickable(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		windowHandle = driver.getWindowHandle();
		String pageHeaderText;
		
		boolean isHeartIconPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_HeartIcon);		
		if (isHeartIconPresent = true) {
			ATUReports.add("Verify Heart Icon is present",  "The Heart Icon should be present", "The Heart Icon is present", true,LogAs.PASSED);			
			genericMethods.Click(driver, home_PageObject.HomePage_SiteHeader_HeartIcon,"Utility Nav Heart Icon");
			pageHeaderText=genericMethods.getHeaderText(driver);
			
			if (pageHeaderText.equalsIgnoreCase("MY FAVORITES")) {
				ATUReports.add("Verify Heart Icon is clickable and My favorites page is displayed",  "The Heart Icon should be clickable and My favorites page should be displayed", "The Heart Icon is clickable and My Favorites page is displayed", true,LogAs.PASSED);				
			}else {
				ATUReports.add("Verify Heart Icon is clickable and My favorites page is displayed",  "The Heart Icon should be clickable and My favorites page should be displayed", "My Favorites page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(pageHeaderText, "MY FAVORITES");
			}
		}else {
			ATUReports.add("Verify Heart Icon is present",  "The Heart Icon should be present", "The Heart Icon is not present", true,LogAs.FAILED);
			Assert.assertEquals(isHeartIconPresent, true);
		}		
	}
	
	public void verifyAllSiteHeaderElements(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		loginRegisterRegister_Reusable=new Desk_LoginRegister_Reusable();
		genericMethods = new GenericMethods();
		windowHandle = driver.getWindowHandle();		
		String newTabTitle;
		String pageHeaderText;
		
		
		boolean isWhsCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_WHS_LK);
		boolean isTheWindowCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_WHS_LK);
		boolean isLoginCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.Home_UtilityNav_TopLogin_LK);
		boolean isRegisterCtaPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.Home_UtilityNav_TopRegister_LK);
		boolean isHeartIconPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_HeartIcon);
		
		//Verify the Warehouse link
		if (isWhsCtaPresent = true) {
			ATUReports.add("Verify Warehouse link is present",  "Warehouse link should be present", "Warehouse link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.HomePage_SiteHeader_WHS_LK,"Warehouse link on Site Header");	
			ATUReports.add("Verify Warehouse link is clickable",  "Warehouse link should be clickable", "Warehouse link is clickable", true,LogAs.PASSED);
			
			newTabTitle=genericMethods.switchTabAndgetH1text(driver);
			if (newTabTitle.equals("newTabTitle")) {
				ATUReports.add("Verify if Warehouse link is working", "Warehouse site should open in a new tab","Warehouse site opened in a new tab",true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify if Warehouse link is working", "Warehouse site should open in a new tab","Warehouse site did not open(in a new tab)",true, LogAs.FAILED);
				Assert.assertEquals(newTabTitle, "newTabTitle");
			}
		}else{
			ATUReports.add("Verify Warehouse link is present",  "Warehouse link should be present", "Warehouse link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isWhsCtaPresent, true,"Warehouse link on Barneys header is not present");
		}

		//Verify the Window link
		if (isTheWindowCtaPresent = true) {
			ATUReports.add("Verify The Window link is present",  "The Window link should be present", "The Window link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.HomePage_SiteHeader_THEWINDOW_LK,"The Window Link");	
			
			newTabTitle=genericMethods.switchTabAndgetPageTitle(driver);
			if (newTabTitle.equals("The Window | Barneys New York")) {
				ATUReports.add("Verify if The Window link is working", "The Window site should open in a new tab","The Window site opened in a new tab",true, LogAs.PASSED);
			}else {
				ATUReports.add("Verify if The Window link is working", "The Window site should open in a new tab","The Window site did not open in a new tab",true, LogAs.FAILED);
				Assert.assertEquals(newTabTitle, "The Window | Barneys New York");
			}
			ATUReports.add("Verify The Window link is clickable",  "The Window link should be clickable", "The Window link is clickable", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify The Window link is present",  "The Window link should be present", "The Window link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isTheWindowCtaPresent, true);
		}
		
		//Verify Login Link is working and login Page is displayed		
		if (isLoginCtaPresent = true) {
			ATUReports.add("Verify Log In link is present",  "The Log In link should be present", "The Log In link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.Home_UtilityNav_TopLogin_LK,"UtilityNav Top Login");	
			
			pageHeaderText=genericMethods.getHeaderText(driver);
			if (pageHeaderText.equals("Log in to your account")) {
				ATUReports.add("Verify page Displayed",  "The Log In link should be clickable and Login page should be displayed", "The Log In link is clickable and Login page is displayed", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify page Displayed",  "The Log In link should be clickable and Login page should be displayed", "The Login Page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(pageHeaderText, "Log in to your account");
			}			
			//genericMethods.Click(driver, home_PageObject.HomePage_BNY_SiteHeader_LOGO, "Home Page Logo");
			//ATUReports.add("Verify User is navigated to HomePage",  "User should be navigated to HomePage", "User is navigated to HomePage", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Log In link is present",  "The Log In link should be present", "The Log In link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isLoginCtaPresent, true);
		}
		
		//Verify Register Link is working and register Page is displayed			
		if (isRegisterCtaPresent = true) {
			ATUReports.add("Verify Register link is present",  "The Register link should be present", "The Register link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, home_PageObject.Home_UtilityNav_TopRegister_LK,"Utility Nav register link");	
			
			pageHeaderText=genericMethods.getHeaderText(driver);
			if (pageHeaderText.equals("Create An Account")) {
				ATUReports.add("Verify page Displayed",  "The Register link should be clickable and Register page should be displayed", "The Register link is clickable and Login page is displayed", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify page Displayed",  "The Register link should be clickable and Register page should be displayed", "The Register Page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(pageHeaderText, "Log in to your account");
			}
		}else {
			ATUReports.add("Verify Register link is present",  "The Register link should be present", "The Register link is present", true,LogAs.FAILED);
			Assert.assertEquals(isRegisterCtaPresent, true);
		}
	
		//Verify favourite Heart icon 
		loginRegisterRegister_Reusable.loginFromHeader(driver, "gvarma0904@yopmail.com", "12345678");
		if (isHeartIconPresent = true) {
			ATUReports.add("Verify Heart Icon is present",  "The Heart Icon should be present", "The Heart Icon is present", true,LogAs.PASSED);			
			genericMethods.Click(driver, home_PageObject.HomePage_SiteHeader_HeartIcon,"Utility Nav Heart Icon");
			pageHeaderText=genericMethods.getHeaderText(driver);
			
			if (pageHeaderText.equals("My Favorites")) {
				ATUReports.add("Verify Heart Icon is clickable and My favorites page is displayed",  "The Heart Icon should be clickable and My favorites page should be displayed", "The Heart Icon is clickable and My Favorites page is displayed", true,LogAs.PASSED);				
			}else {
				ATUReports.add("Verify Heart Icon is clickable and My favorites page is displayed",  "The Heart Icon should be clickable and My favorites page should be displayed", "My Favorites page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(pageHeaderText, "My Favorites");
			}
		}else {
			ATUReports.add("Verify Heart Icon is present",  "The Heart Icon should be present", "The Heart Icon is not present", true,LogAs.FAILED);
			Assert.assertEquals(isHeartIconPresent, true);
		}		
	}
	
	/*This method hovers 
	 * on all the top categories present on the Global Nav
	 */
	public void globalNavElements(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 20);
		WebElement topNavElementForMouseHover;
		String hoveredElementClass;
		
		boolean isGlobalNavPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_TopNav_Elements);		
		xplicitWait.until(ExpectedConditions.visibilityOf(home_PageObject.HomePage_SiteHeader_TopNav_Elements));
		
		if (isGlobalNavPresent = true) {
		//List<WebElement> listOfTopCategories = driver.findElements(By.xpath("//div[@id='top-nav']/ul[@id='topnav-level-1']/li"));
		ATUReports.add("Verify presenc of Global navigation on Site", "Global Navigation should be visible on Site","Global Navigation is visible",true,LogAs.PASSED);
		
			for (int i = 0; i < home_PageObject.HomePage_SiteHeader_allTopGlobalNavElements_LK.size(); i++) {
				topNavElementForMouseHover=(home_PageObject.HomePage_SiteHeader_allTopGlobalNavElements_LK).get(i);
				//topNavElementForMouseHover=driver.findElement(By.xpath("//div[@id='top-nav']/ul[@id='topnav-level-1']/li["+(i+1)+"]"));
				genericMethods.mouseHover(driver,topNavElementForMouseHover , "top Nav Element number "+(i+1));
				GenericMethods.Waitformilliseconds(500);
				hoveredElementClass=topNavElementForMouseHover.getAttribute("class");
				if (hoveredElementClass.equalsIgnoreCase("atg_store_dropDownParent activeNav")) {
					ATUReports.add("Verify Top Category is hovered " , topNavElementForMouseHover.getText()+ "  Top Category should be hovered", topNavElementForMouseHover.getText()+" Top Category is hovered", true,LogAs.PASSED);	
				}else{
					ATUReports.add("Verify Top Category is hovered " , topNavElementForMouseHover.getText()+ "  Top Category should be hovered", topNavElementForMouseHover.getText()+" Top Category is not hovered hovered", true,LogAs.FAILED);
					Assert.assertEquals("Global Nav element should be hovered", "Global Nav element was not hovered");
				}			
			}
		}else {
			ATUReports.add("Verify presence of Global navigation on Site", "Global Navigation should be visible on Site","Global Navigation is visible",true,LogAs.PASSED);
			Assert.assertEquals(isGlobalNavPresent, true);
		}
	}
	
	/*public void clickSearchSuggestion(WebDriver driver, GenericMethods genericMethods) {
		home_PageObject = PageFactory.initElements(driver, home_PageObjectect.class);
		
		boolean isSearchPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SiteHeader_Search);
		
		if (isSearchPresent = true) {
			genericMethods.Click(driver, home_PageObject.HomePage_SiteHeader_Search);
			genericMethods.inputValue(driver, home_PageObject.HomePage_Search_Box, "jeans");
			
			genericMethods.mouseHover(driver, home_PageObject.HomePage_Search_Suggestion_List);
			firstSuggestionElement = driver.findElement(By.xpath("//ul[@id='atg_store_searchInput-sayt']//li[@class='sayt-result term hover selected']/div"));
			((JavascriptExecutor)driver).executeScript("arguments[0].click();", firstSuggestionElement);
			System.out.println("Clicked");
			GenericMethods.Waitformilliseconds(2000);
			

		}
	}*/
	
	/*This method checks 
	 * whether the carousel arrows are present on the Hero banner
	 */
	public String heroBannerRightArrowClick(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		boolean isHeroRightArrowPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_HeroBanner_RightArrow);
		System.out.println("hero right arrow"+isHeroRightArrowPresent);
		String heroClassBeforeClick;
		String heroClassAfterClick;
		
		GenericMethods.waitForPageLoaded(driver);
		GenericMethods.Waitformilliseconds(200);
		heroClassBeforeClick=home_PageObject.HomePage_HeroImage_ActiveHero_TV.getAttribute("class");
		if (isHeroRightArrowPresent == true) {
			ATUReports.add("Verify Ticker's Right Arrow is present",  "Ticker's Right Arrow should be present", "Ticker's Right Arrow is present", true,LogAs.PASSED);			
			genericMethods.Click(driver, home_PageObject.HomePage_Ticker_RightArrow,"Ticker right arrow");
			heroClassAfterClick=home_PageObject.HomePage_HeroImage_ActiveHero_TV.getAttribute("class");
			
			if (heroClassAfterClick.equalsIgnoreCase(heroClassAfterClick)) {
				ATUReports.add("Verify Ticker's Right Arrow is clickable",  "Ticker's Right Arrow should be clickable", "Ticker's Right Arrow was not clicked", true,LogAs.FAILED);
				Assert.assertNotEquals(heroClassBeforeClick, heroClassAfterClick);
				return "hero after Click: "+heroClassAfterClick;
			}else {
				ATUReports.add("Verify Ticker's Right Arrow is clickable",  "Ticker's Right Arrow should be clickable", "Ticker's Right Arrow was clicked and hero image changed", true,LogAs.PASSED);
				return "hero before Click: "+heroClassAfterClick;
			}		
		}else {
			ATUReports.add("Verify Ticker's Right Arrow is present",  "Ticker's Right Arrow should be present", "Ticker's Right Arrow is not present as there is only one banner", true,LogAs.INFO);
			//Assert.assertEquals(isHeroRightArrowPresent, true);
			return "hero after Click: "+heroClassBeforeClick;
		}	
	}
	
	public String heroBannerLeftArrowClick(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		boolean isHeroLeftArrowPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_HeroBanner_LeftArrow);
		System.out.println(isHeroLeftArrowPresent);
		String heroClassBeforeClick;
		String heroClassAfterClick;
		
		heroClassBeforeClick=home_PageObject.HomePage_HeroImage_ActiveHero_TV.getAttribute("class");
		if (isHeroLeftArrowPresent == true) {
			ATUReports.add("Verify Ticker's left Arrow is present",  "Ticker's left Arrow should be present", "Ticker's left Arrow is present", true,LogAs.PASSED);			
			genericMethods.Click(driver, home_PageObject.HomePage_Ticker_RightArrow,"Ticker right arrow");
			GenericMethods.Waitformilliseconds(500);
			heroClassAfterClick=home_PageObject.HomePage_HeroImage_ActiveHero_TV.getAttribute("class");
			
			if (heroClassBeforeClick!=heroClassAfterClick) {
				ATUReports.add("Verify Ticker's left Arrow is clickable",  "Ticker's left Arrow should be clickable", "Ticker's left Arrow was clicked and hero image changed", true,LogAs.PASSED);
				return "hero after Click: "+heroClassAfterClick;				
			}else {
				ATUReports.add("Verify Ticker's left Arrow is clickable",  "Ticker's left Arrow should be clickable", "Ticker's left Arrow was not clicked", true,LogAs.FAILED);
				Assert.assertNotEquals(heroClassBeforeClick, heroClassAfterClick);
				return "hero after Click: "+heroClassAfterClick;
			}		
		}else {
			ATUReports.add("Verify Ticker's left Arrow is present",  "Ticker's left Arrow should be present", "Ticker's left Arrow is not present as there is only one banner", true,LogAs.FAILED);
			//Assert.assertEquals(isHeroLeftArrowPresent, true);
			return "hero before Click: "+heroClassBeforeClick;
		}
	}
	
	 /*This method checks 
	  * whether Personalized Content Spot, Secondary Content Spot and We Adore Images are present
	  */
	public void verifyPresenceOfAllContentSpot(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		boolean isSecondarySpotPresent;
		boolean isWeAdorePresent;
		boolean isPersonalizedContentSpotPresent;
		//List<WebElement> weAdoreSection;
		Dimension weAdoreSectionImageDimensions;		
		Dimension dimensions;
		
		isSecondarySpotPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SecondarySpot_Image);
		isWeAdorePresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_WeAdore_Image);
		isPersonalizedContentSpotPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_PersonalizedContentSpot_Image);
		
		if (isSecondarySpotPresent = true) {
			dimensions = driver.findElement(By.cssSelector("img.image")).getSize();
			if ((dimensions.width > 0) && (dimensions.height > 0)){
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.FAILED);
				Assert.assertNotEquals(dimensions, null);
			}
		}else {
			ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is not present", true,LogAs.FAILED);
			Assert.assertEquals(isSecondarySpotPresent, true);
		}
				
		if ((home_PageObject.HomePage_ContentSpots_WeAdore_IMG).size() > 0) {
			ATUReports.add("Verify We Adore section is present", "We Adore section should be present", "We Adore section is present", true,LogAs.PASSED);
			for (int i = 1; i <= (home_PageObject.HomePage_ContentSpots_WeAdore_IMG).size(); i++) {
				weAdoreSectionImageDimensions = (home_PageObject.HomePage_ContentSpots_WeAdore_IMG).get(i-1).getSize();
				if ((weAdoreSectionImageDimensions.width > 0) && (weAdoreSectionImageDimensions.height) > 0) {
					/*System.out.println("We Adore Image " + i + "is present");
					System.out.println("We adore width " + weAdoreSectionImageDimensions.width);
					System.out.println("We adore height " + weAdoreSectionImageDimensions.height);*/
					ATUReports.add("Verify We Adore Section Image" + i +" is present", "We Adore Section Image" + i + "should be present", "We Adore Section Image" + i +"is present", true,LogAs.PASSED);
				}else {
					ATUReports.add("Verify We Adore Section Image" + i +" is present", "We Adore Section Image" + i + "should be present", "We Adore Section Image" + i +"is not present", true,LogAs.FAILED);
					Assert.assertNotEquals(weAdoreSectionImageDimensions, null);
				}
			}
		}else {
			ATUReports.add("Verify We Adore section is present", "We Adore section should be present", "We Adore section is not present", true,LogAs.FAILED);
			Assert.assertNotEquals("We Adore section should be present", "We Adore section should not be present");
		}
		//=========================================================================================
		/*if (isWeAdorePresent = true) {
			dimensions = driver.findElement(By.id("weAdore")).getSize();
			System.out.println("We Adore image Width : "+ dimensions.width);
			System.out.println("We Adore image Height : "+ dimensions.height);
			ATUReports.add("Verify We Adore image is present", "We Adore image should be present", "We adore image is present", true,LogAs.PASSED);
		}
		else {
			System.out.println("We Adore image is not present");
			ATUReports.add("Verify We Adore image is present", "We Adore image should be present", "We Adore image is not present", true,LogAs.FAILED);
			Assert.assertEquals(isWeAdorePresent, true);
		}*/
		
		if (isPersonalizedContentSpotPresent = true) {
			dimensions = driver.findElement(By.id("personalizedRRContent")).getSize();
			System.out.println("Personalized Content Spot Dimensions " + dimensions);
			System.out.println("Personalized Content Spot Width : "+ dimensions.width);
			System.out.println("Personalized Content Spot Height : "+ dimensions.height);
			ATUReports.add("Verify Personalized Content Spot is present", "Personalized Content Spot should be present", "Personalized Content Spot is present", true,LogAs.PASSED);
			if ((dimensions.width > 0) && (dimensions.height > 0)){
				System.out.println("Secondary Spot is Present " + isSecondarySpotPresent);
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.FAILED);
				Assert.assertNotEquals(dimensions, null);
			}
		}else {
			System.out.println("Personalized Content Spot is not present");
			ATUReports.add("Verify Personalized Content Spot is present", "Personalized Content Spot should be present", "Personalized Content Spot is not present", true,LogAs.FAILED);
			Assert.assertEquals(isPersonalizedContentSpotPresent, true);
		}	
	}
		
	public void verifyPresenceOfSecondryCOntentSpot(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		boolean isSecondarySpotPresent;		
		Dimension dimensions;
		
		isSecondarySpotPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_SecondarySpot_Image);
		if (isSecondarySpotPresent = true) {
			dimensions = driver.findElement(By.cssSelector("img.image")).getSize();
			if ((dimensions.width > 0) && (dimensions.height > 0)){
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.FAILED);
				Assert.assertNotEquals(dimensions, null);
			}
		}else {
			ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is not present", true,LogAs.FAILED);
			Assert.assertEquals(isSecondarySpotPresent, true);
		}				
	}
		
	public void verifyPresenceOfWeAdoreContentSpot(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		Dimension weAdoreSectionImageDimensions;
		
		if ((home_PageObject.HomePage_ContentSpots_WeAdore_IMG).size() > 0) {
			ATUReports.add("Verify We Adore section is present", "We Adore section should be present", "We Adore section is present", true,LogAs.PASSED);
			for (int i = 1; i <= (home_PageObject.HomePage_ContentSpots_WeAdore_IMG).size(); i++) {
				weAdoreSectionImageDimensions = (home_PageObject.HomePage_ContentSpots_WeAdore_IMG).get(i-1).getSize();
				if ((weAdoreSectionImageDimensions.width > 0) && (weAdoreSectionImageDimensions.height) > 0) {
					ATUReports.add("Verify We Adore Section Image" + i +" is present", "We Adore Section Image" + i + "should be present", "We Adore Section Image" + i +"is present", true,LogAs.PASSED);
				}else {
					ATUReports.add("Verify We Adore Section Image" + i +" is present", "We Adore Section Image" + i + "should be present", "We Adore Section Image" + i +"is not present", true,LogAs.FAILED);
					Assert.assertNotEquals(weAdoreSectionImageDimensions, null);
				}
			}
		}else {
			ATUReports.add("Verify We Adore section is present", "We Adore section should be present", "We Adore section is not present", true,LogAs.FAILED);
			Assert.assertNotEquals("We Adore section should be present", "We Adore section should not be present");
		}	
	}
	
	public void verifyPresenceOfPersonalizedCOntentSpots(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		boolean isPersonalizedContentSpotPresent;		
		Dimension dimensions;
		
		isPersonalizedContentSpotPresent = genericMethods.isElementPresentforMouseHover(home_PageObject.HomePage_PersonalizedContentSpot_Image);	
		if (isPersonalizedContentSpotPresent = true) {
			dimensions = driver.findElement(By.id("personalizedRRContent")).getSize();
			System.out.println("Personalized Content Spot Dimensions " + dimensions);
			System.out.println("Personalized Content Spot Width : "+ dimensions.width);
			System.out.println("Personalized Content Spot Height : "+ dimensions.height);
			ATUReports.add("Verify Personalized Content Spot is present", "Personalized Content Spot should be present", "Personalized Content Spot is present", true,LogAs.PASSED);
			if ((dimensions.width > 0) && (dimensions.height > 0)){
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify Secondary Content Spot is present", "Secondary Content Spot should be present", "Secondary Content Spot is present", true,LogAs.FAILED);
				Assert.assertNotEquals(dimensions, null);
			}
		}else {
			System.out.println("Personalized Content Spot is not present");
			ATUReports.add("Verify Personalized Content Spot is present", "Personalized Content Spot should be present", "Personalized Content Spot is not present", true,LogAs.FAILED);
			Assert.assertEquals(isPersonalizedContentSpotPresent, true);
		}	
	}
	
	 /*This method checks 
	  * whether social integration icons are clickable
	  */	
	public void clickOnSocialIcons(WebDriver driver,String socialIconName){
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
				
		switch (socialIconName) {
	      case "facebook":
	    	  genericMethods.Click(driver, home_PageObject.Footer_Social_Facebook_LK, "facebook social link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Barneys New York - Home | Facebook")) {
	  			ATUReports.add("Verify Social Page open", "Facebook should open","Facebook is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "Facebook should open","Facebook is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Barneys New York - Home | Facebook");
	  		}	
	  		
	      case "twitter":
	    	  genericMethods.Click(driver, home_PageObject.Footer_Social_twitter_LK, "Twitter social link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Barneys New York (@BarneysNY) | Twitter")) {
	  			ATUReports.add("Verify Social Page open", "Twitter should open","Twitter is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "Twitter should open","Twitter is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Barneys New York (@BarneysNY) | Twitter");
	  		}
	  		
	      case "pintrest":
	    	  genericMethods.Click(driver, home_PageObject.Footer_Social_Pinterest_LK, "Pinterest social link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Barneys New York (barneysny) on Pinterest")) {
	  			ATUReports.add("Verify Social Page open", "Pinterest should open","Pinterest is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "Pinterest should open","Pinterest is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Barneys New York (barneysny) on Pinterest");
	  		}
	  		
	      case "instagram":
	    	  genericMethods.Click(driver, home_PageObject.Footer_Social_Instagram_LK, "Instagram social link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Barneys New York (@barneysny) • Instagram photos and videos")) {
	  			ATUReports.add("Verify Social Page open", "Instagram should open","Instagram is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "Instagram should open","Instagram is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Barneys New York (@barneysny) • Instagram photos and videos");
	  		}
	  		
	      case "youtube":
	    	  genericMethods.Click(driver, home_PageObject.Footer_Social_youtube_LK, "youtube social link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Barneys New York  - YouTube")) {
	  			ATUReports.add("Verify Social Page open", "youtube should open","youtube is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "youtube should open","youtube is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Barneys New York - YouTube");
	  		}
	  		
	      case "google":
	    	  genericMethods.Click(driver, home_PageObject.Footer_Social_Google_LK, "Google social link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Barneys New York - Google+")) {
	  			ATUReports.add("Verify Social Page open", "Google should open","Google is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "Google should open","Google is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Barneys New York - Google+");
	  		}
	  		
	      case "snapchat":
	    	  genericMethods.Click(driver, home_PageObject.Footer_Social_snapchat_LK, "Google SnapChat link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Add me on Snapchat!")) {
	  			ATUReports.add("Verify Social Page open", "SnapChat should open","Snapchat is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "Snapchat should open","SnapChat is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Add me on Snapchat!");
	  		}
	  		
	      case "tumblr":
	    	genericMethods.Click(driver, home_PageObject.Footer_Social_tumblr_LK, "Tumblr social link");
	  		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
	  		System.out.println(socialPageTitle);
	  		if (socialPageTitle.equalsIgnoreCase("Barneys New York")) {
	  			ATUReports.add("Verify Social Page open", "Tumblr should open","Tumblr is Open",true, LogAs.PASSED);
	  		}else {
	  			ATUReports.add("Verify Social Page open", "Tumblr should open","Tumblr is not Open",true, LogAs.FAILED);
	  			Assert.assertEquals(socialPageTitle, "Barneys New York");
	  		}
	  		break;
		
		
	default:
		ATUReports.add("Verify Presence of Social Icons","Social Icons should be present","Social Icons are not present",true,LogAs.FAILED);
		Assert.assertEquals("Social icons should be present", "Social icons are not present");
        break;
		}
	}
	
/*	
 	public void clickFacebookSocialIcon(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
		
		genericMethods.Click(driver, home_PageObject.Footer_Social_Facebook_LK, "facebook social link");
		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
		if (socialPageTitle.equalsIgnoreCase("Barneys New York | Facebook")) {
			ATUReports.add("Verify Social Page open", "Facebook should open","Facebook is Open",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Social Page open", "Facebook should open","Facebook is not Open",true, LogAs.FAILED);
			Assert.assertEquals(socialPageTitle, "Barneys New York | Facebook");
		}		
	}
	
	public void clickTwitterSocialIcon(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
		
		genericMethods.Click(driver, home_PageObject.Footer_Social_twitter_LK, "Twitter social link");
		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
		if (socialPageTitle.equalsIgnoreCase("Barneys New York (@BarneysNY) | Twitter")) {
			ATUReports.add("Verify Social Page open", "Twitter should open","Twitter is Open",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Social Page open", "Twitter should open","Twitter is not Open",true, LogAs.FAILED);
			Assert.assertEquals(socialPageTitle, "Barneys New York (@BarneysNY) | Twitter");
		}		
	}
	
	public void clickPinterestSocialIcon(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
		
		genericMethods.Click(driver, home_PageObject.Footer_Social_Pinterest_LK, "Pinterest social link");
		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
		if (socialPageTitle.equalsIgnoreCase("Barneys New York on Pinterest")) {
			ATUReports.add("Verify Social Page open", "Pinterest should open","Pinterest is Open",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Social Page open", "Pinterest should open","Pinterest is not Open",true, LogAs.FAILED);
			Assert.assertEquals(socialPageTitle, "Barneys New York on Pinterest");
		}		
	}
	
	public void clickInstagramSocialIcon(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
		
		genericMethods.Click(driver, home_PageObject.Footer_Social_Instagram_LK, "Instagram social link");
		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
		if (socialPageTitle.equalsIgnoreCase("Barneys New York (@barneysny) • Instagram photos and videos")) {
			ATUReports.add("Verify Social Page open", "Instagram should open","Instagram is Open",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Social Page open", "Instagram should open","Instagram is not Open",true, LogAs.FAILED);
			Assert.assertEquals(socialPageTitle, "Barneys New York (@barneysny) • Instagram photos and videos");
		}		
	}
	
	public void clickYouTubeSocialIcon(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
		
		genericMethods.Click(driver, home_PageObject.Footer_Social_youtube_LK, "youtube social link");
		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
		if (socialPageTitle.equalsIgnoreCase("Barneys New York - YouTube")) {
			ATUReports.add("Verify Social Page open", "youtube should open","youtube is Open",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Social Page open", "youtube should open","youtube is not Open",true, LogAs.FAILED);
			Assert.assertEquals(socialPageTitle, "Barneys New York - YouTube");
		}		
	}
	
	public void clickGoogleSocialIcon(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
		
		genericMethods.Click(driver, home_PageObject.Footer_Social_Google_LK, "Google social link");
		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
		if (socialPageTitle.equalsIgnoreCase("Barneys New York - Google+")) {
			ATUReports.add("Verify Social Page open", "Google should open","Google is Open",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Social Page open", "Google should open","Google is not Open",true, LogAs.FAILED);
			Assert.assertEquals(socialPageTitle, "Barneys New York - Google+");
		}		
	}
	
	public void clickTumblrSocialIcon(WebDriver driver) {
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods = new GenericMethods();
		String socialPageTitle;
		
		genericMethods.Click(driver, home_PageObject.Footer_Social_tumblr_LK, "Tumblr social link");
		socialPageTitle=genericMethods.switchTabAndgetPageTitle(driver);
		if (socialPageTitle.equalsIgnoreCase("Barneys New York")) {
			ATUReports.add("Verify Social Page open", "Tumblr should open","Tumblr is Open",true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Social Page open", "Tumblr should open","Tumblr is not Open",true, LogAs.FAILED);
			Assert.assertEquals(socialPageTitle, "Barneys New York");
		}		
	}
*/
	public void goToHomePageUsingLogo(WebDriver driver,String site){
		
		if (site.equalsIgnoreCase("BNY")) {
			GenericMethods.Waitformilliseconds(2000);
			driver.findElement(By.xpath("//div[contains(@class,'primary-logo')]/a")).click();
			GenericMethods.Waitformilliseconds(1000);
		} else {
			GenericMethods.Waitformilliseconds(1000);
			driver.findElement(By.xpath("//h1[contains(@class,'warehouse-logo')]/a[contains(text(),'WAREHOUSE')]")).click();
			GenericMethods.Waitformilliseconds(1000);
		}	
	}
	
	public void navigateToMyAccount(WebDriver driver){
		genericMethods= new GenericMethods();
		home_PageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myAccount_PageObjects= PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
		boolean pageUniqueIdentifierPresent;
		genericMethods.Click(driver, home_PageObject.Home_UtilityNav_LoggedInUser_LK,"My Account My Profile link");
		GenericMethods.Waitformilliseconds(5000);
		WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
		
		pageUniqueIdentifierPresent=genericMethods.isElementPresentforMouseHover(myAccount_PageObjects.MyAcc_Landing_Welcome_TV);
		if (pageUniqueIdentifierPresent) {
			ATUReports.add("Verify Ppage Displayed", "My Account Page should be Displayed","My Account page is Displayed",true, LogAs.PASSED);
		}else{
			ATUReports.add("Verify Ppage Displayed", "My Account Page should be Displayed","My Account page is not Displayed",true, LogAs.FAILED);
			Assert.assertEquals(pageUniqueIdentifierPresent, true);
		}
	}
}
