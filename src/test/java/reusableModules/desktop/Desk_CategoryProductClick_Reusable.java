package reusableModules.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_CategoryProductClick_Reusable {
	static String priceInString;
	static int priceInInt;
	static String xpathInitial;
	static String isPriceElementPresent1;
	static GenericMethods genericMethods;
	static int categoryPriceLimitINT;
	public static String ItemPrice;
	static WebDriverWait wait;
	
	public void browseProductandClickonBNY(WebDriver driver,String categoryPriceLimit){ 
		categoryPriceLimitINT=Integer.parseInt(categoryPriceLimit);	
		genericMethods=new GenericMethods();
		wait=new WebDriverWait(driver, 20);
		GenericMethods.Waitformilliseconds(1000);
		for(int i=1;i<=48;i++){
			xpathInitial="//div[@id='atg_store_prodList']/ul/li["+i+"]";//div[@id='search-result-items']/div
			//genericMethods.waitForPageLoaded(driver);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@class='gridImg loaded']")));
			GenericMethods.Waitformilliseconds(2000);
			isPriceElementPresent1=genericMethods.isElementPresent(driver,xpathInitial+"//div[@class='product-standard-price']" );
			
			//driver.findElement(By.xpath("")).sendKeys(keysToSend);
			
			if (isPriceElementPresent1.equals("present")) {
				GenericMethods.Waitformilliseconds(2000);
				priceInString=driver.findElement(By.xpath(xpathInitial+"//div[@class='product-standard-price']")).getText();	//div/div[2]/div[@class='product-pricing']/span			
				
					priceInString=giveDollerValue(priceInString);
					priceInInt=Integer.parseInt(priceInString);	
					GenericMethods.Waitformilliseconds(2000);
					if (priceInInt<categoryPriceLimitINT ) {
						ATUReports.add("Check The product", "Valid Product Found",true,LogAs.INFO);
						genericMethods.ClickUSingXpath(driver, xpathInitial+"//div[@class='product-image']/a","Browse Product Link");
						GenericMethods.waitForPageLoaded(driver);
						break;													
					}				
			}else{
				System.out.println("Price Element Not Present");
			}/*else {
				GenericMethods.Waitformilliseconds(2000);
				priceInString=driver.findElement(By.xpath(xpathInitial+"//div[@class='product-discounted-price']")).getText();
				//span[@class='product-sales-price']
				
				if (priceInString.equals("$N/A")) {
					System.out.println(i);					
				}else {
					priceInString=giveDollerValue(priceInString);							
					ItemPrice=priceInString;
					priceInInt=Integer.parseInt(priceInString);					
					if (priceInInt<categoryPriceLimitINT ) {
						ATUReports.add("Check The product", "Valid Product Found",true,LogAs.INFO);
						genericMethods.ClickUSingXpath(driver, xpathInitial+"/div/div[1]/a","Browse Product Link");
						genericMethods.waitForPageLoaded(driver);
						break;						
					}				
				}				
			}*/						
		}		
	}
		
	public static String giveDollerValue(String productPrice){		
		String newStr = productPrice.replace("$", "").replace(",", "");
		System.out.println(newStr);
		return newStr;
			
	}

}
