package reusableModules.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import PageObjects.desktop.Desk_BNY_BillingPageObject;
import PageObjects.desktop.Desk_BNY_CartPageObject;
import PageObjects.desktop.Desk_BNY_MyAccountPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_CheckoutShipping_Reusable {
 
	Desk_BNY_CartPageObject cartPageObjects;
	Desk_BNY_ShippingPageObject shipping_PageObject;
	Desk_BNY_BillingPageObject billing_PageObject;
	Desk_BNY_MyAccountPageObject myAccount_PageObjects;
	Desk_MYBagCheckout_Reusable myBagReusable;
	boolean isEditLinkPresent1;
	boolean isStateDDPresent;
	boolean isfirstname ;
	WebDriverWait xplicitWait;
	GenericMethods genericMethods;


  	public void CheckDefaultAddress(WebDriver driver) { 
  		shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
	   genericMethods = new GenericMethods(); 
	   
		ATUReports.add("Customer is on Shipping page", "System should displayi address drop down", true, LogAs.INFO);
		
		String address1 = genericMethods.getText(driver, shipping_PageObject.shipping_ShippingAddress1_TV);
		System.out.println("This is the address printed on shipping page."+ address1);
		//if(address1.equalsIgnoreCase("1122 Massachusetts Avenue")){
		if (address1.contains("3100 FULTON STREET")){
		    ATUReports.add("System displaying same address in dropdown which is Enter by Customer"," Customer enter address as :=  yogeeta bhosale, sydney bay sydney square", "System should display address as = yogeeta bhosale, sydney bay sydney square","System displaying :=" +address1,  true, LogAs.PASSED);				
	    }else{
    	 	ATUReports.add("System is not displaying same address in dropdown which is Enter by Customer"," Customer enter address as :=  yogeeta bhosale, sydney bay sydney square", "System should display address as = yogeeta bhosale, sydney bay sydney square","System displaying :=" +address1,  true, LogAs.FAILED);
	 		Assert.assertEquals("Default Address is Not Selected", "Default address should be Selected");		
		}			
  }
	/*
  public void AddNewAddress(WebDriver driver ) {
	    shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_shipping_PageObject.class);
	    genericMethods = new GenericMethods();
	    genericMethods.Click(driver, shipping_PageObject.CreateAddress_LK,"Create Address link");
	    ATUReports.add("System displaying add new shipping address section ", "System should display add new shipping address section","enter address in new shipping address section", true, LogAs.INFO);
	 }

	
	
  	

  	
  
  public void YourOrdersection(WebDriver driver){
	  shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_shipping_PageObject.class);
	  genericMethods = new GenericMethods(); 
	  myBagReusable = new Desk_MYBagCheckout_Reusable();
	  
	  String yourorder= genericMethods.isElementPresent(driver, "//*[@id='secondary']/div/h3");
	  if(yourorder.equals("present") ){	  
		  	String ordersection = genericMethods.getText(driver, shipping_PageObject.your_order_Gettext);
			if (ordersection.contains("YOUR ORDER")){
				    ATUReports.add("System is displaying Your Order section","System displaying your order section on shipping page", "Your Order section on shipping page","system displaying  :=" +(genericMethods.getText(driver, shipping_PageObject.your_order_Gettext)),  true, LogAs.PASSED);
				    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.your_order_Gettext)), "YOUR ORDER");				
		    }else{
		    	 ATUReports.add("System is not displaying Your Order section","System is not displaying your order section on shipping page", "Your Order section on shipping page","system not displaying  :=" +(genericMethods.getText(driver, shipping_PageObject.your_order_Gettext)),  true, LogAs.FAILED);
				 Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.your_order_Gettext)), "YOUR ORDER");				
			}	  
	  }else{
		  System.out.println("System not displaying your order section on shipping page");
	  }
	  
	  String productShippingname = genericMethods.getText(driver, shipping_PageObject.ProductNameshipping_gettext);
	  genericMethods.Click(driver, shipping_PageObject.ProductImg_LK,"Product Image Link");
	  GenericMethods.Waitformilliseconds(1000);
	  ATUReports.add("Verify system displaying PDP page to customer", "System should display PDP ", true, LogAs.INFO);
	  
	  myBagReusable.GotoShippingPage(driver);
	  GenericMethods.Waitformilliseconds(1000);
	  genericMethods.Click(driver, shipping_PageObject.ProductName_LK,"Product Name link");
	  ATUReports.add("Verify system displaying PDP page to customer", "System should display PDP ", true, LogAs.INFO);
	  GenericMethods.Waitformilliseconds(1000);
	  String productPDPname = genericMethods.getText(driver, shipping_PageObject.ProductNamePDP_gettext);
	  if (productShippingname.contains(productPDPname)){
		  ATUReports.add("Verify system displaying same product on PDP",  "System should display same image on PDP", "System displaying image on pdp:="+ productPDPname, true,LogAs.PASSED);
		  Assert.assertEquals(productShippingname, productPDPname);
	  }else{
		  ATUReports.add("Verify system displaying same product on PDP",  "System is not  displaing same image on PDP", "System displaying image on pdp:="+ productPDPname, true,LogAs.PASSED);
		  Assert.assertEquals(productShippingname, productPDPname);		  
	  } 
  }
  */
 //Separate all validations	
  public void validateShippingPage(WebDriver driver){
		shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
		cartPageObjects = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class); 
		genericMethods = new GenericMethods();
		xplicitWait=new WebDriverWait(driver, 15);
		
		
		ATUReports.add("Verify system displaying edit shipping link or not on shipping page ", "System should display edit shipping link","verify Edit shipping link", true, LogAs.INFO);		
		String isEditLinkPresent1=genericMethods.isElementPresent(driver,"//a[contains(text(),'Edit Address')]" );
		if (isEditLinkPresent1.equals("present")){
			ATUReports.add("Check if user has saved shipping address", "User should have Saved Shipping Addresss", "User has saved Shipping address", true,LogAs.INFO);
			shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class); 
			genericMethods.selectDropdown(driver, shipping_PageObject.shipping_ShippingAddress_DD, "CREATE NEW SHIPPING ADDRESS","Saved Shipping Dropdown");
			GenericMethods.Waitformilliseconds(3000);			
		}
		
		genericMethods.Click(driver, shipping_PageObject.shipping_CONTINUE_BTN,"Shipping Continue Button");	
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_FirstName_TB,"","Shipping Firstname");
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_LastName_TB,"","Shipping Lastname");
		xplicitWait.until(ExpectedConditions.visibilityOf(shipping_PageObject.ErrorMsgfirstname_gettext));
		String firstnamemsg = genericMethods.getText(driver, shipping_PageObject.ErrorMsgfirstname_gettext).toUpperCase();		 
		if (firstnamemsg.equalsIgnoreCase("PLEASE PROVIDE A FIRST NAME.")){		 
			ATUReports.add("System displaying error message when first name is blank","  ", "Error Messages for First Name should be =  Please provide a first name.","Error message for firstName : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgfirstname_gettext)),  true, LogAs.PASSED);
			Assert.assertEquals(firstnamemsg, "PLEASE PROVIDE A FIRST NAME.");		    
		}else{
		   ATUReports.add("System is not displaying error message when first name is blank","  ","Error Messages for First Name should be =  Please provide a first name.","Error message for firstName : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgfirstname_gettext)),  true, LogAs.FAILED);
		   Assert.assertEquals(firstnamemsg, "PLEASE PROVIDE A FIRST NAME.");			 
		}
		
	 	genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_LastName_TB,"","Shipping Lastname");
	 	genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_Address1_TB,"","Shipping Address1");
	 	xplicitWait.until(ExpectedConditions.visibilityOf(shipping_PageObject.ErrorMsglastname_gettext));
	 	String lastnamemsg = genericMethods.getText(driver, shipping_PageObject.ErrorMsglastname_gettext).toUpperCase();
	 	if (lastnamemsg.equalsIgnoreCase("PLEASE PROVIDE A LAST NAME.")) {
	 		ATUReports.add("System displaying error message when last name is blank","  ", "Error Messages for last Name should be =  Please provide a last name.", "Error message for lastName : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsglastname_gettext)),  true, LogAs.PASSED);
	 		Assert.assertEquals(lastnamemsg, "PLEASE PROVIDE A LAST NAME.");
	 	}else{
	 		ATUReports.add("System is not displaying error message when last name is blank","  ", "Error Messages for last Name should be =  Please provide a last name.", "Error message for lastName : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsglastname_gettext)),  true, LogAs.FAILED);
	 		Assert.assertEquals(lastnamemsg, "PLEASE PROVIDE A LAST NAME.");
	 	}
			
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_Address1_TB,"","Shipping Address1");
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_CITY_TB,"","Shipping City");
		xplicitWait.until(ExpectedConditions.visibilityOf(shipping_PageObject.ErrorMsgaddress1_gettext));
		String address1msg = genericMethods.getText(driver, shipping_PageObject.ErrorMsgaddress1_gettext).toUpperCase();
	 	if (address1msg.equalsIgnoreCase("PLEASE ENTER A VALID STREET ADDRESS.")){
		    ATUReports.add("System displaying error message when address1 is blank","  ", "Error Messages for last Name should be =  Please enter a valid street address.","Error message for address1 : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgaddress1_gettext)),  true, LogAs.PASSED);
		    Assert.assertEquals(address1msg, "PLEASE ENTER A VALID STREET ADDRESS.");
	 	}else{
		   ATUReports.add("System is not displaying error message when address1 is blank","  ", "Error Messages for last Name should be =  Please enter a valid street address.","Error message for address1 : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgaddress1_gettext)),  true, LogAs.FAILED);
		   Assert.assertEquals(address1msg, "PLEASE ENTER A VALID STREET ADDRESS.");
	   }
				 
	 	genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_CITY_TB,"","Shipping City");				 
	 	genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_ZIP_TB,"","Shipping Zipcode");
	 	xplicitWait.until(ExpectedConditions.visibilityOf(shipping_PageObject.ErrorMsgCity_gettext));
		String cityValidationMsg = genericMethods.getText(driver, shipping_PageObject.ErrorMsgCity_gettext).toUpperCase();
	 	if (cityValidationMsg.equalsIgnoreCase("PLEASE ENTER A VALID CITY.")){
	 		ATUReports.add("System displaying error message when City is blank","  ", "Error Messages for last Name should be =  Please enter a valid city.","Error message for city : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgCity_gettext)),  true, LogAs.PASSED);
		    Assert.assertEquals(cityValidationMsg, "PLEASE ENTER A VALID CITY.");
	   }else{
		   	ATUReports.add("System is not displaying error message when City is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgCity_gettext)),  true, LogAs.FAILED);
		   	Assert.assertEquals(cityValidationMsg, "PLEASE ENTER A VALID CITY.");
	   }
				 
	 	String statename= genericMethods.getText(driver, shipping_PageObject.GetStateName_gettext);				
	 	if(statename.contains("Shipping To   United States")){
	 		genericMethods.selectDropdown(driver, shipping_PageObject.shipping_ShippingAddress_State_DD, "Select...","Shipping State"); 			 
	 	}else if (statename.contains("Shipping To   Canada")){
	 		genericMethods.selectDropdown(driver, shipping_PageObject.shipping_ShippingAddress_State_DD, "Select...","Shipping State"); 
	 	}else{
	 		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_State_DD , "","Shipping State"); 
	 		GenericMethods.Waitformilliseconds(2000);
	 		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_ZIP_TB,"","Shipping Zipcode");
	 	}
				 
		genericMethods.Click(driver, shipping_PageObject.shipping_CONTINUE_BTN,"Shipping Continue button");			 
		if(statename.contains("United States") || statename.contains("Canada") ){
			xplicitWait.until(ExpectedConditions.visibilityOf(shipping_PageObject.ErrorMsgState_gettext));
			String Statemsg = genericMethods.getText(driver, shipping_PageObject.ErrorMsgState_gettext).toUpperCase();
		if (Statemsg.equalsIgnoreCase("PLEASE SELECT A STATE.")){
			ATUReports.add("System displaying error message when State is blank", "  ", "Error Messages for state should be =  Please enter a valid state.","Error message for State : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgState_gettext)),  true, LogAs.PASSED);
			Assert.assertEquals(Statemsg, "PLEASE SELECT A STATE.");
		}else{
			ATUReports.add("System is not displaying error message when State is blank","  ", "Error Messages for state should be =  please select state","Error message for State : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgState_gettext)),  true, LogAs.FAILED);
			Assert.assertEquals(Statemsg, "PLEASE SELECT A STATE.");
		}
	}
				 			 
		 genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_ZIP_TB,"","Shipping zipcode");
		 genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_Phone_TB,"","Shipping Phone");
		 xplicitWait.until(ExpectedConditions.visibilityOf(shipping_PageObject.ErrorMsgZip_gettext));
		 String Zipmsg = genericMethods.getText(driver, shipping_PageObject.ErrorMsgZip_gettext).toUpperCase();
		 if(Zipmsg.equalsIgnoreCase("PLEASE ENTER A VALID ZIP OR POSTAL CODE.")){
			ATUReports.add("System displaying error message when Zip code is blank","  ", "Error Messages for zip code should be =  please enter valid zip code","Error message for Zip code : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgZip_gettext)),  true, LogAs.PASSED);
		    Assert.assertEquals(Zipmsg, "PLEASE ENTER A VALID ZIP OR POSTAL CODE.");
		 }else{
		   ATUReports.add("System is not displaying error message when Zip code is blank","  ", "Error Messages for zip code should be =  please enter valid zip code","Error message for Zip code : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgZip_gettext)),  true, LogAs.FAILED);
		   Assert.assertEquals(Zipmsg, "PLEASE ENTER A VALID ZIP OR POSTAL CODE.");
		 }
		 
		 genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_Phone_TB,"","Shipping Phone");
		 genericMethods.Click(driver, shipping_PageObject.shipping_CONTINUE_BTN,"Shipping COntinue button");
		 xplicitWait.until(ExpectedConditions.visibilityOf(shipping_PageObject.ErrorMsgphonenumber_gettext));
		 String Phonenumbermsg = genericMethods.getText(driver, shipping_PageObject.ErrorMsgphonenumber_gettext).toUpperCase();				 
		 if (Phonenumbermsg.equalsIgnoreCase("PLEASE ENTER A VALID PHONE NUMBER, IN CASE WE NEED TO CONTACT YOU REGARDING YOUR ORDER.")){
		    ATUReports.add("System displaying error message when phone number is blank","  ", "Error Messages for Phone number should be =  please enter phone number","Error message for Phone number : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgphonenumber_gettext)),  true, LogAs.PASSED);
		    Assert.assertEquals(Phonenumbermsg, "PLEASE ENTER A VALID PHONE NUMBER, IN CASE WE NEED TO CONTACT YOU REGARDING YOUR ORDER.");			  
		 }else{
		   ATUReports.add("System is not displaying error message when phone number is blank","  ", "Error Messages for Phone number should be =  please enter phone number","Error message for phone number is : " +(genericMethods.getText(driver, shipping_PageObject.ErrorMsgphonenumber_gettext)),  true, LogAs.FAILED);
		   Assert.assertEquals(Phonenumbermsg, "PLEASE ENTER A VALID PHONE NUMBER, IN CASE WE NEED TO CONTACT YOU REGARDING YOUR ORDER.");
		 }				 
  	}
  

  	public void startFillingShippingForm(WebDriver driver, String firstName, String lastName,
  			String address1,String address2,String city,String state,String zipCode,String shippingPhone){		 
		shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
		billing_PageObject = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		                                                             
		isEditLinkPresent1=genericMethods.isElementPresentforMouseHover(shipping_PageObject.shipping_EditAddress_LK);
		isfirstname = genericMethods.isElementPresentforMouseHover(shipping_PageObject.shipping_ShippingAddress_FirstName_TB); 
		
		if (isfirstname ){
			 ATUReports.add("Customer is on shipping page", "System should display Shipping page","Shipping page open", true, LogAs.INFO);
			 fill_ShippingForm(driver,  firstName,  lastName, address1, address2, city,state, zipCode, shippingPhone);										
		}else if(isEditLinkPresent1){
			ATUReports.add("Check if user has saved shipping address", "User should have Saved Shipping Addresss", "User has saved Shipping address", true,LogAs.INFO);
			genericMethods.Click(driver,shipping_PageObject.shipping_EditAddress_LK,"Edit Address Link");
			GenericMethods.Waitformilliseconds(2000);
			genericMethods.Click(driver, shipping_PageObject.shipping_CONTINUE_BTN,"Shipping Continue button");
		}else{
		    ATUReports.add("Customer filling shipping form", "System should allow customer to fill shipping form","Customer filling shipping form", true, LogAs.INFO);
			fill_ShippingForm(driver,  firstName,  lastName, address1, address2, city,state, zipCode, shippingPhone);				
		}
		
		//xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Checkout')]")));
		//xplicitWait.until(ExpectedConditions.visibilityOf(billing_PageObject.billing_PageTitle_TV));
  	}
	
  
  	public void fill_ShippingForm(WebDriver driver, String firstName, String lastName,String address1,String address2,String city,String state,String zipCode,String shippingPhone){
		//String isAddAddressToMyAccountCBPresent;
		genericMethods =  new GenericMethods();
		shipping_PageObject=PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
	  
	    genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_FirstName_TB, firstName,"Shipping First Name");
		ATUReports.add("Check if user has saved address", "User should not have Saved Addresss", "User does not have saved address", true,LogAs.INFO);			
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_LastName_TB, lastName,"Shipping Last Name");
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_Address1_TB, address1,"Shipping Address 1");
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_Address2_TB, address2,"Shipping Address 2");
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_CITY_TB, city,"Shipping City");
		
		isStateDDPresent=genericMethods.isElementPresentforMouseHover(shipping_PageObject.shipping_ShippingAddress_State_DD);
		if (isStateDDPresent) {
			ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown Should be Present", true,LogAs.INFO);
			genericMethods.selectDropdown(driver, shipping_PageObject.shipping_ShippingAddress_State_DD, state, "Shipping State");//state=New York
		}else{
			ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
			genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_State_DD,state,"Shipping State");
		}
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_ZIP_TB, zipCode,"Shipping zip Code");
		genericMethods.inputValue(driver, shipping_PageObject.shipping_ShippingAddress_Phone_TB, shippingPhone,"Shipping Phone Number");
		
		
		/*
		 * isAddAddressToMyAccountCBPresent=genericMethods.isElementPresent(driver, "//div[@class='bt-checkbox']/a[@class='bt-checker']");
			if (isAddAddressToMyAccountCBPresent.equalsIgnoreCase("present")) {
			GenericMethods.Waitformilliseconds(2000);
			genericMethods.findVisibleElementandClick(driver, "//div[@class='bt-checkbox']/a[@class='bt-checker']");
			GenericMethods.Waitformilliseconds(2000);
			isAddAddressToMyAccountCBPresent=genericMethods.isElementPresent(driver, "//div[@class='bt-checkbox']/a[@class='bt-checker']");
			if (isAddAddressToMyAccountCBPresent.equalsIgnoreCase("present")) {
				GenericMethods.Waitformilliseconds(2000);
				genericMethods.findVisibleElementandClick(driver, "//div[@class='bt-checkbox']/a[@class='bt-checker']");				
			}			 			
		}*/
		
		GenericMethods.Waitformilliseconds(2000);
	  
	}
  	
  	public void clickShippingPage_ContinueButton(WebDriver driver){
  		shipping_PageObject=PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
  		genericMethods=new GenericMethods();
  		
  		genericMethods.Click(driver, shipping_PageObject.shipping_CONTINUE_BTN, "Shipping COntinue Button");
  	}
  	
  	public void SelectShippingmethod(WebDriver driver,String country,String type) {
  		shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
		genericMethods = new GenericMethods();		  
		String shippingCharges;
		String selectedShippingMethod;
		//String Countryname = genericMethods.getText(driver, shipping_PageObject.GetStateName_gettext);		  
		  
		selectedShippingMethod=genericMethods.getText(driver, shipping_PageObject.shipping_SelectedMethod_TV);
		if (selectedShippingMethod.contains("Ground")) {
			ATUReports.add("Verify Default Shipping Method Selected", "Ground should be selected by Default", "Shipping Method selected by Default is "+selectedShippingMethod,false,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Default Shipping Method Selected", "Ground should be selected by Default", "Shipping Method selected by Default is "+selectedShippingMethod,false,LogAs.WARNING);
			//Assert.assertEquals("Should be Ground", "Ground is Not Selected");
		}
		
		switch (type) {
		case "sameDay":
			genericMethods.Click(driver, shipping_PageObject.shipping_Methods_SameDay_LK, "Same day Shipping Method");
			GenericMethods.Waitformilliseconds(4000);
			shippingCharges=genericMethods.getText(driver, shipping_PageObject.shipping_OrderSubTotals_ShippingCharges_TV);
			if (shippingCharges.equals("$25.00")) {
				ATUReports.add("Verify Cost of Same Day Shipping", "Cost of Same Day should be $25", "Cost of Same day Shipping is "+shippingCharges,false,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Cost of Same Day Shipping", "Cost of Same Day should be $25", "Cost of Same day Shipping is "+shippingCharges,false,LogAs.FAILED);
				Assert.assertEquals(shippingCharges, "$25.00");
			}
			break;
			 
		case "nextDay":
			genericMethods.Click(driver, shipping_PageObject.shipping_Methods_NextBusinessDay_LK, "Next Business day Shipping Method");
			GenericMethods.Waitformilliseconds(4000);
			shippingCharges=genericMethods.getText(driver, shipping_PageObject.shipping_OrderSubTotals_ShippingCharges_TV);
			if (shippingCharges.equals("$20.00")) {
				ATUReports.add("Verify Cost of Next Day Shipping", "Cost of Next Business Day Shipping should be $20", "Cost of Next Business day Shipping is "+shippingCharges,false,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Cost of Next Day Shipping", "Cost of Next Business Day should be $20", "Cost of Next Business day Shipping is "+shippingCharges,false,LogAs.FAILED);
				Assert.assertEquals(shippingCharges, "$20.00");
			}
			break;
				
		case "twoDay":
			genericMethods.Click(driver, shipping_PageObject.shipping_Methods_TwoBusinessDay_LK, "Two Business day Shipping Method");		
			GenericMethods.Waitformilliseconds(4000);
			shippingCharges=genericMethods.getText(driver, shipping_PageObject.shipping_OrderSubTotals_ShippingCharges_TV);
			if (shippingCharges.equals("$15.00")) {
				ATUReports.add("Verify Two of Next Business Day Shipping", "Cost of Two Business day Shipping should be $15", "Cost of Two Business day Shipping is "+shippingCharges,false,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Two of Same Day Shipping", "Cost of Two Business should be $15", "Cost of Two Business day Shipping is "+shippingCharges,false,LogAs.FAILED);
				Assert.assertEquals(shippingCharges, "$15.00");
			}
			break;
					
		case "ground":
			genericMethods.Click(driver, shipping_PageObject.shipping_Methods_Ground_LK, "Ground Shipping Method");			
			GenericMethods.Waitformilliseconds(4000);
			shippingCharges=genericMethods.getText(driver, shipping_PageObject.shipping_OrderSubTotals_ShippingCharges_TV);
			if (shippingCharges.equals("Free")) {
				ATUReports.add("Verify Cost of Same Day Shipping", "Cost of Same should be Free", "Cost of Same day Shipping is "+shippingCharges,false,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Cost of Same Day Shipping", "Cost of Same should be Free", "Cost of Same day Shipping is "+shippingCharges,false,LogAs.FAILED);
				Assert.assertEquals(shippingCharges, "Free");
			}
			break;
		}
		
		/*
		if(country.equalsIgnoreCase("Canada")){
			  GenericMethods.Waitformilliseconds(2000);
			  String defualtshippingmethod = genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext);
			  
			  if (defualtshippingmethod.contains("CAD 33.77")){
				  ATUReports.add("System selecting default shipping method DHL Express", "Default shipping method is selected","default shipping method is DHL Express : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)),  true, LogAs.PASSED);
				  Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)), "CAD 33.77");				
			   }else{
				   ATUReports.add("System is not selecting default shipping method DHL Express", "Default shipping method is not selected","default shipping method is DHL Express : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)),  true, LogAs.FAILED);
				   Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)), "CAD 33.77");
			   }
			 			  
			  
		     genericMethods.Click(driver, shipping_PageObject.Aramex_LK,"Aramx Shipping Option");
			 GenericMethods.Waitformilliseconds(1000);
				    
			 GenericMethods.Waitformilliseconds(2000);
			 String CanadaPost  = genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext);
				  
				  if (CanadaPost.contains("CAD 16.13"))
					   {
					    ATUReports.add("System applying correct charges for shipping method CanadaPost", "Shipping charges for CanadaPost is CAD 16.13 ","CanadaPost is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)),  true, LogAs.PASSED);
					    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)), "CAD 16.13");
					
					   }else
					   {
						   ATUReports.add("System is not applying correct charges for shipping method CanadaPost", "Shipping charges are not for CanadaPost is CAD 16.13 ","CanadaPost is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)),  true, LogAs.PASSED);
						    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)), "CAD 16.13");
					   }
				  
				     genericMethods.Click(driver, shipping_PageObject.DHLExpress_LK,"DHL Shipping Option");
					 GenericMethods.Waitformilliseconds(1000);
					    
					 GenericMethods.Waitformilliseconds(2000);
					  String DHLExpress = genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext);
					//  System.out.println(DHLExpress); 
					  
					  if (DHLExpress.contains("CAD 33.77"))
						   {
						    ATUReports.add("System applying correct charges for shipping method DHLExpress", "Shipping charges for DHLExpress is CAD 65.91 ","DHLExpress is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)),  true, LogAs.PASSED);
						    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)), "CAD 33.77");
						
						   }else
						   {
							   ATUReports.add("System is not applying correct charges for shipping method DHLExpress", "Shipping charges for DHLExpress is not CAD 65.91 ","DHLExpress is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)),  true, LogAs.PASSED);
							    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForCanada_gettext)), "CAD 33.77");
						   }
					  
			
			   }
		 
		  else if(country.equalsIgnoreCase("India")){
			GenericMethods.Waitformilliseconds(2000);
			  String defualtshippingmethod = genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext);
			//  System.out.println(defualtshippingmethod); 
			  
			  if (defualtshippingmethod.contains("INR 1,844.00"))
				   {
				    ATUReports.add("System selecting default shipping method DHL Express", "Default shipping method is selected","default shipping method is DHL Express : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)),  true, LogAs.PASSED);
				    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)), "INR 2,012.00");
				
				   }else
				   {
					   ATUReports.add("System is not selecting default shipping method DHL Express", "Default shipping method is not selected","default shipping method is DHL Express : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)),  true, LogAs.FAILED);
					    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)), "INR 2,012.00");
				   }
			 			  
			  
			     genericMethods.Click(driver, shipping_PageObject.Aramex_LK,"Aramax Shipping Option");
				 GenericMethods.Waitformilliseconds(1000);
				    
				 GenericMethods.Waitformilliseconds(2000);
				  String Aramex = genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext);
				//  System.out.println(Aramex); 
				  
				  if (Aramex.contains("INR 1,618.00"))
					   {
					    ATUReports.add("System applying correct charges for shipping method Aramex", "Shipping charges for Aramex is INR 3,537.00 ","Aramex is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)),  true, LogAs.PASSED);
					    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)), "INR 2,012.00");
					
					   }else
					   {
						   ATUReports.add("System is not applying correct charges for shipping method Aramex", "Shipping charges are not for Aramex is INR 3,537.00 ","Aramex is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)),  true, LogAs.PASSED);
						    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)), "INR 2,012.00");
					   }
				  
				     genericMethods.Click(driver, shipping_PageObject.DHLExpress_LK,"DHL Shipping Option");
					 GenericMethods.Waitformilliseconds(1000);
					    
					 GenericMethods.Waitformilliseconds(2000);
					  String DHLExpress = genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext); 
					  
			  if (DHLExpress.contains("INR 1,843.00"))
				   {
				    ATUReports.add("System applying correct charges for shipping method DHLExpress", "Shipping charges for DHLExpress is INR 3,764.00 ","DHLExpress is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)),  true, LogAs.PASSED);
				    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)), "INR 2,012.00");
				
				   }else
				   {
					   ATUReports.add("System is not applying correct charges for shipping method DHLExpress", "Shipping charges for DHLExpress is not INR 3,764.00 ","DHLExpress is  : " +(genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)),  true, LogAs.PASSED);
					    Assert.assertEquals((genericMethods.getText(driver, shipping_PageObject.ShippingChargesForIndia_gettext)), "INR 2,012.00");
				   }					  		  		  		   
		  }		*/  
  		}
  	
  	public void GiftWrap(WebDriver driver){
  	  shipping_PageObject = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
  	  genericMethods = new GenericMethods();
  	  String giftWrapSectionClass;
  	  String currentGiftWrapCBState;
  	  String currentGiftMsgCBState;
  	  boolean isAddGiftWrapVisible;
  	  
  	  
  	  giftWrapSectionClass=(shipping_PageObject.shipping_AddGiftWrap_LK).getAttribute("class");
	  	if (giftWrapSectionClass.contains("collapsed")) {
	  		genericMethods.Click(driver, shipping_PageObject.shipping_AddGiftWrap_LK,"Add gift wrap link") ;
		}else {
			ATUReports.add("Verify Gift Wrap Section", "","Gift Wrap Section is expanded",true,LogAs.INFO);
		}
  	  
  	  //genericMethods.Click(driver, shipping_PageObject.shipping_AddGiftWrap_LK,"Add gift wrap link") ;
  	 
	  isAddGiftWrapVisible=genericMethods.isElementPresentforMouseHover(shipping_PageObject.shipping_AddGiftWrap_CB);
	  if (isAddGiftWrapVisible) {
		  ATUReports.add("Verify system displaying gift wrap section on shipping page", "System should display Gift warp and Gift Message checkbox on shipping page ","Gift warp and Gift Message checkbox are visible",  true, LogAs.INFO);
	  }else {
		  ATUReports.add("Verify system displaying gift wrap section on shipping page", "System should display Gift warp and Gift Message checkbox on shipping page ","Gift warp and Gift Message checkbox are not visible",  true, LogAs.INFO);
	  }

	  currentGiftWrapCBState=(shipping_PageObject.shipping_AddGiftWrap_CB).getAttribute("class");
	  if (currentGiftWrapCBState.contains("checked")) {  		
  		ATUReports.add("Verify Gift Wrap Checkbox State", "","Gift Wrap Checkbox is Checked",false,LogAs.INFO);
	  }else {
		  genericMethods.Click(driver, shipping_PageObject.shipping_AddGiftWrap_CB,"Add gift Wrap Checkbox") ;
	  }
	  
	  currentGiftMsgCBState=(shipping_PageObject.shipping_AddGiftMessage_CB).getAttribute("class");
	  if (currentGiftMsgCBState.contains("checked")) {
		  ATUReports.add("Verify Gift Message Checkbox State", "","Gift Message checkbox is Checked",false,LogAs.INFO);  		
	  }else {
		  genericMethods.Click(driver, shipping_PageObject.shipping_AddGiftMessage_CB,"Add gift Message Checkbox") ;
	  }

  	  GenericMethods.Waitformilliseconds(2000);
  	  genericMethods.inputValue(driver, shipping_PageObject.shipping_Messageline1_TB, "Hi","Gift Msg line 1");
  	  genericMethods.inputValue(driver, shipping_PageObject.shipping_Messageline2_TB, "Adapty","Gift Msg line 2");
  	  genericMethods.inputValue(driver, shipping_PageObject.shipping_Messageline3_TB, "Solutions","Gift Msg line 3");
  	  genericMethods.inputValue(driver, shipping_PageObject.shipping_Messageline4_TB, "Happy","Gift Msg line 4");
  	  genericMethods.inputValue(driver, shipping_PageObject.shipping_Messageline5_TB, "Birthday","Gift Msg line 5");
  	  genericMethods.inputValue(driver, shipping_PageObject.shipping_Messageline6_TB, "Smile","Gift Msg line 6");
  	  
  	  GenericMethods.Waitformilliseconds(1000);
  	  String Mymessage = genericMethods.getText(driver, shipping_PageObject.shipping_GiftMessage_Cardpriview);
  	
  	  ATUReports.add("Verify system allowing customer to enter shipping address in gift wrap section", "System should allow customer to enter gift message ","message on gift wrap section",  true, LogAs.INFO);
  	  if (Mymessage.equalsIgnoreCase("Hi\nAdapty\nSolutions\nHappy\nBirthday\nSmile")){
  		ATUReports.add("System displaying correct message", "System should display correct message which is enter by customer","Message on code review section",true,LogAs.PASSED);
  		//Assert.assertEquals(Mymessage, "Hi\nAdapty\nSolutions\nHappy\nBirthday\nSmile");				
  	  }else{
  		ATUReports.add("System is not displaying correct message", "System should display correct message which is enter by customer","Message on code review section",true,LogAs.FAILED);
  		Assert.assertEquals(Mymessage, "Hi\nAdapty\nSolutions\nHappy\nBirthday");	
  	  }	
  	  	genericMethods.Click(driver, shipping_PageObject.shipping_AddGiftWrap_CB,"Add gift wrap checkbox");
  	  	genericMethods.Click(driver, shipping_PageObject.shipping_AddGiftMessage_CB,"Add gift message checkbox");
  	  	
        genericMethods.Click(driver, shipping_PageObject.shipping_CONTINUE_BTN,"Billing Continue button");	  
    }
}
		
