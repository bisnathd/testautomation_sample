package reusableModules.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import actions.GenericMethods;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_MyAccountPageObject;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;


public class Desk_MyAccountAddress_Reusable {

 WebDriverWait xplicitWait;
 Desk_BNY_MyAccountPageObject myAccountAddressPageObjects;
 Desk_BNY_HomePageObject myAccountHomePageObjects;
 String isQASPresent;
 String isAddressBoxPresent;
 public static int addressCount;
 String addressName, secondAddress, defaultAddress, userEditedAddress;
 String stateCode;
 WebElement makeDefaultLink;
 GenericMethods genericMethods;

 /* This method hovers mouse on Hi <username> link from Top Navigation 
  * and clicks on My Account link from the dropdown 
  * and then selects Address Book link from the My Account page 
  */
 public void hoverOnMyAccountLink(WebDriver driver) {
  myAccountAddressPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);

  genericMethods = new GenericMethods();
  xplicitWait = new WebDriverWait(driver, 30);

  genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_AccountArrow_LK, "Logged in Username");
  genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_DD_MyAccount_LK, "My Account link");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddressBook_LK, "Address book");

 }

 public void myAccountAddress(WebDriver driver, String MyAccFirstName, String MyAccLastName, String MyAccAddress1, String MyAccAddress2, String MyAccCity, String MyAccZipCode, String MyAccPhone, String MyAccState, String MyAccAddressName) {
  myAccountAddressPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);

  genericMethods = new GenericMethods();
  xplicitWait = new WebDriverWait(driver, 30);

  genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_AccountArrow_LK, "Logged in Username");
  genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_DD_MyAccount_LK, "My Account link");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddressBook_LK, "Address book");
  addressCount = 0;
  /*for (int i = 1; i<=10 ;i++) {
  	
  	isAddressBoxPresent = genericMethods.isElementPresent(driver, myAccountAddressPageObjects.MyAcc_AddressBook_PANEL + "[" + i + "]");
  	if (isAddressBoxPresent=="present") {
  		addressCount=i;
  	}else{
  		break;
  	}
  }*/
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddressBook_AddNewAddr_BTN, "Add New Button");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_AddressName_TB, MyAccAddressName, "Address Name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_FirstName_TB, MyAccFirstName, "First Name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_LastName_TB, MyAccLastName, "Last name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address1_TB, MyAccAddress1, "Address line 1");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address2_TB, MyAccAddress2, "Address Line 2");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_City_TB, MyAccCity, "City");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD, "State Dropdown");
  genericMethods.selectDropdown(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD, MyAccState, "State Dropdown");
  stateCode = (myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD).getAttribute("value");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Zip_TB, MyAccZipCode, "zip Code");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Phone_TB, MyAccPhone, "Phone");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Apply_BTN, "Apply Button");

  GenericMethods.Waitformilliseconds(3000);

  addressCount++;
  GenericMethods.Waitformilliseconds(5000);
  /*isAddressBoxPresent = genericMethods.isElementPresent(driver, myAccountAddressPageObjects.MyAcc_AddressBook_PANEL+"["+ addressCount +"]");
  System.out.println(isAddressBoxPresent);*/
  WebElement newAddressBox;

  GenericMethods.Waitformilliseconds(3000);
  xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='account-address row']/ul/li[" + addressCount + "]//div[@class='accountadd']")));

  newAddressBox = driver.findElement(By.xpath("//div[@class='account-address row']/ul/li[" + addressCount + "]//div[@class='accountadd']"));
  addressName = genericMethods.getText(driver, newAddressBox);

  // Code to put dot (.) in phone number

  String result = MyAccPhone.substring(0, 3) + "." + MyAccPhone.substring(4, 7) + "." + MyAccPhone.substring(6, 10);
  System.out.println(result);


  String UserAddedAddress = MyAccAddressName + "\n" + MyAccAddress1 + " " + MyAccAddress2 + "\n" + MyAccCity + ", " + stateCode + " " + MyAccZipCode + "\n" + "United States" + "\n" + "Phone: " + result;
  if (addressName.equalsIgnoreCase(UserAddedAddress)) {
   ATUReports.add("Verify Address is Added", "Address should be Added", "Address is Added", true, LogAs.PASSED);

  } else {
   ATUReports.add("Verify Address is Added", "Address should be Added", "Address is not Added", true, LogAs.FAILED);
   Assert.assertEquals(addressName, UserAddedAddress);
  }
 }

 public void MyAccountAddressDelete(WebDriver driver) {
  myAccountAddressPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);

  genericMethods = new GenericMethods();
  xplicitWait = new WebDriverWait(driver, 30);

  GenericMethods.Waitformilliseconds(4000);
  genericMethods.mouseHover(driver, myAccountAddressPageObjects.MyAcc_AddressBook_DefaultBox_LB, "Default Address box");
  ((JavascriptExecutor) driver).executeScript("document.getElementsByClassName('atg_store_addressBookDefaultRemove')[0].click();");
  ATUReports.add("Check The Delete Button", "Delete button Found", true, LogAs.INFO);

  boolean NewAddressButton;
  NewAddressButton = genericMethods.isElementPresentforMouseHover(myAccountAddressPageObjects.MyAcc_AddressBook_DefaultAddress);

  if (!NewAddressButton) {
   ATUReports.add("Verify Address from My Account section is deleted", "Address from My Account section should be deleted", "Address from My Account section is deleted", true, LogAs.PASSED);
  } else {
   ATUReports.add("Verify Address from My Account section is deleted", "Address from My Account section should be deleted", "Address from My Account section is not deleted", true, LogAs.FAILED);
   Assert.assertEquals(NewAddressButton, true);
  }
 }


 public void MyAccountAddressMakeDefault(WebDriver driver) {
  myAccountAddressPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);

  genericMethods = new GenericMethods();

  xplicitWait = new WebDriverWait(driver, 30);

  GenericMethods.Waitformilliseconds(4000);
  WebElement defaultAddressBox;
  defaultAddressBox = driver.findElement(By.xpath("//div[@class='account-address row']/ul/li[1]/div[@class='accountadd']"));
  defaultAddress = genericMethods.getText(driver, defaultAddressBox);

  genericMethods.mouseHover(driver, myAccountAddressPageObjects.MyAcc_AddressBook_SecondBox_LB, "Address Book 2nd Address");
  makeDefaultLink = driver.findElement(By.xpath("//input[@class='showmakedefault']"));
  ((JavascriptExecutor) driver).executeScript("arguments[0].click();", makeDefaultLink);

  genericMethods.mouseHover(driver, myAccountAddressPageObjects.MyAcc_AddressBook_DefaultBox_LB, "Address book Default Address");

  WebElement secondAddressBox;
  secondAddressBox = driver.findElement(By.xpath("//div[@class='account-address row']/ul/li[2]/div[@class='accountadd']"));
  GenericMethods.Waitformilliseconds(2000);
  xplicitWait.until(ExpectedConditions.visibilityOf(secondAddressBox));
  secondAddress = genericMethods.getText(driver, secondAddressBox);
  //System.out.println("SecondAddress : " + secondAddress);
  //System.out.println("DefaultAddress : " + defaultAddress);
  if (secondAddress.equalsIgnoreCase(defaultAddress)) {
   ATUReports.add("Verify Address is made default", "Address should be made default", "Address is made default successfully", true, LogAs.PASSED);
  } else {
   ATUReports.add("Verify Address is made default", "Address should be made default", "Address is not made default", true, LogAs.FAILED);
   Assert.assertEquals(secondAddress, defaultAddress);
  }
 }

 public void MyAccountAddressEdit(WebDriver driver, String editAddressName, String editFName, String editLName, String editAddress1, String editAddress2, String editCity, String editState, String editZip, String editPhone) {

  myAccountAddressPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
  genericMethods = new GenericMethods();
  xplicitWait = new WebDriverWait(driver, 30);

  hoverOnMyAccountLink(driver);
  ((JavascriptExecutor) driver).executeScript("document.getElementsByClassName('atg_store_addressBookDefaultEdit add-edit-address')[0].click();");
  ATUReports.add("Check The Edit Link", "Edit link Found", true, LogAs.INFO);
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_EditAddressPopUp_AddressName_TB, editAddressName, "Address Name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_FirstName_TB, editFName, "First Name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_LastName_TB, editLName, "Last Name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address1_TB, editAddress1, "Address line 1");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address2_TB, editAddress2, "Address Line 2");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_City_TB, editCity, "City");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD, "State dropdown");
  genericMethods.selectDropdown(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD, editState, "State");
  stateCode = (myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD).getAttribute("value");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Zip_TB, editZip, "Zip Code");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Phone_TB, editPhone, "Phone no");

  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_EditAddressPopUp_Apply_BTN, "Submit Button");
  boolean editAddressForm = genericMethods.isElementPresentforMouseHover(myAccountAddressPageObjects.MyAcc_EditAddressPopUp_FORM);
  if (editAddressForm == true) {
   ATUReports.add("Verify Edit Address Form is still displayed, if any error occurs", "If any error occurs, Edit Address Form should be still displayed", "Edit Address Form is displayed due to some error on the Edit Address Form ", true, LogAs.INFO);
   genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_EditAddressPopUp_Cancel_BTN, "cancel button");
   ATUReports.add("Verify Edit Address Form has been closed", "Edit Address Form should be closed", "Edit Address Form is closed", true, LogAs.PASSED);
  } else {
   ATUReports.add("Verify Edit Address Form is still displayed, if any error occurs", "If any error occurs, Edit Address Form should be still displayed", "Edit Address Form is not displayed", true, LogAs.INFO);
  }

  GenericMethods.Waitformilliseconds(3000);
  xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='account-address row']/ul/li[1]//div[@class='adr']")));
  WebElement newAddressBox;
  newAddressBox = driver.findElement(By.xpath("//div[@class='account-address row']/ul/li[1]//div[@class='adr']"));

  GenericMethods.Waitformilliseconds(2000);
  xplicitWait.until(ExpectedConditions.visibilityOf(newAddressBox));

  addressName = genericMethods.getText(driver, newAddressBox);
  userEditedAddress = editAddress1 + " " + editAddress2 + "\n" + editCity + ", " + stateCode + " " + editZip + "\n" + "United States";

  if (addressName.equalsIgnoreCase(userEditedAddress)) {
   ATUReports.add("Verify Address in My Account section is edited", "Address in My Account section should be edited", "Address in My Account section is edited", true, LogAs.PASSED);
  } else {
   ATUReports.add("Verify Address in My Account section is edited", "Address in My Account section should be edited", "Address in My Account section is not edited", true, LogAs.FAILED);
   Assert.assertEquals(addressName, userEditedAddress);
  }
 }


 public void MyAccountAddressManualAdd(WebDriver driver) {

  myAccountAddressPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
  genericMethods = new GenericMethods();
  xplicitWait = new WebDriverWait(driver, 30);
  int AddressCount = 0;
  for (int i = 1; i <= 10; i++) {

   isAddressBoxPresent = genericMethods.isElementPresent(driver, "//section[@class='account-options']/ul/li[" + i + "]");
   if (isAddressBoxPresent == "present") {
    AddressCount = i;
   } else {
    break;
   }
  }

  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddressBook_AddNewAddr_BTN, "Add New Button");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_AddressName_TB, "Address Two", "Address name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_FirstName_TB, "Bisnath", "First Name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_LastName_TB, "Dubey", "Last name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address1_TB, "112 Turnpike road", "Address line 1");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address2_TB, "2 tech drive", "Address Line 2");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_City_TB, "Boston", "city");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD, "state dropdown");
  genericMethods.selectDropdown(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD, "New York", "State");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Zip_TB, "10012", "Zip code");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Phone_TB, "3333333333", "Phone Number");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Apply_BTN, "Apply button");


  GenericMethods.Waitformilliseconds(2000);
  isQASPresent = genericMethods.isElementPresent(driver, "//button[@name='dwfrm_addForm_useOrig']");
  if (isQASPresent.equalsIgnoreCase("present")) {
   ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true, LogAs.INFO);
   driver.findElement(By.xpath("//form[@id='dwfrm_addForm']/button")).click();;
  }
  AddressCount++;
  GenericMethods.Waitformilliseconds(2000);
  isAddressBoxPresent = genericMethods.isElementPresent(driver, "//div[@class='account-address row']//ul[@class='clearfix']/li[" + AddressCount + "]");
  System.out.println(AddressCount);
  if (isAddressBoxPresent == "present") {
   ATUReports.add("Verify Second Address is added", "Address should be Added", "Address is Added", true, LogAs.PASSED);
  } else {
   ATUReports.add("Verify Second Address is added", "Address should be Added", "Address is not Added", true, LogAs.FAILED);
   Assert.assertEquals(isAddressBoxPresent, "present");
  }
 }

 public void myAccountAddressValidation(WebDriver driver) {
  myAccountAddressPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPageObject.class);
  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
  genericMethods = new GenericMethods();
  xplicitWait = new WebDriverWait(driver, 30);

  genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_AccountArrow_LK, "My Account arrow");
  genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_DD_MyAccount_LK, "My Account Dropdown");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddressBook_LK, "Address book link");
  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddressBook_AddNewAddr_BTN, "Add New Address Button");
  xplicitWait = new WebDriverWait(driver, 30);
  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_AddressName_TB));
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_AddressName_TB, "", "Address Name");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_FirstName_TB, "", "First Name textbox");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_LastName_TB, "", "Last name textbox");


  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddressPopUp_FirstNameError_TV));
  String firstNameErrorMessage = genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_FirstNameError_TV);
  if (firstNameErrorMessage.contains("Please provide a first name.")) {
   ATUReports.add("System displaying error message when first name is blank", "  ", "Error Messages for First Name should be =  Please provide a first name.", "Error message for firstName : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_FirstNameError_TV)), true, LogAs.PASSED);
  } else {
   ATUReports.add("System is not displaying error message when first name is blank", "  ", "Error Messages for First Name should be =  Please provide a first name.", "Error message for firstName : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_FirstNameError_TV)), true, LogAs.FAILED);
  }

  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address1_TB, "", "Address 1");
  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddressPopUp_LastNameError_TV));
  String lastNameErrorMessage = genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_LastNameError_TV);
  if (lastNameErrorMessage.contains("Please provide a last name.")) {
   ATUReports.add("System displaying error message when last name is blank", "  ", "Error Messages for Last Name should be =  Please provide a Last name.", "Error message for Last Name : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_LastNameError_TV)), true, LogAs.PASSED);
  } else {
   ATUReports.add("System is not displaying error message when last name is blank", "  ", "Error Messages for Last Name should be =  Please provide a Last name.", "Error message for Last Name : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_LastNameError_TV)), true, LogAs.FAILED);
  }

  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Address2_TB, "", "Address 2");
  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_City_TB, "", "City");
  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddressPopUp_AddressOneError_TV));
  String address1ErrorMessage = genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_AddressOneError_TV);
  if (address1ErrorMessage.contains("Please enter a valid street address.")) {
   ATUReports.add("System displaying error message when address1 is blank", "  ", "Error Messages for Address1 should be =  Please enter a valid street address.", "Error message for address1 : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_AddressOneError_TV)), true, LogAs.PASSED);
  } else {
   ATUReports.add("System is not displaying error message when address1 is blank", "  ", "Error Messages for Address1 should be =  Please enter a valid street address.", "Error message for address1 : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_AddressOneError_TV)), true, LogAs.FAILED);
  }

  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_State_DD, "State Dropdown");
  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddressPopUp_CityError_TV));
  String cityErrorMessage = genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_CityError_TV);
  if (cityErrorMessage.contains("Please enter a valid city.")) {
   ATUReports.add("System displaying error message when City is blank", "  ", "Error Messages for City should be =  Please enter a valid city.", "Error message for city : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_CityError_TV)), true, LogAs.PASSED);
  } else {
   ATUReports.add("System is not displaying error message when City is blank", "  ", "Error Messages for City should be =  Please enter a valid city.", "Error message for city : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_CityError_TV)), true, LogAs.FAILED);
  }

  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Zip_TB, "", "Zip Code");
  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddressPopUp_StateError_TV));
  String stateErrorMessage = genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_StateError_TV);
  if (stateErrorMessage.contains("Please select a State.")) {
   ATUReports.add("System displaying error message when State not selected", "  ", "Error Messages for State should be =  Please select a State.", "Error message for State : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_StateError_TV)), true, LogAs.PASSED);
  } else {
   ATUReports.add("System is not displaying error message when State not selected", "  ", "Error Messages for State should be = Please select a State.", "Error message for State : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_StateError_TV)), true, LogAs.FAILED);
  }

  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Phone_TB, "", "Phone No");
  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddressPopUp_ZipError_TV));
  String zipErrorMessage = genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_ZipError_TV);
  if (zipErrorMessage.contains("Please enter a valid ZIP or postal code.")) {
   ATUReports.add("System displaying error message when Zip code is blank", "  ", "Error Messages for zip code should be =  please enter valid zip code", "Error message for Zip code : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_ZipError_TV)), true, LogAs.PASSED);
  } else {
   ATUReports.add("System is not displaying error message when Zip code is blank", "  ", "Error Messages for zip code should be =  please enter valid zip code", "Error message for Zip code : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_ZipError_TV)), true, LogAs.FAILED);
  }

  genericMethods.inputValue(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Zip_TB, "", "Zipcode");
  xplicitWait.until(ExpectedConditions.visibilityOf(myAccountAddressPageObjects.MyAcc_AddressPopUp_PhoneError_TV));
  String phoneErrorMessage = genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_PhoneError_TV);
  if (phoneErrorMessage.contains("Please enter a valid phone number, in case we need to contact you regarding your order.")) {
   ATUReports.add("System displaying error message when phone number is blank", "  ", "Error Messages for Phone number should be =  Please enter a valid phone number, in case we need to contact you regarding your order.", "Error message for Phone number : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_PhoneError_TV)), true, LogAs.PASSED);
  } else {
   ATUReports.add("System is not displaying error message when phone number is blank", "  ", "Error Messages for Phone number should be =  Please enter a valid phone number, in case we need to contact you regarding your order.", "Error message for phone number is : " + (genericMethods.getText(driver, myAccountAddressPageObjects.MyAcc_AddressPopUp_PhoneError_TV)), true, LogAs.FAILED);
  }

  genericMethods.Click(driver, myAccountAddressPageObjects.MyAcc_AddNewAddrPopUp_Apply_BTN, "Apply Button");
 }

}