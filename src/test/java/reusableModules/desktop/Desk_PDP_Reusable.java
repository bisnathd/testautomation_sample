package reusableModules.desktop;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.record.PageBreakRecord.Break;
import org.junit.internal.runners.statements.Fail;
import org.junit.rules.Verifier;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.common.base.Verify;

import setup.WinAuth;
import PageObjects.desktop.Desk_BNY_CartPageObject;
import PageObjects.desktop.Desk_BNY_MYBagPageObjects;
import PageObjects.desktop.Desk_BNY_PDPPageObjects;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_MyFavoritesPageObjects;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_PDP_Reusable {
	GenericMethods genericMethods;
	WebDriverWait xplicitWait ;
	Desk_BNY_PDPPageObjects pdpPageObjects;
	Desk_BNY_HomePageObject LoginToBNY_Objects;
	Desk_BNY_HomePageObject home_PageObjects;
	Desk_BNY_MyFavoritesPageObjects myFav_PageObjects;
	Desk_BNY_MYBagPageObjects myBag_PageObjects;
	Desk_BNY_CartPageObject cartPageObjects;
	Desk_LoginRegister_Reusable loginRegister;
	boolean isColorElementVisible;
	String isElementPresent1;
	String isSizeElementPresent;
	List<WebElement> element;
	String colourClasstext;
	String sizeClasstext;
	String firstSelectableSize;
	WebDriverWait wait;
	String isArrowBesideFavIconPresent;
	Desk_MYBagCheckout_Reusable myBagReusable;
	boolean isQuantityRestrictionDialogBoxVisible;
		
	
	public void changeSelectedColour(WebDriver driver){
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		genericMethods=new GenericMethods();
		isColorElementVisible=genericMethods.isElementVisible(driver, By.xpath("//div[@class='atg_store_colorPicker']"));
		
		if (isColorElementVisible==true) {
			ATUReports.add("Verify Colour Element", "Colour Element should be Visible","Colour Element is Visible",true,LogAs.PASSED);			
			element=driver.findElements(By.xpath("//div[@class='atg_store_colorPicker']/span/a"));
			
			//For loop 1 to check the selected color
			for (int i = 0; i < element.size(); i++) {
				int j=i+1;
				colourClasstext=driver.findElement(By.xpath("//div[@class='atg_store_colorPicker']/span/a["+j+"]")).getAttribute("class");
				System.out.println("Colour class text = " + colourClasstext);
				
				if(colourClasstext.contains("colorSwatch"))
				//if(colourClasstext.contains("colorSwatch")&&j!=1)
				{
					ATUReports.add("Verify selected Colour", "","Selected colour number is "+j,true,LogAs.PASSED);
					genericMethods.Click(driver, pdpPageObjects.PDP_SelectableColour_LK,"selectable color");
					GenericMethods.Waitformilliseconds(3000);
					colourClasstext=driver.findElement(By.xpath("//div[@class='atg_store_colorPicker']/span/a["+j+"]")).getAttribute("class");
					
					//System.out.println("If " +colourClasstext);
					if (colourClasstext.equalsIgnoreCase("colorSwatch color-active")) {
						ATUReports.add("Verify if user is able to select color swatch", "User should be able to select " +j+" color swatch","User was able to select" +j+" Color Swatch",true,LogAs.PASSED);
					} else {
						ATUReports.add("Verify if user is able to select color swatch", "User should be able to select " +j+" color swatch","User was not able to select" +j+" Color Swatch",true,LogAs.FAILED);
						Assert.assertEquals("colorSwatch color-active", colourClasstext);				
					}
					
					break;
				}else if(colourClasstext.equalsIgnoreCase("selected")){
					genericMethods.Click(driver, pdpPageObjects.PDP_SelectableColour_LK,"selectable color");
					GenericMethods.Waitformilliseconds(1000);
					colourClasstext=driver.findElement(By.xpath("//div[@class='atg_store_colorPicker']")).getAttribute("class");
					
					//System.out.println("Else " +colourClasstext);
					if (colourClasstext.equalsIgnoreCase("selected")) {
						ATUReports.add("Verify selected colour after user clicked on 1st colour", "First Colour should be selected","Selected colour number is 2",true,LogAs.PASSED);
					} else {
						ATUReports.add("Verify selected colour after user clicked on 1st colour", "First Colour should be selected","Clicked colour is not selected",true,LogAs.FAILED);
						Assert.assertEquals("selected", colourClasstext);				
					}
					break;
				}
			}
		} else {
			ATUReports.add("Verify Colour Element", "Colour Element should be Visible","Colour Element is not Visible",true,LogAs.PASSED);
			Assert.assertEquals(true, isColorElementVisible);
		}

	}		
	
	/*public void selectSizefortheProduct(WebDriver driver)
		{
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		genericMethods=new GenericMethods();
		isSizeElementPresent=genericMethods.isElementPresent(driver, "//div[@class ='atg_store_sizePicker size-label']");
				
		
		if (isSizeElementPresent.equals("present")) {
			ATUReports.add("Verify Size Element", "Size Element should be Present","Size Element is Present",true,LogAs.PASSED);
			
			element=driver.findElements(By.xpath("//div[@class ='atg_store_sizePicker size-label']/span/a"));
			
			//For loop 1 to check the selected size
			for (int i = 0; i < element.size(); i++) {
				int j=i+1;
				sizeClasstext=driver.findElement(By.xpath("//div[@class ='atg_store_sizePicker size-label']/span/a["+j+"]")).getAttribute("class");
				//System.out.println(sizeClasstext);
				
				if(sizeClasstext.contains("atg_store_oneSize sizePicker")){	
					firstSelectableSize=pdpPageObjects.PDP_Selectable_Size_Element_LK.getText();
					//System.out.println(firstSelectableSize);
					
					ATUReports.add("Verify first size available for selection", "","first size available for selection : "+firstSelectableSize,true,LogAs.INFO);
					genericMethods.Click(driver, pdpPageObjects.PDP_Selectable_Size_Element_LK,"selectable size");
					//genericMethods.javascriptClick(driver,  pdpPageObjects.PDP_Selectable_Size_Element_LK,"selectable size");
					GenericMethods.Waitformilliseconds(2000);					
				
					break;
				}				
			}
			
			if (((pdpPageObjects.PDP_Selected_Size_Element_LK).getText()).equalsIgnoreCase(firstSelectableSize)) {
				ATUReports.add("Verify Selected Size", "Selected Size should be: "+firstSelectableSize,"Selected Size is : "+pdpPageObjects.PDP_Selected_Size_Element_LK.getText(),true, LogAs.PASSED);
			} else {
				Assert.assertEquals(firstSelectableSize, pdpPageObjects.PDP_Selected_Size_Element_LK.getText());
				ATUReports.add("Verify Selected Size", "Selected Size should be: "+firstSelectableSize,"Selected SIze is : "+pdpPageObjects.PDP_Selected_Size_Element_LK.getText(),true, LogAs.FAILED);
			}
			
		} else {
			ATUReports.add("Verify Size Element", "Size Element should be Present","Size Element is not Present",true,LogAs.PASSED);
			Assert.assertEquals("present", isSizeElementPresent);
		}
	}*/

	public String verifyButtontextonMouseHover(WebDriver driver) {
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		genericMethods=new GenericMethods();
		
		genericMethods.mouseHover(driver, pdpPageObjects.AddToCart_BTN,"Add to Cart button");
		return genericMethods.getText(driver, pdpPageObjects.AddToCart_BTN);
	}
	
	public void verifyPDPAccordianStatus(WebDriver driver) {
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		genericMethods=new GenericMethods();

		if ((pdpPageObjects.PDP_Deatails_accordian_LK).getAttribute("class").equalsIgnoreCase("")) {
			ATUReports.add("Verify state of Details accordian", "Details Accordian should be expanded","Details accordian is expanded", true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify state of Details accordian", "Details Accordian should be expanded","Details accordian is Collapsed", true, LogAs.FAILED);
		Assert.assertEquals("", (pdpPageObjects.PDP_Deatails_accordian_LK).getAttribute("class"));
		}
		/*	
		if ((pdpPageObjects.PDP_SizeFit_accordian_LK).getAttribute("class").equalsIgnoreCase("collapsed")) {
			ATUReports.add("Verify state of Size and Fit accordian", "Details Accordian should be collapsed","Size and Fit accordian is collapsed", true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify state of Size and Fit accordian", "Details Accordian should be collapsed","Size and Fit accordian is Expanded", true, LogAs.FAILED);
			Assert.assertEquals("collapsed", (pdpPageObjects.PDP_SizeFit_accordian_LK).getAttribute("class"));
		}
		
		if ((pdpPageObjects.PDP_Designer_accordian_LK).getAttribute("class").equalsIgnoreCase("collapsed")) {
			ATUReports.add("Verify state of Designer accordian", "Details Accordian should be collapsed","Designer accordian is collapsed", true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify state of Designer accordian", "Details Accordian should be collapsed","Designer accordian is Expanded", true, LogAs.FAILED);
			Assert.assertEquals("collapsed", (pdpPageObjects.PDP_Designer_accordian_LK).getAttribute("class"));
		}
		
		if ((pdpPageObjects.PDP_ShippingAndReturns_accordian_LK).getAttribute("class").equalsIgnoreCase("collapsed")) {
			ATUReports.add("Verify state of Shipping and Returns accordian", "Details Accordian should be collapsed","Shipping and Returns accordian is collapsed", true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify state of Shipping and Returns accordian", "Details Accordian should be collapsed","Shipping and Returns accordian is expanded", true, LogAs.FAILED);
			Assert.assertEquals("collapsed", (pdpPageObjects.PDP_ShippingAndReturns_accordian_LK).getAttribute("class"));
		}	
		*/	
	}
	
	public void verifyImagesAfterSeeAll(WebDriver driver)
	{
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		genericMethods=new GenericMethods();
		GenericMethods.Waitformilliseconds(3000);
		element=driver.findElements(By.xpath("//div[@class='atg_store_productImage']//a[contains (text(),'See all Images')]"));
		if (element.size()==1) {
			ATUReports.add("Verify Number of Product Image before click on 'SEE ALL IMAGES' button", "Only 2 images should be displayed","2 Images are shown" ,true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Number of Product Image before click on 'SEE ALL IMAGES' button", "2 images should be displayed","More than 2 Images are shown" ,true, LogAs.FAILED);
			Assert.assertEquals(1, element.size());
		}
		
		genericMethods.Click(driver, pdpPageObjects.PDP_SeeAllImages_BTN,"PDP see all images button");
		GenericMethods.Waitformilliseconds(3000);
		element=driver.findElements(By.xpath("//img[@class='primary-image']"));
		if (element.size()>1) {
			ATUReports.add("Verify Number of Product Image after click on 'SEE ALL IMAGES' button", "More than 2 images should be displayed","More than 2 Images are shown" ,true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Number of Product Image after click on 'SEE ALL IMAGES' button", "more than 2 images should be displayed","only 2 Images are shown" ,true, LogAs.FAILED);
			Assert.assertNotEquals(1, element.size());						
		}
		
		genericMethods.Click(driver, pdpPageObjects.PDP_CollapseAllImages_BTN,"PDP Collapse Images button");
		GenericMethods.Waitformilliseconds(3000);
		element = driver.findElements(By.xpath("//div[@class='atg_store_productImage']"));
		
		if (element.size()==1) {
			ATUReports.add("Verify Number of Product Image after click on 'COLLAPSE IMAGES' button", "Only 2 images should be displayed","2 Images are shown" ,true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Number of Product Image after click on 'COLLAPSE IMAGES' button", "2 images should be displayed","More than 2 Images are shown" ,true, LogAs.FAILED);
			Assert.assertEquals(1, element.size());
		}		
	}
	
	public void verifyNumberOfProductsUnderRecommendedandFeaturing(WebDriver driver){
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();",driver.findElement(By.xpath("//li[@id='recommendation-tab']/a/span[contains (text(),Recommend)]")));
		element=driver.findElements(By.xpath("//li[@class='items rr_item slide slick-slide slick-active']"));
		if (element.size()==4) {
			ATUReports.add("Verify Number of Products displayed on Recommended section", "10 Products should be Displayed under recommended section","Number of products displayed : "+element.size(),true ,LogAs.PASSED);
		} else {
			ATUReports.add("Verify Number of Products displayed on Recommended section", "10 Products should be Displayed under recommended section","Number of products displayed : "+element.size(),true ,LogAs.FAILED);
			Assert.assertEquals(4, element.size());
		}
		
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath("//h5[contains(text(),'Featuring')]")));
		element=driver.findElements(By.xpath("//div[@id='featuring-promos-content']/article"));
		//div[@class='category-main-banner clearfix']/div
		//div[@id="featuring-promos-content"]
		if (element.size()==4) {
			ATUReports.add("Verify Number of rich images displayed under FEATURED section", "4 rich images should be Displayed under Featured section","Number of Rich Images displayed : "+element.size(),true ,LogAs.PASSED);
		} else {
			ATUReports.add("Verify Number of rich images displayed under FEATURED section", "4 rich images should be Displayed under Featured section","Number of Rich Images displayed : "+element.size(),true ,LogAs.FAILED);
		Assert.assertEquals(4, element.size());
		}	
	}
	
	public void AddToFavorites(WebDriver driver){
		String productNameBeforeclickOnFavIcon;
		String productNameAfterclickOnFavIcon;
		loginRegister = PageFactory.initElements(driver, Desk_LoginRegister_Reusable.class);
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		LoginToBNY_Objects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		home_PageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myFav_PageObjects=PageFactory.initElements(driver, Desk_BNY_MyFavoritesPageObjects.class);
		genericMethods=new GenericMethods();
		wait = new WebDriverWait(driver, 15);
		
		productNameBeforeclickOnFavIcon=(driver.findElement(By.xpath("//h2[@class='product-title']"))).getText();
		ATUReports.add("Product Name Before Click on Favorites icon", "", "Product Name before click on FAV icon is :"+productNameBeforeclickOnFavIcon, true, LogAs.INFO);
		
		genericMethods.Click(driver, pdpPageObjects.PDP_AddToFavForGuest_icon_LK,"Add to Favorites Link");

		  //-----------------*********LOGIN Page*********----------------------------
		  //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Log in to your account')]"))); 
		  //ATUReports.add("Login Page Should be displayed", "","Login Page is shown", true, LogAs.INFO);
		  WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
		    
		  genericMethods.inputValue(driver,LoginToBNY_Objects.LoginPage_LoginPanel_Email_TB, "barneys92@yopmail.com","Login Email id");
		  genericMethods.inputValue(driver,LoginToBNY_Objects.LoginPage_LoginPanel_Password_TB, "987654321","Login Password");
		  ATUReports.add("Login page with filled details","","",true, LogAs.INFO);
		  genericMethods.Click(driver,LoginToBNY_Objects.LoginPage_LoginPanel_LOGIN_BTN,"Login Button");

		//---------------************PDP Page***********--------------------------
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[@class='product-title']")));//h1[@class='product-name']
		productNameAfterclickOnFavIcon=(driver.findElement(By.xpath("//h2[@class='product-title']"))).getText();//h1[@class='product-name']
		
		if (productNameBeforeclickOnFavIcon.equalsIgnoreCase(productNameAfterclickOnFavIcon)) {
			ATUReports.add("Check Product Name before and after click on Fav Icons", "Product Name should be same after and before Click", "Product Name is same after and before click on Fav Icon", true, LogAs.PASSED);
		} else {
			ATUReports.add("Check Product Name before and after click on Fav Icons", "Product Name should be same after and before Click", "Product Name is different after and before click on Fav Icon", true, LogAs.FAILED);
			Assert.assertEquals(productNameBeforeclickOnFavIcon, productNameAfterclickOnFavIcon);
		}
		
		genericMethods.Click(driver, home_PageObjects.Home_TopNav_AccountArrow_LK,"Utility Nav Account Arrow");
		GenericMethods.Waitformilliseconds(1000);
		ATUReports.add("Check My Account dropdown arrow", true, LogAs.INFO);
		genericMethods.Click(driver, home_PageObjects.Home_TopDDMyFavorites_DD,"Utility Nav My Fav");
		
		GenericMethods.Waitformilliseconds(2000);
		//MYFAVPage_ListButton_BTN
		genericMethods.Click(driver, myFav_PageObjects.MYFAVPage_ListButton_BTN,"My Favorites List button");
		ATUReports.add("","User should be on MY FAVORITES list page","User is on MY FAVORITES List Page", true, LogAs.INFO);
		genericMethods.Click(driver, myFav_PageObjects.MYFAV_MyFavoritesList_BOX_Element,"My Favorite Listbox");
		GenericMethods.Waitformilliseconds(1000);
		//genericMethods.Click(driver, myFav_PageObjects.MYFAV_MyFavoritesList_BOX_Edit_LK);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='custom-product-list row search-result-content']")));
		//div[@id='search-result-items']//div[@class='product-name']/a
		
		if (productNameAfterclickOnFavIcon.equalsIgnoreCase(genericMethods.getText(driver, myFav_PageObjects.MYFAV_MyFavoritesListPage_1stProductName_LK))) {
			ATUReports.add("Verify if Product is added to My Favorites", "Product with name :"+productNameAfterclickOnFavIcon+" should get added to MY FAVORITES","Product with name " +genericMethods.getText(driver, myFav_PageObjects.MYFAV_MyFavoritesListPage_1stProductName_LK)+" is added to MY FAVORITES",true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify if Product is added to My Favorites", "Product with name :"+productNameAfterclickOnFavIcon+" should get added to MY FAVORITES","Product with name " +genericMethods.getText(driver, myFav_PageObjects.MYFAV_MyFavoritesListPage_1stProductName_LK)+" is added to MY FAVORITES",true, LogAs.PASSED);
			Assert.assertEquals(productNameAfterclickOnFavIcon, genericMethods.getText(driver, myFav_PageObjects.MYFAV_MyFavoritesListPage_1stProductName_LK));
		}

	/*	
		//-----------------*********Remove from My Favorites***********------------------------------
		genericMethods.Click(driver, myFav_PageObjects.MYFAV_MyFavoritesListPage_1stProductName_LK,"Favorites List first Product");
		genericMethods.Click(driver, pdpPageObjects.PDP_AddToFavForLoggedIn_icon_LK,"");
		GenericMethods.Waitformilliseconds(5000);
		ATUReports.add("Favorites Icon should not be filled sin item is removed from MY FAVORITES", true, LogAs.INFO);
		
		isArrowBesideFavIconPresent=genericMethods.isElementPresent(driver, "//div[@class='pdp-faves product-tile']//span[@class='favorites-arrow  hidden-xs hidden-md hidden-sm']");//div[@class='pdp-faves']/a[2]
		if (isArrowBesideFavIconPresent.equalsIgnoreCase("present")) {
			genericMethods.Click(driver, pdpPageObjects.PDP_AddToFav_iconArrow_LK,"Add to Fav arrow");
			GenericMethods.Waitformilliseconds(1000);
			ATUReports.add("Verify Arrow Beside the Favorites Icon","User should be able to see options for adding List", "User can see the options for adding List's to My Favorites",true, LogAs.PASSED);
		}else{
			ATUReports.add("Verify Arrow Beside the Favorites Icon","User should be able to see options for adding List", "User can not see the options for adding List's to My Favorites",true, LogAs.FAILED);
			Assert.assertEquals("present", isArrowBesideFavIconPresent);
		}*/		
	}
	
	public void verifyQuantityadded(WebDriver driver, int quantity){
		String qunatityinString;
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		home_PageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods=new GenericMethods();
		genericMethods.Click(driver, pdpPageObjects.PDP_Selectable_Size_Element_LK,"Selectable Size link");
		
		GenericMethods.Waitformilliseconds(1000);
		for (int i =quantity; i > 0; i--) {		
			qunatityinString=""+i;			
			genericMethods.inputValue(driver, pdpPageObjects.QuantityField_TB,qunatityinString ,"Product Quantity");
			GenericMethods.Waitformilliseconds(1000);
			genericMethods.Click(driver, pdpPageObjects.AddToCart_BTN,"Add to Cart button");
			GenericMethods.Waitformilliseconds(3000);
			
			isQuantityRestrictionDialogBoxVisible=genericMethods.isElementVisible(driver,(By.xpath("//div[@class='inventory-message']")));
			ATUReports.add("Verify Quantity Info Dialog box is present","","Quantity Info Dialog box is :"+isQuantityRestrictionDialogBoxVisible, true,LogAs.INFO);
			if (isQuantityRestrictionDialogBoxVisible==false) {
				ATUReports.add("Verify if Mini Cart is present","Mini Cart should be Present","Mini Cart is Present", true,LogAs.INFO);			
				genericMethods.Click(driver, pdpPageObjects.MiniCart_Checkout_BTN,"Checkout Button");
				GenericMethods.Waitformilliseconds(2000);
				
				if (genericMethods.getText(driver, home_PageObjects.BNY_Banner_MyBag_LK).equalsIgnoreCase("MY BAG "+qunatityinString))	{
					ATUReports.add("Verify Quantity entered on PDP and available on My Bag","Quantity entered on PDP and available on My Bag should be same", "Quantity entered on PDP and available on My Bag is same", true,LogAs.PASSED);
				} else {
					ATUReports.add("Verify Quantity entered on PDP and available on My Bag","Quantity entered on PDP and available on My Bag should be same", "Quantity entered on PDP and available on My Bag is not same", true,LogAs.FAILED);
					Assert.assertEquals("MY BAG "+qunatityinString, genericMethods.getText(driver, driver.findElement(By.xpath("//a[@class='mini-cart-link']"))));
				}				
				break;
			}else{
				genericMethods.Click(driver,driver.findElement(By.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable']/div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span[@class='ui-icon ui-icon-closethick']")),"");
			}
		}	
	}
	
	public void selectAvailableSizeOnPDP(WebDriver driver){
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  genericMethods =  new GenericMethods();
		  pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		  boolean isPDPSizeElementPresent;
		  boolean isSelectableSizeAvailable;
		  
		  	isPDPSizeElementPresent=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_SizeSection_Element);
			if(isPDPSizeElementPresent){
				isSelectableSizeAvailable=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_Selectable_Size_Element_LK);
				if (isSelectableSizeAvailable) {			
					GenericMethods.Waitformilliseconds(1000);
					genericMethods.Click(driver, pdpPageObjects.PDP_Selectable_Size_Element_LK, "Available Size");		
					GenericMethods.waitForPageLoaded(driver);
				}else{
					ATUReports.add("Verify Size selected","", "No Size is available For Selection",false,LogAs.INFO);
				}				
			}else{
				ATUReports.add("Verify Size Selector","","Size Selector is not available", false,LogAs.INFO);	
			}
	  }	
	
	public void selectAvailableColorOnPDP(WebDriver driver){
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  genericMethods =  new GenericMethods();
		  pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		  boolean isPDPColorElementPresent;
		  boolean isSelectableColorAvailable;
		  
		  isPDPColorElementPresent=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_ColorPicker_Element);
		  if(isPDPColorElementPresent){
			  isSelectableColorAvailable=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_SelectableColour_LK);
				if (isSelectableColorAvailable) {			
					GenericMethods.waitForPageLoaded(driver);
					genericMethods.Click(driver, pdpPageObjects.PDP_SelectableColour_LK, "Available Color");	
					GenericMethods.waitForPageLoaded(driver);
					ATUReports.add("Verify Size selected","", "Color is Selected",false,LogAs.INFO);
				}else{
					ATUReports.add("Verify Color selected","", "No Color is available For Selection",false,LogAs.INFO);
				}				
			}else{
				ATUReports.add("Verify Size Selector","","Color Picker is not available", false,LogAs.INFO);	
			}
	  }	
	
	public void GotoPDPUsingURL(WebDriver driver,String URLfromXML, String bnyProductURL , String whsProductURL ) {
		myBagReusable=new Desk_MYBagCheckout_Reusable();
		String  CurrentPageTitle=GenericMethods.getMyPagetitle(driver);			
		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){				
			myBagReusable.emptyMyBag(driver, "WHS");
			driver.get(URLfromXML+whsProductURL);		
		}else{
			myBagReusable.emptyMyBag(driver, "BNY");
			driver.get(URLfromXML+bnyProductURL);				
		}  
		ATUReports.add("Verify customer is on PDP page", "PDP should open","System should display PDP", true, LogAs.INFO);	
	}		

	public void AddtoCartButtononPDP(WebDriver driver){
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
		genericMethods =  new GenericMethods();
		WebDriverWait xwait = new WebDriverWait(driver, 30);
		
		genericMethods.Click(driver, pdpPageObjects.PDP_BUY_BTN,"BUY Button");
		xwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Checkout')]")));
		ATUReports.add("Verify Add to cart button", "Add to cart button should be displayed", "Add to cart button should be displayed", true,LogAs.INFO);
			
		}
	
	
	public void miniBagPopuponPDP(WebDriver driver){
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		cartPageObjects = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
		genericMethods =  new GenericMethods();
		
		ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);

		Boolean popUp=genericMethods.isElementVisible(driver, By.id("showMiniCart"));
		if (popUp= true){ 
			ATUReports.add("Verify if mini bag popup is displayed", "Mini Cart pop up should be displyed","Mini Cart pop up is displayed",true,LogAs.PASSED);
			
				String Popupname = genericMethods.getText(driver, cartPageObjects.MybagfromPDP_gettext);
				System.out.println(Popupname);
				  if (Popupname.contains("MY BAG")){
					ATUReports.add("System displaying Mini Cart pop up", "Customer should be on Mini Cart pop up","Customer is on MY BAG pop up",true,LogAs.PASSED);
					Assert.assertEquals(Popupname, "MY BAG\nTotal 1 item");					
				  }else{
					ATUReports.add("Customer is not on Mini Cart pop up", "Customer should be on Mini Cart pop up","Customer is not on MY BAG pop up",true,LogAs.FAILED);
					Assert.assertEquals(Popupname, "MY BAG\nTotal 1 item");					
				  }
		}else {
			ATUReports.add("Verify if mini bag popup is displayed", "Mini Cart pop up should be displyed","Mini Cart pop up is not displayed",true,LogAs.FAILED);
		}
	}
	
	public void miniBagPopupclickOnCheckout_BT(WebDriver driver){
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		WebDriverWait xwait = new WebDriverWait(driver, 30);
		genericMethods =  new GenericMethods();	
		
		ATUReports.add("Verify BUY button", "Should be Added", "Its is added", true,LogAs.PASSED);
		xwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Checkout')]")));
		ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);
		genericMethods.Click(driver, pdpPageObjects.MiniCart_Checkout_BTN,"Mini Cart Checkout button");
		WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
		
	}
	

	
	}
	

