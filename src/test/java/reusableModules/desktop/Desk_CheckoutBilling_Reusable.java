package reusableModules.desktop;

import org.openqa.selenium.By;
//import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import PageObjects.desktop.Desk_BNY_BillingPageObject;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_GlobalNavPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_MYBagPageObjects;
import PageObjects.desktop.Desk_BNY_MyAccountPaymentPaegObjects;
//import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_OrderReviewPageObject;
//import PageObjects.desktop.Desk_BNY_QASPageObject;
import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import reusableModules.desktop.Desk_MYBagCheckout_Reusable;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_CheckoutBilling_Reusable {
	Desk_BNY_DesignerPageObject designer_PageObjects;
	Desk_BNY_RegistrationPageObject registration_PageObjects;
	Desk_BNY_MyAccountPaymentPaegObjects myAccountPayment_PageObjects;
	Desk_BNY_HomePageObject home_PageObjects;
	Desk_BNY_MYBagPageObjects myBag_PageObjects;
	Desk_BNY_ShippingPageObject Shipping_PageObjects;
	Desk_BNY_BillingPageObject billing_PageObjects;
	Desk_BNY_ShippingPageObject shipping_PageObjects;
	Desk_BNY_OrderReviewPageObject orderReview_PageObject;
	Desk_BNY_GlobalNavPageObject globalNavigation_PageObjects;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	Desk_CheckoutShipping_Reusable checkoutShippingReusable;
	Desk_MyAccountPayment_Reusable myAccountPaymentReusable;
	Desk_PDP_Reusable pdpReusable;
	Desk_MYBagCheckout_Reusable myBagCheckoutReusable;
	Desk_CheckoutBilling_Reusable checkoutBillingReusable;
	Desk_GetOrderNumber_Reusable getOrderNumberReusable;	
	Desk_MyAccountAddress_Reusable myAccountAddressReusable;
	Desk_GlobalNav_Reusable globalNavReusable;	
	String CurrentPageTitle;
	boolean isPaymentDDPresent;
	boolean isStateDDPresent;
	String isErrorMsgPresent;
	String urlFromXML;
	WebElement paymentEditLink ;
	GenericMethods genericMethods;
	WebDriverWait xplicitWait ;
	
	
	
			
	public boolean verifyPresenceOfPaymentDropdown(WebDriver driver){
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		isPaymentDDPresent=genericMethods.isElementPresentforMouseHover(billing_PageObjects.billing_payment_DD);
		return isPaymentDDPresent;
	}
	
	 public void AddPaymentDetails(WebDriver driver,String cardHolderName,String cardNo,String CardMonth,String year,String CVV)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardHolderName_TB, cardHolderName,"Billing First Name");
		genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardCardNo_TB,cardNo , "Billing card number");
		genericMethods.selectDropdown(driver, billing_PageObjects.billing_payment_ExpMonth_DD, CardMonth, "Card Expiry Month");
		genericMethods.selectDropdown(driver, billing_PageObjects.billing_payment_ExpYear_DD, year, "card Expirt year");
		genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardCardCVV_TB,CVV,"CVV Number");
				
	}
	 
	 public void GuestaddPaymentDetails(WebDriver driver,String cardHolderName,String cardNo,String CardMonth,String year,String CVV)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardHolderName_TB, cardHolderName,"Billing First Name");
		genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardCardNo_TB,cardNo , "Billing card number");
		genericMethods.selectDropdown(driver, billing_PageObjects.billing_payment_ExpMonth_DD, CardMonth, "Card Expiry Month");
		genericMethods.selectDropdown(driver, billing_PageObjects.billing_payment_ExpYear_DD, year, "card Expirt year");
		genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardCardCVV_TB,CVV,"CVV Number");
				
	}
	 
	 public void addBillingAddressDetails(WebDriver driver,String firstName, String lastName,String address1,String address2,String city,String billingState,String zipCode,String billingMail,String phone)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);	
		
		//Start Filling Billing Address
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,firstName,"firstName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,lastName,"lastName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,address1,"address1");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,address2,"address2");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_City_TB,city,"city");
		
		isStateDDPresent=genericMethods.isElementPresentforMouseHover(billing_PageObjects.billing_Address_State_DD);
		if (isStateDDPresent) {
			ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
			genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,billingState,"Billing State");
		}else{
			ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,billingState,"State");
		}
		
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_zip_TB,zipCode,"zipCode");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,billingMail,"Email");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,phone,"Phone");		
		//genericMethods.Click(driver, billing_PageObjects.billing_Continue_BTN,"continue_button");
	
		/*
		GenericMethods.Waitformilliseconds(3000);		
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
		BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";
			
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded)){
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddress, BillingAddressAdded);
		}	
		*/	
	}
	//*******************************************************************************************************************
/*
	 public void GuestPriceCheck(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
			checkoutShippingReusable=new Desk_CheckoutShipping_Reusable();
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);		
			myBagCheckoutReusable.SelectUnitedStatesGuestUser(driver);
			
			//-------------------------Shipping Page-----------------------------	
			checkoutShippingReusable.startFillingShippingForm(driver, firstName, lastName, address1, address2, city, State, zipCode, Phone);
			String BillingPrice= genericMethods.getText(driver, billing_PageObjects.Billing_EstimatedTotal_LB);
			genericMethods.Click(driver, billing_PageObjects.Billing_MyBag_LK,"Billing My Bag Link");
			String MyBagPrice= genericMethods.getText(driver, myBag_PageObjects.MYBag_EstimatedTotal_LB);	
			if(BillingPrice.equalsIgnoreCase(MyBagPrice)){
				ATUReports.add("Verify Total price is same as my bag total price", " Total price should be same as my bag total price", "Total price is same as my bag total price", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Total price is same as my bag total price", " Total price should be same as my bag total price", "Total price is not same as my bag total price", true,LogAs.FAILED);
				Assert.assertEquals(BillingPrice, MyBagPrice);
			}		
		}
			
		public void BillingMyBagLinkCheck(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {

			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);		
			myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();
			checkoutShippingReusable=new Desk_CheckoutShipping_Reusable();		
				
	//-------------------------Shipping Page-----------------------------
			myBagCheckoutReusable.SelectUnitedStatesGuestUser(driver);
			checkoutShippingReusable.startFillingShippingForm(driver, firstName, lastName, address1, address2, city, State, zipCode, Phone);
			String isMyBagLinkPresent=genericMethods.isElementPresent(driver,"//div[@id='secondary']/div[1]/div[2]/a" );
			if (isMyBagLinkPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify Edit My Bag Link present", "Edit My Bag Link should be present ", "Edit My Bag Link is present ", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Edit My Bag Link present", "Edit My Bag Link should be present ", "Edit My Bag Link is not present ", true,LogAs.FAILED);
				Assert.assertEquals(isPaymentDDPresent, "notpresent");
			}			
				
			genericMethods.Click(driver, billing_PageObjects.Billing_MyBag_LK,"Billing My Bag Link");
			String MyBagTitle= genericMethods.getText(driver, myBag_PageObjects.MYBag_Title_LB);	
			if(MyBagTitle.equalsIgnoreCase("My Bag")){
				ATUReports.add("Verify My Bag Page is displayed", " My Bag page should be dispalyed", "My Bag page is dispalyed", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify My Bag Page is displayed", " My Bag page should be dispalyed", "My Bag page is dispalyed", true,LogAs.FAILED);
				Assert.assertEquals(MyBagTitle, "My Bag");
			}				
		}
			
		public void BillingCountryCheck(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			myBagCheckoutReusable=new Desk_MYBagCheckout_Reusable();	
			checkoutShippingReusable=new Desk_CheckoutShipping_Reusable();
			checkoutShippingReusable=new Desk_CheckoutShipping_Reusable();		
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			
			//-------------------------Shipping Page-----------------------------	
			myBagCheckoutReusable.SelectUnitedStatesGuestUser(driver);
			checkoutShippingReusable.startFillingShippingForm(driver, firstName, lastName, address1, address2, city, State, zipCode, Phone);
			String BillingCountry=genericMethods.getText(driver, billing_PageObjects.Billing_Country_LB);
			if (BillingCountry.equalsIgnoreCase("Country :   United States")) {
				ATUReports.add("Verify Country is United States", "Country should be United States", "Country is United States ", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Country is United States", "Country should be United States", "Country is not United States ", true,LogAs.FAILED);
				Assert.assertEquals(isPaymentDDPresent, "notpresent");
			}			
		
			genericMethods.Click(driver, billing_PageObjects.Billing_MyBag_LK,"Billing My Bag Link");	
			myBagCheckoutReusable.SelectInternationalCountry(driver, "India");	
			checkoutShippingReusable.startFillingShippingForm(driver, firstName, lastName, address1, address2, city, State, zipCode, Phone);
			BillingCountry=genericMethods.getText(driver, billing_PageObjects.Billing_Country_LB);
			if (BillingCountry.equalsIgnoreCase("Country :   India")) {
				ATUReports.add("Verify Country is India", "Country should be India", "Country is India", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Country is India", "Country should be India", "Country is not India", true,LogAs.FAILED);
				Assert.assertEquals(BillingCountry, "India");
			}		
		}
	 
	 public void BillingNoPaymentSaved(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);		
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
		genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");		
		driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
		 
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"",cardNo);
		if(!cardType.equalsIgnoreCase("BN")){
			org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
			MonthDropdown.selectByVisibleText("April");
			org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
			YearDropdown.selectByVisibleText("2016");		
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");			
		}else{
			System.out.println("");
		}
		
	//Start Filling Billing Address
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
		
		isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
		if (isStateDDPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
			genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");	
		}else{
			ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
		}
		
		String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");		
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
		
		xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Checkout')]")));
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
		}
		GenericMethods.Waitformilliseconds(6000);
		String BillingAddressAdded,BillingAddress;
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
	
 BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";
		
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded)){
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddressAdded,BillingAddress);//actual, expected
		}	
		
	 }
*/	 
	 


//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Saved billing method.$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$/////////
	 
	 public void BillingPaymentSaved(WebDriver driver, String category, String SubCategory, String firstName, String lastName, String address1, String address2, String city, String zipCode, String Phone, String State, String AddressName, String cardType, String cardNo, String cardCVV, String Email, String promo, String GC,String cardHolderName) {
		  //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		  billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		  orderReview_PageObject = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		  genericMethods = new GenericMethods();
		  xplicitWait = new WebDriverWait(driver, 30);
		  GenericMethods.Waitformilliseconds(2000);

		  String FirstNameDisp = driver.findElement(By.id("atg_store_firstNameInput")).getAttribute("value");
		  String CardHolderNameDisp = driver.findElement(By.id("atg_store_paymentInfoAddNewCardHolder")).getAttribute("value");
		  String CityDisp = driver.findElement(By.id("atg_store_localityInput")).getAttribute("value");
		  String ZipDisp = driver.findElement(By.id("atg_store_postalCodeInput")).getAttribute("value");

		  System.out.println(CardHolderNameDisp);
		  System.out.println(FirstNameDisp);
		  System.out.println(CityDisp);
		  System.out.println(ZipDisp);

		  if (CardHolderNameDisp.equalsIgnoreCase(cardHolderName) && FirstNameDisp.equalsIgnoreCase(firstName) && CityDisp.equalsIgnoreCase(city) && ZipDisp.equalsIgnoreCase(zipCode)) {
		   ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is prepopulated", true, LogAs.PASSED);
		  } else {
		   ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is not prepopulated", true, LogAs.FAILED);
		   Assert.assertEquals(CardHolderNameDisp, cardHolderName);
		   Assert.assertEquals(FirstNameDisp, firstName);
		   Assert.assertEquals(CityDisp, city);
		   Assert.assertEquals(ZipDisp, zipCode);
		  }

		  String StateCode = (billing_PageObjects.billing_Address_State_DD).getAttribute("value");
		  System.out.println(StateCode);
		  genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardCardCVV_TB, cardCVV, "cardCVV");
		  genericMethods.Click(driver, billing_PageObjects.billing_Continue_BTN, "continue_button");


		  String BillingAddressAdded, BillingAddress;
		  String expectedText="summarybox";
		  BillingAddressAdded=driver.findElement(By.cssSelector("div[class*='"+expectedText+"']")).getText();
		  System.out.println("Newly added"+BillingAddressAdded);

		  BillingAddress = firstName + " " + lastName + "\n" + address1 + "\n" + address2 + "\n" + city + ", " + StateCode + " " + zipCode + "\nUnited States";
		  System.out.println("On billling page"+BillingAddress);
		  if (BillingAddress.equalsIgnoreCase(BillingAddressAdded)) {
		   ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true, LogAs.PASSED);
		  } else {
		   ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true, LogAs.FAILED);
		   Assert.assertEquals(BillingAddress, BillingAddressAdded);
		  }

		 }	 
	 /*
	 
	 
	 public void BillingTwoPaymentSaved(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		//ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		
		myAccountPaymentReusable= new Desk_MyAccountPayment_Reusable();
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
		if (isPaymentDDPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
				Assert.assertEquals(isPaymentDDPresent, "present");
			}			
		
		String CardTypeDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_type")).getAttribute("value");
		String FirstNameDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).getAttribute("value");
		String CityDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_city")).getAttribute("value");
		String ZipDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_zip")).getAttribute("value");
		
		if(CardTypeDisp.equalsIgnoreCase(cardType)&&FirstNameDisp.equalsIgnoreCase(firstName)&&CityDisp.equalsIgnoreCase(city)&&ZipDisp.equalsIgnoreCase(zipCode)){
			ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is prepopulated", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is not prepopulated", true,LogAs.FAILED);
			Assert.assertEquals(CardTypeDisp, cardType);
			Assert.assertEquals(FirstNameDisp, firstName);
			Assert.assertEquals(CityDisp, city);
			Assert.assertEquals(ZipDisp, zipCode);
		}		
		
	genericMethods.Click(driver,  billing_PageObjects.Billing_payment_DD,"payment_DD");
	genericMethods.Click(driver, billing_PageObjects.Billing_SecondOption_DD,"Secondpayment_DD");
		
	//	genericMethods.SendKeysXTimeswithLocator(driver, billing_PageObjects.Billing_payment_DD, KeyEvent.VK_DOWN, 1);
		GenericMethods.Waitformilliseconds(10000);		
		CardTypeDisp=	driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_type")).getAttribute("value");
		FirstNameDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).getAttribute("value");
		CityDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_city")).getAttribute("value");
		ZipDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_zip")).getAttribute("value");
		
		if(CardTypeDisp.equalsIgnoreCase(myAccountPaymentReusable.CType)&&FirstNameDisp.equalsIgnoreCase(myAccountPaymentReusable.CHName)&&CityDisp.equalsIgnoreCase(myAccountPaymentReusable.PCity)&&ZipDisp.equalsIgnoreCase(myAccountPaymentReusable.PZip)){
			ATUReports.add("Verify Selected myAccountPaymentReusable and Billing Address gets populated", "Selected myAccountPaymentReusable and Billing Address should be prepopulated","Selected myAccountPaymentReusable and Billing Address is prepopulated", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Selected myAccountPaymentReusable and Billing Address gets populated", "Selected myAccountPaymentReusable and Billing Address should be prepopulated","Selected myAccountPaymentReusable and Billing Address is not prepopulated", true,LogAs.FAILED);
			Assert.assertEquals(CardTypeDisp, myAccountPaymentReusable.CType);
			Assert.assertEquals(FirstNameDisp,myAccountPaymentReusable.CHName);
			Assert.assertEquals(CityDisp, myAccountPaymentReusable.PCity);
			Assert.assertEquals(ZipDisp, myAccountPaymentReusable.PZip);
		}				
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
		
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("notpresent")) {

			ATUReports.add("Verify QAS page does not trigger", "QAS page should not trigger when saved myAccountPaymentReusable details is used ", "QAS page is not triggerd when saved myAccountPaymentReusable details is used ", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify QAS page does not trigger", "QAS page should not trigger when saved myAccountPaymentReusable details is used ", "QAS page is triggerd when saved myAccountPaymentReusable details is used ", true,LogAs.FAILED);
			Assert.assertEquals(isQASPresent, "notpresent");
		}			
	
		String BillingAddressAdded,BillingAddress;
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);

		BillingAddress=myAccountPaymentReusable.FName+" "+myAccountPaymentReusable.LName+"\n"+myAccountPaymentReusable.PAdd1+"\n"+myAccountPaymentReusable.PAdd2+"\n"+myAccountPaymentReusable.PCity+", "+myAccountPaymentReusable.PState+" "+myAccountPaymentReusable.PZip+"\nUnited States";
		
if(BillingAddress.equalsIgnoreCase(BillingAddressAdded))
{
	ATUReports.add("Verify Selected Card Billing Address is Added in Order Review page", " Selected Card Billing Address should be Added in Order Review page", " Selected Card Billing Address is Added in Order Review page", true,LogAs.PASSED);
}
else
{
	ATUReports.add("Verify Selected Card Billing Address is Added in Order Review page", " Selected Card Billing Address should be Added in Order Review page", " Selected Card Billing Address is not Added in Order Review page", true,LogAs.FAILED);
	Assert.assertEquals(BillingAddress, BillingAddressAdded);
}	
		
	 }
	 
	 public void AddNewPaymentLastOption(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
		if (isPaymentDDPresent.equalsIgnoreCase("present")) 
			{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
			}
			else
			{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
				Assert.assertEquals(isPaymentDDPresent, "present");
			}			
		
		String CardTypeDisp=	driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_type")).getAttribute("value");
		String FirstNameDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).getAttribute("value");
		String CityDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_city")).getAttribute("value");
		String ZipDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_zip")).getAttribute("value");
		
		if(CardTypeDisp.equalsIgnoreCase(cardType)&&FirstNameDisp.equalsIgnoreCase(firstName)&&CityDisp.equalsIgnoreCase(city)&&ZipDisp.equalsIgnoreCase(Email))
		{
			ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is prepopulated", true,LogAs.PASSED);
		}
		else
		{
			ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is not prepopulated", true,LogAs.FAILED);
			Assert.assertEquals(CardTypeDisp, cardType);
			Assert.assertEquals(FirstNameDisp, firstName);
			Assert.assertEquals(CityDisp, city);
			Assert.assertEquals(ZipDisp, zipCode);
		}		
		
	genericMethods.Click(driver,  billing_PageObjects.Billing_payment_DD,"payment_DD");
	genericMethods.Click(driver, billing_PageObjects.Billing_ThirdOption_DD,"Thirdpayment_DD");

		
	//	genericMethods.SendKeysXTimeswithLocator(driver, billing_PageObjects.Billing_payment_DD, KeyEvent.VK_DOWN, 1);
		genericMethods.Waitformilliseconds(5000);
			

		String 	PaymentDropdown=	driver.findElement(By.id("creditCardList")).getAttribute("value");
		
		if(PaymentDropdown.equalsIgnoreCase("add-new-creditcard"))
		{
			ATUReports.add("Verify Add a New myAccountPaymentReusable Option is selected", " Add a New myAccountPaymentReusable Option should be selected","Add a New myAccountPaymentReusable Option is selected", true,LogAs.PASSED);
		}
		else
		{
			ATUReports.add("Verify Add a New myAccountPaymentReusable Option is selected", " Add a New myAccountPaymentReusable Option should be selected","Add a New myAccountPaymentReusable Option is not selected", true,LogAs.FAILED);
			Assert.assertEquals(PaymentDropdown, "add-new-creditcard");
		
		}		
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
		genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
		//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
		cardType="Amex";
		driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
		
		VISA: 				Visa
		Barneys Card: 		BN
		American Express: 	Amex
		Master Card: 		MasterCard
		Discover Card: 		Discover
		 
		cardNo="373953192351004";
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
		if(!cardType.equalsIgnoreCase("BN")){
			
			
			org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
			MonthDropdown.selectByVisibleText("April");
			org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
			YearDropdown.selectByVisibleText("2016");
			
			
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpMonth_DD);
			genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 3);
			genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);			
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpYear_DD);
			genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 1);
			genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);	
					
			cardCVV="1234";
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
			
		}else{
			System.out.println("");
		}
		
		//genericMethods.Click(driver, billing_PageObjects.billing_Address_SameAsShipping_CB);
		
	//Start Filling Billing Address
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
		
		isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
		if (isStateDDPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
			//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DD);
			genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");
			//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DDOption);	
		}else{
			ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
		}
		
		String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
		System.out.println(StateCode);
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"456","zipCode");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");
	if (promo.equalsIgnoreCase("yes")) {
			addPromo(driver);
		}
		
		
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
		

		
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
		}
		GenericMethods.Waitformilliseconds(2000);
	
		String BillingAddressAdded,BillingAddress;
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
				
		
		
		
		BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";
		
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded))
		{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}
		else
		{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddress, BillingAddressAdded);
		}	
		
		
	 }
	 
	 public void BillingSameAsShipping(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
		if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) 
			{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
			}
			else
			{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
				Assert.assertEquals(isPaymentDDPresent, "notpresent");
			}			
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
		genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
		//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
		
		driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
		
		VISA: 				Visa
		Barneys Card: 		BN
		American Express: 	Amex
		Master Card: 		MasterCard
		Discover Card: 		Discover
		 
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
		if(!cardType.equalsIgnoreCase("BN")){
			
			org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
			MonthDropdown.selectByVisibleText("April");
			org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
			YearDropdown.selectByVisibleText("2016");
			
			
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpMonth_DD);
			genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 3);
			genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);			
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpYear_DD);
			genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 1);
			genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);	
					
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
			
		}else{
			System.out.println("");
		}
		
		//genericMethods.Click(driver, billing_PageObjects.billing_Address_SameAsShipping_CB);
		
	//Start Filling Billing Address
		
		genericMethods.Click(driver, billing_PageObjects.billing_Address_SameAsShipping_CB,"checkbox");
		
		String BillingFirstName,BillingLastName,BillingAddress1,BillingAddress2,BillingCity,BillingState,BillingZip,BillingPhone;
		BillingFirstName=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_firstName")).getAttribute("value");
		BillingLastName=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_lastName")).getAttribute("value");
		BillingAddress1=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_address1")).getAttribute("value");
		BillingAddress2=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_address2")).getAttribute("value");
		BillingCity=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_city")).getAttribute("value");
		BillingState=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_states_state")).getAttribute("value");
		BillingZip=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_zip")).getAttribute("value");
		BillingPhone=	driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_phone")).getAttribute("value");
		
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");

		if(BillingFirstName.equalsIgnoreCase(firstName)&&BillingLastName.equalsIgnoreCase(lastName)&&BillingAddress1.equalsIgnoreCase(address1)&&BillingAddress2.equalsIgnoreCase(address2)&&BillingCity.equalsIgnoreCase(city)&&BillingState.equalsIgnoreCase("NY")&&BillingPhone.equalsIgnoreCase(Phone)&&BillingZip.equalsIgnoreCase(zipCode))
		{
			ATUReports.add("Verify Billing Address is same as shipping address", " Billing Address should be same as shipping address", " Billing Address is same as shipping address", true,LogAs.PASSED);
		}
		else
		{
			ATUReports.add("Verify Billing Address is same as shipping address", " Billing Address should be same as shipping address", " Billing Address is same as shipping address", true,LogAs.FAILED);
			Assert.assertEquals(BillingFirstName, firstName);
			Assert.assertEquals(BillingLastName, lastName);
			Assert.assertEquals(BillingAddress1, address1);
			Assert.assertEquals(BillingAddress2, address2);
			Assert.assertEquals(BillingCity, city);
			Assert.assertEquals(BillingState, "NY");
			Assert.assertEquals(BillingPhone, Phone);
			Assert.assertEquals(BillingZip, zipCode);
		}
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS page should not trigger", " QAS page should not be triggered", " QAS page is triggered", true,LogAs.FAILED);
			Assert.assertEquals(isQASPresent, "present");	
		}
		else
		{
			ATUReports.add("Verify QAS page should not trigger", " QAS page should not be triggered", " QAS page is not triggered", true,LogAs.PASSED);
		}
	
		String BillingAddressAdded,BillingAddress;
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
				
		
		BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+"NY"+" "+zipCode+"\nUnited States";
		
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded))
		{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}
		else
		{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddress, BillingAddressAdded);
		}	
		
	 }
	 
	 public void BillingPromoAdd(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
		if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) 
			{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
			}
			else
			{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
				Assert.assertEquals(isPaymentDDPresent, "notpresent");
			}			
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
		genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
		//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
		
		driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
		
		VISA: 				Visa
		Barneys Card: 		BN
		American Express: 	Amex
		Master Card: 		MasterCard
		Discover Card: 		Discover
		 
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
		if(!cardType.equalsIgnoreCase("BN")){
			
			
			org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
			MonthDropdown.selectByVisibleText("April");
			org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
			YearDropdown.selectByVisibleText("2016");
			
			
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpMonth_DD);
			genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 3);
			genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);			
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpYear_DD);
			genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 1);
			genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);	
					
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
			
		}else{
			System.out.println("");
		}
		
		//genericMethods.Click(driver, billing_PageObjects.billing_Address_SameAsShipping_CB);
		
	//Start Filling Billing Address
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
		
		isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
		if (isStateDDPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
			//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DD);
			genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");
			//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DDOption);	
		}else{
			ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
		}
		
		String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
		System.out.println(StateCode);
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");
	if (promo.equalsIgnoreCase("yes")) {
			addPromo(driver);
		}
		
		String BillingItemPrice=genericMethods.getText(driver, billing_PageObjects.BillingItemPrice_LB);
		addPromo(driver,genericMethods);
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
		

		
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
		}
	
		
GenericMethods.Waitformilliseconds(2000);	
String OrderReviewSubTotal=genericMethods.getText(driver, orderReview_PageObject.BillingPage_SubTotal_LB);

BillingItemPrice=giveDollerValue(BillingItemPrice);
Double	BillingpriceInDouble=Double.parseDouble(BillingItemPrice);		
BillingpriceInDouble=round(BillingpriceInDouble, 2);
System.out.println(BillingpriceInDouble);

OrderReviewSubTotal=giveDollerValue(OrderReviewSubTotal);
Double	OrderReviewSubTotalInDouble=Double.parseDouble(OrderReviewSubTotal);		
OrderReviewSubTotalInDouble=round(OrderReviewSubTotalInDouble, 2);
System.out.println(OrderReviewSubTotalInDouble);

Double Cal,TempCal;
TempCal=(BillingpriceInDouble*0.35);
Cal= (BillingpriceInDouble-TempCal);
Cal=round(Cal,2);

if(Cal==OrderReviewSubTotalInDouble)
{
	ATUReports.add("Verify price is reduced by 35% after applying gift card", "price should be reduced by 35% after applying gift card", "price is reduced by 35% after applying gift card", true,LogAs.PASSED);
}
else
{
	ATUReports.add("Verify price is reduced by 35% after applying gift card", "price should be reduced by 35% after applying gift card", "price is not reduced by 35% after applying gift card", true,LogAs.FAILED);
	Assert.assertEquals(Cal, OrderReviewSubTotalInDouble);
	
}	
		
	 
	 }
	 
	 public void BillingPromoDelete(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			myBag_PageObjects=PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
			if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) 
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
				}
				else
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
					Assert.assertEquals(isPaymentDDPresent, "notpresent");
				}			
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
			//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
			
			driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
			
			VISA: 				Visa
			Barneys Card: 		BN
			American Express: 	Amex
			Master Card: 		MasterCard
			Discover Card: 		Discover
			 
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
			if(!cardType.equalsIgnoreCase("BN")){								
				org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
				MonthDropdown.selectByVisibleText("June");
				org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
				YearDropdown.selectByVisibleText("2016");	
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");				
			}else{
				System.out.println("");
			}
		//Start Filling Billing Address
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
			
			isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
			if (isStateDDPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
				genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");	
			}else{
				ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
			}
			
			String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");

			String BillingItemPrice=genericMethods.getText(driver, billing_PageObjects.BillingItemPrice_LB);
			addPromo(driver,genericMethods);
			genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
			isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
			if (isQASPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
				driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
			}
					
		GenericMethods.Waitformilliseconds(2000);
		String OrderReviewSubTotal=genericMethods.getText(driver, orderReview_PageObject.BillingPage_SubTotal_LB);
	
		BillingItemPrice=giveDollerValue(BillingItemPrice);
		Double	BillingpriceInDouble=Double.parseDouble(BillingItemPrice);		
		BillingpriceInDouble=round(BillingpriceInDouble, 2);
	
		OrderReviewSubTotal=giveDollerValue(OrderReviewSubTotal);
		Double	OrderReviewSubTotalInDouble=Double.parseDouble(OrderReviewSubTotal);		
		OrderReviewSubTotalInDouble=round(OrderReviewSubTotalInDouble, 2);
	
		Double convertedBillingPagePriceAfterPromoCode;
		String BillingPagePrice,ReviewPageOrderTotal;
		
		convertedBillingPagePriceAfterPromoCode= (BillingpriceInDouble-BillingpriceInDouble*0.35);
		convertedBillingPagePriceAfterPromoCode=round(convertedBillingPagePriceAfterPromoCode,2);
		
		BillingPagePrice=convertedBillingPagePriceAfterPromoCode.toString();
		ReviewPageOrderTotal=OrderReviewSubTotalInDouble.toString();
		if(ReviewPageOrderTotal.equalsIgnoreCase(BillingPagePrice)){
			ATUReports.add("Verify price is reduced by 35% after applying gift card", "price should be reduced by 35% after applying gift card", "price is reduced by 35% after applying gift card", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify price is reduced by 35% after applying gift card", "price should be reduced by 35% after applying gift card", "price is not reduced by 35% after applying gift card", true,LogAs.FAILED);
			Assert.assertEquals(BillingPagePrice, ReviewPageOrderTotal);		
		}	

		genericMethods.Click(driver, orderReview_PageObject.Promo_Edit_LK,"Promo_Edit_LK");
		genericMethods.Click(driver, myBag_PageObjects.MYBag_PromoDelete_LK,"MYBag_PromoDelete_LK");
		GenericMethods.Waitformilliseconds(5000);
		String MyBagSubtotalPrice= genericMethods.getText(driver, myBag_PageObjects.MYBag_SubTotal_LB);
		
		MyBagSubtotalPrice=giveDollerValue(MyBagSubtotalPrice);
		Double	MyBagSubtotalPriceInDouble=Double.parseDouble(MyBagSubtotalPrice);		
		MyBagSubtotalPriceInDouble=round(MyBagSubtotalPriceInDouble, 2);
		System.out.println(MyBagSubtotalPriceInDouble);
		
		if((MyBagSubtotalPriceInDouble.toString()).equalsIgnoreCase(BillingpriceInDouble.toString())){
			ATUReports.add("Verify promo is removed and price is adjusted", "promo should be removed and price should be adjusted", "promo is removed and price is adjusted", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify promo is removed and price is adjusted", "promo should be removed and price should be adjusted", "promo is not removed and price is not adjusted", true,LogAs.FAILED);
			Assert.assertEquals(MyBagSubtotalPriceInDouble, BillingpriceInDouble);			
		}
		
	 }
	 
	 public void BillingGCAdd(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			myBag_PageObjects=PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
			if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) 
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
				}
				else
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
					Assert.assertEquals(isPaymentDDPresent, "notpresent");
				}			
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
			//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
			
			driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
			
			VISA: 				Visa
			Barneys Card: 		BN
			American Express: 	Amex
			Master Card: 		MasterCard
			Discover Card: 		Discover
			 
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
			if(!cardType.equalsIgnoreCase("BN")){
				
				
				org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
				MonthDropdown.selectByVisibleText("June");
				org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
				YearDropdown.selectByVisibleText("2016");		
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");				
			}else{
				System.out.println("");
			}
		//Start Filling Billing Address
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
			
			isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
			if (isStateDDPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
				//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DD);
				genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");
				//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DDOption);	
			}else{
				ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
			}
			
			String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
			System.out.println(StateCode);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");
	
			
			addGC(driver,genericMethods);
			
			genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
			xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Checkout')]")));
			isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
			if (isQASPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
				driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
			}
		
			String OrderReviewGCApplied=genericMethods.isElementPresent(driver,"//div[@id='primary']/div[1]/div[5]/div/div" );
			if (OrderReviewGCApplied.equalsIgnoreCase("present")) {
				ATUReports.add("Verify Gift Card Applied on Order review Page", "Gift Card should be applied on Order Review Page", "Gift Card is applied on Order Review Page", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Gift Card Applied on Order review Page", "Gift Card should be applied on Order Review Page", "Gift Card is not applied on Order Review Page", true,LogAs.FAILED);
				Assert.assertEquals(OrderReviewGCApplied, "present");
			}			
	 }
	 
	 public void BillingGCDelete(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			myBag_PageObjects=PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
			if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) {
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
				}else{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
					Assert.assertEquals(isPaymentDDPresent, "notpresent");
				}			
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
			
			driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
			
			VISA: 				Visa
			Barneys Card: 		BN
			American Express: 	Amex
			Master Card: 		MasterCard
			Discover Card: 		Discover
			 
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
			if(!cardType.equalsIgnoreCase("BN")){
				org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
				MonthDropdown.selectByVisibleText("June");
				org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
				YearDropdown.selectByVisibleText("2016");	
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
				
			}else{
				System.out.println("");
			}
			
		//Start Filling Billing Address
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
			
			isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
			if (isStateDDPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
				genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");	
			}else{
				ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
			}
			
			String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
			System.out.println(StateCode);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");
			addGC(driver,genericMethods);
			
			GenericMethods.Waitformilliseconds(2000);
			xplicitWait.until(ExpectedConditions.visibilityOf(billing_PageObjects.BillingGCAppliedRemove_LK));
			genericMethods.Click(driver, billing_PageObjects.BillingGCAppliedRemove_LK,"");
		
			GenericMethods.Waitformilliseconds(4000);
			String YourOrderGC=genericMethods.isElementPresent(driver,"//table[@class='order-totals-table']/tbody/tr[@class='gift-card']/td[contains(text(),'Gift Card')]" );
			if (YourOrderGC.equalsIgnoreCase("notpresent")) {
				ATUReports.add("Verify Gift Card is removed", "Gift Card should be removed", "Gift Card is removed", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Gift Card is removed", "Gift Card should be removed", "Gift Card is not removed", true,LogAs.FAILED);
				Assert.assertEquals(YourOrderGC, "present");
			}				
	 }
	 
		public void SelectInternational(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			myBag_PageObjects=PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			
		
			genericMethods.selectDropdown(driver, myBag_PageObjects.MYBag_Country_DD, "India","Country Dropdown");
			
			genericMethods.Click(driver, myBag_PageObjects.MYBag_CHECKOUT_BTN,"MYbag_checkoutbutton");
			
			
			checkoutShippingReusable=new Desk_CheckoutShipping_Reusable();
			address1="Sentinal Building";
			address2="Hiranandani";
			city="Mumbai";
			zipCode="400072";
			Phone="9967854576";
			checkoutShippingReusable.startFillingShippingForm(driver,firstName,lastName,address1,address2,city,zipCode,Phone);
	
		}

		 public void InternationalPromo(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			 
				billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
				orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
				myBag_PageObjects=PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
				genericMethods =  new GenericMethods();
				xplicitWait = new WebDriverWait(driver, 30);
				
				address1="Sentinal Building";
				address2="Hiranandani";
				city="Mumbai";
				zipCode="400072";
				Phone="9967854576";
				
				isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
				if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) {
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
				}else{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
					Assert.assertEquals(isPaymentDDPresent, "notpresent");
				}		
			
				
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName" );
				genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
				driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
				
				VISA: 				Visa
				Barneys Card: 		BN
				American Express: 	Amex
				Master Card: 		MasterCard
				Discover Card: 		Discover
				 
				
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
				if(!cardType.equalsIgnoreCase("BN")){				
					org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
					MonthDropdown.selectByVisibleText("July");
					org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
					YearDropdown.selectByVisibleText("2016");	
					genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");					
				}else{
					System.out.println("");
				}
				
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
				
				isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
				if (isStateDDPresent.equalsIgnoreCase("present")) {
					ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
					genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");	
				}else{
					ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
					genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"MH","State");
				}
				
				String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
				System.out.println(StateCode);
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");

				String BillingItemPrice=genericMethods.getText(driver, billing_PageObjects.BillingItemPrice_LB);
				String	isPromoPresent=genericMethods.isElementPresent(driver,"//form[@id='dwfrm_billing']/div[1]/div/h3/a" );
				if (isPromoPresent.equalsIgnoreCase("notpresent")) {
						ATUReports.add("Verify if Promo Section is present", "Promo Section should not be present", "Promo Section is not present", true,LogAs.PASSED);
					}else{
						ATUReports.add("Verify if Promo Section is present", "Promo Section should not be present", "Promo Section is present", true,LogAs.FAILED);
						Assert.assertEquals(isPromoPresent, "notpresent");
					}			
				
				
				String	isGiftCardPresent=genericMethods.isElementPresent(driver,"//div[@id='redeem-gift-card']/h3/a" );
				if (isGiftCardPresent.equalsIgnoreCase("notpresent")) {
						ATUReports.add("Verify if  Gift Card Section is present", "Gift Card Section should not be present", "Gift Card Section is not present", true,LogAs.PASSED);
					}else{
						ATUReports.add("Verify if Gift Card Section is present", "Gift Card Section should not be present", "Gift Card Section is present", true,LogAs.FAILED);
						Assert.assertEquals(isPromoPresent, "notpresent");
					}	
				
				genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
				String BillingAddressAdded,BillingAddress;
				BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);										
				BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nIN";				
				if(BillingAddress.equalsIgnoreCase(BillingAddressAdded)){
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
				}else{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
					Assert.assertEquals(BillingAddress, BillingAddressAdded);
				}							
		 }
		 
	  public void BNcardDisableFields(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
				billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
				orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
				myBag_PageObjects=PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
				genericMethods =  new GenericMethods();
				xplicitWait = new WebDriverWait(driver, 30);
	
				String isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
				if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) {
						ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
					}else{
						ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
						Assert.assertEquals(isPaymentDDPresent, "notpresent");
					}		
			
				cardType="BN";
				cardNo="9999999999999995";
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
				genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
				//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
				
				driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
				
				VISA: 				Visa
				Barneys Card: 		BN
				American Express: 	Amex
				Master Card: 		MasterCard
				Discover Card: 		Discover
				 
				
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
				
			Boolean	isMonthDisabled= driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_month")).isEnabled();
			Boolean	isYearDisabled= driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_year")).isEnabled();	
			Boolean	isCVVDisabled= driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_cvn")).isEnabled();	
			
			System.out.println(isMonthDisabled+" "+isYearDisabled+" "+isCVVDisabled);
			if (isMonthDisabled) {
					ATUReports.add("Verify Month dropdown is disabled", " Month dropdown should be disabled", "Month dropdown is not disabled", true,LogAs.FAILED);
					Assert.assertEquals("Month Enabled", "Month Disabled");
				}else{
					ATUReports.add("Verify Month dropdown is disabled", " Month dropdown should be disabled", "Month dropdown is  disabled", true,LogAs.PASSED);
					
				}
				
			if (isYearDisabled) {
				ATUReports.add("Verify Year dropdown is disabled", "Year dropdown should be disabled", "Year dropdown is not disabled", true,LogAs.FAILED);
				Assert.assertEquals("Year Enabled", "Year Disabled");
			}else{
				ATUReports.add("Verify Year dropdown is disabled", " Year dropdown should be disabled", "Year dropdown is  disabled", true,LogAs.PASSED);			
			}
			
			if (isCVVDisabled) {
				ATUReports.add("Verify CVV Text box is disabled", " CVV Text box should be disabled", "CVV Text box is not disabled", true,LogAs.FAILED);
				Assert.assertEquals("CVV Text box Enabled", "CVV Text box Disabled");
			}else{
				ATUReports.add("Verify CVV Text box is disabled", " CVV Text box should be disabled", "CVV Text box is  disabled", true,LogAs.PASSED);
				
			}
			
				
				
		 }
		 
		 public void CVVValidation(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
			 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
				billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
				orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
				genericMethods =  new GenericMethods();
				xplicitWait = new WebDriverWait(driver, 30);
				
				isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
				if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) 
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
				}
				else
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
					Assert.assertEquals(isPaymentDDPresent, "notpresent");
				}			
				
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
				genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
				//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
				
				driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
			
				
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
				if(!cardType.equalsIgnoreCase("BN")){
					
					
					org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
					MonthDropdown.selectByVisibleText("June");
					org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
					YearDropdown.selectByVisibleText("2016");
					cardCVV="1234";
					genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
					
				}else{
					System.out.println("");
				}
				
			
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
				
				isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
				if (isStateDDPresent.equalsIgnoreCase("present")) {
					ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
					//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DD);
					genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");
					//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DDOption);	
				}else{
					ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
					genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
				}
				
				String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
				System.out.println(StateCode);
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email" );
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");
			if (promo.equalsIgnoreCase("yes")) {
					addPromo(driver);
				}
				
				
				genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
				
				isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
				if (isQASPresent.equalsIgnoreCase("present")) {
					ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
					driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
				}
				
				String CVVErrormsg = genericMethods.getText(driver,billing_PageObjects.Billing_CVVError_LB);
				if (CVVErrormsg.contains("Invalid Security Code"))
				   {
				    ATUReports.add("Verify Error message for invalid cvv 1234","  ", "Error Message should be =  Invalid Security Code","Error message for Invalid CVV is : " +(genericMethods.getText(driver,billing_PageObjects.Billing_CVVError_LB)),  true, LogAs.PASSED);
				  //  Assert.assertEquals((genericMethods.getText(driver,shipp.ErrorMsgfirstname_gettext)), "Please provide a first name.");
				
				   }else
				   { 
					   ATUReports.add("Verify Error message for invalid cvv 1234","  ", "Error Message should be =  Invalid Security Code","Error message for Invalid CVV is : " +(genericMethods.getText(driver,billing_PageObjects.Billing_CVVError_LB)),  true, LogAs.FAILED);
					  // Assert.assertEquals((genericMethods.getText(driver,shipp.ErrorMsgfirstname_gettext)), "Please provide a first name.");
					 
				   }
				
				cardCVV="12";
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");

				genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");

				isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
				if (isQASPresent.equalsIgnoreCase("present")) {
						ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
						driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
					}
				 	CVVErrormsg = genericMethods.getText(driver,billing_PageObjects.Billing_CVVError_LB);
				if (CVVErrormsg.contains("Invalid Security Code"))
				   {
				    ATUReports.add("Verify Error message for invalid cvv 1234","  ", "Error Message should be =  Invalid Security Code","Error message for Invalid CVV is : " +(genericMethods.getText(driver,billing_PageObjects.Billing_CVVError_LB)),  true, LogAs.PASSED);
				  //  Assert.assertEquals((genericMethods.getText(driver,shipp.ErrorMsgfirstname_gettext)), "Please provide a first name.");
				
				   }else
				   { 
					   ATUReports.add("Verify Error message for invalid cvv 1234","  ", "Error Message should be =  Invalid Security Code","Error message for Invalid CVV is : " +(genericMethods.getText(driver,billing_PageObjects.Billing_CVVError_LB)),  true, LogAs.FAILED);
					  // Assert.assertEquals((genericMethods.getText(driver,shipp.ErrorMsgfirstname_gettext)), "Please provide a first name.");
					 
				   }
				
				cardCVV="123";
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
			
				genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
				
				isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
				if (isQASPresent.equalsIgnoreCase("present")) {
					ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
					driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
				}			
				GenericMethods.Waitformilliseconds(2000);
				String BillingAddressAdded,BillingAddress;
				BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
																		
		 BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";				
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded))
		{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}
		else
		{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddress, BillingAddressAdded);
		}									
		 }
		 	 
		 public void CardImagesAndCVVTooltipIconVerification(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
				billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
				orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
				myBag_PageObjects=PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
				genericMethods =  new GenericMethods();
				xplicitWait = new WebDriverWait(driver, 30);
				String isCardImagesPresent=genericMethods.isElementPresent(driver,"//div[@id='myAccountPaymentReusable-method-fields']/div[3]/div[2]/div/img" );
				if (isCardImagesPresent.equalsIgnoreCase("present")) 
					{
						ATUReports.add("Verify Card Images are Present", "Card Images should be Present", "Card Images are Present", true,LogAs.PASSED);
					}
					else
					{
						ATUReports.add("Verify Card Images are Present", "Card Images should be Present", "Card Images are not Present", true,LogAs.FAILED);
						Assert.assertEquals(isCardImagesPresent, "present");
					}		
			
				
				String isCVVTooltipPresent=genericMethods.isElementPresent(driver,"//div[@id='ym']/div[3]/div[2]/a" );
				if (isCVVTooltipPresent.equalsIgnoreCase("present")) 
					{
						ATUReports.add("Verify CVV Tool tip are Present", "CVV Tool tip should be Present", "CVV Tool tip are Present", true,LogAs.PASSED);
					}
					else
					{
						ATUReports.add("Verify CVV Tool tip are Present", "CVV Tool tip should be Present", "CVV Tool tip are not Present", true,LogAs.FAILED);
						Assert.assertEquals(isCVVTooltipPresent, "present");
					}		
			
				genericMethods.mouseHover(driver, billing_PageObjects.Billing_CVVToolTip_Icon,"CVVToolTip_Icon");
				
		 }
		 
		 public void EditBillingAddress(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
			if (isPaymentDDPresent.equalsIgnoreCase("notpresent")) 
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
				}
				else
				{
					ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
					Assert.assertEquals(isPaymentDDPresent, "notpresent");
				}			
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
			//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
			
			driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
			
			VISA: 				Visa
			Barneys Card: 		BN
			American Express: 	Amex
			Master Card: 		MasterCard
			Discover Card: 		Discover
			 
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
			if(!cardType.equalsIgnoreCase("BN")){
				
				
				org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
				MonthDropdown.selectByVisibleText("April");
				org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
				YearDropdown.selectByVisibleText("2016");
				
				
				genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpMonth_DD);
				genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 3);
				genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);			
				genericMethods.Click(driver, billing_PageObjects.Billing_paymentExpYear_DD);
				genericMethods.SendKeysXTimes(KeyEvent.VK_DOWN, 1);
				genericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);	
						
				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
				
			}else{
				System.out.println("");
			}
			
			//genericMethods.Click(driver, billing_PageObjects.billing_Address_SameAsShipping_CB);
			
		//Start Filling Billing Address
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
			
			isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
			if (isStateDDPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
				//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DD);
				genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");
				//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DDOption);	
			}else{
				ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
			}
			
			String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
			System.out.println(StateCode);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");
		if (promo.equalsIgnoreCase("yes")) {
				addPromo(driver);
			}
			
			
			genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
			

			
			isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
			if (isQASPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
				driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
			}
		GenericMethods.Waitformilliseconds(2000);
			String BillingAddressAdded,BillingAddress;
			BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);		
	 BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";
			
	if(BillingAddress.equalsIgnoreCase(BillingAddressAdded))
	{
		ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
	}
	else
	{
		ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
		Assert.assertEquals(BillingAddress, BillingAddressAdded);
	}	
			genericMethods.Click(driver, orderReview_PageObject.Billing_AddressEdit_LK,"Billing_AddressEdit_LK");
			GenericMethods.Waitformilliseconds(2000);
			String CardTypeDisp=	driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_type")).getAttribute("value");
			String FirstNameDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).getAttribute("value");
			String CityDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_city")).getAttribute("value");
			String ZipDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_zip")).getAttribute("value");
			String CVVDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_cvn")).getAttribute("value");
			if(CardTypeDisp.equalsIgnoreCase(cardType)&&FirstNameDisp.equalsIgnoreCase(firstName)&&CityDisp.equalsIgnoreCase(city)&&ZipDisp.equalsIgnoreCase(Email))
			{
				ATUReports.add("Verify myAccountPaymentReusable and Billing Address is populated", "myAccountPaymentReusable and Billing Address should be populated", "myAccountPaymentReusable and Billing Address is populated", true,LogAs.PASSED);
			}
			else
			{
				ATUReports.add("Verify myAccountPaymentReusable and Billing Address is populated", "myAccountPaymentReusable and Billing Address should be populated", "myAccountPaymentReusable and Billing Address is not populated", true,LogAs.FAILED);
				Assert.assertEquals(CardTypeDisp, cardType);
				Assert.assertEquals(FirstNameDisp, firstName);
				Assert.assertEquals(CityDisp, city);
				Assert.assertEquals(ZipDisp, zipCode);
			}		
			if(CVVDisp.equalsIgnoreCase(""))
			{
				ATUReports.add("Verify CVV is not populated", "CVV should not be populated", "CVV is not populated", true,LogAs.PASSED);
			}
			else
			{
				ATUReports.add("Verify CVV is not populated", "CVV should not be populated", "CVV is populated", true,LogAs.FAILED);
				Assert.assertEquals(CVVDisp, "");
				
			}
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
		
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");			
			genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
		
			GenericMethods.Waitformilliseconds(2000);
			isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
			if (isQASPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
				driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
			}	
			GenericMethods.Waitformilliseconds(2000);			
			BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
			
	 BillingAddress="Jiya"+" "+"Desai"+"\n"+address1+"\n"+"near garden"+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";			
	if(BillingAddress.equalsIgnoreCase(BillingAddressAdded))
	{
		ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
	}
	else
	{
		ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
		Assert.assertEquals(BillingAddress, BillingAddressAdded);
	}	
			
		 }	 
		 
		
		 public void BillingPaymentSavedEdit(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
			 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			GenericMethods.Waitformilliseconds(2000);
			isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
			if (isPaymentDDPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
				Assert.assertEquals(isPaymentDDPresent, "present");
			}			
			
			String CardTypeDisp=	driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_type")).getAttribute("value");
			String FirstNameDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).getAttribute("value");
			String CityDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_city")).getAttribute("value");
			String ZipDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_zip")).getAttribute("value");
			
			if(CardTypeDisp.equalsIgnoreCase(cardType)&&FirstNameDisp.equalsIgnoreCase(firstName)&&CityDisp.equalsIgnoreCase(city)&&ZipDisp.equalsIgnoreCase(zipCode)){
				ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is prepopulated", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Default myAccountPaymentReusable and Billing Address prepopulated", "Default myAccountPaymentReusable and Billing Address should be prepopulated", "Default myAccountPaymentReusable and Billing Address is not prepopulated", true,LogAs.FAILED);
				Assert.assertEquals(CardTypeDisp, cardType);
				Assert.assertEquals(FirstNameDisp, firstName);
				Assert.assertEquals(CityDisp, city);
				Assert.assertEquals(ZipDisp, zipCode);
			}		
			
			String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
			System.out.println(StateCode);
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
			genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");
			GenericMethods.Waitformilliseconds(2000);
			xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Checkout')]")));
			
			isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
			if (isQASPresent.equalsIgnoreCase("notpresent")) {
				ATUReports.add("Verify QAS page does not trigger", "QAS page should not trigger when saved myAccountPaymentReusable details is used ", "QAS page is not triggerd when saved myAccountPaymentReusable details is used ", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify QAS page does not trigger", "QAS page should not trigger when saved myAccountPaymentReusable details is used ", "QAS page is triggerd when saved myAccountPaymentReusable details is used ", true,LogAs.FAILED);
				Assert.assertEquals(isQASPresent, "notpresent");
			}
				
				
		GenericMethods.Waitformilliseconds(2000);
		String BillingAddressAdded,BillingAddress;
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
		BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";
				
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded)){
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddress, BillingAddressAdded);
		}	
				
		genericMethods.Click(driver, orderReview_PageObject.Billing_PaymentEdit_LK,"PaymentEdit_LK");
		GenericMethods.Waitformilliseconds(2000);
		CardTypeDisp=	driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_type")).getAttribute("value");
		 FirstNameDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).getAttribute("value");
		 CityDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_city")).getAttribute("value");
		 ZipDisp=driver.findElement(By.id("dwfrm_billing_AddressAddress_addressFields_zip")).getAttribute("value");
		 String	 CVVDisp=driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_cvn")).getAttribute("value");
		
		 if(CardTypeDisp.equalsIgnoreCase(cardType)&&FirstNameDisp.equalsIgnoreCase(firstName)&&CityDisp.equalsIgnoreCase(city)&&ZipDisp.equalsIgnoreCase(zipCode)){
			ATUReports.add("Verify myAccountPaymentReusable and Billing Address is populated", "myAccountPaymentReusable and Billing Address should be populated", "myAccountPaymentReusable and Billing Address is populated", true,LogAs.PASSED);
		 }else{
			ATUReports.add("Verify myAccountPaymentReusable and Billing Address is populated", "myAccountPaymentReusable and Billing Address should be populated", "myAccountPaymentReusable and Billing Address is not populated", true,LogAs.FAILED);
			Assert.assertEquals(CardTypeDisp, cardType);
			Assert.assertEquals(FirstNameDisp, firstName);
			Assert.assertEquals(CityDisp, city);
			Assert.assertEquals(ZipDisp, zipCode);
		}	
		
		if(CVVDisp.equalsIgnoreCase("")){
			ATUReports.add("Verify CVV is not populated", "CVV should not be populated", "CVV is not populated", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify CVV is not populated", "CVV should not be populated", "CVV is populated", true,LogAs.FAILED);
			Assert.assertEquals(CVVDisp, "");			
		}
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");
		GenericMethods.Waitformilliseconds(2000);
		String isNotificationPresent=genericMethods.isElementPresent(driver,"//header[@id='header']/div/b[contains(text(),'The billing address tied to this myAccountPaymentReusable option will be updated in your account.')]" );
		if (isNotificationPresent.equalsIgnoreCase("present")) {	
			ATUReports.add("Verify Notification is triggered", "Notification should be triggered", "Notification is triggered", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Notification is triggered", "Notification should be triggered", "Notification is not triggered", true,LogAs.FAILED);
			Assert.assertEquals(isNotificationPresent, "notpresent");
		}
		
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");	
		GenericMethods.Waitformilliseconds(4000);
		
		xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Checkout')]")));
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
		}
		GenericMethods.Waitformilliseconds(2000);
		String PaymentCardHolderName=genericMethods.getText(driver, orderReview_PageObject.PaymentCardHolderName_LB);

	
		if(PaymentCardHolderName.equalsIgnoreCase("Jiya Desai")){
			ATUReports.add("Verify myAccountPaymentReusable Details is edited", "myAccountPaymentReusable Details should be edited", "myAccountPaymentReusable Details is edited", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify myAccountPaymentReusable Details is edited", "myAccountPaymentReusable Details should be edited", "myAccountPaymentReusable Details is not edited", true,LogAs.FAILED);
			Assert.assertEquals(PaymentCardHolderName, "Jiya Desai");
		}
		 
		genericMethods.Click(driver, orderReview_PageObject.OrderReview_COmpletePurchase_BTN,"COmpletePurchase_BTN");	
		myAccountPaymentPageObjects = PageFactory.initElements(driver, Desk_BNY_MyAccountPaymentPaegObjects.class);
		homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);	
		genericMethods.Click(driver, homePageObjects.Home_navigationPageObjects_AccountArrow_LK,"");
		genericMethods.Click(driver, homePageObjects.Home_navigationPageObjects_DD_MyAccount_LK,"");
		genericMethods.Click(driver, myAccountPaymentPageObjects.MyAcc_Payment_LK,"");
		genericMethods.mouseHover(driver, myAccountPaymentPageObjects.MyAcc_PayemntFirstBox_LB,"PayemntFirstBox_LB");
		GenericMethods.Waitformilliseconds(2000);
			
		paymentEditLink = driver.findElement(By.xpath("//section[@class='account-options']//span[@class='tools address-edit']/a[1]"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", paymentEditLink);
		GenericMethods.Waitformilliseconds(2000);
		String CHName=	driver.findElement(By.id("dwfrm_paymentinstruments_creditcards_newcreditcard_owner")).getAttribute("value");
		String AddressTwo=	driver.findElement(By.id("dwfrm_profile_address_address2")).getAttribute("value");
		
		if(CHName.equalsIgnoreCase("Jiya Desai")&&AddressTwo.equalsIgnoreCase("near garden")){
			ATUReports.add("Verify myAccountPaymentReusable is edited in My Account",  "myAccountPaymentReusable should be edited in My Account", "myAccountPaymentReusable is edited in My Account", true,LogAs.PASSED);
			Assert.assertEquals(CHName,"Jiya Desai");
		}else{
			ATUReports.add("Verify myAccountPaymentReusable is edited in My Account",  "myAccountPaymentReusable should be edited in My Account", "myAccountPaymentReusable is not edited in My Account", true,LogAs.FAILED);
			Assert.assertEquals(CHName,"Jiya Desai");
			Assert.assertEquals(AddressTwo,"near garden");
		}	 
	 }
		 
 public void EditBillingAndCheckPromoGC(WebDriver driver,String category,String SubCategory,String firstName, String lastName,String address1,String address2,String city,String zipCode,String Phone,String State, String AddressName, String cardType, String cardNo, String cardCVV,String Email,String promo, String GC)  {
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
		if (isPaymentDDPresent.equalsIgnoreCase("notpresent")){
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(isPaymentDDPresent, "notpresent");
		}			
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB, firstName,"myAccountPaymentReusable Firstname");
		genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"myAccountPaymentReusable Card DD");
		
		driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
		
		VISA: 				Visa
		Barneys Card: 		BN
		American Express: 	Amex
		Master Card: 		MasterCard
		Discover Card: 		Discover
		 
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB, cardNo,"Card Number");
		if(!cardType.equalsIgnoreCase("BN")){
			org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
			MonthDropdown.selectByVisibleText("April");
			org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
			YearDropdown.selectByVisibleText("2016");		
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB, cardCVV,"Card CVV");
			
		}else{
			System.out.println("");
		}
		
		//Start Filling Billing Address
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,firstName,"myAccountPaymentReusable Firstname");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,lastName,"myAccountPaymentReusable last name");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,address1,"myAccountPaymentReusable Address 1");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,address2,"myAccountPaymentReusable Address 2");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,city,"myAccountPaymentReusable City");
		
		isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
		if (isStateDDPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
			genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,State,"myAccountPaymentReusable State DD");
		}else{
			ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","myAccountPaymentReusable State DD");
		}
		
		String StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,zipCode,"myAccountPaymentReusable zipcode");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB, Email,"myAccountPaymentReusable Email");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,Phone,"myAccountPaymentReusable phone");
		addGC(driver,genericMethods);
		addPromo(driver,genericMethods);
		
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"myAccountPaymentReusable Continue button");
		xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Checkout')]")));
		
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
		}
		
		GenericMethods.Waitformilliseconds(2000);
		String BillingAddressAdded,BillingAddress;
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
		
		BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";			
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded)){
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddress, BillingAddressAdded);
		}	
	
		genericMethods.Click(driver, orderReview_PageObject.Billing_AddressEdit_LK,"Billing_AddressEdit_LK");
		GenericMethods.Waitformilliseconds(2000);
		String YourOrderGC=genericMethods.isElementPresent(driver,"//table[@class='order-totals-table']/tbody/tr[@class='gift-card']/td[contains(text(),'Gift Card')]" );
		if (YourOrderGC.equalsIgnoreCase("notpresent")) {
			ATUReports.add("Verify Gift Card is removed", "Gift Card should be removed", "Gift Card is removed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Gift Card is removed", "Gift Card should be removed", "Gift Card is not removed", true,LogAs.FAILED);
			Assert.assertEquals(YourOrderGC, "present");
		}			
	
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"rina","firstName");
		genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"cardtype");
		driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
		
		VISA: 				Visa
		Barneys Card: 		BN
		American Express: 	Amex
		Master Card: 		MasterCard
		Discover Card: 		Discover
		 
		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"************1881","cardNo");
		if(!cardType.equalsIgnoreCase("BN")){
			org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
			MonthDropdown.selectByVisibleText("April");
			org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
			YearDropdown.selectByVisibleText("2016");		
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"456","cardCVV");				
		}else{
			System.out.println("");
		}
		
		//Start Filling Billing Address
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"rina","firstName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"bisht","lastName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"abc","address1");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"hgj","address2");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","city");
		
		isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
		if (isStateDDPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
			genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"UNITED STATE","State");
		}else{
			ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
		}
		
		StateCode=(billing_PageObjects.billing_Address_State_DD).getAttribute("value");
		System.out.println(StateCode);
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"100024","zipCode");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@yopmail.com","Email" );
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9876543778","Phone");	
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"continue_button");

		GenericMethods.Waitformilliseconds(2000);
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();		
		}
	
		GenericMethods.Waitformilliseconds(3000);
		BillingAddressAdded=genericMethods.getText(driver, billing_PageObjects.BillingAddressAdded_LB);
	
		BillingAddress=firstName+" "+lastName+"\n"+address1+"\n"+address2+"\n"+city+", "+StateCode+" "+zipCode+"\nUnited States";
		if(BillingAddress.equalsIgnoreCase(BillingAddressAdded)){
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is added", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Billing Address Added", "Billing address should be added", "Billing address is not added", true,LogAs.FAILED);
			Assert.assertEquals(BillingAddress, BillingAddressAdded);
		}

		String	isPromoPresent=genericMethods.isElementPresent(driver,"//table[@id='cart-table']/tfoot/tr/td[3]/div/a" );
		if (isPromoPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify Promo is applied", "Promo should be applied", "Promo is applied", true,LogAs.INFO);			
		}else{
			ATUReports.add("Verify Promo is applied", "Promo should be applied", "Promo is not applied", true,LogAs.INFO);
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Dummy","");
		}
	 }	 
		 
		 
	
	 public void addPromo (WebDriver driver,GenericMethods genericMethods) {
		 billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		 genericMethods.Waitformilliseconds(1000);
		 genericMethods.mouseHover(driver, billing_PageObjects.Billing_PromoSection_LK,"Promo Section link");
		 genericMethods.Click(driver, billing_PageObjects.Billing_PromoSection_LK,"Promo Section link");
		 genericMethods.Click(driver, billing_PageObjects.Billing_Promo_TB,"Promo Codetext box" );
		 genericMethods.inputValue(driver, billing_PageObjects.Billing_Promo_TB, "112256518","Billing Promo code");
		 ATUReports.add("Verify Promo Code section", "Prmocode section should be open", true,LogAs.INFO);
		 genericMethods.Click(driver, billing_PageObjects.Billing_Promo_BTN,"Billing promo Apply button");
		 //GenericMethods.Waitformilliseconds(3000);
	
	 
	 }
	 public void addGC (WebDriver driver,GenericMethods genericMethods) {
		xplicitWait=new WebDriverWait(driver, 15);
		billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		GenericMethods.Waitformilliseconds(1000);
		genericMethods.mouseHover(driver, billing_PageObjects.Billing_GiftCardSection_LK,"Gift Ca");
		genericMethods.Click(driver, billing_PageObjects.Billing_GiftCardSection_LK,"Gift Card Section");
		GenericMethods.Waitformilliseconds(2000);
		genericMethods.Click(driver, billing_PageObjects.Billing_GiftCard_TB,"Gift Card Textbox");
		genericMethods.inputValue(driver, billing_PageObjects.Billing_GiftCard_TB, "15356689000958","Gift Card");
		ATUReports.add("Verify Gift Card section", "Gift Card section should be open", true,LogAs.INFO);
		genericMethods.Click(driver, billing_PageObjects.Billing_GiftCard_BTN,"Gidt Card Apply button");
	
	xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='accordion']/fieldset/div/div/div/div[2]/div[1]/div")));
	String GCAppliedMessage=genericMethods.isElementPresent(driver,"//div[@id='accordion']/fieldset/div/div/div/div[2]/div[1]/div" );
	if (GCAppliedMessage.equalsIgnoreCase("present")) {
			ATUReports.add("Verify Gift Card Applied Message displayed", "Gift Card Applied Message should be displayed", "Gift Card Applied Message is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Gift Card Applied Message displayed", "Gift Card Applied Message should be displayed", "Gift Card Applied Message is not displayed", true,LogAs.FAILED);
			Assert.assertEquals(GCAppliedMessage, "present");
		}			
	
	String GCAppliedDeleteLK=genericMethods.isElementPresent(driver,"//div[@id='gift-card-area']//a[@class='gc-remove']/span[contains(text(),'Remove')]" );
	if (GCAppliedDeleteLK.equalsIgnoreCase("present")){
			ATUReports.add("Verify Gift Card Delete Link displayed", "Gift Card Delete Link should be displayed", "Gift Card Delete Link is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Gift Card Delete Link displayed", "Gift Card Delete Link should be displayed", "Gift Card Delete Link is not displayed", true,LogAs.FAILED);
			Assert.assertEquals(GCAppliedDeleteLK, "present");
		}	
	
	 
	 }
	 
	 public void EditGC (WebDriver driver) {
		 billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		 
		 genericMethods.Waitformilliseconds(1000);
		 genericMethods.mouseHover(driver, billing_PageObjects.Billing_GiftCardSection_LK,"Goft Card Section");
		 genericMethods.Click(driver, billing_PageObjects.Billing_GiftCardSection_LK,"Gift Card section link");
		 genericMethods.Click(driver, billing_PageObjects.Billing_Promo_TB,"Promo code text box");
		 genericMethods.inputValue(driver, billing_PageObjects.Billing_Promo_TB, "112256518","promo Code");
		 ATUReports.add("Verify Promo Code section", "Prmocode section should be open", true,LogAs.INFO);
		 genericMethods.Click(driver, billing_PageObjects.Billing_Promo_BTN,"Apply Promo Button");
		 //GenericMethods.Waitformilliseconds(3000);	 
	 } 
	 
	 public void startFillingBillingForm(WebDriver driver,String Site,String address1,String city,String state,String zip, String cardType,String CardNo,String CVV, String promo,String giftCard){
			
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			orderReview_PageObject=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			qasPageObjects=PageFactory.initElements(driver, Desk_BNY_QASPageObject.class);
			genericMethods =  new GenericMethods();
			GenericMethods.Waitformilliseconds(3000);
			isPaymentDDPresent=genericMethods.isElementPresent(driver,"//select[@id='creditCardList']" );
			if (isPaymentDDPresent.equalsIgnoreCase("present")) {
				if (Site.equalsIgnoreCase("BNY")) {
					genericMethods.selectDropdown(driver, billing_PageObjects.Billing_payment_DD,"1","ADD NEW myAccountPaymentReusable OPTION");
				}else{
					genericMethods.selectDropdown(driver, billing_PageObjects.Billing_payment_DD,"2", "Add New myAccountPaymentReusable Option");
				}
			}
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB, "Govind","CardHolderNameTextBox");
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_DD,"PaymentDropdown");
			//genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption);
			
			driver.findElement(By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']/option[@value='"+cardType+"']")).click();
			
			VISA: 				Visa
			Barneys Card: 		BN
			American Express: 	Amex
			Master Card: 		MasterCard
			Discover Card: 		Discover
			 
			
			genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB, "4000000000000000","CardNumber");
			if(!cardType.equalsIgnoreCase("BN")){				
				
				org.openqa.selenium.support.ui.Select MonthDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpMonth_DD);
				MonthDropdown.selectByVisibleText("June");
				org.openqa.selenium.support.ui.Select YearDropdown = new org.openqa.selenium.support.ui.Select(billing_PageObjects.Billing_paymentExpYear_DD);
				YearDropdown.selectByVisibleText("2016");

				genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"318","CardCVVnumber");
				
			}else{
				System.out.println("");
			}
			
			
		//Start Filling Billing Address
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"Govind","FirstName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"Varma","LastName");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"l1,l2 road", "Address1");
		
			//genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address2_TB,"Madison Avenue");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","CITYNAME_TextBox");
			
			isStateDDPresent=genericMethods.isElementPresent(driver,"//select[@id='dwfrm_billing_AddressAddress_addressFields_states_state']" );
			if (isStateDDPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify State DD", "State Dropdown Should be Present", "State Dropdown is Present", true,LogAs.INFO);
				//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DD);
				genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD,"Select...","Billing_State_Dropdown");
				//genericMethods.Click(driver, billing_PageObjects.billing_Address_State_DDOption);	
			}else{
				ATUReports.add("Verify State DD", "Province Textbox Should be Present", "Province Textbox Should be Present", true,LogAs.INFO);
				genericMethods.inputValue(driver, billing_PageObjects.billing_Address_State_DD,"Select...","Billing_State_Dropdown");
			}
			
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"10024","ZIPCODE_TextBox");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB, "gvarma@barneys.com","EmailID_TextBox");
			genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB, "3333333333","phonenNO_TextBox");
			
			if(Site.equals("BNY"))
			{
				if (promo.equalsIgnoreCase("yes")) {
					addPromo(driver);
				}
				if(!giftCard.equalsIgnoreCase("no")){
					applyGiftCard(driver,giftCard);
				}
			}
				
			genericMethods.Click(driver, billing_PageObjects.Billing_Continue_BTN,"BillingContinue Button");		
			GenericMethods.Waitformilliseconds(3000);
			genericMethods.waitForPageLoaded(driver);
			isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
			GenericMethods.Waitformilliseconds(2000);
			isQASradioButtonPreset=genericMethods.isElementPresent(driver,"//form[@id='dwfrm_addForm']/fieldset/table/tbody/tr/td[1]/input" );
			if (isQASPresent.equalsIgnoreCase("present")) {
				
				if (isQASradioButtonPreset.equals("present")) {				
					genericMethods.Click(driver, qasPageObjects.QAS_SelectfirstSuggestion_RB,"Suggestion_RB");
					ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
					genericMethods.Click(driver, qasPageObjects.QAS_Submit_BTN,"Submit_BTN");
					genericMethods.waitForPageLoaded(driver);
				}else {
					driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();
					GenericMethods.Waitformilliseconds(1000);
					ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
				}
				GenericMethods.Waitformilliseconds(3000);
				genericMethods.waitForPageLoaded(driver);
			}
			
			
		 }
		 
		 public void clickCompletePurchase(WebDriver driver)
		 {
			 //GenericMethods.Waitformilliseconds(2000);	
			 ATUReports.add("Verify Page displayed", "User should be on Order Review Page", "User is on Review Order Page", true,LogAs.INFO);
			 genericMethods.Click(driver, orderReview_PageObject.OrderReview_COmpletePurchase_BTN,"OrderReview_COmpletePurchase_BTN");
			 genericMethods.waitForPageLoaded(driver);
			
		 }
		 
		 public void applyGiftCard (WebDriver driver,String giftCard){
				genericMethods.Click(driver, billing_PageObjects.Billing_GiftCardSection_LK,"GiftCardSection_LK");
				genericMethods.inputValue(driver, billing_PageObjects.Billing_GiftCard_TB, "","giftCard_TB");
				ATUReports.add("Verify Gift section", "Gift Card section should be open", true,LogAs.INFO);
				genericMethods.Click(driver, billing_PageObjects.Billing_GiftCard_BTN,"GiftCard_BTN");
				genericMethods.waitForPageLoaded(driver);
				ATUReports.add("Verify Visibility of Billing Section", true,LogAs.INFO);
		 }
		 
		 public void addPromo (WebDriver driver) {
		genericMethods.Click(driver, billing_PageObjects.Billing_PromoSection_LK,"Billing_PromoSection_LK");
		genericMethods.Click(driver, billing_PageObjects.Billing_Promo_TB,"Billing_Promo_TB");
		genericMethods.inputValue(driver, billing_PageObjects.Billing_Promo_TB,"859833598","Billing_Promo_TB");
		ATUReports.add("Verify Promo Code section", "Prmocode section should be open", true,LogAs.INFO);
		genericMethods.Click(driver, billing_PageObjects.Billing_Promo_BTN,"Billing_Promo_BTN");
		genericMethods.waitForPageLoaded(driver);
		
		 
		 }

	 public void Billingvalidation(WebDriver driver) {
		    genericMethods= new GenericMethods();
			Shipping_PageObjects = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
					
			genericMethods.Click(driver, billing_PageObjects.Billing_paymentCardType_VISA_DDOption,"Visa DropdownOption");
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardHolderName_TB,"lima","CardHolderName TextBox");		
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardNo_TB,"4000000000000000", "CardNumber");
		genericMethods.inputValue(driver, billing_PageObjects.Billing_paymentCardCardCVV_TB,"318","CardCVVnumber");
		
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_FName_TB,"lima","FirstName");	
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_LName_TB,"gomes", "LastName");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Address1_TB,"l1,l2 road", "Address1");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_CITY_TB,"newyork","CITYNAME_TextBox");
		genericMethods.selectDropdown(driver, billing_PageObjects.billing_Address_State_DD, "Select...","Billing_State_Dropdown");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_ZIP_TB,"10024","ZIPCODE_TextBox");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_Email_TB,"abc@gmail.com","EmailID_TextBox");
		genericMethods.inputValue(driver, billing_PageObjects.billing_Address_phone_TB,"9999999999","phonenNO_TextBox");
		
		genericMethods.Click(driver, billing_PageObjects.Billing_Continue1_BTN,"BillingContinue Button");
		
		String Cardholdemessage = genericMethods.getText(driver, Shipping_PageObjects.erromessageCardName_gettext).toUpperCase();
		String CardNumbermessage = genericMethods.getText(driver, Shipping_PageObjects.erromessageCardNumber_gettext).toUpperCase();
		String CVVmessage = genericMethods.getText(driver, Shipping_PageObjects.erromessageforCVV_gettext).toUpperCase();
		String 	firstNameMessage = genericMethods.getText(driver, Shipping_PageObjects.erromessageforfirstName_gettext).toUpperCase();
		String lastNameMessage = genericMethods.getText(driver, Shipping_PageObjects.erromessageforLastName_gettext).toUpperCase();
		String Address1Msg = genericMethods.getText(driver, Shipping_PageObjects.erromessageforAddress1_gettext).toUpperCase();
		String stateNameMsg= genericMethods.getText(driver, Shipping_PageObjects.GetStateNamefromBilling_gettext).toUpperCase();
		String CityMsg = genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext).toUpperCase();
		String postalMsg = genericMethods.getText(driver, Shipping_PageObjects.errormessageforpostal_gettext).toUpperCase();
		String EmailNumberError=genericMethods.getText(driver, Shipping_PageObjects.errorMessageforEmail_gettext).toUpperCase();
		String phoneNumberErrorMessage=genericMethods.getText(driver, Shipping_PageObjects.errormessageforPhone_gettext).toUpperCase();
		
		
		 if (Cardholdemessage.equalsIgnoreCase("Please enter the name as it appears on your card.")){
			 ATUReports.add("System displaying error message when Cardholder name is blank","  ", "Error Messages for Card holder name should be =  Please enter the name as it appears on your card.","Error message for Card holder name : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageCardName_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(Cardholdemessage, "PLEASE ENTER THE NAME AS IT APPEARS ON YOUR CARD.");			 
		   }else {
			   ATUReports.add("System is not displaying error message when Cardholder name is blank","  ", "Error Messages for Card holder name should be =  Please enter the name as it appears on your card.","Error message for card holder name: " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageCardName_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(Cardholdemessage, "PLEASE ENTER THE NAME AS IT APPEARS ON YOUR CARD.");
			   }
	 
			 if (CardNumbermessage.equalsIgnoreCase("PLEASE ENTER A VALID CARD NUMBER.")) {
			 ATUReports.add("System displaying error message when Card number is blank","  ", "Error Messages for Card number should be =  Please enter a valid card number.","Error message for Card number is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageCardNumber_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(CardNumbermessage, "PLEASE ENTER A VALID CARD NUMBER.");
		   }else{
			   ATUReports.add("System is not displaying error message when Card number is blank","  ", "Error Messages for Card number should be =  Please enter a valid card number.","Error message for Card number is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageCardNumber_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(CardNumbermessage, "PLEASE ENTER A VALID CARD NUMBER.");
		   }
		 
		  if (CVVmessage.equalsIgnoreCase("PLEASE ENTER THE SECURITY CODE THAT APPEARS ON YOUR CARD.")){
			 ATUReports.add("System displaying error message when CVV number is blank","  ", "Error Messages for CVV number should be =  Please enter the security code that appears on your card.","Error message for Card number is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforCVV_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(CVVmessage, "PLEASE ENTER THE SECURITY CODE THAT APPEARS ON YOUR CARD.");
		   }else{
			   ATUReports.add("System is not displaying error message when CVV number is blank","  ", "Error Messages for CVV number should be =  Please enter the security code that appears on your card.","Error message for Card number is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforCVV_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(CVVmessage, "PLEASE ENTER THE SECURITY CODE THAT APPEARS ON YOUR CARD.");
		   }
		  
		 if (firstNameMessage.equalsIgnoreCase("PLEASE PROVIDE A FIRST NAME.")){
			 ATUReports.add("System displaying error message when First name is blank","  ", "Error Messages for First name should be =  Please provide a first name.","Error message for first name is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforfirstName_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(firstNameMessage, "PLEASE PROVIDE A FIRST NAME."); 
		   }else{
			   ATUReports.add("System is not displaying error message when First name is blank","  ", "Error Messages for First name should be =  Please provide a first name.","Error message for first name is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforfirstName_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(firstNameMessage, "PLEASE PROVIDE A FIRST NAME.");
		   }
	
		 
		 if (lastNameMessage.equalsIgnoreCase("PLEASE PROVIDE A LAST NAME.")){
			 ATUReports.add("System displaying error message when last name is blank","  ", "Error Messages for last name should be =  Please provide a last name.","Error message for last name is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforLastName_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(lastNameMessage, "PLEASE PROVIDE A LAST NAME.");
	 	   }else{
			   ATUReports.add("System is not displaying error message when last name is blank","  ", "Error Messages for last name should be =  Please provide a last name.","Error message for last name is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforLastName_gettext)),  true, LogAs.FAILED);
				 Assert.assertEquals(lastNameMessage, "PLEASE PROVIDE A LAST NAME.");
		   }
		 
		 if (Address1Msg.equalsIgnoreCase("PLEASE ENTER A VALID STREET ADDRESS.")){
			 ATUReports.add("System displaying error message when street address is blank","  ", "Error Messages for street address should be =  Please enter a valid street address.","Error message for street address is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforAddress1_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(Address1Msg, "PLEASE ENTER A VALID STREET ADDRESS.");
		   }else{
			   ATUReports.add("System is not displaying error message when street address is blank","  ", "Error Messages for street address should be =  Please enter a valid street address.","Error message for street address is : " +(genericMethods.getText(driver, Shipping_PageObjects.erromessageforAddress1_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(Address1Msg, "PLEASE ENTER A VALID STREET ADDRESS.");
		   }
		 
		 if (CityMsg.equalsIgnoreCase("PLEASE ENTER A VALID CITY.")){
			 ATUReports.add("System displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(CityMsg, "PLEASE ENTER A VALID CITY.");
		   }else{
			   ATUReports.add("System is not displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(CityMsg, "PLEASE ENTER A VALID CITY.");
		   }
		 
		 if (stateNameMsg.equalsIgnoreCase("Please select a State.")){
			 ATUReports.add("System displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(stateNameMsg, "PLEASE SELECT A STATE.");
		   }else{
			   ATUReports.add("System is not displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(stateNameMsg, "PLEASE SELECT A STATE.");
		   }
		 		 		 		 
		 if (postalMsg.equalsIgnoreCase("PLEASE ENTER A VALID ZIP OR POSTAL CODE.")) {
			 ATUReports.add("System displaying error message when zip code is blank","  ", "Error Messages for zip code should be =  Please enter a valid ZIP or postal code.","Error message for zip code is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforpostal_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(postalMsg, "PLEASE ENTER A VALID ZIP OR POSTAL CODE.");
		   }else{
			   ATUReports.add("System is not displaying error message when zip code is blank","  ", "Error Messages for zip code should be =  Please enter a valid ZIP or postal code.","Error message for zip code is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforpostal_gettext)),  true, LogAs.FAILED);
			 Assert.assertEquals(postalMsg, "PLEASE ENTER A VALID ZIP OR POSTAL CODE.");
		   }
		 
		 if (EmailNumberError.equalsIgnoreCase("Please enter a valid email address.")){
			 ATUReports.add("System displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(EmailNumberError, "PLEASE ENTER A VALID EMAIL ADDRESS.");
		   }else{
			   ATUReports.add("System is not displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.FAILED);
			   Assert.assertEquals(EmailNumberError, "PLEASE ENTER A VALID EMAIL ADDRESS.");
		   }
		 
		 if (phoneNumberErrorMessage.equalsIgnoreCase("Please enter the phone number associated with your myAccountPaymentReusable information.")){
			 ATUReports.add("System displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.PASSED);
			 Assert.assertEquals(phoneNumberErrorMessage, "PLEASE ENTER THE PHONE NUMBER ASSOCIATED WITH YOUR myAccountPaymentReusable INFORMATION.");
		   }else{
			 ATUReports.add("System is not displaying error message when city is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city is : " +(genericMethods.getText(driver, Shipping_PageObjects.errormessageforcity_gettext)),  true, LogAs.FAILED);
			 Assert.assertEquals(phoneNumberErrorMessage, "PLEASE ENTER THE PHONE NUMBER ASSOCIATED WITH YOUR myAccountPaymentReusable INFORMATION.");
		   }		 
	  }	 
	 
	 public static String giveDollerValue(String productPrice){		
			String newStr = productPrice.replace("$", "").replace(",", "");
			return newStr;			
		}
	 
	 public static double round(double value, int places) {
		    if (places < 0) throw new IllegalArgumentException();
		    long factor = (long) Math.pow(10, places);
		    value = value * factor;
		    long tmp = Math.round(value);
		    return (double) tmp / factor;
		} 
 
*/
	 
	 public void VerifyCardType(WebDriver driver){
		 billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
		 orderReview_PageObject = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		 genericMethods = new GenericMethods();
		 xplicitWait = new WebDriverWait(driver, 30);
		 GenericMethods.Waitformilliseconds(2000);
		 
		 String NameOfTheCreditCard=genericMethods.getText(driver,billing_PageObjects.billing_payment_DD);
		 System.out.println(NameOfTheCreditCard);
		 
		 if (NameOfTheCreditCard.indexOf("Visa") !=-1){
				System.out.println("system is on visa");
				//System.err.printf("Yes '%s' contains word 'World' %n" , word);
		       }else if(NameOfTheCreditCard.indexOf("Barneys") !=-1){
		           System.err.printf("system is on barneys");	
		       }else {
		    	   System.err.printf("system  has no card barneys");  
		       }	           
 
	 }
	 }

