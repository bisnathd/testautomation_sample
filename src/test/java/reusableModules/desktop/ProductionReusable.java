package reusableModules.desktop;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.text.NumberFormat;

import org.apache.http.auth.AUTH;
import org.apache.poi.hssf.util.HSSFColor.TURQUOISE;
import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import PageObjects.desktop.Desk_BNY_OrderReviewPageObject;
import PageObjects.desktop.Desk_BNY_BillingPageObject;
import PageObjects.desktop.Desk_BNY_CartPageObject;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_GlobalNavPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_LoginRegistrationPageObject;
import PageObjects.desktop.Desk_BNY_MYBagPageObjects;
import PageObjects.desktop.Desk_BNY_PDPPageObjects;
import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import actions.WriteRegistrationData;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class ProductionReusable {
	Desk_BNY_LoginRegistrationPageObject loginRegisterPageObjects;
	Desk_BNY_DesignerPageObject designerPageObjects;
	Desk_BNY_HomePageObject homePageObjects;
	Desk_BNY_RegistrationPageObject registerPageObjects;
	Desk_BNY_PDPPageObjects pdpPageObjects;
	WriteRegistrationData writeRegistrationData;
	Desk_BNY_GlobalNavPageObject globalNavObj;
	Desk_BNY_DesignerPageObject designerPageObj;
	Desk_BNY_MYBagPageObjects myBag_PageObjects;
	Desk_BNY_CartPageObject cartPageObjects;
	Desk_BNY_ShippingPageObject shipping_PageObject;
	Desk_BNY_BillingPageObject billing_PageObjects;
	Desk_BNY_OrderReviewPageObject	OrderReview_PageObjects;

	WebDriverWait xplicitWait;
	GenericMethods genericMethods;
	String timeStamp;
	static int priceInInt;
	public String sTime;
	boolean loggedInUserName;
	boolean loginForm;
	boolean bnyHomePage;
	boolean whsHomePage;
	String CurrentPageTitle;
	boolean loginText;
	boolean registerForm;
	String topCategoryText;
	static int categoryPriceLimitINT;
	static WebDriverWait wait;
	static String xpathInitial;
	static String isPriceElementPresent1;
	static String priceInString;
	String myBagLinkText;
	Boolean CompletePurchaseButton;

	public void loginFromHeader(WebDriver driver,String userEmail, String password){
		homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		CurrentPageTitle=genericMethods.getTitle(driver);
		xplicitWait = new WebDriverWait(driver, 30);
		
		
		xplicitWait.until(ExpectedConditions.visibilityOf(homePageObjects.Home_UtilityNav_TopLogin_LK));		
		genericMethods.Click(driver,homePageObjects.Home_UtilityNav_TopLogin_LK,"Top Login link");
		GenericMethods.Waitformilliseconds(1000);
		
		
		loginForm = genericMethods.isElementPresentforMouseHover(homePageObjects.LoginPage_LoginPanel_FORM);
		if (loginForm == true){			
			ATUReports.add("Verify the Type of User", "User should be guest", "Guest User", true,LogAs.INFO);
			ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Login page is displayed ",  "Login page should not be displayed", "Login page is not displayed", true,LogAs.FAILED);
			Assert.assertEquals(loginForm, true);
		}
		
		genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Email_TB, userEmail,"Username");
		genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Password_TB, password,"Password");
		genericMethods.Click(driver,homePageObjects.LoginPage_LoginPanel_LOGIN_BTN,"Login button");
		
				
		loggedInUserName = genericMethods.isElementPresentforMouseHover(homePageObjects.Home_UtilityNav_LoggedInUser_LK);		
		if(loggedInUserName = true){
			ATUReports.add("Verify user does get Logged In", "User should be logged in successfully", "Login is Success",true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify user does get loggedin","User should be logged in successfully", "Unable to login",true,LogAs.FAILED);	
			Assert.assertEquals(loggedInUserName, true);
				
		}	
	}
	
	 public void clickSubCategoryName(WebDriver driver, String categoryName, String subCategoryName) {
		  globalNavObj = PageFactory.initElements(driver, Desk_BNY_GlobalNavPageObject.class);
		  genericMethods = new GenericMethods();
		  xplicitWait = new WebDriverWait(driver, 30);
			
		  topCategoryText = "//div[@id='top-nav']/ul[@id='topnav-level-1']/li/a[contains(text(),'" + categoryName + "')]";
		  WebElement elementToClick = driver.findElement(By.xpath(topCategoryText));
		  genericMethods.mouseHoverJScript(driver, elementToClick);
		  //genericMethods.javascriptClick(driver, "//div/ul[@class='sub_category topnav-level-2']//li/a[contains(text(),'" + subCategoryName + "')]", "Sub Category");
		  genericMethods.javascriptClick(driver, "//ul[@class='sub_category topnav-level-2']/li/a[contains(text(),'" + subCategoryName + "')]", "Sub Category");

		 }
	 public void browseProductandClickonBNY(WebDriver driver,String categoryPriceLimit){ 
			categoryPriceLimitINT=Integer.parseInt(categoryPriceLimit);	
			genericMethods=new GenericMethods();
			wait=new WebDriverWait(driver, 20);
			GenericMethods.Waitformilliseconds(1000);
			for(int i=1;i<=96;i++){
				xpathInitial="//div[@id='atg_store_prodList']/ul/li["+i+"]";
				//genericMethods.waitForPageLoaded(driver);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@class='gridImg loaded']")));
				GenericMethods.Waitformilliseconds(2000);
				isPriceElementPresent1=genericMethods.isElementPresent(driver,xpathInitial+"//div[@class='product-standard-price']" );
				
				//driver.findElement(By.xpath("")).sendKeys(keysToSend);
				
				if (isPriceElementPresent1.equals("present")) {
					GenericMethods.Waitformilliseconds(2000);
					priceInString=driver.findElement(By.xpath(xpathInitial+"//div[@class='product-standard-price']")).getText();	//div/div[2]/div[@class='product-pricing']/span			
					
						priceInString=giveDollerValue(priceInString);
						priceInInt=Integer.parseInt(priceInString);	
						GenericMethods.Waitformilliseconds(2000);
							ATUReports.add("Check The product", "Valid Product Found",true,LogAs.INFO);
							genericMethods.ClickUSingXpath(driver, xpathInitial+"//div[@class='product-image']/a","Browse Product Link");
							GenericMethods.waitForPageLoaded(driver);
							break;													
						}				
									
			}		
		}
	 

		public void browseProductandClickonWHS(WebDriver driver,String categoryPriceLimit){ 
			categoryPriceLimitINT=Integer.parseInt(categoryPriceLimit);	
			genericMethods=new GenericMethods();
			wait=new WebDriverWait(driver, 20);
			GenericMethods.Waitformilliseconds(1000);
			for(int i=1;i<=96;i++){
				xpathInitial="//div[@id='atg_store_prodList']/ul/li["+i+"]";//div[@id='search-result-items']/div
				//genericMethods.waitForPageLoaded(driver);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@class='gridImg loaded']")));
				GenericMethods.Waitformilliseconds(2000);
				isPriceElementPresent1=genericMethods.isElementPresent(driver,xpathInitial+"//span[@class='product-sales-price']" );
				
				//driver.findElement(By.xpath("")).sendKeys(keysToSend);
				
				if (isPriceElementPresent1.equals("present")) {
					GenericMethods.Waitformilliseconds(5000);
					priceInString=driver.findElement(By.xpath(xpathInitial+"//span[@class='product-sales-price']")).getText();	//div/div[2]/div[@class='product-pricing']/span			
					
						priceInString=giveDollerValue(priceInString);
						priceInInt=Integer.parseInt(priceInString);	
						GenericMethods.Waitformilliseconds(2000);
						if (priceInInt>categoryPriceLimitINT ) {
							ATUReports.add("Check The product", "Valid Product Found",true,LogAs.INFO);
							genericMethods.ClickUSingXpath(driver, xpathInitial+"//div[@class='product-image']/a","Browse Product Link");
							GenericMethods.waitForPageLoaded(driver);
							break;													
						}				
				}else{
					System.out.println("Price Element Not Present");
				}					
			}		
		}
	 
	 public static String giveDollerValue(String productPrice){		
			String newStr = productPrice.replace("$", "").replace(",", "");
			System.out.println(newStr);
			return newStr;
				
		}
	 
	 public void selectAvailableColorOnPDP(WebDriver driver){
		  GenericMethods.waitForPageLoaded(driver);
		  genericMethods =  new GenericMethods();
		  pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		  boolean isPDPColorElementPresent;
		  boolean isSelectableColorAvailable;
		  
		  isPDPColorElementPresent=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_ColorPicker_Element);
		  if(isPDPColorElementPresent){
			  isSelectableColorAvailable=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_SelectableColour_LK);
				if (isSelectableColorAvailable) {			
					GenericMethods.waitForPageLoaded(driver);
					genericMethods.Click(driver, pdpPageObjects.PDP_SelectableColour_LK, "Available Color");	
					GenericMethods.waitForPageLoaded(driver);
					ATUReports.add("Verify Size selected","", "Color is Selected",false,LogAs.INFO);
				}else{
					ATUReports.add("Verify Color selected","", "No Color is available For Selection",false,LogAs.INFO);
				}				
			}else{
				ATUReports.add("Verify Size Selector","","Color Picker is not available", false,LogAs.INFO);	
			}
	  }
	 
	 public void selectAvailableSizeOnPDP(WebDriver driver){
		  GenericMethods.waitForPageLoaded(driver);
		  genericMethods =  new GenericMethods();
		  pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		  boolean isPDPSizeElementPresent;
		  boolean isSelectableSizeAvailable;
		  
		  	isPDPSizeElementPresent=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_SizeSection_Element);
			if(isPDPSizeElementPresent){
				isSelectableSizeAvailable=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_Selectable_Size_Element_LK);
				if (isSelectableSizeAvailable) {			
					GenericMethods.Waitformilliseconds(1000);
					genericMethods.Click(driver, pdpPageObjects.PDP_Selectable_Size_Element_LK, "Available Size");		
					GenericMethods.waitForPageLoaded(driver);
				}else{
					ATUReports.add("Verify Size selected","", "No Size is available For Selection",false,LogAs.INFO);
				}				
			}else{
				ATUReports.add("Verify Size Selector","","Size Selector is not available", false,LogAs.INFO);	
			}
	  }

// Please remove this code after demo*** This is sample code and can be removed after demo.//Bisnath	 
	 public void SimpleselectAvailableSizeOnPDP(WebDriver driver){
		  GenericMethods.waitForPageLoaded(driver);
		  genericMethods =  new GenericMethods();
		  pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
		  boolean isPDPSizeElementPresent;
		  //boolean isSelectableSizeAvailable;
		  
		  	isPDPSizeElementPresent=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_SizeSection_Element);
		  	System.out.println(isPDPSizeElementPresent);
		  	genericMethods.Click(driver, pdpPageObjects.PDP_SizeSection_Element,"Available Size");
		  	/*if(isPDPSizeElementPresent){
				isSelectableSizeAvailable=genericMethods.isElementPresentforMouseHover(pdpPageObjects.PDP_Selectable_Size_Element_LK);
				if (isSelectableSizeAvailable) {			
					GenericMethods.Waitformilliseconds(1000);
					genericMethods.Click(driver, pdpPageObjects.PDP_Selectable_Size_Element_LK, "Available Size");		
					GenericMethods.waitForPageLoaded(driver);
				}else{
					ATUReports.add("Verify Size selected","", "No Size is available For Selection",false,LogAs.INFO);
				}				
			}else{
				ATUReports.add("Verify Size Selector","","Size Selector is not available", false,LogAs.INFO);	
			}*/
	  }
	 
	 public void AddtoCartButtononPDP(WebDriver driver){
			GenericMethods.waitForPageLoaded(driver);
			pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			genericMethods =  new GenericMethods();
			WebDriverWait xwait = new WebDriverWait(driver, 30);
			
			genericMethods.Click(driver, pdpPageObjects.PDP_BUY_BTN,"BUY Button");
			xwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Checkout')]")));
			ATUReports.add("Verify Add to cart button", "Add to cart button should be displayed", "Add to cart button should be displayed", true,LogAs.INFO);
				
			}
	 
	 
		 /*public void AddtoCartButtononPDP(WebDriver driver){
			GenericMethods.waitForPageLoaded(driver);
			pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			WebDriverWait xwait = new WebDriverWait(driver, 30);
			
			wait.until(ExpectedConditions.visibilityOf(pdpPageObjects.PDP_BUY_BTN));
		     driver.findElement(By.id("atg_behavior_addItemToCart")).click();
		     xwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Checkout')]")));
		     ATUReports.add("Verify Add to cart button", "Add to cart button should be displayed", "Add to cart button should be displayed", true,LogAs.INFO);
				
		     
		 }*/
		
		public void miniBagPopuponPDP(WebDriver driver){
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
			cartPageObjects = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
			genericMethods =  new GenericMethods();
			
			ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);

			Boolean popUp=genericMethods.isElementVisible(driver, By.id("showMiniCart"));
			System.out.println(popUp);
			if (popUp= true){ 
				ATUReports.add("Verify if mini bag popup is displayed", "Mini Cart pop up should be displyed","Mini Cart pop up is displayed",true,LogAs.PASSED);
				
					String Popupname = genericMethods.getText(driver, cartPageObjects.MybagfromPDP_gettext);
					System.out.println(Popupname);
					  if (Popupname.contains("MY BAG")){
						ATUReports.add("System displaying Mini Cart pop up", "Customer should be on Mini Cart pop up","Customer is on MY BAG pop up",true,LogAs.PASSED);
										
					  }else{
						ATUReports.add("Customer is not on Mini Cart pop up", "Customer should be on Mini Cart pop up","Customer is not on MY BAG pop up",true,LogAs.FAILED);
									
					  }
			}else {
				ATUReports.add("Verify if mini bag popup is displayed", "Mini Cart pop up should be displyed","Mini Cart pop up is not displayed",true,LogAs.FAILED);
			}
		}
		public void miniBagPopupclickOnCheckout_BT(WebDriver driver){
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			pdpPageObjects = PageFactory.initElements(driver, Desk_BNY_PDPPageObjects.class);
			WebDriverWait xwait = new WebDriverWait(driver, 30);
			genericMethods =  new GenericMethods();	
			
			ATUReports.add("Verify BUY button", "Should be Added", "Its is added", true,LogAs.PASSED);
			xwait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Checkout')]")));
			ATUReports.add("Verify Mini Cart", "Mini Should open","Mini Cart is Open", true, LogAs.INFO);
			genericMethods.Click(driver, pdpPageObjects.MiniCart_Checkout_BTN,"Mini Cart Checkout button");
			
			
		}
		
		//This method selects country from Country Dropdown on Cart Page 
		public  void selectCountry(WebDriver driver, String country) {
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			genericMethods =  new GenericMethods();
			
			genericMethods.selectDropdown(driver, myBag_PageObjects.MYBag_Country_Domestic_DD, country, "Country Dropdown");
			GenericMethods.Waitformilliseconds(2000);
			
		}
		public  void clickCheckoutButton(WebDriver driver) {
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			genericMethods =  new GenericMethods();
			
			genericMethods.Click(driver, myBag_PageObjects.MYBag_CHECKOUT_BTN,"MyBag Checkout BTN");	
			GenericMethods.waitForPageLoaded(driver);
			ATUReports.add("Verify if Checkout Button should be clickable", "Checkout button should be clickable", "Checkout button is clickable", true, LogAs.PASSED);
		}
		
		public void clickShippingPage_ContinueButton(WebDriver driver){
	  		shipping_PageObject=PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
	  		genericMethods=new GenericMethods();
	  		
	  		genericMethods.Click(driver, shipping_PageObject.shipping_CONTINUE_BTN, "Shipping COntinue Button");
	  	}
	  	
		public void enterCVVForLoggedInUser(WebDriver driver,String CVV) {
			
			billing_PageObjects = PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);
			genericMethods=new GenericMethods();
			genericMethods.inputValue(driver, billing_PageObjects.billing_payment_CardCardCVV_TB, CVV, "CVV Number");
			ATUReports.add("Verify CVV #", "CVV # should be entered as :"+ CVV, "CVV # was entered",true,LogAs.PASSED);
			
		}
		
		public void clickbillingPage_ContinueButton(WebDriver driver){
			

			billing_PageObjects=PageFactory.initElements(driver, Desk_BNY_BillingPageObject.class);	
			genericMethods=new GenericMethods();
	  		genericMethods.Click(driver, billing_PageObjects.billing_LoggedInContinue_BTN, "Shipping COntinue Button");
	  		ATUReports.add("Click on Continue button on billing page", "Continue button was clicked", "Button clicked",true,LogAs.PASSED);
	  	}
		
		public void emptyMyBag(WebDriver driver,String site) {
			//ATUReports.setTestCaseReqCoverage("This Test Case Clicks top Login Link");
			myBag_PageObjects = PageFactory.initElements(driver, Desk_BNY_MYBagPageObjects.class);
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			genericMethods =  new GenericMethods();
			boolean isCheckoutButtonPresent;
			List<WebElement> removeLink;
			
			myBagLinkText=genericMethods.getText(driver, "//li [@class='mybag-ul my-bag-link']/a/span");
			System.out.println(myBagLinkText);
			if (myBagLinkText.equalsIgnoreCase("My Bag 0")) {
				ATUReports.add("Verify Items in the Cart", "","There are no items in the cart",true,LogAs.INFO);
			} else {
				genericMethods.Click(driver, myBag_PageObjects.homePage_UtilityNav_MyBag_LK, "My bag link");
				
				isCheckoutButtonPresent=genericMethods.isElementPresentforMouseHover(myBag_PageObjects.MYBag_CHECKOUT_BTN);
				if(isCheckoutButtonPresent==true){			
					removeLink=driver.findElements(By.xpath("//a[@class='removeCartItem']"));
					System.out.println(removeLink);
					ATUReports.add("Verify Items in the Cart", "","There are "+(removeLink.size()+1)+" items in the cart",true, LogAs.INFO);
					
					for (int i = 1; i <= removeLink.size()/2; i++) {
						System.out.println(i);
						genericMethods.Click(driver, myBag_PageObjects.MYBag_productRemove_LK,"MyBag ProductRemove Link");
						GenericMethods.waitForPageLoaded(driver);
					}				
				}else {
					ATUReports.add("Verify Page displayed", "My Bag Page should be displayed","My Bag page is not displayed",true, LogAs.FAILED);
				}
			}			
		}
		public void VerifyBillingPage (WebDriver driver){
			OrderReview_PageObjects=PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);	
			genericMethods = new GenericMethods();
			
			CompletePurchaseButton = genericMethods.isElementPresentforMouseHover(OrderReview_PageObjects.orderReview_CompletePurchase_BTN);
			System.out.println(CompletePurchaseButton);
			if (CompletePurchaseButton=true){
			ATUReports.add("Verify the complete order button", "Compelete order button should be displayed", "Compelete order button is displayed" , true,LogAs.PASSED);
			
			//*********Tax calculator***************
			
			String Tax = genericMethods.getText(driver, OrderReview_PageObjects.taxfromSummary_gettext);
			System.out.println("Tax displayed on billing page under order review section" + Tax);
			String RemoveCurrency = Tax.replace("$","");
			System.out.println("Currency after removing dollar sign "+ RemoveCurrency);
			String RemoveDecimal = RemoveCurrency.replace(".", "");
			System.out.println("Currency after removal decima point " +RemoveDecimal);
			
			Integer y = Integer.valueOf(RemoveDecimal);
			System.out.println("Final tax value after decimal removal " + y);
				if (y > 0){
					
					ATUReports.add("Verify the Tax section", "Tax should be calculated and should not be zeor", "Tax is calcualated as :" + y , true,LogAs.PASSED);
				}else{
					ATUReports.add("Verify the Tax section", "Tax should be calculated and should not be zeor", "Tax is not calcualated value displayed is :" + y , true,LogAs.FAILED);
					Assert.assertEquals(y, y>0);
					//Assert.assertEquals(actual, expected);
				}
			
			}else{
				ATUReports.add("Verify the complete order button", "Compelete order button should be displayed", "Compelete order button is not displayed" , true,LogAs.FAILED);
				Assert.assertEquals(false, true);
			}
			
		}
	}
