package reusableModules.desktop;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;

import actions.prodSanity.ProductionCustomReporter;

import javax.activation.*;

public class Production_MailReportReusable {
	
	
	public static void sendreportinMail(String resultPath, int totalPassed, int totalFailed, int totalSkipped) {
		  final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		  // Get a Properties object
		  String status="Pass";
		  	String reportFilePath = null;
		  	ProductionCustomReporter productionCustomreporter=new ProductionCustomReporter();
		  	ArrayList<Integer> getResultCount;
		     Properties props = System.getProperties();
		     props.setProperty("mail.smtp.host", "smtp.gmail.com");
		     props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		     props.setProperty("mail.smtp.socketFactory.fallback", "false");
		     props.setProperty("mail.smtp.port", "465");
		     props.setProperty("mail.smtp.socketFactory.port", "465");
		     props.put("mail.smtp.auth", "true");
		     props.put("mail.debug", "true");
		     props.put("mail.store.protocol", "pop3");
		     props.put("mail.transport.protocol", "smtp");
		     final String username = "bisnath.dubey@adapty.com";//
		     final String password = "vishnangel";
		     try{
		     Session session = Session.getDefaultInstance(props, 
		                          new Authenticator(){
		                             protected PasswordAuthentication getPasswordAuthentication() {
		                                return new PasswordAuthentication(username, password);
		                             }});

		   // -- Create a new message --
		     Message msg = new MimeMessage(session);

// ------------------------- Set the FROM and TO fields ---------------------------
		     msg.setFrom(new InternetAddress("adaptyautomationreporter@gmail.com"));
		     
		     String[] to = {"barneyssupport@adapty.com"};
		     InternetAddress[] addressTo = new InternetAddress[to.length];
		     for (int i = 0; i < to.length; i++){
               addressTo[i] = new InternetAddress(to[i]);
           }
		     msg.setRecipients(Message.RecipientType.TO, addressTo);
		     
		     String[] toBCC = {"harshal.dharmadhikari@adapty.com"};
		     InternetAddress[] addressToCC = new InternetAddress[toBCC.length];
		     for (int i = 0; i < toBCC.length; i++){
	             addressToCC[i] = new InternetAddress(toBCC[i]);
	         }
		     msg.setRecipients(Message.RecipientType.CC, addressToCC); 
		     
		     
		  //Attchment code starts here
		  // Create the message part 
		     Multipart multipart = new MimeMultipart();
	         BodyPart messageBodyPart = new MimeBodyPart();

	         // Fill the message
	         messageBodyPart.setText("This is test Automation report"); 
	         //multipart.addBodyPart(messageBodyPart);
	         
	         //--------------------------------------------------------------------------------------
	         String htmlText = "</br></br><p>Please  find  the  sanity  Report  for  Barneys  &  Warehouse Production  site,</p></br></br><table style=\"height: 60px; border-color: #000000;\" width=\"58\"><tbody><tr><td><span style=\"color: #07af47;\">Passed</span></td><td>"+totalPassed+"</td></tr><tr><td><span style=\"color: #e20929;\">Fail</span></td><td>"+totalFailed+"</td></tr><tr><td><span style=\"color: #e29a09;\">Skip</span></td><td>"+totalSkipped+"</td></tr></tbody></table>";
	         messageBodyPart.setContent("Hello Team," + htmlText, "text/html");
	         multipart.addBodyPart(messageBodyPart);

	         // Part two is attachment
	         File fileToSend = null;
	         fileToSend=Production_MailReportReusable.getLastReportExcel(resultPath);
	         reportFilePath=fileToSend.getPath();
	         
	         Path path = Paths.get(fileToSend.getAbsoluteFile().toURI());
	         BasicFileAttributes view = null;
	            try {
	                view = Files.getFileAttributeView(path, BasicFileAttributeView.class).readAttributes();
	            } catch (IOException e) {
	                
	                e.printStackTrace();
	            }
	            String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
	            //FileTime creationTime = view.creationTime().;
	            
	            //System.out.println("File Creation Date:============================"+creationTime);
	           
	            if (totalFailed>0) {
	            	status="Fail";
				}
	           	msg.setSubject(status+": Production Automation Report :"+ timeStamp);  
	           	
			    msg.setSentDate(new Date());
	         
	         messageBodyPart = new MimeBodyPart();
	         String filename = reportFilePath;
	         DataSource source = new FileDataSource(filename);
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         messageBodyPart.setFileName("Prod_Test_Report.xls");
	         multipart.addBodyPart(messageBodyPart);

	         // Send the complete message parts
	         msg.setContent(multipart );

		     
		     
		     
		     Transport.send(msg);
		     System.out.println("Message sent.");
		  }catch (MessagingException e)
		     { 
			  System.out.println("Error Cause " + e);
			  }
		     }
	
	public static File getLastReportExcel(String ResultsPath) {
		File lastReportFolder=Production_MailReportReusable.listModifiedDirectory(ResultsPath);
		
	    File fl = new File(lastReportFolder.getPath());
	    long millisec;
	    long milliSecCreated;
	    String path;
	    File[] files = fl.listFiles(new FileFilter() {          
	        public boolean accept(File file) {
	            return file.isFile();
	        }
	    });
	    
	    long lastMod = Long.MIN_VALUE;
	    File choice = null;
	    for (File file : files) {
	    	millisec=file.lastModified();
	    	
	    	Date dt = new Date(millisec);
	    	 path = file.getPath();

	        if (file.lastModified() > lastMod) {
	            choice = file;
	            lastMod = file.lastModified();


	            Path p = Paths.get(file.getAbsoluteFile().toURI());
	            
	            
	        }
	    }
	    //System.out.println(choice.getPath());
	    return choice;
	}
	
	public static File listModifiedDirectory(String dir) {
	    File file = new File(dir);
	    File directoryFile;
	    String[] names = file.list();
	    String folderPath;
	    long milliseconds;
	    String path;
	    File choice = null;
	    long lastMod = Long.MIN_VALUE;

	    for(String name : names){
	    	directoryFile=new File(dir+name);
	        if (directoryFile.isDirectory()){
	        	milliseconds=directoryFile.lastModified();	        	
	    		Date dt = new Date(milliseconds);
	    		path = directoryFile.getPath();
		    	 
	    	 if (directoryFile.lastModified() > lastMod) {
		            choice = directoryFile;
		            lastMod = directoryFile.lastModified();
		      }		    			
	        }	        
	    }
	    System.out.println(choice.getPath());
	    return choice;
	}

	 
	 
}
