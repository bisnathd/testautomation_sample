package reusableModules.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import PageObjects.desktop.Desk_BNY_ChangePasswordPageObject;
import PageObjects.desktop.Desk_BNY_MyAccountPersonalInfoPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;



public class Desk_MyAccountPersonalInfo_Reusable{
	Desk_BNY_MyAccountPersonalInfoPageObject myAccountPersonalInfoPageObject; 
	Desk_BNY_ChangePasswordPageObject changePasswordPageObjects; 
	Desk_LoginRegister_Reusable loginRegisterReusable;
	boolean personalInfoPanel;
	WebDriverWait xplicitWait;
	GenericMethods genericMethods; 
	boolean SetNewPasswordPanel;
	boolean logout;

 //update profile Information
  public void updatePersonalInfo(WebDriver driver, String firstname , String Lastname , String Month, String day , String Year, String Gender ){
	   myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
	   genericMethods =  new GenericMethods();
	     
	   genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_UtilityNav_LoggedInUserName_LK, "Top Username link");
	   //genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Edit link on Personal Info");
	   genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfor_leftNav, "Personal Info LeftNav");
	   ATUReports.add("Verify system does display Edit section for PERSONAL INFO", "system displays Edit section for PERSONAL INFO", "System should display Edit section of PERSONAL INFO",true,LogAs.PASSED);
	   genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_FirstName_TB,"First Name TextBox");
	   genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_FirstName_TB, firstname,"First Name");
	   genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_LastName_TB,"Last Name TextBox");
	   genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_LastName_TB, Lastname,"Last Name");
	   genericMethods.selectDropdown(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Gender_DD, Gender,"Gender");
	   genericMethods.selectDropdown(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_DOB_Month_DD, Month,"Month");
	   genericMethods.selectDropdown(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_DOB_Day_DD, day,"Calender Day");
	   genericMethods.selectDropdown(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_DOB_Year_DD, Year,"Calender Year");
	   
	   ATUReports.add("User updating his PERSONAL INFO", "System should updating PERSONAL INFO",true,LogAs.INFO);
	   genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Submit_BTN,"Submit Button");
	         
	   String element = genericMethods.getText(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_PANEL);
	   String emailText = genericMethods.getText(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Email_TXT);
	   
	   if (element.contains(firstname+" " + Lastname)){
	    ATUReports.add("Verify Profile does get updated", "System updates profile successfully", "Profile Updated successfully",true,LogAs.PASSED);
	    Assert.assertEquals(element, firstname+" " + Lastname+ "\n" + emailText );
	    
	   }else{
	    ATUReports.add("Verify profile does get updated", "Profile is not updated", "Profile should not updated",true,LogAs.FAILED);
	    Assert.assertEquals(element, firstname+" " + Lastname+ "\n" + emailText);    
	   }
  }
  
//Change Email id 
   public void ChangeEmailID(WebDriver driver, String Email, String oldPassword , String ChangeEmail ){ 
 
	    myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
	    genericMethods =  new GenericMethods();   
	    //ATUReports.add("Customer is on Home page", "Customer should be on Home page",true,LogAs.INFO);
	
	    genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_UtilityNav_LoggedInUserName_LK, "Top Username link");
	    genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Personal Info Edit Link");
	    ATUReports.add("Verify system does display Edit PERSONAL INFO section", "System should displays Edit PERSONAL INFO",true,LogAs.PASSED);
	    xplicitWait = new WebDriverWait(driver, 30);
	    xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmail_LK));
	    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmail_LK,"Change Email ID link");
	    ATUReports.add("System displays Change email section of PERSONAL INFO", "System should display Change email section for PERSONAL INFO",true,LogAs.INFO);
	    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmailAddress_TB,"Email ID text box");
	    genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmailAddress_TB, ChangeEmail ,"Email ID");
	    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ConfirmEmailAddress_TB,"Confirm Email ID textbox");
	    genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ConfirmEmailAddress_TB, ChangeEmail,"Confirm Email Address");
	    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Password_TB,"Password text box");
	    genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Password_TB, oldPassword,"Password");
	    ATUReports.add("User updating his Email id", "System should updating Email id",true,LogAs.INFO);
	    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Apply_BTN,"Submit Button");
    
    /*String element = genericMethods.getText(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfoEmail_gettext);
    if (element.contains(ChangeEmail)){
     ATUReports.add("Profile updated successfully", "update profile",true,LogAs.PASSED);
     }else{
     ATUReports.add("Profile is not updated", "Not update",true,LogAs.FAILED);
     Assert.assertEquals(element, ChangeEmail);     
    }*/
    
    
		personalInfoPanel=genericMethods.isElementPresentforMouseHover(myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_PANEL);
		if (personalInfoPanel==true){			
			ATUReports.add("Verify email address does get updated",  "email address should get updated", "email updated successfully", true,LogAs.PASSED);
			}else{
			ATUReports.add("Verify email address does get updated",  "email address should not be updated", "email not updated", true,LogAs.FAILED);
			Assert.assertEquals(personalInfoPanel, true);
		}
   }
    
//Updated previous Email id 
		public void UpdatePreviousEmail(WebDriver driver,String Email, String oldPassword){ 
			myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
		    genericMethods =  new GenericMethods();   
		    
		    //genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_loggedInUser_Topnavigation_LK);
		    genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Personal Info Edit Link");
		    ATUReports.add("Verify system does display Edit PERSONAL INFO section", "System should displays Edit PERSONAL INFO",true,LogAs.INFO);
		    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmail_LK,"Change Email Link");
		    ATUReports.add("System displays Change email section of PERSONAL INFO", "System should display Change email section for PERSONAL INFO",true,LogAs.INFO);
		    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmailAddress_TB,"Email address textbox");
		    genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmailAddress_TB, Email,"Email Address");
		    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ConfirmEmailAddress_TB,"Confirm Email Address Textbox");
		    genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ConfirmEmailAddress_TB,Email, "Confirm Email Address");
		    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Password_TB,"Password textbox");
		    genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Password_TB, oldPassword,"Password");
		    genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Apply_BTN,"Sunmit Button"); 
		    
		    personalInfoPanel=genericMethods.isElementPresentforMouseHover(myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_PANEL);
			if (personalInfoPanel==true){			
				ATUReports.add("Verify previous email address does get updated",  "previous email address should get updated", "previous email updated successfully", true,LogAs.PASSED);
				}else{
				ATUReports.add("Verify previous email address does get updated",  "previous email address should not be updated", "previous email not updated", true,LogAs.FAILED);
				Assert.assertEquals(personalInfoPanel, true);
			}
	   }

	public void EditProfileValidation(WebDriver driver){
	  	myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
		changePasswordPageObjects= PageFactory.initElements(driver, 	Desk_BNY_ChangePasswordPageObject.class);
		genericMethods = new GenericMethods(); 
		
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_UtilityNav_LoggedInUserName_LK,"Logged in username from Utility Nav");
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Edit Personal Info Link");
		genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_FirstName_TB, "","First Name");
		genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_LastName_TB, "","Last Name");
		
		String firstnameerror = genericMethods.getText(driver, myAccountPersonalInfoPageObject.firstnameErrorMeg_gettext);
		if (firstnameerror.equals("Please provide a first name.")){
			ATUReports.add("Verify validation message for first name", "system should display error message when FirstName is blank","Error message is displayed when FirstName is blank",  true, LogAs.PASSED);
			Assert.assertEquals(firstnameerror, "Please provide a first name.");
		}else{
			ATUReports.add("Verify validation message for FirstName", "system should display error message when FirstName is blank","Error message is not displayed when FirstName is blank",  true, LogAs.FAILED);
			Assert.assertEquals((genericMethods.getText(driver, myAccountPersonalInfoPageObject.firstnameErrorMeg_gettext)), "Please provide a first name.");			
		}
	
		genericMethods.selectDropdown(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_DOB_Day_DD, "23","DOB Day");
		genericMethods.selectDropdown(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_DOB_Month_DD, "May","DOB Month");
		
		String Lastnameerror = genericMethods.getText(driver, myAccountPersonalInfoPageObject.LastnameErrorMeg_gettext);
		if (Lastnameerror.equals("Please provide a last name.")) {
			ATUReports.add("Verify validation message for Last Name", "system should display error message when LastName is blank","Error message is displayed when LastName is blank",  true, LogAs.PASSED);
			Assert.assertEquals((genericMethods.getText(driver, myAccountPersonalInfoPageObject.LastnameErrorMeg_gettext)), "Please provide a last name.");
		}else{
			ATUReports.add("Verify validation message for Last Name", "system should display error message when LastName is blank","Error message is not displayed when LastName is blank" ,  true, LogAs.FAILED);
			Assert.assertEquals((genericMethods.getText(driver, myAccountPersonalInfoPageObject.LastnameErrorMeg_gettext)), "Please provide a last name.");
		}	  
	}

	public void ChangeEmailAddressValidation(WebDriver driver){
		myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
		changePasswordPageObjects = PageFactory.initElements(driver, 	Desk_BNY_ChangePasswordPageObject.class);
		genericMethods = new GenericMethods(); 
		
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_UtilityNav_LoggedInUserName_LK,"Logged in username from Utility Nav");
		//genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Personal Info Edit Link");
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfor_leftNav, "Personal Info LeftNav");
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmail_LK,"Change Email Link");
		genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ChangeEmailAddress_TB, "","Email Address" );
		genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_ConfirmEmailAddress_TB, "","Confirm Email");
		
		String Emailerror = genericMethods.getText(driver, myAccountPersonalInfoPageObject.EmailErrorMeg_gettext);
		if (Emailerror.equals("Please provide an email address.")){
			ATUReports.add("Verify validation message for Email id", "system should display error message when email id is blank","Error message is displayed when email id is blank",  true, LogAs.PASSED);
			Assert.assertEquals(Emailerror, "Please provide an email address.");
		 }else{
			 ATUReports.add("Verify validation message for Email id", "system should display error message when email id is blank","Error message is not displayed when email id is blank", true, LogAs.FAILED);
			 Assert.assertEquals(Emailerror, "Please provide an email address.");			
		 }
		
		genericMethods.inputValue(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Password_TB, "","Password");
		String confirmEmailError = genericMethods.getText(driver, myAccountPersonalInfoPageObject.ConfirmEmailErrorMeg_gettext);
		if (confirmEmailError.equals("Please provide an email address.")){
			ATUReports.add("Verify validation message for Confirm Email Address", "System should display error message when confirm email is blank", "Error message is displayed when confirm email is blank",  true, LogAs.PASSED);
			Assert.assertEquals(confirmEmailError, "Please provide an email address.");
		}else{
			ATUReports.add("Verify validation message for Confirm Email Address", "System should display error message when confirm email is blank", "Error message is not displayed when confirm email is blank",  true, LogAs.FAILED);
			Assert.assertEquals(confirmEmailError, "Please provide an email address.");			
		}	
		
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfoPage_Apply_BTN,"Submit Button");
		String passworderror = genericMethods.getText(driver, myAccountPersonalInfoPageObject.PasswordErrorMeg_gettext);
		if (passworderror.equals("Please enter your password.")){
			ATUReports.add("Verify validation message for Password", "System should display error message when password is blank", "Error message is displayed when password is blank",  true, LogAs.PASSED);
			Assert.assertEquals(passworderror, "Please enter your password.");
		 }else{
		 	ATUReports.add("Verify validation message for Password", "System should display error message when password is blank", "Error message is not displayed when password is blank",  true, LogAs.FAILED);
			 Assert.assertEquals(passworderror, "Please enter your password.");			
		 }
				
		
			
	}


	public void ChangePasswordPageValidation(WebDriver driver){ 	
		myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
		changePasswordPageObjects = PageFactory.initElements(driver, Desk_BNY_ChangePasswordPageObject.class);
		genericMethods = new GenericMethods();
		
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_UtilityNav_LoggedInUserName_LK,"Logged In username");
		//genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Personal Info Edit Link");
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfor_leftNav, "Personal Info LeftNav");
		//genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_LK,"Personal Info Link");
		genericMethods.Click(driver, changePasswordPageObjects.MyAccount_ChangePassword_LK,"Change Password Link");
		genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_OldPassword_TB, "","Old passwprd");
		genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_NewPassword_TB, "","New changePasswordPageObjects");
		String oldpasswordmsg = genericMethods.getText(driver, myAccountPersonalInfoPageObject.oldpasswordErrorMeg_gettext);
		if (oldpasswordmsg.equals("Please enter your old password.")){
			ATUReports.add("Verify validation message for old password", "system should display error message when old password is blank" , "system should display error message when old Password is blank",true, LogAs.PASSED);
			Assert.assertEquals(oldpasswordmsg, "Please enter your old password.");
		 }else{
			 ATUReports.add("Verify validation message for old password", "system should display error message when old password is blank" , "system is not displaying error message when old password is blank", true, LogAs.FAILED);
			 Assert.assertEquals(oldpasswordmsg, "Please enter your old password.");			
		 }
		
		genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_ConfirmPassword_TB, "","COnfirm Password");
		
		String Newpasswordmsg = genericMethods.getText(driver, myAccountPersonalInfoPageObject.NewPasswordErrorMeg_gettext);
		if (Newpasswordmsg.equals("Please enter your password.")){
			ATUReports.add("Verify validation message for new Password", "system should display error message when new Password is blank", "system is displaying error message when new Password is blank" , true, LogAs.PASSED);
			Assert.assertEquals(Newpasswordmsg, "Please enter your password.");
		}else{
		   ATUReports.add("Verify validation message for new Password","system should display error message when new Password is blank","system is not displaying error message when new Password is blank",  true, LogAs.FAILED);
		   Assert.assertEquals(Newpasswordmsg, "Please enter your password.");			
		}
		
		genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_Submit_TB,"Submit button");		
		String ConfirmNewPasswordmsg = genericMethods.getText(driver, myAccountPersonalInfoPageObject.ConfirmPasswordErrorMeg_gettext);
		if (ConfirmNewPasswordmsg.equals("Please confirm your password.")){
			ATUReports.add("Verify validation message for confirm Password", "system should display error message when confirm Password is blank", "system is displaying error message when confirm Password is blank",  true, LogAs.PASSED);
			Assert.assertEquals(ConfirmNewPasswordmsg, "Please confirm your password.");
		}else{
			   ATUReports.add("Verify validation message for confirm Password","system should display error message when confirm Password is blank","system is not displaying error message when confirm Password is blank",  true, LogAs.FAILED);
			   Assert.assertEquals(ConfirmNewPasswordmsg, "Please confirm your password.");
		}
	}
	
	
	public void ChangeMyPassword(WebDriver driver,String email ,String oldpassword , String newpassword) {
		changePasswordPageObjects = PageFactory.initElements(driver, Desk_BNY_ChangePasswordPageObject.class);
		myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);
		genericMethods =  new GenericMethods();
		loginRegisterReusable= new Desk_LoginRegister_Reusable();
		
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_UtilityNav_LoggedInUserName_LK,"Logged In Username");
		//genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Personal Info Edit Link");
		genericMethods.Click(driver, myAccountPersonalInfoPageObject.PersonalInfor_leftNav, "personal Info LeftNav");
		ATUReports.add("User is on Edit Personal info section", "User should be on Edit Personal info section",true,LogAs.INFO);
		genericMethods.Click(driver, changePasswordPageObjects.MyAccount_ChangePassword_LK,"Change password Link");
		genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_OldPassword_TB,"Old Password Textbox");
		genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_OldPassword_TB, oldpassword,"Old password");
		genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_NewPassword_TB,"New Password textbox");
		genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_NewPassword_TB, newpassword,"New Password");
		genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_ConfirmPassword_TB,"Confirm Password text Box");
		genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_ConfirmPassword_TB, newpassword,"Conform Password");
		ATUReports.add("User is updating password form Personal info section", "User should be on update password section of  Personal info ",true,LogAs.INFO);
		genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_Submit_TB,"Submit Button");
		genericMethods.Click(driver, changePasswordPageObjects.UtilityNav_TopNavigation_LoggedInUser_DD,"Top Logged In user name");
		genericMethods.Click(driver, changePasswordPageObjects.LogOut_LK,"Submit Button");

		logout=genericMethods.isElementPresentforMouseHover(changePasswordPageObjects.Home_UtilityNav_TopLogin_LK);
		if (logout==true){			
			ATUReports.add("Verify customer does get logout successfully",  "User should get logout" , "User gets logout successfully", true,LogAs.PASSED);
			}else{
			ATUReports.add("Verify customer does get logout successfully",  "User should not get logout", "Unable to logout", true,LogAs.FAILED);
			Assert.assertEquals(logout, true);
		}
		
		loginRegisterReusable.loginFromHeader(driver, email , newpassword);
		ATUReports.add("Password change successfully", "password change",true,LogAs.PASSED);		
}


public void updateexitingpassword(WebDriver driver,String newpassword , String oldPassword){
	genericMethods =  new GenericMethods();
		
	changePasswordPageObjects = PageFactory.initElements(driver, Desk_BNY_ChangePasswordPageObject.class);
	myAccountPersonalInfoPageObject = PageFactory.initElements(driver, Desk_BNY_MyAccountPersonalInfoPageObject.class);

	//ATUReports.add("Customer is on Home page", "System should display Home page",true,LogAs.INFO);
	
	genericMethods.Click(driver, myAccountPersonalInfoPageObject.Home_UtilityNav_LoggedInUserName_LK,"Logged In Username");
	genericMethods.Click(driver, myAccountPersonalInfoPageObject.MYAccount_PersonalInfo_Edit_LK,"Personal Info Edit Link");
	ATUReports.add("Customer is on Edit personal info section", "System should display edit personal info section",true,LogAs.INFO);
	genericMethods.Click(driver, changePasswordPageObjects.MyAccount_ChangePassword_LK,"Change password Link");
	genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_OldPassword_TB,"Old Password Textbox");
	genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_OldPassword_TB, newpassword,"Old password");
	genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_NewPassword_TB,"New Password textbox");
	genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_NewPassword_TB, oldPassword,"New Password");
	genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_ConfirmPassword_TB,"Confirm Password text Box");
	genericMethods.inputValue(driver, changePasswordPageObjects.SetNewPasswordPage_ConfirmPassword_TB, oldPassword,"Conform Password");
	ATUReports.add("User is updating his / her password form Personal info section", "User should be on update password section of  Personal info ",true,LogAs.INFO);
	genericMethods.Click(driver, changePasswordPageObjects.SetNewPasswordPage_Submit_TB,"Submit Button");
	genericMethods.Click(driver, changePasswordPageObjects.UtilityNav_TopNavigation_LoggedInUser_DD,"Logged in Username Link");
	genericMethods.Click(driver, changePasswordPageObjects.LogOut_LK,"Logut Link");
	
	
	logout=genericMethods.isElementPresentforMouseHover(changePasswordPageObjects.Home_UtilityNav_TopLogin_LK);
	if (logout==true){			
		ATUReports.add("Verify customer does get logout successfully",  "User should get logout" , "User gets logout successfully", true,LogAs.PASSED);
		}else{
		ATUReports.add("Verify customer does get logout successfully",  "User should not get logout", "Unable to logout", true,LogAs.FAILED);
		Assert.assertEquals(logout, true);
	}
	
	
	ATUReports.add("Verify previous password does get updated successfully","Previous password updated successfully", "Previous password updated successfully",true,LogAs.PASSED);
				
}
}