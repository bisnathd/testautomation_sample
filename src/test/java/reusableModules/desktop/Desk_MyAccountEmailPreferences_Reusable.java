package reusableModules.desktop;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import PageObjects.desktop.Desk_BNY_MyAccountEmailPreferencesPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;

public class Desk_MyAccountEmailPreferences_Reusable {

	GenericMethods genericMethods;
	String isSizeElementPresent;
	Desk_BNY_MyAccountEmailPreferencesPageObject myAccountEmailPreferencesObject;
	String productSalesPriceonPDP;
	String productSalesPriceonMiniCart;
	String productSalesPriceonMYBag;
	String xpathInitial;
	String isPreorderTextPresent;
	String subScribedEmailList;
	String IscurrentSUbscriptionPresent;
WebDriverWait xplicitWait;
	//-----------------------****Un-SubScribe from All Email List****---------------------------------------
	public void unSubScribeFromAllEmailList(WebDriver driver){
		ATUReports.setAuthorInfo("Govind", Utils.getCurrentTime(),"1.0");
		myAccountEmailPreferencesObject = PageFactory.initElements(driver, Desk_BNY_MyAccountEmailPreferencesPageObject.class);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		genericMethods =  new GenericMethods();
		xplicitWait=new WebDriverWait(driver, 15);
		
		genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeader_Myaccount");
		GenericMethods.waitForPageLoaded(driver);
		subScribedEmailList=genericMethods.getText(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_SubscribedEmailList_TV);
		
		genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN, "ViewEdit");
			//System.out.println(subScribedEmailList);
			ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : "+ subScribedEmailList,true,LogAs.INFO);
			
			GenericMethods.waitForPageLoaded(driver);
			if (subScribedEmailList.equalsIgnoreCase("Barneys New York\nBarneys Warehouse")) {
				//xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
				//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
				//((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
				genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNY_CB,"BNY Checkbox");
				genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNYWH_CB,"WHS Checkbox");
				genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_Update_BTN,"Email Update BTN");
				ATUReports.add("Verify Checkboxes against the Email List(On Email Preferences Page)", "Checkboxes against the Email should be unchecked","Checkboxes againt the Email is Unchecked",true,LogAs.INFO);
				genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeader");
			} else if (subScribedEmailList.equalsIgnoreCase("Barneys New York")) {
				GenericMethods.waitForPageLoaded(driver);
				xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
				//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
				//((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
				genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNY_CB,"BNY Checkbox");
				genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_Update_BTN,"WHS Checkbox");
				ATUReports.add("Verify Checkboxes againt the Email List(On Email Preferences Page)", "Checkboxes against the Email should be unchecked","Checkboxes againt the Email is Unchecked",true,LogAs.INFO);
				genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeaderMyAccount Link");
			}else if (subScribedEmailList.equalsIgnoreCase("Barneys Warehouse")) {
				GenericMethods.waitForPageLoaded(driver);
				xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
				//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
				//((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
				genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNYWH_CB,"WHS Checkbox");
				genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_Update_BTN,"Email Update BTN");
				ATUReports.add("Verify Checkboxes againt the Email List(On Email Preferences Page)", "Checkboxes against the Email should be unchecked","Checkboxes againt the Email is Unchecked",true,LogAs.INFO);
				genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeaderMyAccount Link");
			}	else if (subScribedEmailList.equalsIgnoreCase("You have no email preferences.")) {
				GenericMethods.waitForPageLoaded(driver);
				subScribedEmailList="";
				ATUReports.add("Verify subscribed Email List after Un-subscribing is complete", "No Subscribed Email list should be present","List of Subscribed Email List : ---",true,LogAs.PASSED);
				Assert.assertEquals(subScribedEmailList, "");
			}else if (subScribedEmailList.equalsIgnoreCase("")) {
				GenericMethods.waitForPageLoaded(driver);
				subScribedEmailList="";
				ATUReports.add("Verify subscribed Email List after Un-subscribing is complete", "No Subscribed Email list should be present","List of Subscribed Email List : ---",true,LogAs.PASSED);
				Assert.assertEquals(subScribedEmailList, "");
			}
		}


	
	//-----------------------****SubScribe to Barneys  Email List****---------------------------------------
	public void subScribetoBNYEmailList(WebDriver driver){
		ATUReports.setAuthorInfo("Govind", Utils.getCurrentTime(),"1.0");
		myAccountEmailPreferencesObject = PageFactory.initElements(driver, Desk_BNY_MyAccountEmailPreferencesPageObject.class);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		genericMethods =  new GenericMethods();
		
		//-----------------------------------------------------------------------------------------------------
	genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeaderMyAccount Link");
	IscurrentSUbscriptionPresent=genericMethods.isElementPresent(driver, "//section[@class='col-xs-12 col-sm-6 col-md-6 col-lg-4 overview-emailprefs']/p[2]");
	if (IscurrentSUbscriptionPresent.equalsIgnoreCase("present")) {
		subScribedEmailList=genericMethods.getText(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_SubscribedEmailList_TV);
		ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : "+ subScribedEmailList,true,LogAs.INFO);
	}else {
		ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : -----",true,LogAs.INFO);		
	}
	
	xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));	
	//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	
	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNY_CB,"BNY Checkbox");
	Assert.assertEquals(reuseCodeForEMailPreferences(driver), "Barneys New York");
	
	///////////////Unsubscribe from all Email List//////////////////
	xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
	//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNY_CB,"BNY Checkbox");
	ATUReports.add("","","User has un-subscribed from Barneys Mail List", true, LogAs.INFO);
	Assert.assertEquals(reuseCodeForEMailPreferences(driver), "");
		
}
	//-----------------------****SubScribe to WHS  Email List****---------------------------------------
	public void subScribetoWHSEmailList(WebDriver driver)
	{

		ATUReports.setAuthorInfo("Govind", Utils.getCurrentTime(),"1.0");
		myAccountEmailPreferencesObject = PageFactory.initElements(driver, Desk_BNY_MyAccountEmailPreferencesPageObject.class);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		genericMethods =  new GenericMethods();
		
		//--------------------------------------------------------------------------------------------------
	genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeaderMyAccount Link");
	IscurrentSUbscriptionPresent=genericMethods.isElementPresent(driver, "//section[@class='col-xs-12 col-sm-6 col-md-6 col-lg-4 overview-emailprefs']/p[2]");
	if (IscurrentSUbscriptionPresent.equalsIgnoreCase("present")) {
		subScribedEmailList=genericMethods.getText(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_SubscribedEmailList_TV);
		ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : "+ subScribedEmailList,true,LogAs.INFO);
	}else {
		ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : -----",true,LogAs.INFO);		
	}
	GenericMethods.Waitformilliseconds(2000);
	xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
	//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);

	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNYWH_CB,"WHS Checkbox");
	GenericMethods.waitForPageLoaded(driver);
	Assert.assertEquals(reuseCodeForEMailPreferences(driver), "Barneys Warehouse");
	///////////////Unsubscribe from all Email List//////////////////
	xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
	//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNYWH_CB,"WHS Checkbox");
	ATUReports.add("","","User has un-subscribed from Warehouse Mail List", true, LogAs.INFO);
	Assert.assertEquals(reuseCodeForEMailPreferences(driver), "");
	}
	
	//-----------------------****SubScribe to Barneys and WHS  Email List****---------------------------------------
	public void subScribetoBNYWHSEmailList(WebDriver driver){
		ATUReports.setAuthorInfo("Govind", Utils.getCurrentTime(),"1.0");
		myAccountEmailPreferencesObject = PageFactory.initElements(driver, Desk_BNY_MyAccountEmailPreferencesPageObject.class);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebDriverWait xwait = new WebDriverWait(driver, 30);
		genericMethods =  new GenericMethods();
		
		//---------------------------------------------------------------------------------------------------------
	genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeaderMyAccount Link");
	IscurrentSUbscriptionPresent=genericMethods.isElementPresent(driver, "//section[@class='col-xs-12 col-sm-6 col-md-6 col-lg-4 overview-emailprefs']/p[2]");
	if (IscurrentSUbscriptionPresent.equalsIgnoreCase("present")) {
		subScribedEmailList=genericMethods.getText(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_SubscribedEmailList_TV);
		ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : "+ subScribedEmailList,true,LogAs.INFO);
	}else {
		ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : -----",true,LogAs.INFO);		
	}
	xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
	//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNY_CB,"BNY Checkbox");
	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNYWH_CB,"WHS Checkbox");
	Assert.assertEquals(reuseCodeForEMailPreferences(driver), "Barneys New York\nBarneys Warehouse");
	///////////////Unsubscribe from all Email List//////////////////
	xplicitWait.until(ExpectedConditions.elementToBeClickable(myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN));
	//genericMethods.Click(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	((JavascriptExecutor)driver).executeScript("arguments[0].click();",myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_ViewEdit_BTN);
	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNY_CB,"BNY Checkbox");
	genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_BNYWH_CB,"WHS Checkbox");
	ATUReports.add("","","User has un-subscribed from Warehouse and Barneys Mail List", true, LogAs.INFO);
	Assert.assertEquals(reuseCodeForEMailPreferences(driver), "");
	}
	
		public String reuseCodeForEMailPreferences(WebDriver driver){
			WebDriverWait xwait = new WebDriverWait(driver, 30);
			
			genericMethods.Click(driver, myAccountEmailPreferencesObject.EmailPreferencesPage_EmailList_Update_BTN,"Email Update BTN");
			GenericMethods.waitForPageLoaded(driver);
			genericMethods.Click(driver, myAccountEmailPreferencesObject.TopHeader_MyAccount_LK,"TopHeaderMyAccount Link");
			GenericMethods.waitForPageLoaded(driver);
		
		xwait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='col-xs-12 col-sm-6 col-md-6 col-lg-4 overview-emailprefs']/h2")));
	
		subScribedEmailList=genericMethods.getText(driver, myAccountEmailPreferencesObject.MyAccount_Home_EmailPreferences_SubscribedEmailList_TV);
		System.out.println(subScribedEmailList);
		if (subScribedEmailList.equalsIgnoreCase("")) {
			subScribedEmailList="";
			ATUReports.add("Verify Users current Email Subscription", "No Current Email Subscription:  ",true,LogAs.INFO);		
		}else if(subScribedEmailList.equalsIgnoreCase("You have no email preferences.")){
			subScribedEmailList="";
			ATUReports.add("Verify Users current Email Subscription", "No Current Email Subscription:  ",true,LogAs.INFO);		
		}else{
			ATUReports.add("Verify Users current Email Subscription", "Currently Subscribed Email List is : "+ subScribedEmailList,true,LogAs.INFO);
		}
		return subScribedEmailList;
			}
	}
