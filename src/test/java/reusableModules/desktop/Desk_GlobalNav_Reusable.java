package reusableModules.desktop;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import setup.MultipleBrowser;
import PageObjects.desktop.Desk_BNY_CategoryLandingPageObject;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_GlobalNavPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_GlobalNav_Reusable extends MultipleBrowser {

 WebDriverWait xplicitWait;
 Desk_BNY_HomePageObject homePageObj;
 Desk_BNY_GlobalNavPageObject globalNavObj;
 Desk_BNY_DesignerPageObject designerPageObj;
 String windowHandle;
 boolean isGlobalNavPresent;
 static Desk_BNY_NavigationPageObject GlobalNav_Objects;
 static Desk_BNY_CategoryLandingPageObject bnyCategoryLandingObject;
 static GenericMethods genericMethods;
 String topCategoryText;
 String TopelementMouseHover;
 boolean Flyout;

 public void categoryClick(WebDriver driver, String categoryName) {
  globalNavObj = PageFactory.initElements(driver, Desk_BNY_GlobalNavPageObject.class);
  homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
  genericMethods = new GenericMethods();
  isGlobalNavPresent = genericMethods.isElementPresentforMouseHover(homePageObj.HomePage_SiteHeader_TopNav_Elements);

  //***************************To verify if the global nav is visible OR not.*****************************************************	
  if (isGlobalNavPresent = true) {
   topCategoryText = "//ul[@id='topnav-level-1']/li/a[contains(text(),'" + categoryName + "')]";
   WebElement TopelementMouseHover = driver.findElement(By.xpath(topCategoryText));
   genericMethods.mouseHoverJScript(driver, TopelementMouseHover);

   //**************************To verify flyout is displayes when mouse hovered on top category************************************
   Flyout = genericMethods.isElementVisible(driver, By.xpath("//div[@class='flyout-inner-left featured-margin-right']"));

   if (Flyout = true) {
    ATUReports.add("Verify if the flyout is displayed.", "Flyout on mouser hover should be displayed", "Flyout on mouse hover is displayed", true, LogAs.PASSED);
   } else {
    ATUReports.add("Verify if the flyout is displayed.", "Flyout on mouser hover should be displayed", "Flyout on mouse hover is not displayed", true, LogAs.FAILED);
    Assert.assertEquals(Flyout, true, "Flyout is not displayed");
   }

   //ATUReports.add("Verify " + categoryName + " is display", categoryName + " category should be display", categoryName + " category is display", true,LogAs.PASSED);
   genericMethods.ClickUSingXpath(driver, "//ul[@id='topnav-level-1']/li/a[contains(text(),'" + categoryName + "')]", "Category Name");

   //**************************To Verify if the category landing page is open and then write the ATU step************************
   ATUReports.add("Verify " + categoryName + " is clicked", categoryName + " category should be clicked", categoryName + " category is clicked", true, LogAs.PASSED);

   if (genericMethods.getText(driver, globalNavObj.TopCategory_In_BreadCrumb_LeftNav_LK).equalsIgnoreCase(categoryName)) {
    ATUReports.add("Verify " + categoryName + " Landing page should open", categoryName + " category landing page should be opened", categoryName + " category landing page is opened", true, LogAs.PASSED);
   } else {
    ATUReports.add("Verify  " + categoryName + " Landing page should open", categoryName + " category landing page should be opened", categoryName + " category landing page is not opened", true, LogAs.FAILED);
   }
  } else {
   ATUReports.add("Verify Global Nav is not present", "Global Nav should be present", "Global Nav is ", true, LogAs.FAILED);
   Assert.assertEquals(isGlobalNavPresent, true);
  }
 }

 public void clickSubCategoryName(WebDriver driver, String categoryName, String subCategoryName) {
  globalNavObj = PageFactory.initElements(driver, Desk_BNY_GlobalNavPageObject.class);
  genericMethods = new GenericMethods();
  xplicitWait = new WebDriverWait(driver, 30);
	
  topCategoryText = "//div[@id='top-nav']/ul[@id='topnav-level-1']/li/a[contains(text(),'" + categoryName + "')]";
  WebElement elementToClick = driver.findElement(By.xpath(topCategoryText));
  genericMethods.mouseHoverJScript(driver, elementToClick);
  //genericMethods.javascriptClick(driver, "//div/ul[@class='sub_category topnav-level-2']//li/a[contains(text(),'" + subCategoryName + "')]", "Sub Category");
  genericMethods.javascriptClick(driver, "//ul[@class='sub_category topnav-level-2']/li/a[contains(text(),'" + subCategoryName + "')]", "Sub Category");

 }


 public void globalNav(WebDriver driver, WebElement locator) {

  homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
  globalNavObj = PageFactory.initElements(driver, Desk_BNY_GlobalNavPageObject.class);
  WebElement clickGloballNavElements;
  //boolean isGlobalNavPresent = genericMethods.isElementPresentforMouseHover(homePageObj.HomePage_SiteHeader_TopNav_Elements);

  if (isGlobalNavPresent = true) {
   List < WebElement > listOfTopCategories = (List < WebElement > ) driver.findElement(By.id("topnav-level-1")).findElements(By.tagName("li"));
   List < WebElement > listOfParentCategories = (List < WebElement > ) driver.findElement(By.cssSelector("ul.sub_category.topnav-level-2")).findElements(By.tagName("li"));
   for (WebElement listElements: listOfTopCategories) {
    genericMethods.mouseHover(driver, listElements, "List Elements");
    for (WebElement categoryLists: listOfTopCategories) {
     genericMethods.Click(driver, locator, "Locator");
     for (int i = 1; i <= listOfParentCategories.size(); i++) {
      clickGloballNavElements = driver.findElement(By.xpath("//ul[@class='sub_category topnav-level-2']/li[" + i + "]"));
      xplicitWait = new WebDriverWait(driver, 20);
      xplicitWait.until(ExpectedConditions.visibilityOf(clickGloballNavElements));
      genericMethods.Click(driver, clickGloballNavElements, "Global Nav Elements");
      genericMethods.mouseHover(driver, listElements, "List Elements");
     }
    }
   }
  }
 }


 public void CategoryMouseHover(WebDriver driver, String mainCategory) {
  genericMethods = new GenericMethods();
  GlobalNav_Objects = PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);

  switch (mainCategory) {
   case "Women":
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_Women_LK, "Top Women Link");
    ATUReports.add("Women Navigation in expanded form", true, LogAs.INFO);
    break;

   case "New Arrivals":
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_NewArrivals_LK, "Top New Arrivals Link");
    ATUReports.add("New Arrival Navigation in expanded form", true, LogAs.INFO);
    break;

   case "Men":
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_Men_LK, "Top Men Link");
    ATUReports.add("Men Navigation in expanded form", true, LogAs.INFO);
    break;

   case "Beauty":
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_Beauty_LK, "Top Beauty Link");
    ATUReports.add("Beauty Navigation in expanded form", true, LogAs.INFO);
    break;

   case "Home":
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_Home_LK, "Top Home Link");
    ATUReports.add("Home Navigation in expanded form", true, LogAs.INFO);
    break;

   case "Kids":
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_Kids_LK, "Top Kids Link");
    ATUReports.add("Kids Navigation in expanded form", true, LogAs.INFO);
    break;

   case "Editorial":
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_Editorial_LK, "Top Editorial Link");
    ATUReports.add("Home Navigation in expanded form", true, LogAs.INFO);
    break;

   default:
    genericMethods.mouseHover(driver, GlobalNav_Objects.TopNavigation_Women_LK, "Top Women Link");
    ATUReports.add("Women Navigation in expanded form", true, LogAs.INFO);
    break;
  }
 }

 public static void CategoryClick(WebDriver driver, String mainCategory) {
  genericMethods = new GenericMethods();
  GlobalNav_Objects = PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);

  switch (mainCategory) {
   case "Women":
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Women_LK, "Top Women Link");
    ATUReports.add("Women Navigation in expanded form", true, LogAs.INFO);

   case "New Arrivals":
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_NewArrivals_LK, "Top New Arrival Link");
    ATUReports.add("New Arrival Navigation in expanded form", true, LogAs.INFO);

   case "Men":
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Men_LK, "Top Men Link");
    ATUReports.add("Men Navigation in expanded form", true, LogAs.INFO);

   case "Beauty":
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Beauty_LK, "Top Beauty Link");
    ATUReports.add("Beauty Navigation in expanded form", true, LogAs.INFO);

   case "Home":
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Home_LK, "Top Home Link");
    ATUReports.add("Home Navigation in expanded form", true, LogAs.INFO);

   case "Kids":
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Kids_LK, "Top Kids Link");
    ATUReports.add("Kids Navigation in expanded form", true, LogAs.INFO);

   case "Editorial":
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Editorial_LK, "Top Editorial Link");
    ATUReports.add("Home Navigation in expanded form", true, LogAs.INFO);

   default:
    genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Women_LK, "Top Women Link");
    ATUReports.add("Women Navigation in expanded form", true, LogAs.INFO);
    break;
  }
 }

 public static void NewArrivalsMouseHover(WebDriver driver) {
  //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
  GlobalNav_Objects = PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);

 }

 public static void SearchClick(WebDriver driver) {
	  GlobalNav_Objects = PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
	  genericMethods.Click(driver, GlobalNav_Objects.TopNavigation_Search_LK, "Top Search Link");
	  ATUReports.add("Check The product", "Valid Product Found", true, LogAs.INFO);
 }

 public void clickSubSubCategoryName(WebDriver driver, String categoryName, String subsubCategoryName) {
	  globalNavObj = PageFactory.initElements(driver, Desk_BNY_GlobalNavPageObject.class);
	  genericMethods = new GenericMethods();
	  xplicitWait = new WebDriverWait(driver, 30);
	
	  //String topCategoryText = "//id('topnav-level-1')/li/a[contains(text(),'" + categoryName + "')]";	
	  topCategoryText = "//div[@id='top-nav']/ul[@id='topnav-level-1']/li/a[contains(text(),'" + categoryName + "')]";
	  WebElement elementToClick = driver.findElement(By.xpath(topCategoryText));
	  genericMethods.mouseHoverJScript(driver, elementToClick);
	  genericMethods.ClickUSingXpath(driver, "//div[@class='flyout']//ul[@class='sub_category topnav-level-3']/li/a[contains(text(),'" + subsubCategoryName + "')]", "Sub Category");
	
 }

 public void clickDesignerName(WebDriver driver, String categoryName, String subCategoryName) {
	  globalNavObj = PageFactory.initElements(driver, Desk_BNY_GlobalNavPageObject.class);
	  designerPageObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	  genericMethods = new GenericMethods();
	  xplicitWait = new WebDriverWait(driver, 30);
	
	  //String topCategoryText = "//id('topnav-level-1')/li/a[contains(text(),'" + categoryName + "')]";	
	  topCategoryText = "//div[@id='top-nav']/ul[@id='topnav-level-1']/li/a[contains(text(),'" + categoryName + "')]";
	  WebElement elementToClick = driver.findElement(By.xpath(topCategoryText));
	  genericMethods.mouseHoverJScript(driver, elementToClick);
	  genericMethods.javascriptClick(driver, "//div/ul[@class='sub_category topnav-level-2']//li/a[contains(text(),'" + subCategoryName + "')]", "Sub Category");
	  xplicitWait.until(ExpectedConditions.visibilityOf(designerPageObj.Designer_Banner));
	
	  if (genericMethods.isElementVisible(driver, By.xpath("//div[@class='designer-banner']"))) {
	   ATUReports.add("Verify " + subCategoryName + " designer landing page should open", subCategoryName + " designer landing page should be opened", subCategoryName + " designer landing page is opened", true, LogAs.PASSED);
	  } else {
	   ATUReports.add("Verify " + subCategoryName + " designer landing page should open", subCategoryName + " designer landing page should be opened", subCategoryName + " designer landing page is not opened", true, LogAs.FAILED);
	  }
	 }



}