package reusableModules.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import setup.MultipleBrowser;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import reusableModules.desktop.Desk_LoginRegister_Reusable;

public class Desk_MyDesigner_Reusable extends MultipleBrowser {
	Desk_LoginRegister_Reusable login;
	Desk_BNY_NavigationPageObject TopNav;
	Desk_BNY_DesignerPageObject myDesignerObj;
	Desk_BNY_HomePageObject homePageObj;
	Desk_GlobalNav_Reusable globalNavReusable;
	WebDriverWait xplicitWait ;
	String CurrentPageTitle;
	String designersAdded;
	Desk_BNY_RegistrationPageObject Register_Objects;
	GenericMethods genericMethods;	

	public void DesignerSelect(WebDriver driver, String DesignerName) {
		
		homePageObj = PageFactory.initElements(driver,Desk_BNY_HomePageObject.class);
		myDesignerObj = PageFactory.initElements(driver,Desk_BNY_DesignerPageObject.class);		  
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		String DesignerAddedName;
		
		String DesignerPage= genericMethods.isElementPresent(driver, "//ul[@id='designer-list']/li//li");//section[@class='account-top']/a			
		if (DesignerPage.equalsIgnoreCase("notpresent")){
			genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"LogIn User My Account Dropdown Link");		
			genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");
		}
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
		//genericMethods.Click(driver, myDesignerObj.AddDesigner_BTN,"");
		genericMethods.inputValue(driver, myDesignerObj.MyDesigner_Search_TB, DesignerName,"MyDesigners Search TB");
		genericMethods.Click(driver, myDesignerObj.DesignerCheckbox_CB,"Designers CheckBox");
		
		DesignerAddedName=genericMethods.getText(driver, myDesignerObj.DesignerAdded_LB);	
		if (DesignerAddedName.equalsIgnoreCase(DesignerName)) {
			ATUReports.add("Verify Designer added",  DesignerName+ " designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Designer added",  DesignerName +" designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
			Assert.assertEquals(DesignerAddedName, DesignerName);
		}
	}
		
	public void addAndRemoveDesignerFromMyDesigners(WebDriver driver, String designerAlphaLabel)	{
		
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class); 
		myDesignerObj= PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
		
		genericMethods = new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		//xplicitWait.until(ExpectedConditions.visibilityOf(homePageObj.Home_TopNav_HiUsername_LK));
		boolean isHiUsernamePresent = genericMethods.isElementPresentforMouseHover(homePageObj.Home_UtilityNav_LoggedInUser_LK);
		System.out.println(isHiUsernamePresent);
		if (isHiUsernamePresent == true) {
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"TopNav_HiUsername Link");
		xplicitWait = new WebDriverWait(driver, 10);		
		
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"My Designer Link");
		xplicitWait = new WebDriverWait(driver, 10);
		
		xplicitWait.until(ExpectedConditions.visibilityOf(myDesignerObj.MyDesigner_DesignersAlphabets_Labels));
		genericMethods.ClickUSingXpath(driver, "//div[@class='row']//ul//li//a[contains(text(),'All')]", "Selects All designer");
		
		genericMethods.ClickUSingXpath(driver, "//ul[@class='edit-designer-list']//span[contains(text(),'" + designerAlphaLabel +"' )]","desinger name");
	
		xplicitWait = new WebDriverWait(driver, 20);
		
		boolean isFirstDesignerPresent = genericMethods.isElementPresentforMouseHover(myDesignerObj.MyDesigner_FirstDesignerInList_LK);
		System.out.println("isFirstDesignerPresent" + isFirstDesignerPresent);
		if (isFirstDesignerPresent = true) {
			xplicitWait = new WebDriverWait(driver, 10);
			xplicitWait.until(ExpectedConditions.visibilityOf(myDesignerObj.MyDesigner_DesignersList));
			xplicitWait = new WebDriverWait(driver, 30);
			ATUReports.add("Verify First Designer in the List is present", "The First Designer in the List should be present", "The First Designer in the List is present", true,LogAs.PASSED);
			Assert.assertEquals(isFirstDesignerPresent, true);
		}
		else{
			ATUReports.add("Verify First Designer in the List is present", "The First Designer in the List should be present", "The First Designer in the List is not present", true,LogAs.FAILED);
			Assert.assertEquals(isFirstDesignerPresent, true);
		}
		
		WebElement designerChecked = driver.findElement(By.xpath("//ul[@class='edit-designer-list']//span[contains(text(),'" + designerAlphaLabel +"' )]"));
		designersAdded = genericMethods.getText(driver, myDesignerObj.MyDesigner_AddedFirstDesigner_Name);
		String designerCheckedText = genericMethods.getText(driver, designerChecked);
		System.out.println(designersAdded + "***" + designerCheckedText);
		if (designersAdded.contains(designerCheckedText)) {
			ATUReports.add("Verify Selected Designer " + designerCheckedText + " is added", "Selected Designer " + designerCheckedText + " should be added", "Selected Designer " + designerCheckedText + " is added", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Selected Designer " + designerCheckedText + " is added", "Selected Designer " + designerCheckedText + " should be added", "Selected Designer " + designerCheckedText + " is not added", true,LogAs.FAILED);
			Assert.assertEquals(designersAdded, designerCheckedText);
		}
	 }
		
		boolean isClearAllDesignersLinkPresent = genericMethods.isElementPresentforMouseHover(myDesignerObj.MyDesigner_ClearAllDesigners_LK);
		//designersAdded = genericMethods.getText(driver, myDesignerObj.MyDesigner_AddedDesigner_Names_Space);
		if (isClearAllDesignersLinkPresent == true) {
			ATUReports.add("Verify Clear All Designers link is present", "Clear All Designers link should be present", "Clear All Designers link is present", true,LogAs.PASSED);
			xplicitWait.until(ExpectedConditions.visibilityOf(myDesignerObj.MyDesigner_ClearAllDesigners_LK));
			genericMethods.Click(driver, myDesignerObj.MyDesigner_ClearAllDesigners_LK,"ClearAllDesigner Link");
			genericMethods.Click(driver, myDesignerObj.MyDesigner_DeleteAllDesignersPopUp_YES_BTN,"DeleteAllDesignersPopUp_Yes_BTN");
			xplicitWait = new WebDriverWait(driver, 10);
		}else {
			ATUReports.add("Verify Clear All Designers link is present", "Clear All Designers link should be present", "Clear All Designers link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isClearAllDesignersLinkPresent, true);
		}
		GenericMethods.Waitformilliseconds(2000);
		String designersAddedBlankSpace = genericMethods.getText(driver, myDesignerObj.MyDesigner_AddedDesigner_Names_Space);
		if (designersAddedBlankSpace.isEmpty()) {
			ATUReports.add("Verify All Designers are deleted", "All Designers should be deleted", "All Designers are deleted", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify All Designers are deleted", "All Designers should be deleted", "All Designers are not deleted", true,LogAs.FAILED);
			Assert.assertEquals(designersAddedBlankSpace, true);
		}
	}
	
	public void searchAndAddDesignerFromMyDesigners(WebDriver driver, String designerTerm)	{
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class); 
		myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
		genericMethods = new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);		
		
		//xplicitWait.until(ExpectedConditions.visibilityOf(homePageObj.Home_TopNav_HiUsername_LK));
		boolean isHiUsernamePresent = genericMethods.isElementPresentforMouseHover(homePageObj.Home_UtilityNav_LoggedInUser_LK);
		System.out.println(isHiUsernamePresent);
		if (isHiUsernamePresent == true) {
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"TopNav_HiUsername Link");
		xplicitWait = new WebDriverWait(driver, 10);
		
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"MyDesigner Link");
		xplicitWait = new WebDriverWait(driver, 10);
		xplicitWait.until(ExpectedConditions.visibilityOf(myDesignerObj.MyDesigner_Search_TB));
		
		genericMethods.inputValue(driver, myDesignerObj.MyDesigner_Search_TB, designerTerm,"Designer Search Textbox");
		
		GenericMethods.Waitformilliseconds(2000);
		genericMethods.Click(driver, myDesignerObj.MyDesigner_SearchedDesigner,"Searched Designer");
		
		GenericMethods.Waitformilliseconds(2000);
		designersAdded = genericMethods.getText(driver, myDesignerObj.MyDesigner_AddedFirstDesigner_Name);

		if (designersAdded.equalsIgnoreCase(designerTerm)) {
			ATUReports.add("Verify searched designer" + designerTerm +"is added", "Searched designer" + designerTerm + "should be added", "Searched designer" + designerTerm + "is added", true,LogAs.PASSED);
		}
		else {
			ATUReports.add("Verify searched designer" + designerTerm +"is added", "Searched designer" + designerTerm + "should be added", "Searched designer" + designerTerm + "is not added", true,LogAs.FAILED);
			Assert.assertEquals(designersAdded, designerTerm);
		}
	}
}
		
	public void deleteDesignersFromMyDesigners(WebDriver driver,GenericMethods genericMethods) {
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class); 
		myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
		
		genericMethods.Click(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK,"Utlity nav logged in Username");
		xplicitWait = new WebDriverWait(driver, 10);
		
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"My Favorites Left Nav link");
		xplicitWait = new WebDriverWait(driver, 10);
		
		boolean isClearAllDesignersLinkPresent = genericMethods.isElementPresentforMouseHover(myDesignerObj.MyDesigner_ClearAllDesigners_LK);
		designersAdded = genericMethods.getText(driver, myDesignerObj.MyDesigner_AddedDesigner_Names_Space);
		if (isClearAllDesignersLinkPresent == true) {
			ATUReports.add("Verify Clear All Designers link is present", "Clear All Designers link should be present", "Clear All Designers link is present", true,LogAs.PASSED);
			genericMethods.Click(driver, myDesignerObj.MyDesigner_ClearAllDesigners_LK,"My Designers Clear All Designers Link");
			genericMethods.Click(driver, myDesignerObj.MyDesigner_DeleteAllDesignersPopUp_YES_BTN,"Delete Designers Popup YES button");
			xplicitWait = new WebDriverWait(driver, 10);
		}else {
			ATUReports.add("Verify Clear All Designers link is present", "Clear All Designers link should be present", "Clear All Designers link is not present", true,LogAs.FAILED);
			Assert.assertEquals(isClearAllDesignersLinkPresent, true);
		}		
		if (designersAdded == "") {
			ATUReports.add("Verify All Designers are deleted", "All Designers should be deleted", "All Designers are deleted", true,LogAs.PASSED);
			Assert.assertEquals(designersAdded, "");
		}else {
			ATUReports.add("Verify All Designers are deleted", "All Designers should be deleted", "All Designers are not deleted", true,LogAs.FAILED);
			Assert.assertEquals(designersAdded, "");
		}
	}
	
	 public void DesignerDelete(WebDriver driver) {		
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);		  
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		boolean DesignerList;
	
		genericMethods.Click(driver, myDesignerObj.MyDesigner_ClearAllDesigners_LK,"My Designers Clear All Designers Link");
		genericMethods.Click(driver, myDesignerObj.ClearDesignerYes_BTN,"Delete Designers Popup YES button");
		xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//section[@class='designer-options']")));//section[@class='account-top']/a
		GenericMethods.Waitformilliseconds(3000);
		//DesignerList=genericMethods.isElementVisible(driver, By.xpath("//ul[@id='designer-list']/li//li"));
		DesignerList=genericMethods.isElementPresentforMouseHover(myDesignerObj.AddedDesignerlist);		
		if (DesignerList==false){ 
			ATUReports.add("Verify All Designers are deleted","All Designers should be deleted", "All Designers sucessfully deleted",true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify All Designers are deleted","All Designers Should be deleted", "All Designers not deleted",true,LogAs.FAILED);
			}	
	}
	
	public void DesignerDLP(WebDriver driver,GenericMethods genericMethods)  {
		
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
		TopNav=PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		Register_Objects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);		  
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String guestuser;
		String DesignerAddedName;
		
		genericMethods.Click(driver, myDesignerObj.DesignerstartS_LK,"Designers List for S");
		genericMethods.Click(driver, myDesignerObj.DesignerSaintLaurent_LK,"Designer Saint Laurent Link");
		genericMethods.Click(driver, myDesignerObj.DLPGuestAddToMyDesigners_BTN,"DLP Add to my Designer button guest user");
		genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");				
		guestuser=genericMethods.isElementPresent(driver, "//span[@class='icon dropdown-toggle hidden-xs dropdown-button']");//b[@class='caret']
		
		if(guestuser=="notpresent")	{
			genericMethods.Click(driver, homePageObj.BNY_Popup_Register_LK,"Top Nav Register Link");			
			xplicitWait.until(ExpectedConditions.visibilityOfElementLocated((By) Register_Objects.RegisterPage_RegisterPanel_Email_TB));
			genericMethods.inputValue(driver, Register_Objects.RegisterPage_RegisterPanel_Email_TB, "l17@barneys.com","Register Email Textbox");
			genericMethods.inputValue(driver, Register_Objects.RegisterPage_RegisterPanel_Password_TB, "12345678","Register Password Textbox");
			genericMethods.inputValue(driver, Register_Objects.RegisterPage_RegisterPanel_ConfirmPassword_TB, "12345678","Register Confirm Password Textbox");
			ATUReports.add("Login Popup",true, LogAs.INFO);
			genericMethods.Click(driver, Register_Objects.RegisterPage_RegisterPanel_LogIN_BTN,"Register Login Button");
		
			if ((genericMethods.getText(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK)).equalsIgnoreCase("LOG IN")) {
				ATUReports.add("User should be registered successfully", "Unable to Register",true,LogAs.FAILED);
				Assert.assertNotEquals((genericMethods.getText(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK)), "LOG IN");			
			}else{	
				ATUReports.add("User should be registered successfully", "Registration sucessful ",true,LogAs.PASSED);
				Assert.assertNotEquals((genericMethods.getText(driver, homePageObj.Home_UtilityNav_LoggedInUser_LK)), "LOG IN");					
			}	
	}
		genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");		
		genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");			
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
	
		DesignerAddedName=genericMethods.getText(driver, myDesignerObj.DesignerAdded_LB);
	
		if ((genericMethods.checkEqual(DesignerAddedName, "Saint Laurent"))) {
			ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
			Assert.assertEquals(DesignerAddedName, "Saint Laurent");
		}	
	}

public void DesignerSelectCheckBox(WebDriver driver)  {
	
	homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	String DesignerAddedName,DesignerSelected;
	  
	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");
	genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");	
	genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
	//genericMethods.Click(driver, myDesignerObj.AddDesigner_BTN,"");
	genericMethods.Click(driver, myDesignerObj.DesignerCheckbox_CB,"Designers Checkbox");
	
	
	DesignerAddedName=genericMethods.getText(driver, myDesignerObj.DesignerAdded_LB);
	DesignerSelected=genericMethods.getText(driver, myDesignerObj.DesignerName_CB);
	if ((genericMethods.checkEqual(DesignerAddedName, DesignerSelected))) {
		ATUReports.add("Verify Designer added",  DesignerSelected+" designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer added",  DesignerSelected+"  designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.INFO);
		Assert.assertEquals(DesignerAddedName, DesignerSelected);
	}
}

public void DesignerUnselectCheckBox(WebDriver driver)  {
	
	homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	  
	genericMethods =  new GenericMethods();
	String DesignerPresent;
	xplicitWait = new WebDriverWait(driver, 30);
	genericMethods.Click(driver, myDesignerObj.DesignerCheckbox_CB,"Designers Checkbox");
	GenericMethods.Waitformilliseconds(1000);
		
	DesignerPresent=genericMethods.isElementPresent(driver,"//ul[@id='designer-list']/li/ul/li/a");
	if ((genericMethods.checkEqual(DesignerPresent, "notpresent"))) {
		ATUReports.add("Verify Designer added",  "Designer should be delete", "Designer is deleted", true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer added",  "Designer should be deleted", "Designer added is not deleted", true,LogAs.FAILED);
		Assert.assertEquals(DesignerPresent, "notpresent");
	}
}

public void DesignerAddUsingDLPTopNav(WebDriver driver,String Category,String SubCategory)  {
	
	homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	globalNavReusable= new Desk_GlobalNav_Reusable();
	  
	genericMethods =  new GenericMethods();
	String DesignerAddedName;
	xplicitWait = new WebDriverWait(driver, 30);

	globalNavReusable.CategoryMouseHover(driver, Category);
	globalNavReusable.clickSubCategoryName(driver,Category,SubCategory);	
	
	genericMethods.Click(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN,"Add to my Designer button logged in user");
	GenericMethods.Waitformilliseconds(5000);
	String DesignerButtonName= genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	if(DesignerButtonName.equalsIgnoreCase("In My Designers")||DesignerButtonName.equalsIgnoreCase("Added")){
		ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name changes to 'In My Designers'", true,LogAs.PASSED);			
	}else{
		ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name does not change to 'In My Designers'", true,LogAs.FAILED);	
		Assert.assertEquals(DesignerButtonName, "In My Designer");	
	}	
	genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");	
	genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");
	genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
	
	DesignerAddedName=genericMethods.getText(driver, myDesignerObj.DesignerAdded_LB);
	if ((genericMethods.checkEqual(DesignerAddedName, "Saint Laurent"))) {
		ATUReports.add("Verify Designer added",  "'Saint Laurent' designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer added",  "Saint Laurent' designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
		Assert.assertEquals(DesignerAddedName, "Saint Laurent");
	}
}

	public void DesignerRemoveUsingDLPTopNav(WebDriver driver,String Category,String SubCategory)  {
	
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	  
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		globalNavReusable.CategoryMouseHover(driver, Category);
		globalNavReusable.clickSubCategoryName(driver,Category,SubCategory);	
		
		genericMethods.mouseHover(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN,"Add to my Designer button logged in user");
		new Actions(driver).moveToElement(myDesignerObj.DLPLoggedinAddToMyDesigners_BTN).perform();
		String DesignerButtonName=  driver.findElement(By.xpath("//div[@class='add-button']/a")).getText();//div[@class='in-store-buttons']/a
		
		System.out.println(DesignerButtonName);
		if(DesignerButtonName.equalsIgnoreCase("In My Designers")||DesignerButtonName.equalsIgnoreCase("Remove from My Designers")){	
			ATUReports.add("Verify on hover In My Designers button name changes to Remove from My Designers",  " Button name should change to 'Remove from My Designers'",  " Button name changes to 'Remove from My Designers'", true,LogAs.PASSED);			
		}else{
			ATUReports.add("Verify on hover In My Designers button name changes to Remove from My Designers",  " Button name should change to 'Remove from My Designers'",  " Button name does not change to 'Remove from My Designers'", true,LogAs.FAILED);	
			Assert.assertEquals(DesignerButtonName, "Remove from My Designers");	
		}
	
		GenericMethods.Waitformilliseconds(5000);
		genericMethods.Click(driver, myDesignerObj.DLPLoggedinRemoveDesigner_BTN,"Remove Designer button logged in user");
		GenericMethods.Waitformilliseconds(5000);
		
		DesignerButtonName=  genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	 
		if(DesignerButtonName.equalsIgnoreCase("Add to My Designers")){		
			ATUReports.add("Verify  button name changes to Add to My Designers",  " Button name should change to 'Add to My Designers'",  " Button name changes to 'Add to My Designers'", true,LogAs.PASSED);				
		}else{
			ATUReports.add("Verify  button name changes to Add to My Designers",  " Button name should change to 'Add to My Designers'",  " Button name does not changes to 'Add to My Designers'", true,LogAs.FAILED);	
			Assert.assertEquals(DesignerButtonName, "Add to My Designers");		
		}
		
		genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");	
		genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");	
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
		GenericMethods.Waitformilliseconds(5000);
		boolean DesignerList;
		DesignerList=genericMethods.isElementPresentforMouseHover(myDesignerObj.AddedDesignerlist);		
		if (DesignerList==false){ 
			ATUReports.add("Verify All Designers are deleted","All Designers should be deleted", "All Designers sucessfully deleted",true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify All Designers are deleted","All Designers Should be deleted", "All Designers not deleted",true,LogAs.FAILED);
			}	
	}

public void GuestDesignerAddUsingDLPSearch(WebDriver driver,String Email, String Password)  {
	
	homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	login=new Desk_LoginRegister_Reusable();
	String LoginPage;
	String DesignerButton;
	
	genericMethods.Click(driver, myDesignerObj.DLPGuestAddToMyDesigners_BTN,"DLP Add to my Designer button guest user");
	
	LoginPage=genericMethods.isElementPresent(driver,"//div[@id='atg_store_accountLogin']");
	if (LoginPage=="present"){
		ATUReports.add("Verify Login Page is displayed ",  "Login page  should be displayed", "Login page  is displayed", true,LogAs.PASSED);
	}else{
		ATUReports.add("Verify  Login Page  is displayed ",  " Login Page  should be displayed", " Login Page is not displayed", true,LogAs.FAILED);			
	}
	
	login.generalLogin1(driver, Email);
	
	DesignerButton=genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	
	if (DesignerButton.equalsIgnoreCase("In My Designers")||DesignerButton.equalsIgnoreCase("Remove from My Designers")) {
		ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is added", true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is not added", true,LogAs.FAILED);
		Assert.assertEquals(DesignerButton, "In My Designers");
	}	

}

public void DesignerRemoveUsingDLP(WebDriver driver)  {
	
	homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	
	genericMethods.Click(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN,"Add to my Designer button logged in user");
	GenericMethods.Waitformilliseconds(5000);
	String DesignerButtonName=  genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	 
		if(DesignerButtonName.equalsIgnoreCase("Add to My Designers"))
		{	
			ATUReports.add("Verify  button name changes to Add to My Designers",  " Button name should change to 'Add to My Designers'",  " Button name changes to 'Add to My Designers'", true,LogAs.PASSED);	
		}
		else
		{
			ATUReports.add("Verify  button name changes to Add to My Designers",  " Button name should change to 'Add to My Designers'",  " Button name does not changes to 'Add to My Designers'", true,LogAs.FAILED);	
			Assert.assertEquals(DesignerButtonName, "Add to My Designers");
		
		}	
		genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");
		genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");	
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
		GenericMethods.Waitformilliseconds(5000);
		String AddDesignerButton=genericMethods.isElementPresent(driver, "//section[@class='account-top designer-list']");
		if (AddDesignerButton.equalsIgnoreCase("present")) {
			ATUReports.add("Verify Designer is removed fom My Account",  "Designer should be removed from My Designers", "Designer is removed from My Designers", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Designer is removed fom My Account",  "Designer should be removed from My Designers", "Designer is not removed from My Designers", true,LogAs.FAILED);
			Assert.assertEquals(AddDesignerButton, "present");
		}
}

	public void GuestAddDesignersfromAllDesignerDLP(WebDriver driver,String Email, String Password)  {
	
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
		TopNav=PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
		Register_Objects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);  
	  
		genericMethods =  new GenericMethods();
		login=new Desk_LoginRegister_Reusable();
		String DesignerButton;
		String LoginPage;
		String DesignerAddedName;
		xplicitWait = new WebDriverWait(driver, 30);

		genericMethods.Click(driver, myDesignerObj.DesignerstartS_LK,"Designers List for S");
		genericMethods.Click(driver, myDesignerObj.DesignerSaintLaurent_LK,"Designer Saint Laurent Link");
		genericMethods.Click(driver, myDesignerObj.DLPGuestAddToMyDesigners_BTN,"DLP Add to my Designer button guest user");

		LoginPage=genericMethods.isElementPresent(driver,"//div[@id='atg_store_accountLogin']");
	
		if (LoginPage=="present"){
			ATUReports.add("Verify Login Page is displayed ",  "Login page  should be displayed", "Login page  is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify  Login Page  is displayed ",  " Login Page   should be displayed", " Login Page   is not displayed", true,LogAs.FAILED);
		}
		
		login.generalLogin1(driver, Email);
		
		DesignerButton=genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
		
		if (DesignerButton.equalsIgnoreCase("In My Designers")||DesignerButton.equalsIgnoreCase("Remove from My Designers")) {
			ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is added", true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Designer is added",  " designer should be added", "Designer is not added", true,LogAs.FAILED);
			Assert.assertEquals(DesignerButton, "In My Designers");
		}	
		genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");		
		genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
	
		DesignerAddedName=genericMethods.getText(driver, myDesignerObj.DesignerAdded_LB);

		if ((genericMethods.checkEqual(DesignerAddedName, "Saint Laurent"))) {
			ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
			Assert.assertEquals(DesignerAddedName, "Saint Laurent");
		}	

}
	public void GuestDesignerRemoveUsingDLPAllDesigners(WebDriver driver)  {
	
		homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		genericMethods.Click(driver, myDesignerObj.DesignerstartS_LK,"Designers List for S");
		genericMethods.Click(driver, myDesignerObj.DesignerAdded_LB,"Added Designers Label");
		
		genericMethods.Click(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN,"Add to my Designer button logged in user");
		GenericMethods.Waitformilliseconds(9000);
		String DesignerButtonName=  genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	 
		if(DesignerButtonName.equalsIgnoreCase("Add to My Designers")){		
			ATUReports.add("Verify  button name changes to Add to My Designers",  " Button name should change to 'Add to My Designers'",  " Button name changes to 'Add to My Designers'", true,LogAs.PASSED);	
		}else{
			ATUReports.add("Verify  button name changes to Add to My Designers",  " Button name should change to 'Add to My Designers'",  " Button name does not changes to 'Add to My Designers'", true,LogAs.FAILED);	
			Assert.assertEquals(DesignerButtonName, "Add to My Designers");		
		}	
		genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");	
		genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");	
		genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
		GenericMethods.Waitformilliseconds(5000);
		String DesignerList=genericMethods.isElementPresent(driver, "//ul[@id='designer-list']/li/ul/li");
		if (DesignerList.equalsIgnoreCase("present")) {
			ATUReports.add("Verify Designer is removed fom My Account",  "Designer should be removed from My Designers", "Designer is not removed from My Designers", true,LogAs.FAILED);
			Assert.assertEquals(DesignerList, "present");
		}else {
			ATUReports.add("Verify Designer is removed fom My Account",  "Designer should be removed from My Designers", "Designer is removed from My Designers", true,LogAs.PASSED);
		}
}

public void DesignerClickfromMyDesigners(WebDriver driver)  {
	
	homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	
	genericMethods.Click(driver, myDesignerObj.DesignerAdded_LB,"Added Designers Label");
	String DesignerButtonName= genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	if(DesignerButtonName.equalsIgnoreCase("In My Designers")||DesignerButtonName.equalsIgnoreCase("Added")){
		ATUReports.add("Verify DLP page is displayed",  "DLP page should be displayed",  " DLP page is displayed", true,LogAs.PASSED);			
	}else{
		ATUReports.add("Verify DLP page is displayed",  " DLP page should be displayed",  " DLP page is not displayed", true,LogAs.FAILED);	
		Assert.assertEquals(DesignerButtonName, "In My Designer");	
	}
		
	//String DLPPresent=genericMethods.isElementPresent(driver,myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	String DLPPresent=genericMethods.isElementPresent(driver, "//div[@class='add-button']/a");
	if (DLPPresent.equalsIgnoreCase("present")) {
		ATUReports.add("Verify DLP page is displayed",  "DLP page should be displayed", "DLP page is displayed", true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify DLP page is displayed",  "DLP page should be displayed", "DLP page is not displayed", true,LogAs.FAILED);
		Assert.assertEquals(DLPPresent, "present");
		}
	
	} 

public void DesignersAddUsingDLPTopNav(WebDriver driver,String DesignerName)  {
	
	homePageObj = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	myDesignerObj = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	  
	genericMethods =  new GenericMethods();
	String DesignerAddedName;
	xplicitWait = new WebDriverWait(driver, 30);
	
	genericMethods.Click(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN,"Add to my Designer button logged in user");
	GenericMethods.Waitformilliseconds(5000);
	String DesignerButtonName= genericMethods.getText(driver, myDesignerObj.DLPLoggedinAddToMyDesigners_BTN);
	if(DesignerButtonName.equalsIgnoreCase("In My Designers")||DesignerButtonName.equalsIgnoreCase("Added")){
		ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name changes to 'In My Designers'", true,LogAs.PASSED);			
	}else{
		ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name does not change to 'In My Designers'", true,LogAs.FAILED);	
		Assert.assertEquals(DesignerButtonName, "In My Designer");	
	}	
	genericMethods.Click(driver, homePageObj.Home_TopNav_AccountArrow_LK,"Utility Nav Account dropdown arrow");	
	genericMethods.Click(driver, homePageObj.Home_TopNav_DD_MyAccount_LK,"Top Nav DD My Account");
	genericMethods.Click(driver, myDesignerObj.MyAcc_LeftNav_MyDesigner_LK,"Designers Link My Account");
	
	DesignerAddedName=genericMethods.getText(driver, myDesignerObj.DesignerAdded_LB);
	if ((genericMethods.checkEqual(DesignerAddedName,DesignerName))) {
		ATUReports.add("Verify Designer added",  "'The Row' designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer added",  "'The Row' designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
		Assert.assertEquals(DesignerAddedName, DesignerName);
		}
	}

}
