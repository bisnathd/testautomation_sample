package reusableModules.desktop;

import java.awt.event.KeyEvent;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import setup.WinAuth;
import PageObjects.desktop.Desk_BNY_CategoryLandingPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_SearchPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_SearchResultsAndFilters_Reusable {
	Desk_BNY_CategoryLandingPageObject categoryLandingPageObject;
	Desk_BNY_SearchPageObject searchPageObject;
	Desk_BNY_HomePageObject homePageObject;
	GenericMethods genericMethods;
	String isSearchHeadingPresent;
	String isSearchtextBoxPresent;
 	WebElement firstSuggestionElement;
    boolean isBNYSearchPresent;
    boolean isWHSSearchPresent;
    boolean tryANewSearchLbl;
    boolean searchSuggestion;
    boolean isTryANewSearchTBPresent;
    boolean isSearchSortByPresent;
    boolean bnyHomePage;
    boolean whsHomePage;
    String searchTitle = "Search Results For";
    WebDriverWait xplicitwait;
    String isProductPresent;
    public static int productCount;
	
	public void enterSearchTextandStartSearchBNY(WebDriver driver, String type,String searchText){		
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
		searchPageObject = PageFactory.initElements(driver, Desk_BNY_SearchPageObject.class);
		genericMethods = new GenericMethods();
        xplicitwait = new WebDriverWait(driver, 30);

		genericMethods.Click(driver,searchPageObject.BNY_HomePage_SiteHeader_Search,"MainNav Search Link");
		ATUReports.add("Click on search link and verify search text box is opened", "search text box should be opened ", "Search text box is opened", true,LogAs.INFO);
		genericMethods.inputValue(driver,searchPageObject.HomePage_Search_Box, searchText,"Search Textbox");
		xplicitwait.until(ExpectedConditions.visibilityOfElementLocated(By.id("atg_store_searchInput-sayt")));
		GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);
		WinAuth.aitoIT_AuthHandling(driver, "storefront","barneys");
		
		ATUReports.add("Enter text in search text box",  true,LogAs.INFO);
		ATUReports.add("Verify Suggestions", "Suggestions along with rich images should appear", "Suggestions are appearing", true, LogAs.INFO);
		
		
		
/*		isSearchHeadingPresent=genericMethods.isElementPresent(driver, "id='atg_store_searchInput-sayt'");
		if (isSearchHeadingPresent.equalsIgnoreCase("present")) {
			//genericMethods.Click(driver, categoryLandingPageObject.SearchSuggestions_Heading_LK,"Search Suggestion Heading Link");

			isSearchtextBoxPresent=genericMethods.isElementPresent(driver, "//input[@id='search-input']");
			if(isSearchtextBoxPresent.equalsIgnoreCase("present")){			
			genericMethods.Click(driver,searchPageObject.HomePage_Search_Box,"CLP_MainNav Search Textbox");
			GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 2);
			}
		
		} else {
			GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 2);
		} 
*/
		if (type.equalsIgnoreCase("DLP")) {
			String Expected;
			Expected =searchText.toUpperCase();
			if (genericMethods.getText(driver,categoryLandingPageObject.DLP_ImageText_TV).equalsIgnoreCase(Expected)) {
				ATUReports.add("Verify Search results", "User should be on DLP", "User is on DLP", true, LogAs.PASSED);
			} else {
				ATUReports.add("Verify Search results", "User should be on DLP", "User is not on DLP", true, LogAs.FAILED);
				Assert.assertEquals(Expected, genericMethods.getText(driver,categoryLandingPageObject.DLP_ImageText_TV));
			}
			
		}else if (type.equalsIgnoreCase("CATEGORY")) {
			String Expected;
			Expected =searchText.toUpperCase();
			if (genericMethods.getText(driver,categoryLandingPageObject.categoryLanding_pageHeading_TV).contains("FEATURED FAVORITES")) {
				ATUReports.add("Verify Search results", "User should be on Category Landing Page", "User is on Category Landing Page", true, LogAs.PASSED);
			} else {
				ATUReports.add("Verify Search results", "User should be on Category Landing Page", "User is not on Category Landing Page", true, LogAs.FAILED);
				Assert.assertEquals(Expected+"’S FEATURED FAVORITES", genericMethods.getText(driver,categoryLandingPageObject.categoryLanding_pageHeading_TV));
			}			
	} 
	else{
		String Expected;
		Expected =searchText.toUpperCase();
		//GenericMethods.waitForPageLoaded(driver);
		//xplicitwait.until(ExpectedConditions.visibilityOfElementLocated(By.id("atg_store_searchInput-sayt")));

		if (genericMethods.getText(driver,categoryLandingPageObject.Search_PageTitle_TV).equalsIgnoreCase("SEARCH RESULTS FOR \""+Expected+"\"")) {
			ATUReports.add("Verify Search results", "User should be on Search Results Page", "User is on Search Results Page", true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify Search results", "User should be on Search Results Page", "User is not on Search Results Page", true, LogAs.FAILED);
			Assert.assertEquals("SEARCH RESULTS FOR \""+Expected+"\"", genericMethods.getText(driver,categoryLandingPageObject.Search_PageTitle_TV));
		}		
	}
	}
	
	public void ZeroSearchResultpage(WebDriver driver,String searchText){
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
		searchPageObject = PageFactory.initElements(driver, Desk_BNY_SearchPageObject.class);
		genericMethods = new GenericMethods();
        xplicitwait = new WebDriverWait(driver, 40);

		genericMethods.Click(driver,searchPageObject.BNY_HomePage_SiteHeader_Search,"MainNav Search Link");
		ATUReports.add("Click on search link and verify search text box is opened", "search text box should be opened ", "Search text box is opened", true,LogAs.INFO);
		genericMethods.inputValue(driver,searchPageObject.HomePage_Search_Box,searchText ,"Search Textbox");
		xplicitwait = new WebDriverWait(driver, 40);
		GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);
		WinAuth.aitoIT_AuthHandling(driver, "storefront","barneys");
		
		//String Expected;

		//Expected =searchText.toUpperCase();
		
        xplicitwait.until(ExpectedConditions.visibilityOf(categoryLandingPageObject.zeroSearchResults_pageHeading_TV));
        if (genericMethods.getText(driver,categoryLandingPageObject.zeroSearchResults_pageHeading_TV).equalsIgnoreCase("0 SEARCH RESULTS FOR ”"+searchText+"”")){
			ATUReports.add("Verify Search results", "User should be on 0 Search Results page", "User is on 0 Search Results page", true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify Search results", "User should be on 0 Search Results page", "User is not on "+genericMethods.getText(driver,categoryLandingPageObject.zeroSearchResults_pageHeading_TV)+" page", true, LogAs.FAILED);
			//Assert.assertEquals("0 SEARCH RESULTS FOR \""+Expected+"\"", genericMethods.getText(driver, categoryLandingPageObject.zeroSearchResults_pageHeading_TV));
		}			
	}

	
	
	public void enterSearchTextandStartSearchWHS(WebDriver driver, String type,String searchText){
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
		genericMethods=new GenericMethods();
		xplicitwait = new WebDriverWait(driver, 40);
		
		genericMethods.Click(driver,categoryLandingPageObject.MainNavigation_WHS_Search_LK,"WHS Search Link");
		genericMethods.inputValue(driver,categoryLandingPageObject.MainNavigation_Search_TB, searchText,"CLP_MainNav Search Textbox");
		//genericMethods.Click(driver,categoryLandingPageObject.MainNavigation_WHS_SearchICON_LK,"WHS SearchIcon Link");
		
		GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);
		WinAuth.aitoIT_AuthHandling(driver, "storefront","barneys");
		
		WinAuth.aitoIT_AuthHandling(driver, "storefront","barneys");
		GenericMethods.Waitformilliseconds(1000);
		GenericMethods.waitForPageLoaded(driver);
		ATUReports.add("Verify Suggestions", "Suggestions along with rich images should appear", "Suggestions are appearing", true, LogAs.INFO);

		if (type.equalsIgnoreCase("CATEGORY")) {
			String Expected;
			Expected =searchText.toLowerCase();
			if (genericMethods.getText(driver,categoryLandingPageObject.categoryLanding_WHS_pageHeading_TV).equalsIgnoreCase(Expected)) {
				ATUReports.add("Verify Search results", "User should be on Category Landing Page", "User is on Category Landing Page", true, LogAs.PASSED);
			} else {
				ATUReports.add("Verify Search results", "User should be on Category Landing Page", "User is not on Category Landing Page", true, LogAs.FAILED);
				Assert.assertEquals(Expected+"’S FEATURED FAVORITES", genericMethods.getText(driver,categoryLandingPageObject.categoryLanding_pageHeading_TV));
			}			
	}else if (type.equalsIgnoreCase("ZERO")) {
		String Expected;
		Expected =searchText.toLowerCase();
		if (genericMethods.getText(driver,categoryLandingPageObject.zeroSearchResults_pageHeading_TV).equalsIgnoreCase("0 SEARCH RESULTS FOR \""+Expected+"\"")) {
			ATUReports.add("Verify Search results", "User should be on 0 Search Results page", "User is on 0 Search Results page", true, LogAs.PASSED);
		} else {
			System.out.println("0 SEARCH RESULTS FOR \""+Expected+"\"");
			System.out.println(genericMethods.getText(driver,categoryLandingPageObject.zeroSearchResults_pageHeading_TV));
			ATUReports.add("Verify Search results", "User should be on 0 Search Results page", "User is not on "+genericMethods.getText(driver,categoryLandingPageObject.zeroSearchResults_pageHeading_TV)+" page", true, LogAs.FAILED);
			Assert.assertEquals("0 SEARCH RESULTS FOR \""+Expected+"\"", genericMethods.getText(driver,categoryLandingPageObject.zeroSearchResults_pageHeading_TV));
		}			
	}else{
		String Expected;
		Expected =searchText.toUpperCase();
		if (genericMethods.getText(driver,categoryLandingPageObject.Search_PageTitle_TV).equalsIgnoreCase("SEARCH RESULTS FOR \""+Expected+"\"")) {
			ATUReports.add("Verify Search results", "User should be on Search Results Page", "User is on Search Results Page", true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify Search results", "User should be on Search Results Page", "User is not on Search Results Page", true, LogAs.FAILED);
			Assert.assertEquals("SEARCH RESULTS FOR \""+Expected+"\"", genericMethods.getText(driver,categoryLandingPageObject.Search_PageTitle_TV));
		}		
	}
	}
	
	public void applySort(WebDriver driver,String sortText){
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
		genericMethods=new GenericMethods();
		genericMethods.selectDropdown(driver,categoryLandingPageObject.categoryLanding_SortBy_DD, sortText,"SortBy Select Dropdown");
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(categoryLandingPageObject.ProductResultPage_48_96_All_TK));
		ATUReports.add("Verify value in the Sort By Dropdown", "Value in the Sort By Dropdown should be "+sortText,"_____", true, LogAs.INFO);
}
	
	public void selectView(WebDriver driver,String viewType){
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
		genericMethods=new GenericMethods();
		WebDriverWait wait = new WebDriverWait(driver, 15);
		List<WebElement> elements;
        xplicitwait= new WebDriverWait(driver, 15);

		
		if (viewType.equalsIgnoreCase("48")) {
			genericMethods.Click(driver,categoryLandingPageObject.categoryLanding_TopView48_LK,"TopView48 LK");
			wait.until(ExpectedConditions.visibilityOf(categoryLandingPageObject.ProductResultPage_48_96_All_TK));
			ATUReports.add("Verify View Selected", "48 View Should be Selected","48 View is Selected", true, LogAs.INFO);			
			elements=driver.findElements(By.xpath("//div[@id='atg_store_prodList']/ul/li"));
			Assert.assertEquals(elements.size(), 48);
			
		}else if (viewType.equalsIgnoreCase("96")) {
            xplicitwait.until(ExpectedConditions.elementToBeClickable(categoryLandingPageObject.categoryLanding_TopView96_LK));
			genericMethods.Click(driver,categoryLandingPageObject.categoryLanding_TopView96_LK,"TopView96 LK");
			GenericMethods.Waitformilliseconds(5000);
			ATUReports.add("Verify View Selected", "96 View Should be Selected","96 View is Selected", true, LogAs.INFO);
			elements=driver.findElements(By.xpath("//div[@id='atg_store_prodList']/ul/li"));
			org.testng.Assert.assertEquals(elements.size(), 96);
		}else if (viewType.equalsIgnoreCase("ALL")) {
			genericMethods.Click(driver,categoryLandingPageObject.categoryLanding_TopViewAll_LK,"TopViewAll LK");
			GenericMethods.Waitformilliseconds(10000);
			xplicitwait.until(ExpectedConditions.visibilityOf(categoryLandingPageObject.ProductResultPage_48_96_All_TK));
			ATUReports.add("Verify View Selected", "ALL View Should be Selected","ALL View is Selected", true, LogAs.INFO);
			elements=driver.findElements(By.xpath("//div[@id='atg_store_prodList']/ul/li"));
			
		}			
	}
	
	public void selectView4x6x(WebDriver driver,String viewType){
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
		genericMethods=new GenericMethods();
		List<WebElement> elements;
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		if (viewType.equalsIgnoreCase("4x")) {
			genericMethods.Click(driver,categoryLandingPageObject.categoryLanding_View4x_LK,"CLP_View4x Link");		
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']/ul/li")));
			ATUReports.add("Verify View Selected", "4X View Should be Selected","4X View is Selected", true, LogAs.INFO);
			
		}else if (viewType.equalsIgnoreCase("6x")) {
			genericMethods.Click(driver,categoryLandingPageObject.categoryLanding_View6x_LK,"CLP_View6x Link");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']/ul/li")));
			ATUReports.add("Verify View Selected", "6X View Should be Selected","6X View is Selected", true, LogAs.INFO);
		}			
	}
	
	public void selectFilters(WebDriver driver,String filterType){	
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
		genericMethods=new GenericMethods();
		String designerName;
		String ColorName;
		String SizeNumber;
		String MaterialName;
		String newArrival;
		
	
		WebDriverWait wait = new WebDriverWait(driver, 30);
	if (filterType.equalsIgnoreCase("designer")) {
		designerName=genericMethods.getText(driver, categoryLandingPageObject.Category_Designer3_CB);
		genericMethods.Click(driver, categoryLandingPageObject.Category_Designer3_CB,"DesignerFilter Checkbox");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
		GenericMethods.Waitformilliseconds(9000);
		if (genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))).equalsIgnoreCase(designerName)) {
			ATUReports.add("Verify Designer Name for the displyed Product","Designer Name should be"+designerName ,"Designer Name is :"+genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))),true, LogAs.PASSED);
		} else {
			ATUReports.add("Verify Designer Name for the displyed Product",designerName,"Designer Name should be" +designerName ,"Designer Name is :"+genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))),true, LogAs.FAILED);
			Assert.assertEquals(designerName, genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))));
		}
	} else if (filterType.equalsIgnoreCase("color")) {			
		//genericMethods.Click(driver, categoryLandingPageObject.Category_ColorFilter_expand_Arrow,"Color Expand Arrow");
		ColorName=genericMethods.getText(driver, categoryLandingPageObject.Category_ColorFilter_colour1_CB);
		genericMethods.Click(driver, categoryLandingPageObject.Category_ColorFilter_colour1_CB,"ColorFilter Checkbox");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
		GenericMethods.Waitformilliseconds(4000);
		System.out.println(ColorName);
		ATUReports.add("Verify Colour Filter Selected and Products Displayed",ColorName,"Products for the selectd colour should be displayed in" +ColorName,"Products for the selectd colour is displayed in "+ColorName,true, LogAs.PASSED);
	}else if (filterType.equalsIgnoreCase("size")) {			
		SizeNumber=genericMethods.getText(driver, categoryLandingPageObject.Category_SizeFilter1_CB);
		genericMethods.Click(driver, categoryLandingPageObject.Category_SizeFilter1_CB, "1st Size Checkbox");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
		GenericMethods.Waitformilliseconds(4000);
		System.out.println(SizeNumber);
		ATUReports.add("Verify Size Filter Selected and Products Displayed",SizeNumber,"Products for the selectd size should be displayed in" +SizeNumber,"Products are filtered according to " +SizeNumber,true, LogAs.PASSED);
	}else if (filterType.equalsIgnoreCase("material")) {
		MaterialName=genericMethods.getText(driver, categoryLandingPageObject.Category_MaterialFilter1_CB);
		genericMethods.Click(driver, categoryLandingPageObject.Category_MaterialFilter1_CB, "1st Material Checkbox");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
		GenericMethods.Waitformilliseconds(4000);
		System.out.println(MaterialName);
		ATUReports.add("Verify Material Filter is selected and Products are Displayed",MaterialName,"Products for the selectd Material name should be displayed in" +MaterialName,"Products are filtered according to " +MaterialName,true, LogAs.PASSED);
	}
	else if (filterType.equalsIgnoreCase("NewArrival")) {
		newArrival=genericMethods.getText(driver, categoryLandingPageObject.Category_NewArrivalFilter1_LK);
		genericMethods.Click(driver, categoryLandingPageObject.Category_NewArrivalFilter1_LK, "New Arrival Link");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
		GenericMethods.Waitformilliseconds(4000);
		String Expected;
		Expected =newArrival.toUpperCase();
		if (genericMethods.getText(driver,categoryLandingPageObject.Category_NewArrivalFilter1_LK).equalsIgnoreCase(Expected+" : Selected")) {
			ATUReports.add("Verify New Arrival text",Expected,"New Arrival text should get appended with SELECTED", "System shoud display new arrival text as New Arrivals : Selected", true, LogAs.PASSED);
		} else {
			ATUReports.add("erify New Arrival text",Expected, "New Arrival text should get appended with SELECTED","System does not display ew arrival text as New Arrivals : Selected", true, LogAs.FAILED);
			Assert.assertEquals(Expected+" : Selected", genericMethods.getText(driver,categoryLandingPageObject.Category_NewArrivalFilter1_LK));
		}
		System.out.println(newArrival);
		ATUReports.add("Verify New Arrival Filter is selected and Products are Displayed",newArrival,"Products for the New Arrivall should be displayed in","Products are filtered according to " +newArrival,true, LogAs.PASSED);
	}
		
}
	
	public void selectPageination(WebDriver driver,Integer pageNumber){
		WebDriverWait wait = new WebDriverWait(driver, 15);
		pageNumber=pageNumber+1;
		String ActiveNumber;
		String InactiveNumber;
		
		String paginationColourSelectedPage;
		categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[@class='atg_store_pagination']/span/ul/li[2]")));
        genericMethods=new GenericMethods();
       
        ActiveNumber=driver.findElement(By.xpath("//div[@class='atg_store_pagination']/span/ul/li[2]/a")).getCssValue("color");        
   
        System.out.println(ActiveNumber);
		if (ActiveNumber.equalsIgnoreCase("rgba(136, 136, 136, 1)")) {
			ATUReports.add("Verify Color of page 1 before click on other page","Page 1 is by default active clicked", "Page 1 colour should be rgba(136, 136, 136, 1)","Colour of Page 1 under pagination is " +ActiveNumber, true, LogAs.PASSED);
			System.out.println(ActiveNumber);
		} else {
			ATUReports.add("Verify Clor of page 1 before click on other page","Page 1 is by default active clicked","Page 1 colour should be rgba(136, 136, 136, 1)","Colour of Page 1 under pagination is " +ActiveNumber, true, LogAs.FAILED);
			Assert.assertEquals("rgba(136, 136, 136, 1)", ActiveNumber);
		}
		/*wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains (text(),96)]")));
		ATUReports.add("Verify the default page view","By Default 96 view should be displayed", "View 96 is displayed","View 96 is displayed" , true, LogAs.PASSED);
		*/
		genericMethods.ClickUSingXpath(driver, "//div[@class='atg_store_pagination']/span/ul/li["+pageNumber+"]/a","Page Number");
		wait.until(ExpectedConditions.visibilityOf(categoryLandingPageObject.categoryLanding_TopView96_LK));
		//yyyy wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains (text(),48)]")));
		
		GenericMethods.Waitformilliseconds(6000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='atg_store_pagination']/span/ul/li["+pageNumber+"]/a")));
		paginationColourSelectedPage=driver.findElement(By.xpath("//div[@class='atg_store_pagination']/span/ul/li["+pageNumber+"]/a")).getCssValue("color");
		if (paginationColourSelectedPage.equalsIgnoreCase("rgba(136, 136, 136, 1)")) {
			pageNumber=pageNumber-1;
			System.out.println(paginationColourSelectedPage);
			ATUReports.add("Verify Color of page : "+ pageNumber,"Clicked on" +pageNumber, "Page After Click on new page Number" +pageNumber+"  colour should be rgba(136, 136, 136, 1)", "Page Number's colour for page: "+pageNumber+" is "+paginationColourSelectedPage, true, LogAs.PASSED);
		} else {
			pageNumber=pageNumber-1;
			ATUReports.add("Verify Color of page : "+pageNumber, "Page After Click on new page Number"+pageNumber+"  colour should be rgba(136, 136, 136, 1)", "Page Number's colour for page: "+pageNumber+" is "+paginationColourSelectedPage, true, LogAs.FAILED);
			Assert.assertEquals("rgba(0, 0, 0, 1)", paginationColourSelectedPage);
		} 
		
		InactiveNumber=driver.findElement(By.xpath("//div[@class='atg_store_pagination']/span/ul/li[2]/a")).getCssValue("color");
		ATUReports.add("Verify Colour of page 1 after page " + pageNumber +"is selected","Page 1 colour should now be rgba(0, 0, 0, 1)","page 1 color should be "+ InactiveNumber, true, LogAs.INFO);
	    Assert.assertEquals("rgba(0, 0, 0, 1)",InactiveNumber );
	    System.out.println(InactiveNumber);
	
	}

	
	public void showSearchSuggestion(WebDriver driver, String correctSearchTerm) {
    	searchPageObject = PageFactory.initElements(driver, Desk_BNY_SearchPageObject.class);
    	homePageObject = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		
		if (isBNYSearchPresent = true) {
			genericMethods.Click(driver, searchPageObject.BNY_HomePage_SiteHeader_Search,"Click");
			ATUReports.add("Verify Search box is clicked", "Search box is clicked", true, LogAs.INFO);
			genericMethods.inputValue(driver, searchPageObject.HomePage_Search_Box, correctSearchTerm,"Search");
			ATUReports.add("Verify Search Term is entered", "Search term is entered", true, LogAs.INFO);
			
			genericMethods.mouseHover(driver, searchPageObject.Search_Suggestion_List,"Search Suggestion List");
			
			xplicitwait = new WebDriverWait(driver, 30);
			xplicitwait.until(ExpectedConditions.visibilityOf(searchPageObject.Search_Suggestion_List));
			
			searchSuggestion = genericMethods.isElementPresentforMouseHover(searchPageObject.Search_Suggestion_List);
			
			if (searchSuggestion = true) {
				ATUReports.add("Verify Search Suggestion is displayed", "Search Suggestion should be displayed", "Search Suggestion is displayed", true, LogAs.PASSED);	
			}else {
				ATUReports.add("Verify Search Suggestion is displayed", "Search Suggestion should be displayed", "Search Suggestion is not displayed", true, LogAs.FAILED);
			}
		}
	}
    
    public void zeroSearchResultPage(WebDriver driver,String wrongSearchTerm) {
    	searchPageObject = PageFactory.initElements(driver, Desk_BNY_SearchPageObject.class);
    	genericMethods=new GenericMethods();
    	 xplicitwait= new WebDriverWait(driver, 15);
    	
    	isBNYSearchPresent = genericMethods.isElementPresentforMouseHover(searchPageObject.BNY_HomePage_SiteHeader_Search);
    	if (isBNYSearchPresent = true) {
			genericMethods.Click(driver, searchPageObject.BNY_HomePage_SiteHeader_Search,"Search Link");
			ATUReports.add("Verify Search box is clicked", "Search box is clicked", true, LogAs.INFO);
			genericMethods.inputValue(driver, searchPageObject.HomePage_Search_Box, wrongSearchTerm,"Search");
			ATUReports.add("Verify Search Term is entered", "Search term is entered", true, LogAs.INFO);
			
			GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER, 1);
			WinAuth.aitoIT_AuthHandling(driver, "storefront","barneys");
		
			xplicitwait.until(ExpectedConditions.visibilityOf(searchPageObject.ZeroSearchResultPage_TryANewSearch_LB));
			
			if (genericMethods.getText(driver, searchPageObject.ZeroSearchResultPage_TryANewSearch_LB).equalsIgnoreCase("Try a new search")) {
				ATUReports.add("Verify Zero Search Result page is displayed", "Zero Search Result page should be displayed", "Zero Search Result page is opened", true, LogAs.PASSED);	
			}else {
				ATUReports.add("Verify Zero Search Result page is displayed", "Zero Search Result page should be displayed", "Zero Search Result page is not opened", true, LogAs.FAILED);
			}
        }
    }
    
    public void searchFromZeroSearchResultpage(WebDriver driver,String correctSearchTerm) {
    	searchPageObject = PageFactory.initElements(driver, Desk_BNY_SearchPageObject.class);
    	genericMethods = new GenericMethods();
    	isTryANewSearchTBPresent = genericMethods.isElementPresentforMouseHover(searchPageObject.ZeroSearchResultPage_TryANewSearch_TB);
    	
    	if (isTryANewSearchTBPresent = true) {
    		ATUReports.add("Verify Try A New Search TextBox is displayed", "Try A New Search TextBox should be displayed", "Try A New Search TextBox is displayed", true, LogAs.PASSED);
    		genericMethods.inputValue(driver, searchPageObject.ZeroSearchResultPage_TryANewSearch_TB, correctSearchTerm,"Search");
    		ATUReports.add("Verify Search Term is entered in Try A New Search textbox", "Search term is entered in Try A New Search textbox", true, LogAs.INFO);
    		}
    	else {
    		ATUReports.add("Verify Try A New Search TextBox is displayed", "Try A New Search TextBox should be displayed", "Try A New Search TextBox is not displayed", true, LogAs.FAILED);
    	}
    	    driver.findElement(By.id("atg_store_newSearchInput")).sendKeys(Keys.ENTER);
    	    WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
    	xplicitwait = new WebDriverWait(driver, 30);
    	xplicitwait.until(ExpectedConditions.visibilityOf(searchPageObject.SearchResultsPage_Title_LB));
    	
    	if (genericMethods.getText(driver, searchPageObject.SearchResultsPage_Title_LB).equalsIgnoreCase(searchTitle + " " + "\"" + correctSearchTerm + "\"")) {
			ATUReports.add("Verify Search Result page is displayed", "Search Result page should be displayed", "Search Result page is displayed", true, LogAs.PASSED);
		}
		else {
			ATUReports.add("Verify Search Result page is displayed", "Search Result page should be displayed", "Search Result page is not displayed", true, LogAs.FAILED);
		}
    	
    }
    
    public void specificTerm (WebDriver driver){
    	searchPageObject = PageFactory.initElements(driver, Desk_BNY_SearchPageObject.class);
    	genericMethods =  new GenericMethods();
    	isBNYSearchPresent = genericMethods.isElementPresentforMouseHover(searchPageObject.BNY_HomePage_SiteHeader_Search);
    	 
    	if (isBNYSearchPresent = true){
    		genericMethods.Click(driver, searchPageObject.BNY_HomePage_SiteHeader_Search, "Search Link");
    		ATUReports.add("Verify Search box is clicked", "Search box is clicked", true, LogAs.INFO);
    		genericMethods.inputValue(driver, searchPageObject.HomePage_Search_Box, "jea", "Incomplete OR wrong search term");
    		xplicitwait = new WebDriverWait(driver, 30);
    		GenericMethods.SendKeysXTimes(KeyEvent.VK_ENTER,2);
    		WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
    		GenericMethods.Waitformilliseconds(2000);
    		ATUReports.add("Verify the search result page","jea","Verify the search result page","Search result page should display correct search term",true,LogAs.PASSED);
    	}		
	}
    
//**********************************We have covred this test case***************************************************************   
    
    public void clickSearchSuggestion(WebDriver driver,String correctSearchTerm) {
    	searchPageObject = PageFactory.initElements(driver, Desk_BNY_SearchPageObject.class);
    	genericMethods = new GenericMethods();
    	isBNYSearchPresent = genericMethods.isElementPresentforMouseHover(searchPageObject.BNY_HomePage_SiteHeader_Search);
		
	if (isBNYSearchPresent = true) {
		genericMethods.Click(driver, searchPageObject.BNY_HomePage_SiteHeader_Search,"Search Link");
		ATUReports.add("Verify Search box is clicked", "Search box is clicked", true, LogAs.INFO);
		genericMethods.inputValue(driver, searchPageObject.HomePage_Search_Box, "jeans","Search");
		ATUReports.add("Verify Search Term is entered", "Search term is entered", true, LogAs.INFO);
		genericMethods.mouseHover(driver, searchPageObject.Search_Suggestion_List,"Search");
		
		xplicitwait = new WebDriverWait(driver, 30);
		xplicitwait.until(ExpectedConditions.visibilityOf(searchPageObject.Search_Suggestion_List));
		
		searchSuggestion = genericMethods.isElementPresentforMouseHover(searchPageObject.Search_Suggestion_List);		
		if (searchSuggestion = true) {
			ATUReports.add("Verify Search Suggestion is displayed", "Search Suggestion should be displayed", "Search Suggestion is displayed", true, LogAs.PASSED);	
		}else {
			ATUReports.add("Verify Search Suggestion is displayed", "Search Suggestion should be displayed", "Search Suggestion is not displayed", true, LogAs.FAILED);
		}
		
		firstSuggestionElement = driver.findElement(By.xpath("//ul[@id='atg_store_searchInput-sayt']//li[1]/div"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", firstSuggestionElement);
		
		GenericMethods.Waitformilliseconds(2000);		
		if (genericMethods.getText(driver, searchPageObject.SearchResultsPage_Title_LB).equalsIgnoreCase(searchTitle + " " + "\"" + correctSearchTerm + "\"")) {
			ATUReports.add("Verify Search Result page is displayed", "Search Result page should be displayed", "Search Result page is displayed", true, LogAs.PASSED);
		}else {
			ATUReports.add("Verify Search Result page is displayed", "Search Result page should be displayed", "Search Result page is not displayed", true, LogAs.FAILED);
		}
	}
   }
}
