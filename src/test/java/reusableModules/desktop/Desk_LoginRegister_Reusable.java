package reusableModules.desktop;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import setup.WinAuth;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_LoginRegistrationPageObject;
import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import actions.GenericMethods;
import actions.WriteRegistrationData;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_LoginRegister_Reusable{
		Desk_BNY_LoginRegistrationPageObject loginRegisterPageObjects;
		Desk_BNY_DesignerPageObject designerPageObjects;
		Desk_BNY_HomePageObject homePageObjects;
		Desk_BNY_RegistrationPageObject registerPageObjects;
		WebDriverWait xplicitWait;
		GenericMethods genericMethods;
		WriteRegistrationData writeRegistrationData;
		String timeStamp;
		public String sTime;
		boolean loggedInUserName;
		boolean loginForm;
		boolean bnyHomePage;
		boolean whsHomePage;
		String CurrentPageTitle;
		boolean loginText;
		boolean registerForm;
		
		
//==========================================BNY Login===============================================
		public void verifyHomePage(WebDriver driver){
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			genericMethods =  new GenericMethods();
			
			bnyHomePage = genericMethods.isElementPresentforMouseHover(homePageObjects.BNYHomePage_CONTENT);
			whsHomePage = genericMethods.isElementPresentforMouseHover(homePageObjects.WHSHomePage_CONTENT);
			
			if (bnyHomePage){			
				ATUReports.add("Verify BNYHome page is displayed ",  "BNYHome page should be displayed", "BNYHome page is displayed", true,LogAs.INFO);
				Assert.assertEquals(bnyHomePage, true);	
			}else if(whsHomePage){
			
				ATUReports.add("Verify WHSHome page is displayed ",  "WHSHome page should be displayed", "WHSHome page is displayed", true,LogAs.INFO);
			}else{
				ATUReports.add("Verify Home page is displayed ",  "Home page should not be displayed", "Home page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(bnyHomePage, false);
				Assert.assertEquals(whsHomePage, false);
			}		
		}
			
		public void loginFromHeader(WebDriver driver,String userEmail, String password){
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			genericMethods =  new GenericMethods();
			CurrentPageTitle=genericMethods.getTitle(driver);
			xplicitWait = new WebDriverWait(driver, 30);
			
			/*
			if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
				Assert.assertEquals(genericMethods.getText(driver,homePageObjects.Home_UtilityNav_TopLogin_LK), "Log In");
			}else{
				Assert.assertEquals(genericMethods.getText(driver,homePageObjects.Home_UtilityNav_TopLogin_LK), "LOG IN");
			}
			*/
			
			xplicitWait.until(ExpectedConditions.visibilityOf(homePageObjects.Home_UtilityNav_TopLogin_LK));		
			genericMethods.Click(driver,homePageObjects.Home_UtilityNav_TopLogin_LK,"Top Login link");
			GenericMethods.Waitformilliseconds(1000);
			WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
			
			loginForm = genericMethods.isElementPresentforMouseHover(homePageObjects.LoginPage_LoginPanel_FORM);
			if (loginForm == true){			
				ATUReports.add("Verify the Type of User", "User should be guest", "Guest User", true,LogAs.INFO);
				ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Login page is displayed ",  "Login page should not be displayed", "Login page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(loginForm, true);
			}
			
			genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Email_TB, userEmail,"Username");
			genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Password_TB, password,"Password");
			genericMethods.Click(driver,homePageObjects.LoginPage_LoginPanel_LOGIN_BTN,"Login button");
			
			WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
			
			loggedInUserName = genericMethods.isElementPresentforMouseHover(homePageObjects.Home_UtilityNav_LoggedInUser_LK);		
			if(loggedInUserName = true){
				ATUReports.add("Verify user does get Logged In", "User should be logged in successfully", "Login is Success",true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify user does get loggedin","User should be logged in successfully", "Unable to login",true,LogAs.FAILED);	
				Assert.assertEquals(loggedInUserName, true);
					
			}	
		}
	//===========================================MY BAG link not yet developed==================================		
			/*if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
				mybagCheckout =new MYBagCheckout();
				mybagCheckout.emptyMyBag(driver, "WHS");
			}else{
				mybagCheckout =new MYBagCheckout();
				mybagCheckout.emptyMyBag(driver, "BNY");
			}
	 }*/
		

		
		public void forGotPassword(String emailType,WebDriver driver,String userEmail) {
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			genericMethods =  new GenericMethods();
			String messageAfterSend;
			
			CurrentPageTitle=genericMethods.getTitle(driver);		
			genericMethods.Click(driver,homePageObjects.Home_UtilityNav_TopLogin_LK,"Top Login link");
			WinAuth.aitoIT_AuthHandling(driver, "storefront","barneys");
			genericMethods.Click(driver,homePageObjects.LoginPopup_ForgotPassword_LK,"Forgot Password Link");
			genericMethods.inputValue(driver,homePageObjects.LoginPopup_ForgotPassword_Email_TB,userEmail,"Email ID");
			genericMethods.Click(driver,homePageObjects.LoginPopup_ForgotPassword_Send_BTN,"Send Button");
			GenericMethods.Waitformilliseconds(1000);
			

			if(emailType.equalsIgnoreCase("empty")){
				messageAfterSend=genericMethods.getText(driver,homePageObjects.LoginPopup_ForgotPassword_BlankEmail_TV);
				if(messageAfterSend.equalsIgnoreCase("Provide your account Login to receive an email to reset your password.")){
					ATUReports.add("Verify Error Message", "Error message should be 'Please provide a valid Email.'", "Error Message is :" +messageAfterSend, true, LogAs.PASSED);
					Assert.assertEquals(messageAfterSend, "Provide your account Login to receive an email to reset your password.");
				}else{
					ATUReports.add("Verify Error Message", "Error message should be 'Please provide a valid Email.'", "Error Message is :" +messageAfterSend, true, LogAs.FAILED);
					Assert.assertEquals(messageAfterSend, "Provide your account Login to receive an email to reset your password.");
				}			
			}else if (emailType.equalsIgnoreCase("invalid")) {
				messageAfterSend=genericMethods.getText(driver,homePageObjects.LoginPopup_ForgotPassword_InvalidMail_Validation_TV);
				if(messageAfterSend.equalsIgnoreCase("No matching login was found. Please check your spelling and try again.")){
					ATUReports.add("Verify Error Message", "Error message should be 'Provide your account login to receive an email to reset your password.'", "Error Message is :" +messageAfterSend, true, LogAs.PASSED);
					Assert.assertEquals(messageAfterSend, "No matching login was found. Please check your spelling and try again.");
				}else{
					ATUReports.add("Verify Error Message", "Error message should be 'Provide your account login to receive an email to reset your password.'", "Error Message is :" +messageAfterSend, true, LogAs.FAILED);
					Assert.assertEquals(messageAfterSend, "No matching login was found. Please check your spelling and try again.");
				}			
			}else if (emailType.equalsIgnoreCase("valid")) {
				GenericMethods.Waitformilliseconds(3000);
				messageAfterSend=genericMethods.getText(driver,homePageObjects.LoginPopup_ForgotPassword_MesgAfterSuccess_TV);
				System.out.println(messageAfterSend);
				
				if(messageAfterSend.equalsIgnoreCase("REQUEST TO RESET YOUR PASSWORD RECEIVED")){
					ATUReports.add("Verify Message", "Message should be 'A temporary link to reset your password has been emailed to you. Please click the link to reset your password and log-in to your account.'", "Message is : " + messageAfterSend + " 'A temporary link to reset your password has been emailed to you. Please click the link to reset your password and log-in to your account.' ", true, LogAs.PASSED);
					genericMethods.Click(driver,homePageObjects.LoginPopup_ForgotPassword_MesgAfterSuccess_GoToHome_LK,"Go to Home Link");
					ATUReports.add("Page Displayed", "Should Navigate to Home Page'", "User is on Home Page", true, LogAs.PASSED);
				}else{
					ATUReports.add("Verify Message", "Message should be 'A temporary link to reset your password has been emailed to you. Please click the link to reset your password and log-in to your account.'", "Message is : " + messageAfterSend + "'A temporary link to reset your password has been emailed to you. Please click the link to reset your password and log-in to your account.'" + " is not displayed",false, LogAs.FAILED);
					Assert.assertEquals(messageAfterSend, "REQUEST TO RESET YOUR PASSWORD RECEIVED");
				}
			}
		
		}

		public void loginDirectlyIntoForm(WebDriver driver,String userEmail, String password) {
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			genericMethods =  new GenericMethods();

			genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Email_TB, userEmail,"Username");
			genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Password_TB, password,"Password");
			ATUReports.add("Login Popup",true, LogAs.INFO);
			genericMethods.Click(driver,homePageObjects.LoginPage_LoginPanel_LOGIN_BTN,"Login Button");
			GenericMethods.Waitformilliseconds(5000);
			if ((genericMethods.getText(driver,homePageObjects.Home_UtilityNav_TopLogin_LK)).equalsIgnoreCase("Log In")) {
				ATUReports.add("User should be logged in successfully", "Unable to login",true,LogAs.FAILED);
				org.testng.Assert.assertNotEquals((genericMethods.getText(driver,homePageObjects.Home_UtilityNav_TopLogin_LK)), "Log In");			
			}else{	
				ATUReports.add("User should be logged in successfully", "Login is Success",true,LogAs.PASSED);
				Assert.assertNotEquals((genericMethods.getText(driver,homePageObjects.Home_UtilityNav_TopLogin_LK)), "Log In");					
			}				
		}		
		
		
//========================================================================================================================	
		
//=============================================Desk register Reusable=========================================================	
		public void registerFromHeader(WebDriver driver)  {
			
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			registerPageObjects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);		
			writeRegistrationData=new WriteRegistrationData();
			genericMethods =  new GenericMethods();		
			writeRegistrationData=new WriteRegistrationData();
			xplicitWait = new WebDriverWait(driver, 30);
			timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			
			System.out.println(timeStamp );
			sTime=timeStamp;
			
			xplicitWait.until(ExpectedConditions.visibilityOf(registerPageObjects.Home_TopRegister_LK));
			genericMethods.Click(driver, registerPageObjects.Home_TopRegister_LK,"Top Register Link");
			WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
			
			xplicitWait.until(ExpectedConditions.visibilityOf(registerPageObjects.RegisterPage_RegisterPanel_Form));
			//registerForm = genericMethods.isElementPresentforMouseHover(registerPageObjects.RegisterPage_RegisterPanel_Form);
			//System.out.println("***" + registerForm + "***");
			
			/*if (registerForm == true){
				ATUReports.add("Verify the Type of User", "User should be guest", "Guest User", true,LogAs.INFO);
				ATUReports.add("Verify Register page is displayed ",  "Register page should be displayed", "Register page is  displayed", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Register page is displayed ",  "Register page should not be displayed", "Register page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(registerForm, true);
			}*/
			
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_Email_TB, timeStamp +"@barneys.com","Username");
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_Password_TB, "12345678","Password");
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_ConfirmPassword_TB, "12345678","Confirm Password");
			ATUReports.add("Verify Fields and UI", true,LogAs.INFO);
			genericMethods.Click(driver, registerPageObjects.RegisterPage_RegisterPanel_LogIN_BTN,"Register Button");
			GenericMethods.Waitformilliseconds(4000);
			
			loginText= genericMethods.isElementPresentforMouseHover(homePageObjects.Home_UtilityNav_LoggedInUser_LK);
			if(loginText){
				ATUReports.add("Verify user does get registered", "User should be registered successfully", "registration is Success",true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify user does get registered","User should not be registered successfully", "Unable to registered",true,LogAs.FAILED);	
				Assert.assertEquals(loginText, false);
			}		
		}
		
		public void registerWithExistingMailID(WebDriver driver,String userEmail, String password, String confirmPassword){
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			registerPageObjects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);
			genericMethods =  new GenericMethods();
			
			/*
			 * CurrentPageTitle=genericMethods.getTitle(driver);			
			if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
				Assert.assertEquals(genericMethods.getText(driver,homePageObjects.BNY_Home_TopLogin_LK), "Log In");
			}else{
				Assert.assertEquals(genericMethods.getText(driver,homePageObjects.BNY_Home_TopLogin_LK), "LOG IN");
			}
			*/
			genericMethods.Click(driver,registerPageObjects.Home_TopRegister_LK,"Top Register link");
			WinAuth.aitoIT_AuthHandling(driver, "storefront", "barneys");
			registerForm = genericMethods.isElementPresentforMouseHover(registerPageObjects.RegisterPage_RegisterPanel_Form);
			if (registerForm == true){
				ATUReports.add("Verify the Type of User", "User should be guest", "Guest User", true,LogAs.INFO);
				ATUReports.add("Verify Registration page is displayed ",  "Registration page should be displayed", "Registration page is displayed", true,LogAs.PASSED);	
			}else{
				ATUReports.add("Verify registration page is displayed ",  "Registration page should be displayed", "Registration page is not displayed", true,LogAs.FAILED);
				Assert.assertEquals(registerForm, true);
			}
			
			genericMethods.inputValue(driver,registerPageObjects.RegisterPage_RegisterPanel_Email_TB, userEmail,"Username");
			genericMethods.inputValue(driver,registerPageObjects.RegisterPage_RegisterPanel_Password_TB, password,"Password");
			genericMethods.inputValue(driver,registerPageObjects.RegisterPage_RegisterPanel_ConfirmPassword_TB, confirmPassword,"confirm password");
			genericMethods.Click(driver,registerPageObjects.RegisterPage_RegisterPanel_LogIN_BTN,"Register Button");
			GenericMethods.waitForPageLoaded(driver);
			
			loginText= genericMethods.isElementPresentforMouseHover(homePageObjects.Home_UtilityNav_LoggedInUser_LK);
			System.out.println(loginText);
			if(loginText){
				ATUReports.add("Verify user does get registered", "User should be registered successfully", "registration is Success",true,LogAs.FAILED);
			}else {
				ATUReports.add("Verify if registered user is not able to Registered again","Existing User email Id & Password","System should not allow user to register", "Unable to registered",true,LogAs.PASSED);	
				Assert.assertEquals(loginText, false);
			}
		}
		
		public void registerDirectlyIntoForm(WebDriver driver,String timeStamp) {
			
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			registerPageObjects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);	
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			writeRegistrationData=new WriteRegistrationData();
			
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_Email_TB, timeStamp +"@yopmail.com","Username");
			System.out.println(timeStamp +"@yopmail.com");
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_Password_TB, "12345678","Password");
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_ConfirmPassword_TB, "12345678","Confirm Password");

			ATUReports.add("Register Popup",true, LogAs.INFO);
			genericMethods.Click(driver, registerPageObjects.RegisterPage_RegisterPanel_LogIN_BTN,"Register Button");
			GenericMethods.Waitformilliseconds(6000);
			if ((genericMethods.getText(driver, homePageObjects.Home_UtilityNav_TopLogin_LK)).equalsIgnoreCase("LOG IN")) {
				ATUReports.add("User should be registered successfully", "Unable to Register",true,LogAs.FAILED);
				Assert.assertNotEquals((genericMethods.getText(driver, homePageObjects.Home_UtilityNav_TopLogin_LK)), "LOG IN");			
			}else{	
				ATUReports.add("Verify if User is registered or not ","User should be registered successfully", "Registration sucessful with email :"+timeStamp +"@yopmail.com",true,LogAs.PASSED);
				Assert.assertNotEquals((genericMethods.getText(driver, homePageObjects.Home_UtilityNav_TopLogin_LK)), "LOG IN");					
				writeRegistrationData.writeDataToExcel("src/test/java/testDataFiles/New_user_registration.xls","Registration", timeStamp +"@yopmail.com");
			}	
		}
		
		public void checkoutRegister(WebDriver driver,String timeStamp) {
			
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			registerPageObjects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);	
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			//GV:Check If user is Guest, Simply don't assume that user is logged in
			ATUReports.add("Check Type of User", "Should be guest", "Guest User", true,LogAs.INFO);
			
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_Email_TB, timeStamp +"@barneys.com","Username");
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_Password_TB, "12345678","Password");
			genericMethods.inputValue(driver, registerPageObjects.RegisterPage_RegisterPanel_ConfirmPassword_TB, "12345678","Confirm Password");
		
			ATUReports.add("Register Popup",true, LogAs.INFO);
			genericMethods.Click(driver, registerPageObjects.RegisterPage_RegisterPanel_LogIN_BTN,"Register Button");
			String ShippingPage;
			ShippingPage=genericMethods.isElementPresent(driver,"//div[@class='row checkout-header']/h2");
			if (ShippingPage=="present"){
				ATUReports.add("Verify Shipping page is displayed ",  "Shipping page should be displayed", "Shipping page is displayed", true,LogAs.PASSED);
				//writeRegistrationData.writeDataToExcel("src/test/java/testDataFiles/New_user_registration.xls","Registration", timeStamp +"@yopmail.com");
			}else{
				ATUReports.add("Verify Shipping page is displayed ",  "Shipping page should be displayed", "Shipping page is not displayed", true,LogAs.FAILED);
			}
		}
//========================================================================================================================			
	public void catBrowseLogin(WebDriver driver)  {
		  genericMethods =  new GenericMethods();
		  loginRegisterPageObjects = PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
		  String LoginButton;
		
		try{
			genericMethods.mouseHover(driver, loginRegisterPageObjects.First_Item_LK,"GuestUser Login Link");	
			((JavascriptExecutor)driver).executeScript("document.getElementsByClassName('user-login favorite-status hidden-xs')[0].click();"); 
		}catch(Exception e){
			genericMethods.mouseHover(driver, loginRegisterPageObjects.First_Item_LK,"GuestUser Login Link");
			GenericMethods.Waitformilliseconds(2000);
			genericMethods.Click(driver, loginRegisterPageObjects.CategoryBrowse_HeartIcon_LK,"HeartIcon_Login Link");
		}
		
		//ATUReports.add("Guest user clicks on heart icon", true, LogAs.INFO);
		LoginButton=genericMethods.isElementPresent(driver,"//form[@id='dwfrm_login']/fieldset/div[5]/button");
		if (LoginButton=="present"){
			ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is not displayed", true,LogAs.FAILED);			
		}		
	}
		
	public void verifyPageDisplayed(WebDriver driver,String Page){
		loginRegisterPageObjects= PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
		String CatBrowse, PDPPage, MyDesigners,BNYTitle, WHSTitle;
		if (Page.equalsIgnoreCase("Category Browse")){
			CatBrowse= genericMethods.isElementPresent(driver, "//div[@class='search-header']/h1");
			if (CatBrowse.equalsIgnoreCase("present")){
				ATUReports.add("Verify Whether Category Browse page is displayed after login", "Category browse page should be displayed", "Category browse page is displayed", true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify Whether Category Browse page is displayed after login", "Category browse page should be displayed", "Category browse page is not displayed", true, LogAs.PASSED);
				Assert.assertEquals("Category browse page present", "Category browse page not present");
			}
		}else if(Page.equalsIgnoreCase("PDP")){
			PDPPage= genericMethods.isElementPresent(driver, "//h1[@class='brand']/a");
			if (PDPPage.equalsIgnoreCase("present")){
				ATUReports.add("Verify Whether PDP page is displayed after login", "PDP page should be displayed", "PDP page is displayed", true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify Whether PDP page is displayed after login", "PDP page should be displayed", "PDP page is not displayed", true, LogAs.PASSED);
				Assert.assertEquals("PDP page present", "PDP page not present");
			}	
		}else if(Page.equalsIgnoreCase("My Designers")){
			MyDesigners= genericMethods.isElementPresent(driver, "//section[@class='account-top']/h1");
			if (MyDesigners.equalsIgnoreCase("present")){
				ATUReports.add("Verify Whether My Designers page is displayed after login", "My Designers page should be displayed", "My Designers page is displayed", true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify Whether My Designers page is displayed after login", "My Designers page should be displayed", "My Designers page is not displayed", true, LogAs.PASSED);
				Assert.assertEquals("My Designers page present", "My Designers page not present");
			}	
		}else if(Page.equalsIgnoreCase("BNY Home Page")){
			BNYTitle= driver.getTitle();
			if (BNYTitle.equalsIgnoreCase("Barneys New York: Luxury Designer Handbags, Shoes and Clothing")){
				ATUReports.add("Verify Whether Home page is displayed after login", "Home page should be displayed", "Home page is displayed", true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify Whether Home page is displayed after login", "Home page should be displayed", "Home page is not displayed", true, LogAs.PASSED);
				Assert.assertEquals("Home page present", "Home page not present");
			}	
		}else if(Page.equalsIgnoreCase("WHS Home Page")){
			WHSTitle= driver.getTitle();
			if (WHSTitle.equalsIgnoreCase("Barneys Warehouse")){
				ATUReports.add("Verify Whether Home page is displayed after login", "Home page should be displayed", "Home page is displayed", true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify Whether Home page is displayed after login", "Home page should be displayed", "Home page is not displayed", true, LogAs.PASSED);
				Assert.assertEquals("Home page present", "Home page not present");
			}	
		}		
	}

		public void loginFromPDP(WebDriver driver)  {
			genericMethods =  new GenericMethods();
			loginRegisterPageObjects = PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
			String LoginButton;
			genericMethods.Click(driver, loginRegisterPageObjects.PDP_HeartIcon_LK,"PdpHeartIcon_Login Link");
			//Verify that user is not Logged in
			ATUReports.add(" Guest User clicks on heart icon on PDP", true,LogAs.INFO);
			LoginButton=genericMethods.isElementPresent(driver,"//form[@id='dwfrm_login']/fieldset/div[5]/button");
			if (LoginButton=="present"){
				ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
			}else{
				ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is not displayed", true,LogAs.FAILED);
			}	
		}
	
	public void loginFromMyBagCheckout(WebDriver driver, String Email, String Password)  {
		genericMethods =  new GenericMethods();
		loginRegisterPageObjects = PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
		String LoginPopup,ShippingPage;
		String Popup;
		GenericMethods.Waitformilliseconds(2000);
		Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
		if (Popup=="present"){
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is not  displayed", true,LogAs.FAILED);		
		}
	
	/*	LoginPopup=genericMethods.isElementPresent(driver,"//div[@class='login-box login-guest']/div/form/fieldset/div/button");
		if (LoginPopup=="present")
		{
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is displayed", true,LogAs.PASSED);
		}
		else
		{
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is not displayed", true,LogAs.FAILED);
			
		
		}*/
		genericMethods.inputValue(driver, loginRegisterPageObjects.MYBAG_CheckoutEmail_TB, Email,"Login Email");
		genericMethods.inputValue(driver, loginRegisterPageObjects.MYBAG_CheckoutPassword_TB, Password,"Login password");
		genericMethods.Click(driver, loginRegisterPageObjects.MYBAG_CheckoutLogin_BTN,"Checkout Login Button");
		//genericMethods.Waitformilliseconds(1000);
		
		ShippingPage=genericMethods.isElementPresent(driver,"//div[@class='row checkout-header']/h2");
		if (ShippingPage=="present"){
			ATUReports.add("Verify Shipping page is displayed ",  "Shipping page should be displayed", "Shipping page is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Shipping page is displayed ",  "Shipping page should be displayed", "Shipping page is not displayed", true,LogAs.FAILED);			
		}		
	}

	public void registerFromMyBagCheckout(WebDriver driver)  {
		genericMethods =  new GenericMethods();
		loginRegisterPageObjects = PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
		String LoginPopup,RegisterPopup;
	
		String Popup;
		Popup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
		if (Popup=="present"){
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is not  displayed", true,LogAs.FAILED);	
		}
		
		/*LoginPopup=genericMethods.isElementPresent(driver,"//div[@class='login-box login-guest']/div/form/fieldset/div/button");
		if (LoginPopup=="present"){
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Login popup is displayed ",  "Login popup should be displayed", "Login popup is not displayed", true,LogAs.FAILED);					
		}
		*/
		
		genericMethods.Click(driver, loginRegisterPageObjects.MYBAG_CheckoutRegisterHere_LK,"MyBag_CheckoutRegister Link");	
		RegisterPopup=genericMethods.isElementPresent(driver,"//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']/a/span");
		if (RegisterPopup=="present"){
			ATUReports.add("Verify Register Popup is displayed ",  "Register Popup  should be displayed", "Register Popup  is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Register Popup  is displayed ",  "Register Popup  should be displayed", "Register Popup  is not displayed", true,LogAs.FAILED);					
		}				
	}
	
	public void registerFromDLPAdd(WebDriver driver)  {
		
	//	HomeDesigners = PageFactory.initElements(driver, BNYHomePageObject.class);
		designerPageObjects = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
		//  TopNav=PageFactory.initElements(driver, BNYNavigationObject.class);
		//  registerPageObjects = PageFactory.initElements(driver, BNYRegistrationObject.class);				  
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);

		genericMethods.Click(driver, designerPageObjects.DesignerstartS_LK,"DesignerStartS Link");
		genericMethods.Click(driver, designerPageObjects.DesignerSaintLaurent_LK,"DesignerSaintLaurent Link");
		genericMethods.Click(driver, designerPageObjects.DLPGuestAddToMyDesigners_BTN,"DLPGuestAddToMyDesigners Button");
		ATUReports.add("User is redirected to DLP page", true,LogAs.INFO);
		ATUReports.add("User clicks on 'Add to my designer's button on DLP page", true,LogAs.INFO);
		
		String LoginPage;
		LoginPage=genericMethods.isElementPresent(driver,"//form[@id='dwfrm_login']/fieldset/div[5]/button");

		if (LoginPage=="present"){
			ATUReports.add("Verify Login Page is displayed ",  "Login page  should be displayed", "Login page  is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify  Login Page  is displayed ",  " Login Page   should be displayed", " Login Page   is not displayed", true,LogAs.FAILED);		
		}
		/*String guestuser;
		guestuser=genericMethods.isElementPresent(driver, "//span[@class='icon dropdown-toggle hidden-xs dropdown-button']");//b[@class='caret']
				
			if(guestuser=="notpresent")	{

			genericMethods.Click(driver, HomeDesigners.BNY_Popup_Register_LK);
			genericMethods.Waitformilliseconds(10000);
			genericMethods.inputValue(driver, registerPageObjects.Register_Email_TB, "l17@barneys.com");
			//genericMethods.Waitformilliseconds(3000);
			genericMethods.inputValue(driver, registerPageObjects.Register_Password_TB, "12345678");
			genericMethods.inputValue(driver, registerPageObjects.Register_ConfirmPassword_TB, "12345678");
			//genericMethods.Waitformilliseconds(3000);
			ATUReports.add("Login Popup",true, LogAs.INFO);
			genericMethods.Click(driver, registerPageObjects.Register_BTN);
			
			if ((genericMethods.getText(HomeDesigners.Home_TopLogin_TXT)).equalsIgnoreCase("LOG IN")) {
				ATUReports.add("User should be registered successfully", "Unable to Register",true,LogAs.FAILED);
				Assert.assertNotEquals((genericMethods.getText(HomeDesigners.Home_TopLogin_TXT)), "LOG IN");
				
			}else
			{	ATUReports.add("User should be registered successfully", "Registration sucessful ",true,LogAs.PASSED);
				Assert.assertNotEquals((genericMethods.getText(HomeDesigners.Home_TopLogin_TXT)), "LOG IN");					
			}	
		}
			genericMethods.Click(driver, HomeDesigners.Home_TopAccountArrow_LK);
			
			genericMethods.Click(driver, HomeDesigners.Home_TopDDMyAccount_DD);
				
		genericMethods.Click(driver, designerPageObjects.MyAcc_Designer_LK);
		
		String DesignerAddedName;
		DesignerAddedName=genericMethods.getText(designerPageObjects.DesignerAdded_LB);

		if ((genericMethods.checkEqual(DesignerAddedName, "Saint Laurent"))) {
			ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
			Assert.assertEquals(DesignerAddedName, "Saint Laurent");
		}
		*/
	}
	
	public void loginFromMyDesignerFilter(WebDriver driver)  {
		genericMethods =  new GenericMethods();
		loginRegisterPageObjects = PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
		String LoginButton;
		String MyDesignerXpath;
		MyDesignerXpath= genericMethods.isElementPresent(driver, "//div[@id='secondary']/div[3]/div[3]/div[2]/div/div[2]/div/a[2]");
		if (MyDesignerXpath.equalsIgnoreCase("present")){
			genericMethods.Click(driver, loginRegisterPageObjects.MyDesignerFilterDev_LK,"MyDesignerFilterDev Link");	
		}else{
			genericMethods.Click(driver, loginRegisterPageObjects.MyDesignerFilterProd_LK,"MyDesignerFilterProd Link");	
		}
	}
	
	public void loginFromMyDesignerEdit(WebDriver driver)  {
		genericMethods =  new GenericMethods();
		loginRegisterPageObjects = PageFactory.initElements(driver, Desk_BNY_LoginRegistrationPageObject.class);
		String LoginButton;
		String MyDesignerXpath;
		
		MyDesignerXpath= genericMethods.isElementPresent(driver, "//div[@id='secondary']/div[3]/div[3]/div[2]/div/div[2]/a[2]");
		if (MyDesignerXpath.equalsIgnoreCase("present")){
			genericMethods.Click(driver, loginRegisterPageObjects.MyDesignerEditDev_LK,"MyDesignerEditDev Link");	
		}else{
			genericMethods.Click(driver, loginRegisterPageObjects.MyDesignerEditProd_LK,"MyDesignerFilterProd Link");	
		}
		/*
		LoginButton=genericMethods.isElementPresent(driver,"//form[@id='dwfrm_login']/fieldset/div[5]/button");
		if (LoginButton=="present"){
			ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is displayed", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Login page is displayed ",  "Login page should be displayed", "Login page is not displayed", true,LogAs.FAILED);	
		}
		*/
	}	
	
	public void generalLogin(WebDriver driver){
		genericMethods =  new GenericMethods();
		homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Email_TB , "govind0904@yopmail.com", "Login TB");
		genericMethods.inputValue(driver, homePageObjects.LoginPage_LoginPanel_Password_TB, "12345678", "Login TB");
		genericMethods.Click(driver, homePageObjects.LoginPage_LoginPanel_LOGIN_BTN, "Login Button");
		
	}
	
	public void generalLogin1(WebDriver driver,String loginEmail){
		genericMethods =  new GenericMethods();
		homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods.inputValue(driver,homePageObjects.LoginPage_LoginPanel_Email_TB , loginEmail+"@yopmail.com", "Login TB");
		genericMethods.inputValue(driver, homePageObjects.LoginPage_LoginPanel_Password_TB, "12345678", "Login TB");
		genericMethods.Click(driver, homePageObjects.LoginPage_LoginPanel_LOGIN_BTN, "Login Button");		
	}
	
}