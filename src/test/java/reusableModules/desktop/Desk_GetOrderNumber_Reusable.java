package reusableModules.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import PageObjects.desktop.Desk_BNY_OrderConfirmationPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_GetOrderNumber_Reusable {
	String Order_No;
	GenericMethods genericMethods;
	Desk_BNY_OrderConfirmationPageObject orderConfirmationPageObject;
	
	 public void printOrderNo(WebDriver driver){
		 
		 //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		 ATUReports.add("Verify Order Number", "Order Number is", "Order_No", true,LogAs.INFO);
		 orderConfirmationPageObject = PageFactory.initElements(driver, Desk_BNY_OrderConfirmationPageObject.class);
		 Order_No=driver.findElement(By.xpath("//div[@class='order-paragraphs']/p")).getText();
		 //Order_No=genericMethods.getText(orderConfirmationPageObject.OrderCOnfirmation_OrderRefNumber_TV);
		 ATUReports.add("Verify Order Number", "Order Number is", Order_No, true,LogAs.INFO);	 
	 }
}
