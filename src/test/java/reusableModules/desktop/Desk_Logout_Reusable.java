package reusableModules.desktop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_ChangePasswordPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import PageObjects.desktop.Desk_BNY_LoginRegistrationPageObject;
import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import actions.GenericMethods;
import actions.WriteRegistrationData;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_Logout_Reusable {
	
	private static final boolean IsElementPresent = false;
	Desk_BNY_ChangePasswordPageObject changepassword;
	Desk_BNY_HomePageObject homePageObjects;
	GenericMethods genericMethods;
	String CurrentPageTitle;
	WebDriverWait xplicitWait;
	Desk_BNY_LoginRegistrationPageObject loginRegisterPageObjects;
	Desk_BNY_RegistrationPageObject registerPageObjects;
	boolean loggedInUserName;
	boolean loginForm;
	boolean bnyHomePage;
	boolean whsHomePage;
	boolean loginText;
	boolean registerForm;
	
	public void logout(WebDriver driver) {
		changepassword = PageFactory.initElements(driver, Desk_BNY_ChangePasswordPageObject.class);
		genericMethods =  new GenericMethods();		
		genericMethods.Click(driver, changepassword.HeaderUserName_LK,"ChangePassword HeaderUsername Link");
		genericMethods.Click(driver, changepassword.LogOut_LK,"ChangePassword Logout Link");
		genericMethods.Click(driver, changepassword.Homepage_logo,"ChangePassword Homepage Logo");		
	}
		
	public void generallogout(WebDriver driver) {
		changepassword = PageFactory.initElements(driver, Desk_BNY_ChangePasswordPageObject.class);
		genericMethods =  new GenericMethods();				
		genericMethods.Click(driver, changepassword.HeaderUserName_LK,"ChangePassword HeaderUsername Link");
		ATUReports.add("Click on arrow next to Hi<name>", "Arrow should be clicked", "Arrow is clicked and menu is opened", true,LogAs.INFO);
		genericMethods.Click(driver, changepassword.LogOut_LK,"Logout Link");		
	}
//================= User LogOut if LoggedIn===================================
		public void logOutifLoggedIn(WebDriver driver){
			homePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			genericMethods =  new GenericMethods();		
			xplicitWait = new WebDriverWait(driver, 30);
			boolean isLoggedInUserAccountArrowPresent;
			
			CurrentPageTitle=genericMethods.getTitle(driver);
			System.out.println(CurrentPageTitle);
			
			isLoggedInUserAccountArrowPresent=genericMethods.isElementPresentforMouseHover(homePageObjects.Home_TopNav_AccountArrow_LK);
			
			if (isLoggedInUserAccountArrowPresent==true) {
				ATUReports.add("verify User status", "User should be Logged In","User is Logged In",true,LogAs.INFO);
				generallogout(driver);
				/*genericMethods.Click(driver, homePageObjects.Home_TopNav_AccountArrow_LK, "Utility Nav Account dropdown arrow");
				genericMethods.Click(driver, homePageObjects.Home_UtilityNav_TopLogout_LK, "LogOutButton");*/
				//GenericMethods.Waitformilliseconds(5000);
				isLoggedInUserAccountArrowPresent=genericMethods.isElementPresentforMouseHover(homePageObjects.Home_TopNav_AccountArrow_LK);
				GenericMethods.Waitformilliseconds(5000);
				if (isLoggedInUserAccountArrowPresent=true) {
					ATUReports.add("verify User status", "User is still logged in","User is unable to logout",true,LogAs.INFO);
				}else {
					ATUReports.add("verify User status", "User is logged out","User is logged out successfully",true,LogAs.PASSED);
				}
			}else {
				ATUReports.add("verify User status", "User should be Logged In","User is already logged out",true,LogAs.INFO);
			}		    
		}
	} 

