package reusableModules.desktop;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import PageObjects.desktop.Desk_BNY_LookBookPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;



public class Desk_LookBook_Reusable {	
		Desk_BNY_LookBookPageObject lookbook;
		GenericMethods genericMethods;
		public String isLookBookPresent;
		WebDriverWait xplicitwait;
  
  public boolean LookBook(WebDriver driver) {
	  lookbook = PageFactory.initElements(driver, Desk_BNY_LookBookPageObject.class);
	  genericMethods = new GenericMethods();
	  xplicitwait = new WebDriverWait(driver, 30);
	  
	  genericMethods.mouseHover(driver, lookbook.Editorial_LK,"Editorial Link from top Nav");
	  genericMethods.Click(driver,lookbook.Editorial_LK,"Editorial Lookbook Link");
	  //GenericMethods.waitForPageLoaded(driver);
	  genericMethods.Click(driver, lookbook.LeftNav_LookBook_LK,"LookBook from Left Nav");
	  GenericMethods.waitForPageLoaded(driver);
	  
	  
	  boolean result= false;
	  int attemps = 0;
	  while (attemps < 2){
		  try{
	  String VerifyLookBookPage = genericMethods.getText(driver, lookbook.LookbookLanding_PG);
	  		result = true;
	  		
	  if (VerifyLookBookPage.equalsIgnoreCase("Lookbooks")) {
		  ATUReports.add("Verify the page","-","System should display lookbook landing page","Lookbook landing page with header :" +(genericMethods.getText(driver, lookbook.LookbookLanding_PG)) +" is displayed",true,LogAs.PASSED);
	 
		  String women= genericMethods.getText(driver, lookbook.LookbooksWomen_title);
		  System.out.println(women);
		  		if (women.equalsIgnoreCase("Women's Lookbooks")){
		  				ATUReports.add("Verify Women's title","-","Verify the women title on lookbook landing page","Lookbook landing page should display " + women + " title",true,LogAs.PASSED);
		  				String womensNoOfLookbook = genericMethods.getText(driver, lookbook.NoOfLookbooksForwomen);
		  				ATUReports.add("Number of lookbooks for women","-","Verify the number of lookbooks","Number of lookbooks for women is " + womensNoOfLookbook, true,LogAs.INFO); 
			  
		  				boolean isElementpresent = genericMethods.isElementPresentforMouseHover(lookbook.ViewMore_Women_LK);
		  				System.out.println(isElementpresent);
		  				
		  					if (lookbook.NoOfArticelsforWomen.size() > 6 && isElementpresent==true){			  
		  						System.out.println("System is on womens title");
		  						Assert.assertEquals(isElementpresent, true);
		  						ATUReports.add("Verify view more link for women's","-","View more link for women should be displayed", "View more link for womens is present",false,LogAs.PASSED);
		  					}else {
		  							//Assert.assertEquals(isElementpresent, false);
		  							ATUReports.add("Verify view more link for women's","-","View more link for women should be displayed", " Bussiness has disabled the View more link from BCC",true,LogAs.INFO);
		  					}
		  		}else {
		  				ATUReports.add("Verify Women's title","-","Verify the women title on lookbook landing page","Women's title is not displayed on lookbook landing page",true,LogAs.FAILED);
		  				Assert.assertEquals(women, "Women's Lookbooks");
		  		}
		  		GenericMethods.Waitformilliseconds(5000);
	  String men = genericMethods.getText(driver, lookbook.Lookbooksmen_title);
		  System.out.println(men);
		  		if (men.equalsIgnoreCase("Men's Lookbooks")){
		  				ATUReports.add("Verify Men's title","-","Verify the Men title on lookbook landing page","Lookbook landing page should display " + men + " title",true,LogAs.PASSED);
		  				String mensNoOfLookbook = genericMethods.getText(driver, lookbook.NoOfLookbooksForMen);
		  				ATUReports.add("Number of lookbooks for men","-","Verify the number of lookbooks for men","Number of lookbooks for men is " + mensNoOfLookbook, true,LogAs.INFO);
			  
		  				boolean isElementpresent1 = genericMethods.isElementPresentforMouseHover(lookbook.ViewMore_Men_LK);
		  					if (lookbook.NoOfArticelsforMen.size() > 6 && isElementpresent1 == true){
		  						System.out.println("System is on mens title");
		  						
		  						ATUReports.add("Verify view more link for men's","-","View more link for men should be displayed", "View more link for mens is present",false,LogAs.PASSED);  
		  					}else {
		  							//Assert.assertEquals(isElementpresent1, true);
		  							ATUReports.add("Verify view more link for Men's","-","View more link for men should be displayed", "Bussiness has disabled the View more link from BCC",true,LogAs.INFO);  
		  					}  
		  		}else {
		  				ATUReports.add("Verify Men's title","-","Verify the Men title on lookbook landing page","Men's title is not displayed on lookbook landing page.",true,LogAs.FAILED);
		  				Assert.assertEquals(women, "Men's Lookbooks");
		  		}
	  }else{
		  	ATUReports.add("Verify the page","-","System should display lookbook landing page","System does not display lookbook landing page",false,LogAs.FAILED);			  
		  	Assert.assertEquals(VerifyLookBookPage,"LOOKBOOKS");
	  } break;
		  }catch (StaleElementReferenceException e){
			System.out.println("system has trown stale element exception trying second time");  
		  }
		  attemps++;
	  } return result;
		  
  }
  
  

  public boolean lookbookBrowerPage(WebDriver driver, String Subcategory){
	  lookbook = PageFactory.initElements(driver, Desk_BNY_LookBookPageObject.class);
	  genericMethods = new GenericMethods();
	  xplicitwait = new WebDriverWait(driver, 30);
	  
	  genericMethods.mouseHover(driver, lookbook.Editorial_LK,"Editorial Link from top Nav");
	  genericMethods.Click(driver,lookbook.Editorial_LK,"Editorial Lookbook Link");
	  xplicitwait.until(ExpectedConditions.visibilityOf(lookbook.LeftNav_LookBook_LK));
	  
	  //GenericMethods.waitForPageLoaded(driver);
	  genericMethods.Click(driver, lookbook.LeftNav_LookBook_LK,"LookBook from Left Nav");
	  GenericMethods.waitForPageLoaded(driver);
	  
	  String firstArticleNamePresent;
	  String articelNameOnLookbookBrowserPage;
	  boolean isPaginationButtonPresent;
	  
	  boolean result = false;
	  int attemps = 0;
	  while (attemps < 3){
		  try{
	  
	  String VerifyLookBookPage = genericMethods.getText(driver, lookbook.LookbookLanding_PG);
	  if (VerifyLookBookPage.equalsIgnoreCase("Lookbooks")) {
		  ATUReports.add("Verify the page","-","System should display lookbook landing page","Lookbook landing page with header :" +(genericMethods.getText(driver, lookbook.LookbookLanding_PG)) +" is displayed",true,LogAs.PASSED);
		  if (Subcategory.equalsIgnoreCase("Women's Lookbooks")){
			  genericMethods.Click(driver,lookbook.Women_Lookbooks_LK, "Womens Link from lookbook landing page");
			  GenericMethods.Waitformilliseconds(10000);
			  xplicitwait.until(ExpectedConditions.visibilityOf(lookbook.Women_Lookbooks_1stArticle));
			  firstArticleNamePresent=genericMethods.getText(driver, lookbook.Women_Lookbooks_1stArticle);
			  System.out.println(firstArticleNamePresent);
			  ATUReports.add("Verify the First article displayed on women's lookbook page",firstArticleNamePresent,"1st Article should be " + firstArticleNamePresent ,"First article is :" + firstArticleNamePresent,true,LogAs.PASSED);
			  genericMethods.ClickUSingXpath(driver, "//div[@id='ajaxContainer']/div/section/article/a/h4[contains(text(),'"+firstArticleNamePresent+"')]", "First article on women's lookbook landing page");
			  GenericMethods.waitForPageLoaded(driver);
			  articelNameOnLookbookBrowserPage=genericMethods.getText(driver, "//div[@id='atg_store_content']//h1[contains(text(),'"+firstArticleNamePresent+"')]");
			  	if(articelNameOnLookbookBrowserPage.equalsIgnoreCase(firstArticleNamePresent)){
			  		ATUReports.add("Verify the artile name on lookbook browse page",firstArticleNamePresent +" article name" ,"System should display"+ firstArticleNamePresent +" as first article name ","System display's " + firstArticleNamePresent +" as first article",true,LogAs.PASSED);
			  		isPaginationButtonPresent=genericMethods.isElementPresentforMouseHover(lookbook.pagination2_BTN);
			  		if(isPaginationButtonPresent=true){
			  			genericMethods.Click(driver, lookbook.pagination2_BTN,"on page 2");
					  	ATUReports.add("System is displaying page 2 for women's lookbook","Page 2","Page 2 should be displayed for women's lookbook ", "User has selected second page on lookbook",true, LogAs.INFO);
			  		}else{
			  			ATUReports.add("System is not displaying page 2 for women's lookbook","Page 2","Page 2 should be displayed for women's lookbook ", "System does not displays page 2",true, LogAs.FAILED);
			  			Assert.assertEquals(isPaginationButtonPresent, true);
			  		}
			  	}else{
			  		ATUReports.add("Verify the artile name on lookbook browse page",firstArticleNamePresent +" article name" ,"System should display"+ firstArticleNamePresent +" as first article name ","System does not display " + firstArticleNamePresent +" as first article",true,LogAs.FAILED);
			  		Assert.assertEquals(articelNameOnLookbookBrowserPage, firstArticleNamePresent);
			  	}
			  
		  }else if(Subcategory.equalsIgnoreCase("Men's Lookbooks")){
			  System.out.println("System is here");
			  
		  }
	  }else{
		  	ATUReports.add("Verify the page","-","System should display lookbook landing page","System does not display lookbook landing page",false,LogAs.FAILED);			  
		  	Assert.assertEquals(VerifyLookBookPage,"LOOKBOOKS");
	  }break;
		  }catch (StaleElementReferenceException e){
			System.out.println("system has trown stale element exception trying second time");  
		  }
		  attemps++;
	  } return result;
  	}
}	  
/*	  
	  String womenname= genericMethods.getText(driver, lookbook.Women_Lookbooks_gettext);
	  if (womenname.contains("Shop Isabel Marant, modern icon of French style.")) {
		  ATUReports.add("System is displaying lookbook for women"," lookbook for women ", "first link for lookbook  : " +(genericMethods.getText(driver, lookbook.Women_Lookbooks_gettext)),  true, LogAs.PASSED);
	   }else {
		   ATUReports.add("System is not displaying lookbook for women"," lookbook for women ", "first link for lookbook  : " +(genericMethods.getText(driver, lookbook.Women_Lookbooks_gettext)),  true, LogAs.FAILED);				
	   }
	  genericMethods.Click(driver, lookbook.ShopIsabel_LK,"Lookbook Womenbrandname Link");
	  String womenbrandname= genericMethods.getText(driver, lookbook.ShopIsabel_gettext);
	  
	  if (womenname.contains("Shop Isabel Marant, modern icon of French style.")){
	    ATUReports.add("System is displaying lookbook for women"," lookbook for women ", "first link for lookbook  : " +(genericMethods.getText(driver, lookbook.ShopIsabel_gettext)),  true, LogAs.PASSED);
	    Assert.assertEquals((genericMethods.getText(driver, lookbook.ShopIsabel_gettext)), "Shop Isabel Marant, modern icon of French style.");			
	   }else {
		   ATUReports.add("System is not displaying lookbook for women"," lookbook for women ", "first link for lookbook  : " +(genericMethods.getText(driver, lookbook.ShopIsabel_gettext)),  true, LogAs.FAILED);
		   Assert.assertEquals((genericMethods.getText(driver, lookbook.ShopIsabel_gettext)), "Shop Isabel Marant, modern icon of French style.");				
	   }
	  
	  genericMethods.Click(driver, lookbook.pagination2_BTN,"Lookbook Pagination2 BTN");
	  ATUReports.add("System is displaying pagination for lookbook for women"," pagination for lookbook for women ", "Customer select second page on lookbook",  true, LogAs.INFO);
	  ATUReports.add("System is displaying ViewAllproduct for lookbook for women"," ViewAllproduct for lookbook for women ", "Customer select View All product on lookbook",  true, LogAs.INFO);

	  genericMethods.Click(driver, lookbook.Editorial_LK,"Editorial Lookbook link");
	  genericMethods.Click(driver, lookbook.Women_Lookbooks_LK,"Women Lookbooks Link");			  
	  genericMethods.Click(driver, lookbook.Men_Lookbooks_link,"Men Lookbooks Link");			  
	  genericMethods.Click(driver, lookbook.RalphLaurenBlack_LK,"Lookbook Brandname Link");
	  
	  String Mensproductname= genericMethods.getText(driver, lookbook.RalphLaurenBlack_gettext);
	  if (womenname.contains("Strike a Chord: Jon Batiste Models Ralph Lauren Black Label.")){
	    ATUReports.add("System is displaying lookbook for men"," lookbook for Men ", "first link for Men lookbook  : " +(genericMethods.getText(driver, lookbook.RalphLaurenBlack_gettext)),  true, LogAs.PASSED);
	    Assert.assertEquals((genericMethods.getText(driver, lookbook.RalphLaurenBlack_gettext)), "Strike a Chord: Jon Batiste Models Ralph Lauren Black Label.");			
	   }else{
		   ATUReports.add("System is not displaying lookbook for men"," lookbook for men ", "first link for Men lookbook  : " +(genericMethods.getText(driver, lookbook.RalphLaurenBlack_gettext)),  true, LogAs.FAILED);
		   Assert.assertEquals((genericMethods.getText(driver, lookbook.RalphLaurenBlack_gettext)), "Strike a Chord: Jon Batiste Models Ralph Lauren Black Label.");				
	   }
	  
	  genericMethods.Click(driver, lookbook.Menpagination2_LK,"Lookbook Men Pagination2 Link");
	  genericMethods.Click(driver, lookbook.twitter_LK,"Lookbook Twitter Link");
	  genericMethods.Click(driver, lookbook.Pinterest_LK,"Lookbook Pinterest Link");	  
  }*/
  
 

  
