package reusableModules.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import PageObjects.desktop.Desk_BNY_OrderReviewPageObject;
import PageObjects.desktop.Desk_BNY_QASPageObject;
import PageObjects.desktop.Desk_BNY_OrderReviewPageObject;
import PageObjects.desktop.Desk_BNY_ShippingPageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;



public class Desk_ReviewOrderPage_Reusable 
{
	Desk_MYBagCheckout_Reusable myBagCheckout;
	String CurrentPageTitle;
	Desk_BNY_QASPageObject qasPageObjects;
	String isQASPresent;
	String isQASradioButtonPreset;
	WebDriverWait xplicitWait;
	Desk_LoginRegister_Reusable loginRegisterReusable;
	GenericMethods genericMethods;
	public void GotoReviewPAge(WebDriver driver,String emailAddress,String password,String URLFromXML,String Email ,String Password,String Country ,String FName ,String	LName ,String ShippingAddress1,String ShippingAddress2 ,String ShippingCity,String ShippingZip,String	ShippingPhone ,String CardType,String CardNo ,String CardCVV, String BillingAddress1,String	BillingCity,String	BillingState,String	BillingZip	,String Promocode,String BarneysUrl1,String WareHouseUrl2,String  Site , String GiftCard) {

	    genericMethods = new GenericMethods();	     
		Desk_LoginRegister_Reusable loginRegisterReusable = new Desk_LoginRegister_Reusable();		
		Desk_BNY_OrderReviewPageObject review;
		review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		loginRegisterReusable.loginFromHeader(driver, emailAddress, password);


		//-------------------Empty My Bag module ---------------
		myBagCheckout=new Desk_MYBagCheckout_Reusable();
		CurrentPageTitle=genericMethods.getTitle(driver);		
		if (CurrentPageTitle.equalsIgnoreCase("Barneys Warehouse")){			
			myBagCheckout.emptyMyBag(driver, "WHS");
		}else{
			myBagCheckout.emptyMyBag(driver, "BNY");
		}
		
		//-------------------Empty My Bag module ends---------------	
		Desk_AddToCart_Reusable.GotoPDP(driver, URLFromXML,BarneysUrl1, WareHouseUrl2);
		Desk_AddToCart_Reusable cart= new Desk_AddToCart_Reusable();
		cart.AddCartNEW(driver);
		
		Desk_FillShippingForm_Reusable shipping = new Desk_FillShippingForm_Reusable();
		shipping.startFillingShippingForm(driver, FName, LName, ShippingAddress1 , ShippingAddress2, ShippingCity,"New York", ShippingZip, ShippingPhone);
		Desk_FillBillingForm_Reusable billing = new Desk_FillBillingForm_Reusable();
		billing.startFillingBillingForm(driver, Site, BillingAddress1, BillingCity, BillingState, BillingZip, CardType, CardNo, CardCVV, Promocode, GiftCard);
		
		GenericMethods.Waitformilliseconds(2000);
		genericMethods.waitForPageLoaded(driver);
		String pagename = genericMethods.getText(driver, review.ReviewOrderPage_PageTitle_TV);
		if(pagename.equalsIgnoreCase("Review Order")){
			  ATUReports.add("Verify system displaying review order page","System should display review order page", "System displaying review order page","System displaying =: " +(genericMethods.getText(driver,review.ReviewOrderPage_PageTitle_TV)),  true, LogAs.PASSED);
			  Assert.assertEquals((genericMethods.getText(driver, review.ReviewOrderPage_PageTitle_TV)), "Review Order");
		}else{
			 ATUReports.add("Verify system is not displaying review order page","System should display review order page", "System displaying review order page","System displaying =: " +(genericMethods.getText(driver,review.ReviewOrderPage_PageTitle_TV)),  true, LogAs.FAILED);
			 Assert.assertEquals((genericMethods.getText(driver, review.ReviewOrderPage_PageTitle_TV)), "Review Order");
		}					
	}

	public void EditShipping(WebDriver driver,String Email ,String Password,String Country ,String FName ,String	LName ,String ShippingAddress1,String ShippingAddress2 ,String ShippingCity,String ShippingZip,String	ShippingPhone ,String CardType,String CardNo ,String CardCVV, String BillingAddress1,String	BillingCity,String	BillingState,String	BillingZip	,String Promocode,String BarneysUrl1,String WareHouseUrl2,String  Site , String GiftCard) {
		
		Desk_BNY_OrderReviewPageObject review;
	     genericMethods = new GenericMethods();
	     xplicitWait=new WebDriverWait(driver, 15);
		 review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		 qasPageObjects=PageFactory.initElements(driver, Desk_BNY_QASPageObject.class);
		 
	     genericMethods.Click(driver, review.ReviewOrder_EditShippingAddress_LK,"ReviewOrder EditShipAdd Link");
	     
	     xplicitWait.until(ExpectedConditions.visibilityOf(review.ShippingPage_GetText));
			String Shippingpagename = genericMethods.getText(driver, review.ShippingPage_GetText);
			if(Shippingpagename.equalsIgnoreCase("Shipping")){
				  ATUReports.add("Verify system displaying shipping page","System should display shipping page", "System displaying shipping page","System displaying =: " +(genericMethods.getText(driver,review.ShippingPage_GetText)),  true, LogAs.PASSED);
				  Assert.assertEquals((genericMethods.getText(driver, review.ShippingPage_GetText)), "Shipping");
			}else{
				 ATUReports.add("Verify system is not displaying shipping page","System should display shipping page", "System displaying shipping page","System displaying =: " +(genericMethods.getText(driver,review.ShippingPage_GetText)),  true, LogAs.FAILED);
				 Assert.assertEquals((genericMethods.getText(driver,review.ShippingPage_GetText)), "Shipping");
			}
		
		genericMethods.Click(driver,review.EditShipping_LK,"EditShipping Link");
		GenericMethods.Waitformilliseconds(1000);
		genericMethods.inputValue(driver,review.FirstName_TB, "yogita3","FirstName Textbox");
		genericMethods.Click(driver,review.Continue_BTN,"Continue BTN");
		GenericMethods.Waitformilliseconds(1000);
		
		
		
		genericMethods.waitForPageLoaded(driver);		
		xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(),'Checkout')]")));
		isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		GenericMethods.Waitformilliseconds(2000);
		isQASradioButtonPreset=genericMethods.isElementPresent(driver,"//form[@id='dwfrm_addForm']/fieldset/table/tbody/tr/td[1]/input" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']")).click();
			GenericMethods.Waitformilliseconds(1000);
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			GenericMethods.Waitformilliseconds(3000);
		}
				
		GenericMethods.Waitformilliseconds(1000);
		genericMethods.inputValue(driver,review.CVV_TB, "123","CVV Textbox");
		genericMethods.Click(driver,review.BillingContinue_BTN,"Billing Continue BTN");			
		}		
	

	public void EditBilling(WebDriver driver,String Email ,String Password,String Country ,String FName ,String	LName ,String ShippingAddress1,String ShippingAddress2 ,String ShippingCity,String ShippingZip,String	ShippingPhone ,String CardType,String CardNo ,String CardCVV, String BillingAddress1,String	BillingCity,String	BillingState,String	BillingZip	,String Promocode,String BarneysUrl1,String WareHouseUrl2,String  Site , String GiftCard) 
	{
		Desk_BNY_OrderReviewPageObject review;
		review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods = new GenericMethods();
			 
		genericMethods.Click(driver,review.EditBillingreview_LK,"EditBillingReview Link");
		GenericMethods.Waitformilliseconds(1000);
		     
	     String Shippingpagename = genericMethods.getText(driver,review.BillingPage_GetText);
			if(Shippingpagename.equalsIgnoreCase("Billing"))
			{
				  ATUReports.add("Verify system displaying Billing page","System should display billing page", "System displaying billing page","System displaying =: " +(genericMethods.getText(driver,review.BillingPage_GetText)),  true, LogAs.PASSED);
				  Assert.assertEquals((genericMethods.getText(driver,review.BillingPage_GetText)), "Billing");

			}else
			{
				 ATUReports.add("Verify system is not displaying billing page","System should display billing page", "System displaying billing page","System displaying =: " +(genericMethods.getText(driver,review.BillingPage_GetText)),  true, LogAs.FAILED);
				 Assert.assertEquals((genericMethods.getText(driver,review.BillingPage_GetText)), "Billing");

			}
				
			genericMethods.inputValue(driver,review.CVV_TB, "123","CVV Textbox");
			genericMethods.Click(driver,review.BillingContinue_BTN,"Billing Continue BTN");			
			ATUReports.add("Verify page displayed","", "","",  true, LogAs.INFO);
		
	}
	
	public void EditPaymentoption(WebDriver driver,String Email ,String Password,String Country ,String FName ,String	LName ,String ShippingAddress1,String ShippingAddress2 ,String ShippingCity,String ShippingZip,String	ShippingPhone ,String CardType,String CardNo ,String CardCVV, String BillingAddress1,String	BillingCity,String	BillingState,String	BillingZip	,String Promocode,String BarneysUrl1,String WareHouseUrl2,String  Site , String GiftCard)
	{
		Desk_BNY_OrderReviewPageObject review;
		 review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
	     genericMethods = new GenericMethods();
		 genericMethods.Click(driver,review.Edit_Payment_LK,"EditPayment Link");
		 GenericMethods.Waitformilliseconds(1000);
		     
		     String Shippingpagename = genericMethods.getText(driver,review.BillingPage_GetText);
				if(Shippingpagename.equalsIgnoreCase("Billing"))
				{
					  ATUReports.add("Verify system displaying Billing page","System should display billing page", "System displaying billing page","System displaying =: " +(genericMethods.getText(driver,review.BillingPage_GetText)),  true, LogAs.PASSED);
					  Assert.assertEquals((genericMethods.getText(driver,review.BillingPage_GetText)), "Billing");

				}else{
					 ATUReports.add("Verify system is not displaying billing page","System should display billing page", "System displaying billing page","System displaying =: " +(genericMethods.getText(driver,review.BillingPage_GetText)),  true, LogAs.FAILED);
					 Assert.assertEquals((genericMethods.getText(driver,review.BillingPage_GetText)), "Billing");

				}
			
		    GenericMethods.Waitformilliseconds(1000);
		    genericMethods.inputValue(driver,review.CardholderName_TB, "Yogeeta1","CardholderName Textbox");
		    genericMethods.inputValue(driver,review.CVV_TB, "123","CVV Textbox");			
			genericMethods.Click(driver,review.BillingContinue_BTN,"Billing Continue BTN");
		
	}

	public void EditMyBag(WebDriver driver,String Email ,String Password,String Country ,String FName ,String	LName ,String ShippingAddress1,String ShippingAddress2 ,String ShippingCity,String ShippingZip,String	ShippingPhone ,String CardType,String CardNo ,String CardCVV, String BillingAddress1,String	BillingCity,String	BillingState,String	BillingZip	,String Promocode,String BarneysUrl1,String WareHouseUrl2,String  Site , String GiftCard)
	{
		Desk_BNY_OrderReviewPageObject review;
			 review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		     genericMethods = new GenericMethods();
		     
		     genericMethods.Click(driver,review.Edit_MyBag_LK,"Edit MyBag Link");
		     GenericMethods.Waitformilliseconds(1000);
		     
		     String pagename = genericMethods.getText(driver,review.Edit_MyBag_gettext);
		     
		     if (pagename.contains("MY BAG"))
		    {
		     ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is on MY BAG page",true,LogAs.PASSED);
		     Assert.assertEquals((genericMethods.getText(driver,review.Edit_MyBag_gettext)), "MY BAG");
		     
		    }else
		    {
		     ATUReports.add("Customer is on MY BAG", "Customer should be on MY BAG Page","Customer is not on MY BAG page",true,LogAs.FAILED);
		     Assert.assertEquals((genericMethods.getText(driver,review.Edit_MyBag_gettext)), "MY BAG");		     
		    }
		     
		    genericMethods.inputValue(driver,review.MyBagQuantity_TB, "1","MyBagQuantity Textbox");
		    genericMethods.Click(driver,review.Update_LK,"Update Link");
		    GenericMethods.Waitformilliseconds(2000);
		    genericMethods.Click(driver,review.ContinuefromCart_BTN,"ContinuefromCart BTN");
		    GenericMethods.Waitformilliseconds(1000);
		    genericMethods.Click(driver,review.Continue_BTN,"Continue BTN");
			GenericMethods.Waitformilliseconds(1000);
			genericMethods.inputValue(driver,review.CVV_TB, "123","CVV Textbox");
			genericMethods.Click(driver,review.BillingContinue_BTN,"Billing Continue BTN");
	}	


	public void OrderSummary(WebDriver driver, String Email ,String Password,String Country ,String FName ,String	LName ,String ShippingAddress1,String ShippingAddress2 ,String ShippingCity,String ShippingZip,String	ShippingPhone ,String CardType,String CardNo ,String CardCVV, String BillingAddress1,String	BillingCity,String	BillingState,String	BillingZip	,String Promocode,String BarneysUrl1,String WareHouseUrl2,String  Site , String GiftCard)
	{
		Desk_BNY_OrderReviewPageObject review;
		Desk_BNY_ShippingPageObject shipp1;
		double shipping;
		String Cartprice;
	    WebElement price;
			 review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
			 shipp1 = PageFactory.initElements(driver, Desk_BNY_ShippingPageObject.class);
		     genericMethods = new GenericMethods();
		     genericMethods.Click(driver,review.ReviewOrder_EditShippingAddress_LK,"ReviewOrder EditShipAdd Link");
		 	 String defualtshippingmethodUS = genericMethods.getText(driver,shipp1.ShippingChargesForUS_gettext);
		   				  
				  if (defualtshippingmethodUS.contains("$0.00"))
				   {
				    ATUReports.add("System selecting default shipping method Ground", "Default shipping method is selected","default shipping method is Ground : " +(genericMethods.getText(driver,shipp1.ShippingChargesForUS_gettext)),  true, LogAs.PASSED);
				    Assert.assertEquals((genericMethods.getText(driver,shipp1.ShippingChargesForUS_gettext)), "$0.00");
				
				   }else
				   {
					   ATUReports.add("System is not selecting default shipping method Ground", "Default shipping method is not  selected","default shipping method is not selected Ground : " +(genericMethods.getText(driver,shipp1.ShippingChargesForUS_gettext)),  true, LogAs.FAILED);
					   Assert.assertEquals((genericMethods.getText(driver,shipp1.ShippingChargesForUS_gettext)), "$0.00");
				   }
				    GenericMethods.Waitformilliseconds(1000);
				    genericMethods.Click(driver,review.Continue_BTN,"Continue BTN");
					GenericMethods.Waitformilliseconds(1000);
					genericMethods.inputValue(driver,review.CVV_TB, "123","CVV Textbox");
					genericMethods.Click(driver,review.BillingContinue_BTN,"BillinContinue BTN");
				
				String islink = genericMethods.isElementPresent(driver, "//*[@id='cart-table']/tbody/tr/td[4]/span[4]");
				if(islink.equals("present")){
					 Cartprice = genericMethods.getText(driver,review.PricefromCartpage_gettext);			
				}else{
					 Cartprice = genericMethods.getText(driver,review.PricefromCartpagewithcoupon_gettext);
				}
				
					String subtotal = genericMethods.getText(driver,review.Pricefromsummary_gettext) ; 
					if (Cartprice.contains(subtotal))
					   {
					    ATUReports.add("System selecting default shipping method Ground", "Default shipping method is selected","default shipping method is Ground : " +(genericMethods.getText(driver,review.PricefromCartpage_gettext)),  true, LogAs.PASSED);
					    Assert.assertEquals((genericMethods.getText(driver,review.PricefromCartpage_gettext)), subtotal);
					
					   }else
					   {
						   ATUReports.add("System is not selecting default shipping method Ground", "Default shipping method is not  selected","default shipping method is not selected Ground : " +(genericMethods.getText(driver,review.PricefromCartpage_gettext)),  true, LogAs.FAILED);
						   Assert.assertEquals((genericMethods.getText(driver,review.PricefromCartpage_gettext)), subtotal);
					   }
					
				String tax1 = genericMethods.getText(driver,review.taxfromSummary_gettext) ;
				String ordertotal1 = genericMethods.getText(driver,review.Ordertotalfromsummary_gettext);
				
					  
				if (defualtshippingmethodUS.equalsIgnoreCase("$0.00") )
				{
					 shipping = 0.00;
				}else if(defualtshippingmethodUS.equalsIgnoreCase("$18.00"))
				{
					 shipping = 18.00;
				}else
				{
					 shipping = 25.00;
				}
				 
				double subtotal1 = Double.parseDouble(subtotal.substring(1));
				double tax = Double.parseDouble(tax1.substring(1));
				double ordertotal = Double.parseDouble(ordertotal1.substring(1));
				double total = shipping+subtotal1+tax;
				
				if(Math.abs(ordertotal) ==  Math.abs(total ))
				{
				    ATUReports.add("System displaying order total correct on order review page", "Order total correct","Order Total is  : " +ordertotal ,  true, LogAs.PASSED);
				    Assert.assertEquals(ordertotal ,total );
				
				   }else
				   {
					   ATUReports.add("System is not displaying order total correct on order review page", "Order total correct","Order Total is  : " +ordertotal,  true, LogAs.FAILED);
					   Assert.assertEquals(ordertotal ,total );
				   }				
	}
	
	public void HaveQuestion(WebDriver driver) {
		Desk_BNY_OrderReviewPageObject review;
		review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
		genericMethods = new GenericMethods();
	    GenericMethods.Waitformilliseconds(5000);
	    genericMethods.waitForPageLoaded(driver);
	    String havquestion =genericMethods.getText(driver,review.havequetion_gettext); 
		   
	   if (havquestion.contains("HAVE QUESTIONS?")){
	     ATUReports.add("System displays HAVE QUESTIONS? section on order summary section", "System should display HAVE QUESTIONS? section","Customer is order review page",true,LogAs.PASSED);
	     Assert.assertEquals((genericMethods.getText(driver,review.havequetion_gettext)), "HAVE QUESTIONS?\nWE'D LOVE TO HELP.");		     
	    }else{
	    	 ATUReports.add("System is not displays HAVE QUESTIONS? section on order summary section", "System should display HAVE QUESTIONS? section","Customer is order review page",true,LogAs.FAILED);
		     Assert.assertEquals((genericMethods.getText(driver,review.havequetion_gettext)), "HAVE QUESTIONS?\nWE'D LOVE TO HELP.");		     
	    }		   
	   genericMethods.Click(driver,review.Email_LK,"Email Link");
	   genericMethods.Click(driver,review.policy_LK,"Policy Link");
		 
	}

	public void Couponcode(WebDriver driver,String Email ,String Password,String Country ,String FName ,String	LName ,String ShippingAddress1,String ShippingAddress2 ,String ShippingCity,String ShippingZip,String	ShippingPhone ,String CardType,String CardNo ,String CardCVV, String BillingAddress1,String	BillingCity,String	BillingState,String	BillingZip	,String Promocode,String BarneysUrl1,String WareHouseUrl2,String  Site , String GiftCard){
		Desk_BNY_OrderReviewPageObject review;
		review = PageFactory.initElements(driver, Desk_BNY_OrderReviewPageObject.class);
	    genericMethods = new GenericMethods();
	    
	    //Promocode			    
	    String Sectionname = genericMethods.getText(driver,review.Couponcode_gettext);
	    if (Sectionname.contains("Coupon")){
	     ATUReports.add("System displaying coupon section on order review page", "System should display Coupon section","Customer is order review page",true,LogAs.PASSED);
	     Assert.assertEquals((genericMethods.getText(driver,review.Couponcode_gettext)), "Coupon");			     
	    }else{
	    	ATUReports.add("System is not displaying coupon section on order review page", "System should display Coupon section","Customer is order review page",true,LogAs.FAILED);
		     Assert.assertEquals((genericMethods.getText(driver,review.Couponcode_gettext)), "Coupon");				     
	    }
	    
	    String Couponstatus = genericMethods.getText(driver,review.CouponcodeApplied_gettext);
	    if(Couponstatus.equals("Applied")) {
		     ATUReports.add("System displaying coupon section on order review page", "System should display Coupon section","Customer is order review page",true,LogAs.PASSED);
		     Assert.assertEquals((genericMethods.getText(driver,review.CouponcodeApplied_gettext)), "Applied");			     
		 }else{
		    	ATUReports.add("System is not displaying coupon section on order review page", "System should display Coupon section","Customer is order review page",true,LogAs.FAILED);
			     Assert.assertEquals((genericMethods.getText(driver,review.CouponcodeApplied_gettext)), "Applied");					     
	     }
	    
	    genericMethods.Click(driver,review.EditCopon_LK,"EditCopon Link");
	    GenericMethods.Waitformilliseconds(1000);			    
	    String pagename1 = genericMethods.getText(driver,review.Edit_MyBag_gettext);
	    if(pagename1.equals("MY BAG")) {
		     ATUReports.add("System displaying My Bag page", "System should display My Bag page","Customer is My Bag page",true,LogAs.PASSED);
		     Assert.assertEquals((genericMethods.getText(driver,review.Edit_MyBag_gettext)), "MY BAG");				     
		 }else{
			 ATUReports.add("System is not displaying My Bag page", "System should display My Bag page","Customer is My Bag page",true,LogAs.PASSED);
		     Assert.assertEquals((genericMethods.getText(driver,review.Edit_MyBag_gettext)), "MY BAG");				     
	     }			    
	    genericMethods.Click(driver,review.Remove_LK,"Remove Link");
	}
}
