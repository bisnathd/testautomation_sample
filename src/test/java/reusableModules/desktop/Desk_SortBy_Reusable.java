package reusableModules.desktop;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import PageObjects.desktop.Desk_BNY_CartPageObject;
import PageObjects.desktop.Desk_BNY_CategoryLandingPageObject;
import PageObjects.desktop.Desk_BNY_DesignerPageObject;
import PageObjects.desktop.Desk_BNY_NavigationPageObject;
import PageObjects.desktop.Desk_BNY_RegistrationPageObject;
import PageObjects.desktop.Desk_BNY_SortByPageObject;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;



public class Desk_SortBy_Reusable {
	
	Desk_BNY_NavigationPageObject TopNav;
	Desk_BNY_HomePageObject HomeDesigners;
	Desk_BNY_DesignerPageObject Designer_Objects;
	Desk_BNY_CartPageObject cart;
	Desk_BNY_SortByPageObject sort;
	Desk_BNY_RegistrationPageObject Register_Objects;
	WebDriverWait xplicitWait ;
	String CurrentPageTitle;
	GenericMethods genericMethods;
	static String priceInString,DesignerSelected,DesignerInString,SizeSelected;
	static int priceInInt;
	static int IntBefore, IntAfter;
	static String xpathInitialbefore;
	static String xpathInitialafter;
	static String isPriceElementPresent1,isPriceElementPresent2,isDesignerElementPresent1,isSizeElementPresent1,isBuyElementPresent1;
	int sorted=0;

	
/*	public void SortByHighToLow(WebDriver driver) {
		cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
		sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		//genericMethods.Click(driver, sort.MyAcc_SortBy_DD);
		genericMethods.selectDropdown(driver, sort.MyAcc_SortBy_DD, "Price High To Low","Sort Select Dropdown");
		genericMethods.Waitformilliseconds(10000);;
	}*/
	
public void SortByHighToLow(WebDriver driver,String sortText){
		sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
		genericMethods=new GenericMethods();
		genericMethods.selectDropdown(driver,sort.BrowsePage_SortBy_DD, sortText,"SortBy Select Dropdown");
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(sort.ProductResultPage_48_96_All_TK));
		ATUReports.add("Verify value in the Sort By Dropdown", "Value in the Sort By Dropdown should be "+sortText,"Value in dropdown is as per" + sortText, true, LogAs.PASSED);	
}

public void SortByLowToHigh(WebDriver driver) {
		cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
		sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		genericMethods.Click(driver, sort.BrowsePage_SortBy_DD,"SortBy Select Dropdown");
		//genericMethods.selectDropdown(driver, sort.rowsePage_SortBy_DD, "Price Low To High","Sort Select Dropdown");
		//genericMethods.Waitformilliseconds(10000);;
	}

public void FilterByDesigner(WebDriver driver) {
	
	cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);

	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	
	genericMethods.Click(driver, sort.MyAcc_DesignerFilter_LK,"DesignerFilter Link");
	DesignerSelected=driver.findElement(By.xpath("//div[@class='jspContainer']/div/li[9]/a")).getText();
	DesignerSelected=DesignerSelected.toUpperCase();
	System.out.println(DesignerSelected);
	//genericMethods.selectDropdown(driver, sort.MyAcc_SortBy_DD, "Price High To Low");
	GenericMethods.Waitformilliseconds(10000);;
}
		public  void PriceHighToLow(WebDriver driver,String sortText)
		{ 
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			for(int i=1;i<=6;i++){
				xpathInitialbefore="//div[@id='search-result-items']/div["+i+"]";
				xpathInitialafter="//div[@id='search-result-items']/div["+(i+1)+"]";
				isPriceElementPresent1=genericMethods.isElementPresent(driver,xpathInitialbefore+"/div/div[2]/div[@class='product-pricing']/span" );
				if (isPriceElementPresent1.equals("present")) {
					priceInString=driver.findElement(By.xpath(xpathInitialbefore+"/div/div[2]/div[@class='product-pricing']/span")).getText();
					if (priceInString.equals("$N/A")) {
						System.out.println(i);						
					}else {
						priceInString=giveDollerValue(priceInString);
						priceInInt=Integer.parseInt(priceInString);			
						IntBefore=priceInInt;
					}				
				}else {
					priceInString=driver.findElement(By.xpath(xpathInitialbefore+"/div[3]/div[2]/div[@class='product-pricing']/div/div[@class='clearfix']/span[@class='product-sales-price']")).getText();
					if (priceInString.equals("$N/A")) {
						System.out.println(i);
					}else {
						priceInString=giveDollerValue(priceInString);
						priceInInt=Integer.parseInt(priceInString);					
						IntBefore=priceInInt;
					}				
				}
				isPriceElementPresent2=genericMethods.isElementPresent(driver,xpathInitialafter+"/div/div[2]/div[@class='product-pricing']/span" );
				if (isPriceElementPresent1.equals("present")) {
					priceInString=driver.findElement(By.xpath(xpathInitialafter+"/div/div[2]/div[@class='product-pricing']/span")).getText();
					
					if (priceInString.equals("$N/A")) {
						System.out.println(i);
						
						}else {
								priceInString=giveDollerValue(priceInString);
							
							
							priceInInt=Integer.parseInt(priceInString);			
							
							
							IntAfter=priceInInt;
							
							
					}
					
				}else {
					priceInString=driver.findElement(By.xpath(xpathInitialafter+"/div[3]/div[2]/div[@class='product-pricing']/div/div[@class='clearfix']/span[@class='product-sales-price']")).getText();
					
					if (priceInString.equals("$N/A")) {
						System.out.println(i);
						
						}else {
								priceInString=giveDollerValue(priceInString);
							
							
							priceInInt=Integer.parseInt(priceInString);					
							IntAfter=priceInInt;
						}				
					}
			if (IntBefore<IntAfter)
			{sorted=1;
				ATUReports.add("Check Whether the price is sorted", "Should be sorted High to Low", "Is not Sorted High to Low", true,LogAs.FAILED);
			break;
			}
			
			
			}
			if (sorted==0)
			{
				ATUReports.add("Check Whether the price is sorted", "Should be sorted High to Low", "Is Sorted High to Low", true,LogAs.FAILED);			
			}		
		
		
		}
		
		public  void PriceLowToHigh(WebDriver driver) 
		{ 
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			for(int i=1;i<=6;i++){
				xpathInitialbefore="//div[@id='search-result-items']/div["+i+"]";
				xpathInitialafter="//div[@id='search-result-items']/div["+(i+1)+"]";
				isPriceElementPresent1=genericMethods.isElementPresent(driver,xpathInitialbefore+"/div/div[2]/div[@class='product-pricing']/span" );
				if (isPriceElementPresent1.equals("present")) {
					priceInString=driver.findElement(By.xpath(xpathInitialbefore+"/div/div[2]/div[@class='product-pricing']/span")).getText();
					
					if (priceInString.equals("$N/A")) {
						System.out.println(i);
						
						}else {
								priceInString=giveDollerValue(priceInString);
							
							
							priceInInt=Integer.parseInt(priceInString);			
							
							
							IntBefore=priceInInt;
							
							
					}
					
				}else {
					priceInString=driver.findElement(By.xpath(xpathInitialbefore+"/div[3]/div[2]/div[@class='product-pricing']/div/div[@class='clearfix']/span[@class='product-sales-price']")).getText();
					
					if (priceInString.equals("$N/A")) {
						System.out.println(i);
						
						}else {
								priceInString=giveDollerValue(priceInString);
							
							
							priceInInt=Integer.parseInt(priceInString);					
							IntBefore=priceInInt;
						}				
					}
				isPriceElementPresent2=genericMethods.isElementPresent(driver,xpathInitialafter+"/div/div[2]/div[@class='product-pricing']/span" );
				if (isPriceElementPresent1.equals("present")) {
					priceInString=driver.findElement(By.xpath(xpathInitialafter+"/div/div[2]/div[@class='product-pricing']/span")).getText();
					
					if (priceInString.equals("$N/A")) {
						System.out.println(i);
						
						}else {
								priceInString=giveDollerValue(priceInString);
							
							
							priceInInt=Integer.parseInt(priceInString);			
							
							
							IntAfter=priceInInt;
							
							
					}
					
				}else {
					priceInString=driver.findElement(By.xpath(xpathInitialafter+"/div[3]/div[2]/div[@class='product-pricing']/div/div[@class='clearfix']/span[@class='product-sales-price']")).getText();
					
					if (priceInString.equals("$N/A")) {
						System.out.println(i);
						
						}else {
								priceInString=giveDollerValue(priceInString);
							
							
							priceInInt=Integer.parseInt(priceInString);					
							IntAfter=priceInInt;
						}				
					}
			if (IntBefore>IntAfter)
			{sorted=1;
				ATUReports.add("Check Whether the price is sorted", "Should be sorted Low to High", "Is not Sorted Low to High", true,LogAs.FAILED);
			break;
			}
			
			
			}
			if (sorted==0)
			{
				ATUReports.add("Check Whether the price is sorted", "Should be sorted Low to High", "Is Sorted Low to High", true,LogAs.FAILED);			
			}		
		
		
		}
		public static String giveDollerValue(String productPrice)
		{
			
			String onlyDollerValue;
			
			
			String newStr = productPrice.replace("$", "").replace(",", "");
			System.out.println(newStr);
			return newStr;
			
			
		}
		public  void DesignerFilter(WebDriver driver) 
		{ 
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			int filter=0;
			DesignerSelected=driver.findElement(By.xpath("//div[@class='jspContainer']/div/li[9]/a")).getText();
			DesignerSelected=DesignerSelected.toUpperCase();
			for(int i=1;i<=6;i++){
				xpathInitialbefore="//div[@class='search-result-items tiles-container clearfix hide-compare ajax']/div["+i+"]";
			//	xpathInitialafter="//div[@id='search-result-items']/div["+(i+1)+"]";
				isDesignerElementPresent1=genericMethods.isElementPresent(driver,xpathInitialbefore+"/div[3]/div[2]/div[1]/a" );
				if (isDesignerElementPresent1.equals("present")) {
					DesignerInString=driver.findElement(By.xpath(xpathInitialbefore+"/div[3]/div[2]/div[1]/a")).getText();
					DesignerInString=DesignerInString.toUpperCase();
					System.out.println(DesignerInString);
				}else {
					DesignerInString=driver.findElement(By.xpath(xpathInitialbefore+"/div[3]/div[2]/div[1]/a")).getText();
					DesignerInString=DesignerInString.toUpperCase();
					System.out.println(DesignerInString);
							
					}
				if (DesignerInString.equalsIgnoreCase(DesignerSelected))
				{
					filter=0;
				}else
				{
					filter=1;
					ATUReports.add("Check Whether the page is filtered by designer", "Should be filtered by designer", "Is not filtered by designer", true,LogAs.FAILED);
				break;
				}
				
				
			
			}
			if (filter==0)
			{
				ATUReports.add("Check Whether the page is filtered by designer", "Should be filtered by designer", "Is filtered by designer", true,LogAs.PASSED);			
			}		
		
		
		}
		
		public  void SizeFilter(WebDriver driver) 
		{ 
			
			cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
			sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
		/*	genericMethods.Click(driver, sort.Category_SizeFilter1_CB,"SizeFilter Link");
			xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
			genericMethods.Click(driver, sort.MyAcc_FirstSizeSelect_LK,"FirstSizeSelect Link");
			SizeSelected=driver.findElement(By.xpath("//div[@id='secondary']/div[3]/div[4]/div[2]/div/div/ul/div/div[1]/li[1]/a")).getText();
			SizeSelected=SizeSelected.toUpperCase();
*/			
			isSizeElementPresent1=genericMethods.isElementPresent(driver,"//div[@class='search-result-items tiles-container clearfix hide-compare ajax']/div[1]/div[3]/div[2]/div[1]/a" );
			
			
				if (isSizeElementPresent1.equals("present")) 
				{
					ATUReports.add("Check Whether the page is loaded after size filter is applied", "Page should load with relevant products", "Page gets loaded with products", true,LogAs.PASSED);
					driver.findElement(By.xpath("//div[@class='search-result-items tiles-container clearfix hide-compare ajax']/div[1]/div[3]/div[2]/div[1]/a")).click();
					isBuyElementPresent1=genericMethods.isElementPresent(driver,"//div[@class='product-add-to-cart']/form/fieldset/div[2]/div[2]/button" );
					if (isBuyElementPresent1.equals("present")) {
						ATUReports.add("Check Whether the PDP page loads afetr product is clicked", "PDP page should load", "PDP page loads", true,LogAs.PASSED);	
					}
				

				}
				else
				
					{	isBuyElementPresent1=genericMethods.isElementPresent(driver,"//div[@class='product-add-to-cart']/form/fieldset/div[2]/div[2]/button" );
					if (isBuyElementPresent1.equals("present")) {
						ATUReports.add("Check Whether the PDP page loads if filter has only one product", "PDP page should load", "PDP page loads", true,LogAs.PASSED);	
					}
				}
				
		}
		public  void ColorFilter(WebDriver driver) 
		{ 
			
			cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
			sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);
			
			genericMethods.Waitformilliseconds(5000);
			genericMethods.mouseHover(driver, sort.MyAcc_ColorFilter_LK,"Color Filter Link");
			genericMethods.Click(driver, sort.MyAcc_ColorFilter_LK,"Color Filter Link");
			genericMethods.Waitformilliseconds(5000);
			genericMethods.Click(driver, sort.MyAcc_FirstColorFilter_LK,"FirstColor Filter Link");
			//SizeSelected=driver.findElement(By.xpath("//div[@id='secondary']/div[3]/div[4]/div[2]/div/div/ul/div/div[1]/li[1]/a")).getText();
			//SizeSelected=SizeSelected.toUpperCase();
				
			isSizeElementPresent1=genericMethods.isElementPresent(driver,"//div[@class='search-result-items tiles-container clearfix hide-compare ajax']/div[1]/div[3]/div[2]/div[1]/a" );
			
				if (isSizeElementPresent1.equals("present")) 
				{
					ATUReports.add("Check Whether the page is loaded after color filter is applied", "Page should load with relevant products", "Page gets loaded with products", true,LogAs.PASSED);
					driver.findElement(By.xpath("//div[@class='search-result-items tiles-container clearfix hide-compare ajax']/div[1]/div[3]/div[2]/div[1]/a")).click();
					isBuyElementPresent1=genericMethods.isElementPresent(driver,"//div[@class='product-add-to-cart']/form/fieldset/div[2]/div[2]/button" );
					if (isBuyElementPresent1.equals("present")) {
						ATUReports.add("Check Whether the PDP page loads after product is clicked", "PDP page should load", "PDP page loads", true,LogAs.PASSED);	
					}
				}
				else
				
					{	isBuyElementPresent1=genericMethods.isElementPresent(driver,"//div[@class='product-add-to-cart']/form/fieldset/div[2]/div[2]/button" );
					if (isBuyElementPresent1.equals("present")) {
						ATUReports.add("Check Whether the PDP page loads if filter has only one product", "PDP page should load", "PDP page loads", true,LogAs.PASSED);	
					}
				}				
		}
		
		public void AddDesignersfromAllDesignerDLP(WebDriver driver)  {
			
			HomeDesigners = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
			Designer_Objects = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
			TopNav=PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
			Register_Objects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);
			  
			  
			genericMethods =  new GenericMethods();
			xplicitWait = new WebDriverWait(driver, 30);


			genericMethods.Click(driver, Designer_Objects.DesignerstartS_LK,"Designer Start Link");
			genericMethods.Click(driver, Designer_Objects.DesignerSaintLaurent_LK,"Designer SaintLaurent Link");
			genericMethods.Click(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN,"Loggedin AddToMyDesigner BTN");

			
			String DesignerButton;
			DesignerButton=genericMethods.getText(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN);
			genericMethods.Waitformilliseconds(5000);
			String DesignerButtonName= genericMethods.getText(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN);
			if(DesignerButtonName.equalsIgnoreCase("In My Designers")||DesignerButtonName.equalsIgnoreCase("Added"))
			{
			
				ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name changes to 'In My Designers'", true,LogAs.PASSED);	
				
			}
			else
			{
				ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name does not change to 'In My Designers'", true,LogAs.FAILED);	
				Assert.assertEquals(DesignerButtonName, "In My Designer");
			
			}
			
				genericMethods.Click(driver, HomeDesigners.Home_TopNav_AccountArrow_LK,"TopNav AccountArrow Link");
				genericMethods.Click(driver, HomeDesigners.Home_TopNav_DD_MyAccount_LK,"TopNav Dropdown MyAccount Link");
				genericMethods.Click(driver, Designer_Objects.MyAcc_LeftNav_MyDesigner_LK,"MyAccount Designer Link");
				String DesignerAddedName;
				DesignerAddedName=genericMethods.getText(driver, Designer_Objects.DesignerAdded_LB);

			if ((genericMethods.checkEqual(DesignerAddedName, "Saint Laurent"))) {
				ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
			}else {
				ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
				Assert.assertEquals(DesignerAddedName, "Saint Laurent");
			}
			

		}
		

public void FilterByMyDesigner(WebDriver driver) {
	
	cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);

	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	String UserState;
	UserState=genericMethods.isElementPresent(driver, "//ul[@class='menu-utility-user']/li[2]/a[2]/b");
	if (UserState.equalsIgnoreCase("present")){
		genericMethods.Click(driver, sort.LoggedInMyDesignersFilter_LK,"LoggedIn MyDesigner Filter Link");	
	}else{
		genericMethods.Click(driver, sort.GuestMyDesignersFilter_LK,"Guest MyDesigner Filter Link");
	}
	GenericMethods.Waitformilliseconds(1000);
}

public void EditMyDesignerFilter(WebDriver driver) {
	cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);

	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	String UserState;
	UserState=genericMethods.isElementPresent(driver, "//ul[@class='menu-utility-user']/li[2]/a[2]/b");
	if (UserState.equalsIgnoreCase("present"))
	{
		genericMethods.Click(driver, sort.LoggedInMyDesignersEdit_LK,"LoggedIn Designer Link");	
	}else{
		genericMethods.Click(driver, sort.GuestMyDesignersEdit_LK,"Guest DesignerEdit Link");
	}
	
	GenericMethods.Waitformilliseconds(10000);
}


public  void VerifyMyDesignerFilter(WebDriver driver) 
{ 
	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	int filter=0;
	DesignerSelected="Saint Laurent";
	DesignerSelected=DesignerSelected.toUpperCase();
	for(int i=1;i<=6;i++){
		xpathInitialbefore="//div[@class='search-result-items tiles-container clearfix hide-compare ajax']/div["+i+"]";
	//	xpathInitialafter="//div[@id='search-result-items']/div["+(i+1)+"]";
		isDesignerElementPresent1=genericMethods.isElementPresent(driver,xpathInitialbefore+"/div[3]/div[2]/div[1]/a" );
		if (isDesignerElementPresent1.equals("present")) {
			DesignerInString=driver.findElement(By.xpath(xpathInitialbefore+"/div[3]/div[2]/div[1]/a")).getText();
			DesignerInString=DesignerInString.toUpperCase();
			System.out.println(DesignerInString);
		}else {
			xpathInitialbefore="//div[@class='search-result-items tiles-container clearfix hide-compare ']/div["+i+"]";
			DesignerInString=driver.findElement(By.xpath(xpathInitialbefore+"/div[3]/div[2]/div[1]/a")).getText();
			DesignerInString=DesignerInString.toUpperCase();
			System.out.println(DesignerInString);
					
			}
		if (DesignerInString.equalsIgnoreCase(DesignerSelected))
		{
			filter=0;
		}else{
			filter=1;
			ATUReports.add("Check Whether the page is filtered by designer", "Should be filtered by designer", "Is not filtered by designer", true,LogAs.FAILED);
			Assert.assertEquals(DesignerInString, DesignerSelected);
			break;
		}
		
		
	
	}
	if (filter==0)
	{
		ATUReports.add("Check Whether the page is filtered by designer", "Should be filtered by designer", "Is filtered by designer", true,LogAs.PASSED);			
	}		


}

public void FilterByMyDesignerNoDesignerSaved(WebDriver driver) {
	
	cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);

	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	
	genericMethods.Click(driver, sort.LoggedInMyDesignersFilter_LK,"My Designer Link");
	String NoDesignerSavedPopup;
	NoDesignerSavedPopup=genericMethods.isElementPresent(driver, "//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable']/div[11]/div/button[1]");
	if (NoDesignerSavedPopup.equalsIgnoreCase("present"))
	{
		ATUReports.add("Check Whether No Designers Saved Popup displayed", " No Designers Saved Popup should be displayed", " No Designers Saved Popup is displayed", true,LogAs.PASSED);
	}else{
		ATUReports.add("Check Whether No Designers Saved Popup displayed", " No Designers Saved Popup should be displayed", " No Designers Saved Popup is not displayed", true,LogAs.FAILED);
		Assert.assertEquals(NoDesignerSavedPopup, "present");
	}
	genericMethods.Click(driver, sort.NoDesignerSavedAddDesigner_BTN,"Add Designer BTN");
	String DesignerPage;
	NoDesignerSavedPopup=genericMethods.isElementPresent(driver, "//section[@class='account-top']/a");
	if (NoDesignerSavedPopup.equalsIgnoreCase("present"))
	{
		ATUReports.add("Check Whether Designers Page is displayed", "Designers Page should be displayed", "Designers Page is displayed", true,LogAs.PASSED);
	}else{
		ATUReports.add("Check Whether Designers Page is displayed", "Designers Page should be displayed", "Designers Page is not displayed", true,LogAs.FAILED);
		Assert.assertEquals(NoDesignerSavedPopup, "present");
	}
	GenericMethods.Waitformilliseconds(2000);
}

public void selectFilters(WebDriver driver,String filterType){	
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
	genericMethods=new GenericMethods();
	String designerName;
	String ColorName;
	String SizeNumber;
	String MaterialName;
	String newArrival;
	

	WebDriverWait wait = new WebDriverWait(driver, 30);
if (filterType.equalsIgnoreCase("designer")) {
	designerName=genericMethods.getText(driver, sort.Category_Designer3_CB);
	genericMethods.Click(driver, sort.Category_Designer3_CB,"DesignerFilter Checkbox");
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
	GenericMethods.Waitformilliseconds(9000);
	if (genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))).equalsIgnoreCase(designerName)) {
		ATUReports.add("Verify Designer Name for the displyed Product","Designer Name should be"+designerName ,"Designer Name is :"+genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))),true, LogAs.PASSED);
	} else {
		ATUReports.add("Verify Designer Name for the displyed Product",designerName,"Designer Name should be" +designerName ,"Designer Name is :"+genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))),true, LogAs.FAILED);
		Assert.assertEquals(designerName, genericMethods.getText(driver, driver.findElement(By.xpath("//div[@class='brand']/a"))));
	}
}else if (filterType.equalsIgnoreCase("color")) {			
	//genericMethods.Click(driver, categoryLandingPageObject.Category_ColorFilter_expand_Arrow,"Color Expand Arrow");
	boolean isColorElementVisible = genericMethods.isElementVisible(driver, By.xpath("//div[@id='accordion']//div/h4/a[contains(text(),'Color')]"));
	ATUReports.add("Verify Color Filter Displayed on browse page","Browse page should display color filter","Color filter is displayed on browse page","Color filter is displayed",true, LogAs.PASSED);
	System.out.println(isColorElementVisible);	
	if(isColorElementVisible=true){
			ColorName=genericMethods.getText(driver, sort.Category_ColorFilter_colour1_CB);
			System.out.println(ColorName);
			genericMethods.Click(driver, sort.Category_ColorFilter_colour1_CB,"ColorFilter Checkbox");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
			GenericMethods.Waitformilliseconds(4000);
			System.out.println(ColorName);
			ATUReports.add("Verify Colour Filter Selected and Products Displayed",ColorName,"Products for the selectd colour should be displayed in" +ColorName,"Products for the selectd colour is displayed in "+ColorName,true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify Color Filter Displayed on browse page","Browse page should display color filter","Color filter is displayed on browse page","Color filter is displayed",true, LogAs.PASSED);
				Assert.assertEquals(isColorElementVisible, false);
			}

}else if (filterType.equalsIgnoreCase("size")) {			
	boolean isSizeElementVisible = genericMethods.isElementVisible(driver, By.xpath("//div[@id='accordion']//div/h4/a[contains(text(),'Size')]"));
	System.out.println(isSizeElementVisible);
	ATUReports.add("Verify Size Filter Displayed on browse page","Browse page should display size filter","Size filter is displayed on browse page","Size filter is displayed",true, LogAs.PASSED);
		if(isSizeElementVisible=true){
			SizeNumber=genericMethods.getText(driver, sort.Category_SizeFilter1_CB);
			genericMethods.Click(driver, sort.Category_SizeFilter1_CB, "1st Size Checkbox");
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
			GenericMethods.Waitformilliseconds(4000);
			System.out.println(SizeNumber);
			ATUReports.add("Verify Size Filter Selected and Products Displayed",SizeNumber,"Products for the selectd size should be displayed in" +SizeNumber,"Products are filtered according to " +SizeNumber,true, LogAs.PASSED);
			}else{
				ATUReports.add("Verify Size Filter Displayed on browse page","Browse page should display size filter","Size filter is missing on browse page","Size filter is not displayed",true, LogAs.FAILED);
				Assert.assertEquals(isSizeElementVisible, false);
			}
		
}else if (filterType.equalsIgnoreCase("material")) {
	MaterialName=genericMethods.getText(driver, sort.Category_MaterialFilter1_CB);
	genericMethods.Click(driver, sort.Category_MaterialFilter1_CB, "1st Material Checkbox");
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
	GenericMethods.Waitformilliseconds(4000);
	System.out.println(MaterialName);
	ATUReports.add("Verify Material Filter is selected and Products are Displayed",MaterialName,"Products for the selectd Material name should be displayed in" +MaterialName,"Products are filtered according to " +MaterialName,true, LogAs.PASSED);
}
else if (filterType.equalsIgnoreCase("NewArrival")) {
	newArrival=genericMethods.getText(driver, sort.Category_NewArrivalFilter1_LK);
	genericMethods.Click(driver, sort.Category_NewArrivalFilter1_LK, "New Arrival Link");
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_prodList']//div[@class='brand']/a")));
	GenericMethods.Waitformilliseconds(4000);
	String Expected;
	Expected =newArrival.toUpperCase();
	if (genericMethods.getText(driver,sort.Category_NewArrivalFilter1_LK).equalsIgnoreCase(Expected+" : Selected")) {
		ATUReports.add("Verify New Arrival text",Expected,"New Arrival text should get appended with SELECTED", "System shoud display new arrival text as New Arrivals : Selected", true, LogAs.PASSED);
	} else {
		ATUReports.add("erify New Arrival text",Expected, "New Arrival text should get appended with SELECTED","System does not display ew arrival text as New Arrivals : Selected", true, LogAs.FAILED);
		Assert.assertEquals(Expected+" : Selected", genericMethods.getText(driver,sort.Category_NewArrivalFilter1_LK));
	}
	System.out.println(newArrival);
	ATUReports.add("Verify New Arrival Filter is selected and Products are Displayed",newArrival,"Products for the New Arrivall should be displayed in","Products are filtered according to " +newArrival,true, LogAs.PASSED);
}
	
}

public void AddDesignersfromAllDesignersDLP(WebDriver driver)  {
	
	HomeDesigners = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	Designer_Objects = PageFactory.initElements(driver, Desk_BNY_DesignerPageObject.class);
	TopNav=PageFactory.initElements(driver, Desk_BNY_NavigationPageObject.class);
	Register_Objects = PageFactory.initElements(driver, Desk_BNY_RegistrationPageObject.class);
	  
	  
	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);


	genericMethods.Click(driver, Designer_Objects.DesignerstartS_LK,"Designer Start Link");
	genericMethods.Click(driver, Designer_Objects.DesignerSaintLaurent_LK,"Designer SaintLaurent Link");
	genericMethods.Click(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN,"Loggedin AddToMyDesigner BTN");

	
	String DesignerButton;
	DesignerButton=genericMethods.getText(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN);
	GenericMethods.Waitformilliseconds(5000);
	System.out.println(DesignerButton);
	
	String DesignerButtonName= genericMethods.getText(driver, Designer_Objects.DLPLoggedinAddToMyDesigners_BTN);
	if(DesignerButtonName.equalsIgnoreCase("In My Designers")||DesignerButtonName.equalsIgnoreCase("Added"))
	{
	
		ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name changes to 'In My Designers'", true,LogAs.PASSED);	
		
	}
	else
	{
		ATUReports.add("Verify Add to My Designers button name changes to In my designers",  " Button name should change to 'In My Designers'",  " Button name does not change to 'In My Designers'", true,LogAs.FAILED);	
		Assert.assertEquals(DesignerButtonName, "In My Designer");
	
	}
		
		genericMethods.Click(driver, HomeDesigners.Home_TopNav_Account_LK,"TopNav Account Link");
		GenericMethods.waitForPageLoaded(driver);
		//genericMethods.Click(driver, HomeDesigners.Home_TopNav_DD_MyAccount_LK,"TopNav Dropdown MyAccount Link");
		genericMethods.Click(driver, Designer_Objects.MyAcc_LeftNav_MyDesigner_LK,"MyAccount Designer Link");
		String DesignerAddedName;
		DesignerAddedName=genericMethods.getText(driver, Designer_Objects.DesignerAdded_LB);

	if ((genericMethods.checkEqual(DesignerAddedName, "Saint Laurent"))) {
		ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.PASSED);
	}else {
		ATUReports.add("Verify Designer added using DLP",  "Saint Laurent designer should be added", "Designer added is "+ DesignerAddedName, true,LogAs.FAILED);
		Assert.assertEquals(DesignerAddedName, "Saint Laurent");
	}
	

}


public void selectPageination(WebDriver driver,Integer pageNumber){
	WebDriverWait wait = new WebDriverWait(driver, 15);
	pageNumber=pageNumber+1;
	String ActiveNumber;
	String InactiveNumber;
	
	String paginationColourSelectedPage;
    wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[@class='atg_store_pagination']/span/ul/li[2]")));
    genericMethods=new GenericMethods();
   
    ActiveNumber=driver.findElement(By.xpath("//div[@class='atg_store_pagination']/span/ul/li[2]/a")).getCssValue("color");        

    System.out.println(ActiveNumber);
	if (ActiveNumber.equalsIgnoreCase("rgba(136, 136, 136, 1)")) {
		ATUReports.add("Verify Color of page 1 before click on other page","Page 1 is by default active clicked", "Page 1 colour should be rgba(136, 136, 136, 1)","Colour of Page 1 under pagination is " +ActiveNumber, true, LogAs.PASSED);
		System.out.println(ActiveNumber);
	} else {
		ATUReports.add("Verify Clor of page 1 before click on other page","Page 1 is by default active clicked","Page 1 colour should be rgba(136, 136, 136, 1)","Colour of Page 1 under pagination is " +ActiveNumber, true, LogAs.FAILED);
		Assert.assertEquals("rgba(136, 136, 136, 1)", ActiveNumber);
	}
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains (text(),48)]")));
	genericMethods.ClickUSingXpath(driver, "//div[@class='atg_store_pagination']/span/ul/li["+pageNumber+"]/a","Page Number");
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[contains (text(),48)]")));
	GenericMethods.Waitformilliseconds(6000);
	
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='atg_store_pagination']/span/ul/li["+pageNumber+"]/a")));
	paginationColourSelectedPage=driver.findElement(By.xpath("//div[@class='atg_store_pagination']/span/ul/li["+pageNumber+"]/a")).getCssValue("color");
	if (paginationColourSelectedPage.equalsIgnoreCase("rgba(136, 136, 136, 1)")) {
		pageNumber=pageNumber-1;
		System.out.println(paginationColourSelectedPage);
		ATUReports.add("Verify Color of page : "+ pageNumber,"Clicked on" +pageNumber, "Page After Click on new page Number" +pageNumber+"  colour should be rgba(136, 136, 136, 1)", "Page Number's colour for page: "+pageNumber+" is "+paginationColourSelectedPage, true, LogAs.PASSED);
	} else {
		pageNumber=pageNumber-1;
		ATUReports.add("Verify Color of page : "+pageNumber, "Page After Click on new page Number"+pageNumber+"  colour should be rgba(136, 136, 136, 1)", "Page Number's colour for page: "+pageNumber+" is "+paginationColourSelectedPage, true, LogAs.FAILED);
		Assert.assertEquals("rgba(0, 0, 0, 1)", paginationColourSelectedPage);
	} 
	
	InactiveNumber=driver.findElement(By.xpath("//div[@class='atg_store_pagination']/span/ul/li[2]/a")).getCssValue("color");
	ATUReports.add("Verify Colour of page 1 after page " + pageNumber +"is selected","Page 1 colour should now be rgba(0, 0, 0, 1)","page 1 color should be "+ InactiveNumber, true, LogAs.INFO);
    Assert.assertEquals("rgba(0, 0, 0, 1)",InactiveNumber );
    System.out.println(InactiveNumber);

}

public void applySort(WebDriver driver,String sortText){
	//categoryLandingPageObject = PageFactory.initElements(driver, Desk_BNY_CategoryLandingPageObject.class);
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
	genericMethods=new GenericMethods();
	WebDriverWait wait = new WebDriverWait(driver, 15);
	genericMethods.selectDropdown(driver,sort.BrowsePage_SortBy_DD, sortText,"SortBy Select Dropdown");
	wait = new WebDriverWait(driver, 15);
	wait.until(ExpectedConditions.visibilityOf(sort.ProductResultPage_48_96_All_TK));
	ATUReports.add("Verify value in the Sort By Dropdown", "Value in the Sort By Dropdown should be "+sortText,"_____", true, LogAs.INFO);
}

public void selectView(WebDriver driver,String viewType){
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
	genericMethods=new GenericMethods();
	WebDriverWait wait = new WebDriverWait(driver, 15);
	List<WebElement> elements;
   
	
	if (viewType.equalsIgnoreCase("48")) {
		genericMethods.Click(driver,sort.categoryLanding_TopView48_LK,"TopView48 LK");
		wait.until(ExpectedConditions.visibilityOf(sort.ProductResultPage_48_96_All_TK));
		ATUReports.add("Verify View Selected", "48 View Should be Selected","48 View is Selected", true, LogAs.INFO);			
		elements=driver.findElements(By.xpath("//div[@id='atg_store_prodList']/ul/li"));
		Assert.assertEquals(elements.size(), 48);
		
	}else if (viewType.equalsIgnoreCase("96")) {
        wait.until(ExpectedConditions.elementToBeClickable(sort.categoryLanding_TopView96_LK));
		genericMethods.Click(driver,sort.categoryLanding_TopView96_LK,"TopView96 LK");
		GenericMethods.Waitformilliseconds(5000);
		ATUReports.add("Verify View Selected", "96 View Should be Selected","96 View is Selected", true, LogAs.INFO);
		elements=driver.findElements(By.xpath("//div[@id='atg_store_prodList']/ul/li"));
		org.testng.Assert.assertEquals(elements.size(), 96);
	}else if (viewType.equalsIgnoreCase("ALL")) {
		genericMethods.Click(driver,sort.categoryLanding_TopViewAll_LK,"TopViewAll LK");
		GenericMethods.Waitformilliseconds(10000);
		wait.until(ExpectedConditions.visibilityOf(sort.ProductResultPage_48_96_All_TK));
		ATUReports.add("Verify View Selected", "ALL View Should be Selected","ALL View is Selected", true, LogAs.INFO);
		elements=driver.findElements(By.xpath("//div[@id='atg_store_prodList']/ul/li"));
		
	}			
}

public void FilterByMyDesigners(WebDriver driver) {
	
	cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);

	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	
	genericMethods.Click(driver, sort.MyDesignersFilter_LK,"MyDesigner Filter Link");
	GenericMethods.Waitformilliseconds(1000);
	}

public void selectView4x6x(WebDriver driver,String viewType){
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);
	genericMethods=new GenericMethods();
	String ActiveNumber;
	WebDriverWait wait = new WebDriverWait(driver, 15);
	
	if (viewType.equalsIgnoreCase("4x")) {
		genericMethods.Click(driver,sort.categoryLanding_View4x_LK,"CLP_View4x Link");		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='search-result-items']/div[48]")));
		ATUReports.add("Verify View Selected", "4X View Should be Selected","4X View is Selected", true, LogAs.PASSED);
		
	}else if (viewType.equalsIgnoreCase("6x")) {
		genericMethods.Click(driver,sort.categoryLanding_View6x_LK,"CLP_View6x Link");
		GenericMethods.Waitformilliseconds(4000);
		ActiveNumber=driver.findElement(By.xpath("//a[contains(text(),'6x')]")).getCssValue("color"); 
		System.out.println(ActiveNumber);
				if (ActiveNumber.equalsIgnoreCase("rgba(35, 82, 124, 1)")) {
					ATUReports.add("Verify Color of 6X button before click on other page","6X button is by default active clicked", "6X colour should be rgba(35, 82, 124, 1)","Colour of 6X button is " +ActiveNumber, true, LogAs.PASSED);
					ATUReports.add("Verify View Selected", "6X View Should be Selected","6X View is Selected", true, LogAs.PASSED);
					
				} else {
					ATUReports.add("Verify Clor of 6X button before click on other page","6X button is by default active clicked","6X button colour should be rgba(35, 82, 124, 1)","Colour of 6X button is " +ActiveNumber, true, LogAs.FAILED);
					Assert.assertEquals("rgba(35, 82, 124, 1)", ActiveNumber);
				}	
			
		}	
	}	
//Edit link on designer left nav, browse page. 
public void EditMyDesignersFilter(WebDriver driver) {
	cart = PageFactory.initElements(driver, Desk_BNY_CartPageObject.class);
	sort = PageFactory.initElements(driver, Desk_BNY_SortByPageObject.class);

	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	String UserState;
	UserState=genericMethods.isElementPresent(driver, "//div[@id='Designer']//a[contains(text(),'Edit')]");
	if (UserState.equalsIgnoreCase("present"))
	{
		genericMethods.Click(driver, sort.BrowsePage_DesignerEdit_LK,"LoggedIn Designer edit Link");
		ATUReports.add("Verify if edit link on designer filter is present","Edit link on designr should be present","Edit link on designer link is :"+ UserState, true, LogAs.PASSED);
	}else{
		ATUReports.add("Verify if edit link on designer filter is present","Edit link on designr should be present","Edit link on designer link is :"+ UserState, true, LogAs.FAILED);
		Assert.assertEquals(UserState,"notpresent");
	}
	
	GenericMethods.Waitformilliseconds(10000);
 }

}
	




		