package reusableModules.desktop;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import PageObjects.desktop.Desk_BNY_MyAccountPaymentPaegObjects;
import PageObjects.desktop.Desk_BNY_HomePageObject;
import actions.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_MyAccountPayment_Reusable {
	
	WebDriverWait xplicitWait ;
	Desk_BNY_MyAccountPaymentPaegObjects myAccountPayment;
	Desk_BNY_HomePageObject myAccountHomePageObjects;
	String isQASPresent;
	String isPaymentBoxPresent;
	int PaymentCount;
	String PaymentData;
	String StateCode;
	String MonthCode;
	String ExpirationMonth;
	String ExpirationYear;
	WebElement editElementLink;	
	GenericMethods genericMethods;
	
	static public String CHName,FName,LName, PAdd1, PAdd2,PCity,PState,PZip,PPhone,CType,EmailAdd;

	public void MyAccountPaymentMethod(WebDriver driver, String MyAccFirstName, String MyAccLastName,String MyAccAddress1,String MyAccAddress2,String MyAccCity,String MyAccZipCode,String MyAccPhone, String MyAccState, String MyAccAddressName, String CardNo, String cvv, String Email,String CardHolderName,String ExpMonth, String ExpYear){
		myAccountPayment = PageFactory.initElements(driver, Desk_BNY_MyAccountPaymentPaegObjects.class);
		myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		
		genericMethods.Click(driver, myAccountHomePageObjects.Home_UtilityNav_LoggedInUser_LK,"Utility Nav logged In User Name");
		//genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_DD_MyAccount_LK, "My account link");
		genericMethods.Click(driver, myAccountPayment.MyAcc_LeftNav_Payment_LK,"Left Nav Payment link");
		
		for (int i = 1; i<=10 ;i++) {
			
			isPaymentBoxPresent = genericMethods.isElementPresent(driver,"//div[@id='atg_store_storedCreditCards']/ul/li[" + i + "]");
			if (isPaymentBoxPresent=="present") {
				PaymentCount=i;
				System.out.println("This payment count" + PaymentCount);
			}else{
				break;
			}
		}
		genericMethods.Click(driver, myAccountPayment.MyAcc_PaymentOptions_AddPaymentOption_BTN,"Add Payment Button");
		xplicitWait = new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.AddPayment_CardHolderName_TB));
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CardHolderName_TB, CardHolderName,"Card Holder first Name");
		//genericMethods.Click(driver, myAccountPayment.MyAcc_CardType_DD);
		//driver.findElement(By.xpath("//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']/option[@value='"+CardType+"']")).click();
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CardNumber_TB, CardNo,"Card Number");
		genericMethods.Click(driver, myAccountPayment.AddPayment_ExpirationMonth_DD, "Month expiration dropdowns");
		genericMethods.selectDropdown(driver, myAccountPayment.AddPayment_ExpirationMonth_DD,ExpMonth, "Expiration Month");
		ExpirationMonth= (myAccountPayment.AddPayment_ExpirationMonth_DD).getAttribute("value");
		genericMethods.Click(driver, myAccountPayment.AddPayment_ExpirationYear_DD, "Expiration year");
		genericMethods.selectDropdown(driver, myAccountPayment.AddPayment_ExpirationYear_DD, ExpYear, "Expiration year");
		ExpirationYear=(myAccountPayment.AddPayment_ExpirationYear_DD).getAttribute("value");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CVV_TB, cvv, "CVV Number");
		
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingFirstName_TB, MyAccFirstName,"Billing First name");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingLastName_TB, MyAccLastName,"Billing Last Name");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress1_TB, MyAccAddress1,"Billing Address 1");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress2_TB, MyAccAddress2,"Billing Address 2");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingCity_TB, MyAccCity,"Billing City");
		genericMethods.Click(driver, myAccountPayment.AddPayment_BillingState_DD,"Billing State Dropdown");
		genericMethods.selectDropdown(driver, myAccountPayment.AddPayment_BillingState_DD, MyAccState,"Billing State");
		StateCode=(myAccountPayment.AddPayment_BillingState_DD).getAttribute("value");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingZipCodeTB, MyAccZipCode,"Billing Zip Code");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingEmail_TB, Email,"Billing Email");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingPhone_TB, MyAccPhone,"Billing Phone");
		genericMethods.Click(driver, myAccountPayment.AddPayment_SAVE_BTN,"Save Button");
		
		
		GenericMethods.Waitformilliseconds(2000);
		PaymentCount++;
		isPaymentBoxPresent = genericMethods.isElementPresent(driver,"//div[@id='atg_store_storedCreditCards']/ul/li[" +PaymentCount + "]");
		WebElement newPaymentBox;
		//newPaymentBox=driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul[@class='clearfix']/li[" + PaymentCount + "]"));
		newPaymentBox=driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul[@class='clearfix']/li"));
	
		PaymentData	=genericMethods.getText(driver, newPaymentBox);
		System.out.println(PaymentData);
	    char[] arr;
	    arr = PaymentData.toCharArray();
	    System.out.println(arr);
	    String [] myarray = PaymentData.split("\n");
	
	    String word =myarray[2];
			int lenght = word.length();
			System.out.println(lenght);
			String cardlastno1 = word.substring(word.length() - 4);
			System.out.println(cardlastno1);
			
			ATUReports.add("Verify Payment added",  "Card Number should end with"+cardlastno1, "Card Number ends with"+ cardlastno1, true,LogAs.PASSED);
				
	}
	
	public void MyAccountPaymentDelete(WebDriver driver)  {
		myAccountPayment = PageFactory.initElements(driver, Desk_BNY_MyAccountPaymentPaegObjects.class);
		myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		GenericMethods.Waitformilliseconds(4000);
		
		try{
		genericMethods.mouseHover(driver, myAccountPayment.AddPayment_PaymentSecond_Box,"Second Payment Box");
		genericMethods.Click(driver, myAccountPayment.AddPayment_Delete_Symbol,"top left Delete cross");
		//((JavascriptExecutor)driver).executeScript("document.getElementsByClassName('delete-card')[0].click();"); 
		}catch(Exception e){
			genericMethods.mouseHover(driver, myAccountPayment.AddPayment_PaymentFirst_Box,"First Payment Box");
			genericMethods.Click(driver, myAccountPayment.AddPayment_Delete_Symbol,"top left Delete cross");
			//((JavascriptExecutor)driver).executeScript("document.getElementsByClassName('delete-card')[0].click();"); 
		}
		
		ATUReports.add("Check The Delete Button", "Delete button Found",true,LogAs.INFO);
		genericMethods.Click(driver, myAccountPayment.AddPayment_DeleteThisCard_YES_BTN,"Confirmation box 'YES' link");
		
		String PaymentDelete;
		
		PaymentDelete = genericMethods.isElementPresent(driver,"//div[@id='atg_store_storedCreditCards']/ul/li[2]/div");
		if (PaymentDelete == "notpresent"){
			ATUReports.add("Verify Payment Deleted",  "Payment should be deleted", "Payment is deleted", true,LogAs.PASSED);
		}else{
			ATUReports.add("Verify Payment Deleted",  "Payment should be deleted", "Payment is not deleted", true,LogAs.FAILED);					
		}
	}

    public void MyAccountPaymentDefault(WebDriver driver)  {
	myAccountPayment = PageFactory.initElements(driver, Desk_BNY_MyAccountPaymentPaegObjects.class);
	myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
	genericMethods =  new GenericMethods();
	xplicitWait = new WebDriverWait(driver, 30);
	GenericMethods.Waitformilliseconds(3000);
		
		WebElement SecondPaymentBox;
		SecondPaymentBox=driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul/li[2]/div"));
		PaymentData	=genericMethods.getText(driver, SecondPaymentBox);
		char[] arr;
		arr = PaymentData.toCharArray();
		String [] myarray1 = PaymentData.split("\n");
		
		String word1 =myarray1[2];
		int lenght = word1.length();
		String cardlastno1 = word1.substring(word1.length() - 4);
		
		genericMethods.mouseHover(driver, myAccountPayment.AddPayment_PaymentSecond_Box,"Payment Second box");
		//((JavascriptExecutor)driver).executeScript("document.getElementsByClassName('btn address-make-default payment-default')[0].click();");
		genericMethods.Click(driver, myAccountPayment.AddPayment_PaymentSecondBox_MakeDefault_LK,"Payment Second box 'Make Default' link");
		ATUReports.add("Verify the Make Default link", "Make Default link should be present","Make Default link is present", true,LogAs.INFO);
		
		WebElement DefaultPaymentBox;
		xplicitWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='atg_store_storedCreditCards']/ul/li[1]/div")));
		GenericMethods.Waitformilliseconds(4000);
		DefaultPaymentBox = driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul/li[1]/div"));
		PaymentData	= genericMethods.getText(driver, DefaultPaymentBox);
		char arr1[];
		arr1=PaymentData.toCharArray();
		String [] myarray = PaymentData.split("\n");
		
		String word = myarray[2];
		int lenght1 = word.length();
		String cardlastno2 = word.substring(word.length() - 4);
			
		if (cardlastno1.equalsIgnoreCase(cardlastno2)) {
			ATUReports.add("Verify Payment becomes default payment",  "Default Card Number should end with"+cardlastno1, "Default Card Number ends with"+ cardlastno1, true,LogAs.PASSED);
		}else {
			ATUReports.add("Verify Payment becomes default paymen",  "Default Card Number should end with"+cardlastno1, "Card Number ends with"+ cardlastno1, true,LogAs.FAILED);
			Assert.assertEquals(cardlastno1, cardlastno2);
		}
	}

	public void MyAccountPaymentManual(WebDriver driver){
		myAccountPayment = PageFactory.initElements(driver, Desk_BNY_MyAccountPaymentPaegObjects.class);
		myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountHomePageObjects.Home_UtilityNav_LoggedInUser_LK));
		
		genericMethods.Click(driver, myAccountHomePageObjects.Home_UtilityNav_LoggedInUser_LK,"Top Nav Logged In User Name");
		//genericMethods.Click(driver, myAccountHomePageObjects.Home_TopNav_HiUsername_LK);
		GenericMethods.Waitformilliseconds(5000);
		//xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_LeftNav_Payment_LK));
		
		genericMethods.Click(driver, myAccountPayment.MyAcc_LeftNav_Payment_LK,"Left Nav Payment Link");
		
		for (int i = 1; i<=10 ;i++) {
			isPaymentBoxPresent = genericMethods.isElementPresent(driver,"//div[@id='atg_store_storedCreditCards']/ul/li[" + i + "]");
			if (isPaymentBoxPresent == "present") {
				PaymentCount = i;
			}else{
				break;
			}
		}
		
		genericMethods.Click(driver, myAccountPayment.MyAcc_PaymentOptions_AddPaymentOption_BTN,"Add Payment Button");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CardHolderName_TB, "Sam Visa","Cardholder Name");
		//genericMethods.Click(driver, myAccountPayment.MyAcc_CardType_DD);
		//driver.findElement(By.xpath("//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']/option[@value='BN']")).click();
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CardNumber_TB, "6011111111111117","Payment Card Number");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CVV_TB,"123","payment CVV Number");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingFirstName_TB, "Samueal","Billing First name");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingLastName_TB, "Henry","Billing Last name");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress1_TB, "100 Eliot Rd","Billing Address 1");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress2_TB, "near Central park","Billing Address 2");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingCity_TB, "Arlington","Billing City");
		genericMethods.Click(driver, myAccountPayment.AddPayment_BillingState_DD,"Billing State Dropdown");
		genericMethods.selectDropdown(driver, myAccountPayment.AddPayment_BillingState_DD, "New York","Billing State DD option");
		StateCode=(myAccountPayment.AddPayment_BillingState_DD).getAttribute("value");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingZipCodeTB, "02474","Billing Zip Code");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingEmail_TB, "test.card@barneys.com","Billing Email");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingPhone_TB, "3333333333","Billing phone Number");		
		CHName=driver.findElement(By.id("atg_store_paymentInfoAddNewCardHolder")).getAttribute("value");
		//CType=	driver.findElement(By.id("dwfrm_paymentinstruments_creditcards_newcreditcard_type")).getAttribute("value");
		FName=driver.findElement(By.id("atg_store_firstNameInput")).getAttribute("value");
		LName=driver.findElement(By.id("atg_store_lastNameInput")).getAttribute("value");
		PAdd1=driver.findElement(By.id("atg_store_streetAddressInput")).getAttribute("value");
		PAdd2=driver.findElement(By.id("atg_store_streetAddressOptionalInput")).getAttribute("value");
		PCity=driver.findElement(By.id("atg_store_localityInput")).getAttribute("value");
		PState=driver.findElement(By.id("atg_store_stateSelect")).getAttribute("value");
		PZip=driver.findElement(By.id("atg_store_postalCodeInput")).getAttribute("value");
		EmailAdd=driver.findElement(By.id("atg_store_emailInput")).getAttribute("value");
		PPhone=driver.findElement(By.id("atg_store_telephoneInput")).getAttribute("value");
		genericMethods.Click(driver, myAccountPayment.AddPayment_SAVE_BTN,"Add payment Save button");
		
		//============================Commenting Temporary as QAS is not implemented============================
		/*isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
		if (isQASPresent.equalsIgnoreCase("present")) {
			ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
			driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']" )).click();;		
		}*/
		PaymentCount++;
		isPaymentBoxPresent = genericMethods.isElementPresent(driver,"//div[@id='atg_store_storedCreditCards']/ul/li["+PaymentCount +"]");
		WebElement newPaymentBox;
		newPaymentBox=driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul/li["+ PaymentCount +"]/div"));
	
		PaymentData	=genericMethods.getText(driver, newPaymentBox);
		char arr[];
		arr=PaymentData.toCharArray();
		String [] myarray = PaymentData.split("\n");
		//System.out.println(myarray[2]);
		
		String word =myarray[2];
		int lenght = word.length();
		String cardlastno1 = word.substring(word.length() - 4);
		//String word =myarray[2];
			
		//	int lenght1 = CardNo.length();
		String cardlastno2 = "1117";
		WebElement VerifyPaymentBox;
		int TempPayment=0;
		char arr1[];
		String [] myarray2 ;
		String word2;
		int lenght1;
		String cardBefore;
		
		for (int i = 1; i<=2 ;i++) {
			
			VerifyPaymentBox=driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul/li[" + i + "]/div"));
			PaymentData	=genericMethods.getText(driver, VerifyPaymentBox);
			
			arr1=PaymentData.toCharArray();
		 myarray2 = PaymentData.split("\n");
		//	System.out.println(myarray2[2]);
			
			 word2 =myarray2[2];
					 lenght1 = word2.length();
					 cardBefore = word2.substring(word2.length() - 4);
			
			//System.out.println(cardBefore);
			
			if (cardBefore.equalsIgnoreCase(cardlastno2)) {
				ATUReports.add("Verify Payment added",  "Card Number should end with"+cardlastno2, "Card Number ends with"+ cardlastno1, true,LogAs.PASSED);
				TempPayment=1;
				break;
			}
			
			}
		if (TempPayment==0)
		{
			ATUReports.add("Verify Payment added",  "Card Number should end with"+cardlastno2, "Card Number ends with"+ cardlastno1, true,LogAs.FAILED);
			Assert.assertEquals(cardlastno1, cardlastno2);
		}
		
		
	}
		public void MyAccountAddressEdit(WebDriver driver)  {
			

		  //ATUReports.setTestCaseReqCoverage("This Method Fills Shipping form");
		  // MyAccountPayment = PageFactory.initElements(driver, BNYMyAccountObject.class);
		  myAccountPayment = PageFactory.initElements(driver, Desk_BNY_MyAccountPaymentPaegObjects.class);
		  myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);
		  genericMethods =  new GenericMethods();
		  xplicitWait = new WebDriverWait(driver, 30);			
			
	
			genericMethods.mouseHover(driver, myAccountPayment.MyAcc_PayemntFirstBox_LB,"First payment Box");
			
			editElementLink=driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul/li[1]//a[@id='accountCreditCardEdit']"));
			((JavascriptExecutor)driver).executeScript("arguments[0].click();", editElementLink);
			ATUReports.add("Check The Edit Link", "Edit link Found",true,LogAs.INFO);
			genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingFirstName_TB, "Ms.snehal","Billing First name");
			genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress1_TB, "101 Main Street Brown","Billing Address 1");
			genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress2_TB, "Powai","Billing Address 2");
			genericMethods.Click(driver, myAccountPayment.EditPayment_SAVE_BTN,"Edit billing Save Button");

			//======================Temporary commenting as QAS is not present==================================
			/*isQASPresent=genericMethods.isElementPresent(driver,"//button[@name='dwfrm_addForm_useOrig']" );
			if (isQASPresent.equalsIgnoreCase("present")) {
				ATUReports.add("Verify QAS Page", "QAS should be present", "QAS is present", true,LogAs.INFO);
				driver.findElement(By.xpath("//button[@name='dwfrm_addForm_useOrig']" )).click();;		
			}*/
			
			genericMethods.mouseHover(driver, myAccountPayment.AddPayment_PaymentFirst_Box,"Payment First Box");

			editElementLink = driver.findElement(By.xpath("//div[@id='atg_store_storedCreditCards']/ul/li[1]//a[@id='accountCreditCardEdit']"));
			((JavascriptExecutor)driver).executeScript("arguments[0].click();", editElementLink);
			 
			GenericMethods.Waitformilliseconds(2000);
			String FirstName =driver.findElement(By.id("atg_store_firstNameInput")).getAttribute("value");		
			if(FirstName.equalsIgnoreCase("Ms.snehal")){
				ATUReports.add("Verify Billing address is edited ",  "Billing Address should be Edited", "Billing Address is Edited", true,LogAs.PASSED);
				Assert.assertEquals(FirstName,"Ms.snehal");
			}
			else
			{
				ATUReports.add("Verify Billing address is edited ",  "Billing Address should be Edited", "Billing Address is not Edited", true,LogAs.FAILED);
				Assert.assertEquals(FirstName,"Ms.snehal");
			}
			
			
		}

public void MyAccountPaymentValidation(WebDriver driver)  {
		myAccountPayment = PageFactory.initElements(driver,Desk_BNY_MyAccountPaymentPaegObjects.class);
		myAccountHomePageObjects = PageFactory.initElements(driver, Desk_BNY_HomePageObject.class);	
			
		genericMethods =  new GenericMethods();
		xplicitWait = new WebDriverWait(driver, 30);
		genericMethods.Click(driver, myAccountHomePageObjects.Home_UtilityNav_LoggedInUser_LK,"Hi,Username Link");				
		genericMethods.Click(driver, myAccountPayment.MyAcc_LeftNav_Payment_LK,"Payment Option Link");
		genericMethods.Click(driver, myAccountPayment.MyAcc_PaymentOptions_AddPaymentOption_BTN,"Add payment option");
		xplicitWait = new WebDriverWait(driver, 30);
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CardHolderName_TB,"","Card Holder name TB");	
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CardNumber_TB, "","Card Number");
		genericMethods.Click(driver, myAccountPayment.AddPayment_ExpirationMonth_DD, "Month expiration dropdowns");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorCardHolderName_LB));
		String Cardnamemsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCardHolderName_LB);		
		if (Cardnamemsg.contains("Please enter the name as it appears on your card.")) {
		    ATUReports.add("System displaying error message when Card Holder name is blank","  ", "Error Messages for Card Holder  Name should be =  Please enter the name as it appears on your card.","Error message for Card Holder Name : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCardHolderName_LB)),  true, LogAs.PASSED);
		}else{ 
			ATUReports.add("System is not displaying error message when Card Holder name is blank","  ","Error Messages for  Card Holder Name should be =  Please enter the name as it appears on your card.","Error message for Card Holder Name : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCardHolderName_LB)),  true, LogAs.FAILED);
		}
		
		//genericMethods.Click(driver, myAccountPayment.AddPayment_ExpirationMonth_DD, "Month expiration dropdowns");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorCardNumber_LB));
		String CardNumbermsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCardNumber_LB);		
		if (CardNumbermsg.contains("Please enter a valid card number.")) {
		    ATUReports.add("System displaying error message when Card number is blank","  ", "Error Messages for Card number should be =  Please enter a valid card number.","Error message for Card Number : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCardNumber_LB)),  true, LogAs.PASSED);
		}else{ 
			ATUReports.add("System is not displaying error message when Card number is blank","  ","Error Messages for  number should be =   Please enter a valid card number","Error message for Card Number : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCardNumber_LB)),  true, LogAs.FAILED);
		}
		
		genericMethods.Click(driver, myAccountPayment.AddPayment_ExpirationYear_DD, "Year dropdown");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorMonth_LB));
		String Cardmonthmsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorMonth_LB);		
		if (Cardmonthmsg.contains("Please select an Expiration Month")) {
		    ATUReports.add("System displaying error message when Expiration Month is blank","  ", "Error Messages for Expiration month should be = Please select an Expiration Month","Error message for expiration month : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorMonth_LB)),  true, LogAs.PASSED);
		}else{ 
			ATUReports.add("System is not displaying error message when Expiration month is blank","  ","Error Messages for Expiration month should be =  Please select an Expiration Month.","Error message for Card Holder Name : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorMonth_LB)),  true, LogAs.FAILED);
		}
		
	
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_CVV_TB, "","CVV Number");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorYear_LB));
		String CardYearmsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorYear_LB);
		if (CardYearmsg.contains("Please select an Expiration Year")){
		    ATUReports.add("System displaying error message when Card Expiration Year is blank","  ", "Error Messages for Card Expiration Year should be =  Please select an Expiration Year","Error message for Card Expiration year : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorYear_LB)),  true, LogAs.PASSED);
		}else{ 
			ATUReports.add("System is not displaying error message when Card Expiration Year is blank","  ","Error Messages for Card Expiration Year should be =  Please select an Expiration Year","Error message for Card Expiration year : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorYear_LB)),  true, LogAs.FAILED);
		}
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingFirstName_TB,"","First Name");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorSecurityCode_LB));
		String CVVmsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorSecurityCode_LB);		
		if (CVVmsg.contains("Please enter the security code that appears on your card.")){
		    ATUReports.add("System displaying error message when CVV Number is blank","  ", "Error Messages for CVV Number should be =  Please enter the security code that appears on your card.","Error message for Card security Number : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorSecurityCode_LB)),  true, LogAs.PASSED);
		}else{ 
			ATUReports.add("System is not displaying error message when CVV Number is blank","  ","Error Messages for  CVV Number should be =  Please enter the security code that appears on your card.","Error message for Card security Number: " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorSecurityCode_LB)),  true, LogAs.FAILED);
		}
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingLastName_TB, "","Last Name");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorFirstName_LB));
		String firstnamemsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorFirstName_LB);		
		if (firstnamemsg.contains("Please provide a first name.")) {
		    ATUReports.add("System displaying error message when first name is blank","  ", "Error Messages for First Name should be =  Please provide a First name.","Error message for First Name : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorFirstName_LB)),  true, LogAs.PASSED);
		}else{
			ATUReports.add("System is not displaying error message when first name is blank","  ","Error Messages for First Name should be =  Please provide a first name.","Error message for First Name : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorFirstName_LB)),  true, LogAs.FAILED);
		}
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress1_TB, "","Address 1 TB");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorLastName_LB));
		String lastnamemsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorLastName_LB);
		if (lastnamemsg.contains("Please provide a last name.")) {
		    ATUReports.add("System displaying error message when last name is blank","  ", "Error Messages for Last Name should be =  Please provide a Last name.","Error message for Last Name : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorLastName_LB)),  true, LogAs.PASSED);
		}else{
		   ATUReports.add("System is not displaying error message when last name is blank","  ","Error Messages for Last Name should be =  Please provide a Last name.","Error message for Last Name : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorLastName_LB)),  true, LogAs.FAILED);
		}
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingAddress2_TB, "","Address 2 TB");	
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorAddressOne_LB));
		String address1msg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorAddressOne_LB);		
		if (address1msg.contains("Please enter a valid street address.")){
		    ATUReports.add("System displaying error message when address1 is blank","  ", "Error Messages for last Name should be =  Please enter a valid street address.","Error message for address1 : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorAddressOne_LB)),  true, LogAs.PASSED);
	    }else{
		   ATUReports.add("System is not displaying error message when address1 is blank","  ", "Error Messages for last Name should be =  Please enter a valid street address.","Error message for address1 : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorAddressOne_LB)),  true, LogAs.FAILED);
	    }
		
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingCity_TB, "","City");
	
		genericMethods.Click(driver, myAccountPayment.AddPayment_BillingState_DD,"");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorCity_LB));
		String citymsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCity_LB); 
		if (citymsg.contains("Please enter a valid city.")){
		    ATUReports.add("System displaying error message when City is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCity_LB)),  true, LogAs.PASSED);
		}else{
			   ATUReports.add("System is not displaying error message when City is blank","  ", "Error Messages for city should be =  Please enter a valid city.","Error message for city : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorCity_LB)),  true, LogAs.FAILED);
		}
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingZipCodeTB, "","Zip Code");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorState_LB));
		String statemsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorState_LB); 
		if (statemsg.contains("Please select a State.")){
		    ATUReports.add("System displaying error message when state is blank","  ", "Error Messages for state should be =  Please select a State.","Error message for city : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorState_LB)),  true, LogAs.PASSED);
		}else{
			   ATUReports.add("System is not displaying error message when state is blank","  ", "Error Messages for state should be =  Please select a State.","Error message for city : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorState_LB)),  true, LogAs.FAILED);
		}
		
		
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingEmail_TB, "","Email TB");
		genericMethods.inputValue(driver, myAccountPayment.AddPayment_BillingPhone_TB, "","PhoneNumber");
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorZip_LB));
		String Zipmsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorZip_LB); 
		if (Zipmsg.contains("Please enter a valid ZIP or postal code.")){
		    ATUReports.add("System displaying error message when zip code is blank","  ", "Error Messages for zip code should be =  Please enter a valid ZIP or postal code.","Error message for Zip code : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorZip_LB)),  true, LogAs.PASSED);
		}else{
			   ATUReports.add("System is not displaying error message when zip code is blank","  ", "Error Messages for Zip code should be =  Please enter a valid ZIP or postal code.","Error message for Zip code : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorZip_LB)),  true, LogAs.FAILED);
		}
				
		xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorEmail_LB));
		 String Emailmsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorEmail_LB);
		 if (Emailmsg.contains("Please provide an email address.")){
		    ATUReports.add("System displaying error message when Email id is blank","  ", "Error Messages for Email id should be =  Please provide an email address.","Error message for Email id : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorEmail_LB)),  true, LogAs.PASSED);
		 }else{
			   ATUReports.add("System is not displaying error message when Email id is blank","  ", "Error Messages for Email id should be =  Please provide an email address.","Error message for Email id : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorEmail_LB)),  true, LogAs.FAILED);
		 }
		 genericMethods.Click(driver, myAccountPayment.AddPayment_SAVE_BTN,"Save Button");	 		 
		 xplicitWait.until(ExpectedConditions.visibilityOf(myAccountPayment.MyAcc_ErrorPhone_LB));
		 String Phonenumbermsg = genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorPhone_LB);
		 if (Phonenumbermsg.contains("Please enter a valid phone number, in case we need to contact you regarding your order.")){
		    ATUReports.add("System displaying error message when phone number is blank","  ", "Error Messages for Phone number should be =  Please enter a valid phone number, in case we need to contact you regarding your order.","Error message for Phone number : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorPhone_LB)),  true, LogAs.PASSED);
	     }else{
		   ATUReports.add("System is not displaying error message when phone number is blank","  ", "Error Messages for Phone number should be =  Please enter a valid phone number, in case we need to contact you regarding your order.","Error message for phone number is : " +(genericMethods.getText(driver, myAccountPayment.MyAcc_ErrorPhone_LB)),  true, LogAs.FAILED);
		 }
		 
		 genericMethods.Click(driver, myAccountPayment.AddPayment_SAVE_BTN,"Save Button");
}
		
}
