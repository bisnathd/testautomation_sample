package reusableModules.desktop;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;

public class Desk_CheckLinks_Reusable {
	
	static WebElement sectionforCheckingLink;
	public static void checkAllPageLinks(WebDriver driver,String pageToCheck,String section){
		
     driver.get(pageToCheck);
     
     switch (section) {
     case "banner": 
   	  		//GenericMethods.Waitformilliseconds(2000);
   	  		sectionforCheckingLink = driver.findElement(By.xpath("//header[@id='header']"));
   			ATUReports.add("Start Validating Banner Links",true,LogAs.INFO);
 		 break;
 		 
     case "topnavigation": 
	  		//GenericMethods.Waitformilliseconds(2000);
	  		sectionforCheckingLink = driver.findElement(By.xpath("//nav[@id='navigation']"));
			ATUReports.add("Start Validating Footer Links",true,LogAs.INFO);
		 break;
 		 
     case "footer": 
      	  //GenericMethods.Waitformilliseconds(2000);
      	  sectionforCheckingLink = driver.findElement(By.xpath("//footer[@id='footer']"));
      	  ATUReports.add("Start Validating Footer Links",true,LogAs.INFO);
    		 break;

     case "mainpage": 
     	  //GenericMethods.Waitformilliseconds(2000);
     	  sectionforCheckingLink = driver.findElement(By.xpath("//div[@id='main']"));
     	  ATUReports.add("Start Validating all main page Links",true,LogAs.INFO);
   		 break;
   		 
     default:
    	 //GenericMethods.Waitformilliseconds(2000);
    		 sectionforCheckingLink = driver.findElement(By.xpath(section));
        	 ATUReports.add("Start Validating Section Links",true,LogAs.INFO);
             break;
	
    	 
     }
     
     //WebElement sectionToCheckLink= driver.findElement(By.xpath("//footer[@id='footer']/div[2]/div/div/nav[1]"));
     
     
     List<WebElement> ele=sectionforCheckingLink.findElements(By.tagName("a"));
	     for(int i=0;i<ele.size();i++) {
	    	 int statusCode=0;
	     	try{
	     		
	     			statusCode=getResponseCode((ele.get(i)).getAttribute("href"));
	     			System.out.println(statusCode+" :Valid Links:->  "+(ele.get(i)).getAttribute("href")+"Link text:->"+ele.get(i).getText() );
	     			ATUReports.add(statusCode+" : Valid Links :->  ", (ele.get(i)).getText(),(ele.get(i)).getAttribute("href"),false, LogAs.PASSED );
	     	
	     	}catch(MalformedURLException e){
	     			System.out.println("Malformed URL :->  "+(ele.get(i)).getAttribute("href")+"Link text:->"+ele.get(i).getText() );
	     			ATUReports.add("Malformed URL :-> ", (ele.get(i)).getText(),(ele.get(i)).getAttribute("href"),false, LogAs.WARNING );
	     	} catch (IOException e) {
				e.printStackTrace();
			}
	     	
		     if(statusCode==404) {
		     System.out.println("404: INVALIDLINKS :->  "+(ele.get(i)).getAttribute("href")+"Link text:->"+ele.get(i).getText() );
		     //ATUReports.add("404: INVALIDLINKS :->  ", (ele.get(i)).getText(),(ele.get(i)).getAttribute("href"),false, LogAs.FAILED );
		     }
		   }
		}
	
public static int getResponseCode(String urlString) throws IOException,MalformedURLException{
    URL u = new URL(urlString);
    
    HttpURLConnection h =  (HttpURLConnection)  u.openConnection();
    h.setRequestMethod("GET");
    h.connect();
    return h.getResponseCode();
   }
}
