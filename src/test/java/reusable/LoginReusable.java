package reusable;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import actionsAndSetup.ExcelToMap;
import actionsAndSetup.GenericMethods;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class LoginReusable{
	static WebDriver driver;
	ExcelToMap loginRepository;
	GenericMethods genericMethods;
	String loginPageObjectPath="src\\test\\java\\pageObjects\\LoginObjects.xlsx";
	
	public LoginReusable(WebDriver driver){
		LoginReusable.driver=driver;
	}

	public void clickTopLoginLink(){
		loginRepository=new ExcelToMap(driver,loginPageObjectPath,"login");
		genericMethods=new GenericMethods();
		//System.out.println(10/0);
		
		genericMethods.Click(driver, loginRepository.getWebElement("home_header_login_LK"), "Top Login Link");			
	
	}
	
	public void fillLoginForm(String username,String password){
		 loginRepository=new ExcelToMap(driver,loginPageObjectPath,"login");
		 genericMethods=new GenericMethods();
		
		 genericMethods.inputValue(driver, loginRepository.getWebElement("login_loginform_username_TB"), username, "Username");
		 genericMethods.inputValue(driver, loginRepository.getWebElement("login_loginform_password_TB"), password, "Password");			
	}
	
	public void clickLoginButton(){
		loginRepository=new ExcelToMap(driver,"src\\test\\java\\pageObjects\\LoginObjects.xlsx","login");
		genericMethods=new GenericMethods();
		
		genericMethods.Click(driver, loginRepository.getWebElement("login_loginform_login_BTN"), "Login Button");		
	}
	
	public void verifyUserLoggedIn(){
		loginRepository=new ExcelToMap(driver,"src\\test\\java\\pageObjects\\LoginObjects.xlsx","login");	
		genericMethods=new GenericMethods();
		String loggedInUserName=genericMethods.getText(driver,loginRepository.getWebElement("login_header_UserName_TV"));
		if (loggedInUserName.equalsIgnoreCase("GOVIND0904")) {
			ATUReports.add("Verify loggedIn username", "GOVIND0904",loggedInUserName,LogAs.PASSED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		} else {
			ATUReports.add("Verify loggedIn username", "GOVIND0904",loggedInUserName,LogAs.FAILED,new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			Assert.assertEquals(loggedInUserName,"GOVIND0904");
		}
					
	}
	
}
